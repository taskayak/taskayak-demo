﻿using hrm.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace hrm
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.Add("DomainRoute", new DomainRoute(
                "taskayak.com",                                     // Domain with parameters
                "{controller}/{action}/{id}",                                        // URL with parameters
                new { subdomain = "www", controller = "main", action = "Index", id = UrlParameter.Optional }  // Parameter defaults
            ));

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "main", action = "index", id = UrlParameter.Optional }
            );
        }
    }
}
