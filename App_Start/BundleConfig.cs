﻿using System.Web;
using System.Web.Optimization;

namespace hrm
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
          
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/bootstrap.min.js", "~/Scripts/bootstrap-select.min.js", "~/Scripts/bootstrap-toggle.min.js", "~/Scripts/jquery.slimscroll.min.js"
                        , "~/Scripts/smoothscroll.min.js", "~/Scripts/alertify.min.js", "~/scripts/wysihtml5x-toolbar.min.js", "~/Scripts/handlebars.runtime.min.js"
                        , "~/scripts/bootstrap3-wysihtml5.min.js", "~/Scripts/bootstrap-datetimepicker.min.js", "~/Scripts/datatables.min.js", "~/Scripts/bootbox.min.js"
                        , "~/Scripts/scripts.min.js", "~/Scripts/form-elements-page.min.js"

                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/bootstrap.min.css", "~/fontawesome/css/all.min.css", "~/Content/responsive.min.css",
                         "~/Content/alertify.min.css",
                          "~/Content/alertify-bootstrap-3.min.css",
                          "~/Content/bootstrap3-wysihtml5.min.css",
                          "~/Content/bootstrap-toggle.min.css",
                          "~/Content/bootstrap-datetimepicker.min.css",
                          "~/Content/bootstrap-select.min.css",
                          "~/Content/datatables.min.css",
                      "~/Content/admin.min.css",
                      "~/Content/layout.min.css", "~/Content/style.min.css", "~/Content/site.min.css"
                      ));

            BundleTable.EnableOptimizations = true;
        }


    }
}
