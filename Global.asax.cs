﻿using hrm.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace hrm
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        protected void Application_BeginRequest()
        {
            if (!Context.Request.IsSecureConnection && !Context.Request.IsLocal)
                Response.Redirect(Context.Request.Url.ToString().Replace("http:", "https:"));
            if (!Context.Request.IsLocal)
            {
                var url = Context.Request.Url.Host.Replace("https://", "").Replace("www.", "");
                var subdomain = url.Substring(0, url.IndexOf(".")).ToLower();
                if (subdomain != "taskayak" && url != "taskayak.com" && subdomain != "app")
                {

                    domainservices _dmnser = new domainservices();
                    int domainid = _dmnser.validate_domain(subdomain);
                    if (domainid > 0)
                    {
                        HttpCookie myCookie = new HttpCookie("domain");
                        myCookie.Value = domainid.ToString();
                        myCookie.Expires = DateTime.Now.AddDays(365d);
                        HttpContext.Current.Response.Cookies.Add(myCookie);
                    }
                    else
                    {
                        Response.Redirect("https://taskayak.com/error/index?val=" + subdomain);
                    }
                }
               
            }
        }

    }
}

