﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrm.Database
{
    public class JsonpResult : JsonResult
    {
        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }
            HttpRequestBase request = context.HttpContext.Request;
            HttpResponseBase response = context.HttpContext.Response;
            string item = context.RouteData.Values["callback"] as string ?? request["callback"];
            if (!string.IsNullOrEmpty(item))
            {
                if (string.IsNullOrEmpty(base.ContentType))
                {
                    base.ContentType = "application/x-javascript";
                }
                response.Write(string.Format("{0}(", item));
            }
            base.ExecuteResult(context);
            if (!string.IsNullOrEmpty(item))
            {
                response.Write(")");
            }
        }
    }
}