﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class ConnectionHelper
    {
        public SqlConnection con;
        public ConnectionHelper()
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ToString());
        }
    }
}