﻿using hrm.Models;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class noticeboardServices
    {
        public noticemodel get_all_active_notice(int workspaceid, int? id, int? status = null)
        {
            SqlHelper sqlHelper = new SqlHelper();
            noticemodel _mdl = new noticemodel();
            List<notice_items> _model = new List<notice_items>();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@workspaceid", workspaceid }
                };
                if (id.HasValue)
                {
                    _srt.Add("@id", id.Value);
                }
                if (status.HasValue)
                {
                    _srt.Add("@status", status.Value);
                }
                var ds = sqlHelper.fillDataSet("tbl_notice_getall", string.Empty, _srt); sqlHelper.Dispose();
                var dt = ds.Tables[0];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    notice_items _item = new notice_items();
                    _item.descripion = dt.Rows[i]["Description"].ToString();
                    _item.titile = dt.Rows[i]["Notice"].ToString();
                    _item.statusid = Convert.ToInt32(dt.Rows[i]["status"].ToString());
                    _item.id = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.PublishedDate = Convert.ToDateTime(dt.Rows[i]["PublishedDate"]).ToString("yyyy-MM-dd hh:mm tt");
                    _model.Add(_item);
                }
                if (ds.Tables.Count > 1)
                {
                    var dt1 = ds.Tables[1];
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        _mdl.descripion = dt1.Rows[i]["Description"].ToString();
                        _mdl.titile = dt1.Rows[i]["Notice"].ToString();
                        _mdl.PublishedDate = Convert.ToDateTime(dt1.Rows[i]["PublishedDate"]).ToString("yyyy-MM-dd");
                        _mdl.id = Convert.ToInt32(dt1.Rows[i]["id"].ToString());
                    }
                    dt1.Dispose();
                }
                else
                {
                    if (dt.Rows.Count > 0)
                    {
                        _mdl.descripion = dt.Rows[0]["Description"].ToString();
                        _mdl.titile = dt.Rows[0]["Notice"].ToString();
                        _mdl.PublishedDate = Convert.ToDateTime(dt.Rows[0]["PublishedDate"]).ToString("yyyy-MM-dd");
                        _mdl.id = Convert.ToInt32(dt.Rows[0]["id"].ToString());
                    }
                }
                dt.Dispose();
                ds.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _mdl.error_text = "Error:" + exception.Message;
            }
            _mdl._list = _model;
            return _mdl;
        }

        public noticemodel get_all_active_notice(int? id, int? status = null, int webspaceid = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            noticemodel _mdl = new noticemodel();
            List<notice_items> _model = new List<notice_items>();
            try
            {
                SortedList _srt = new SortedList();
                if (id.HasValue)
                {
                    _srt.Add("@id", id.Value);
                }
                if (status.HasValue)
                {
                    _srt.Add("@status", status.Value);
                }
                _srt.Add("@webspaceid", webspaceid);
                var ds = sqlHelper.fillDataSet("tbl_notice_getall_byworkspace", string.Empty, _srt); sqlHelper.Dispose();
                var dt = ds.Tables[0];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    notice_items _item = new notice_items();
                    _item.descripion = dt.Rows[i]["Description"].ToString();
                    _item.titile = dt.Rows[i]["Notice"].ToString();
                    _item.statusid = Convert.ToInt32(dt.Rows[i]["status"].ToString());
                    _item.id = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.PublishedDate = Convert.ToDateTime(dt.Rows[i]["PublishedDate"]).ToString("yyyy-MM-dd hh:mm tt");
                    _model.Add(_item);
                }
                if (ds.Tables.Count > 1)
                {
                    var dt1 = ds.Tables[1];
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        _mdl.descripion = dt1.Rows[i]["Description"].ToString();
                        _mdl.titile = dt1.Rows[i]["Notice"].ToString();
                        _mdl.PublishedDate = Convert.ToDateTime(dt1.Rows[i]["PublishedDate"]).ToString("yyyy-MM-dd");
                        _mdl.id = Convert.ToInt32(dt1.Rows[i]["id"].ToString());
                    }
                    dt1.Dispose();
                }
                else
                {
                    if (dt.Rows.Count > 0)
                    {
                        _mdl.descripion = dt.Rows[0]["Description"].ToString();
                        _mdl.titile = dt.Rows[0]["Notice"].ToString();
                        _mdl.PublishedDate = Convert.ToDateTime(dt.Rows[0]["PublishedDate"]).ToString("yyyy-MM-dd");
                        _mdl.id = Convert.ToInt32(dt.Rows[0]["id"].ToString());
                    }
                }
                dt.Dispose();
                ds.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _mdl.error_text = "Error:" + exception.Message;
            }
            _mdl._list = _model;
            return _mdl;
        }
        public string add_new_notice(noticemodel notice, int userid, int workspaceid)
        {
            string message;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@title", notice.titile },
                    { "@statusid", notice.statusid },
                    { "@description", notice.descripion },
                    { "@createdby", userid },
                    { "@workspaceid", workspaceid }
                };
                message = sqlHelper.executeNonQueryWMessage("tbl_notice_addnotice", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }
        public string update_notice(noticemodel notice, int userid)
        {
            string message;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@title", notice.titile },
                    { "@statusid", notice.statusid },
                    { "@description", notice.descripion },
                    { "@id", notice.id },
                    { "@createdby", userid }
                };
                message = sqlHelper.executeNonQueryWMessage("tbl_notice_updatenotice", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error: " + exception.Message;
            }
            return message;
        }

        public string get_notice(int id)
        {
            string message;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", id }
                };
                message = sqlHelper.executeNonQueryWMessage("getonoiticeBY_id", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }

        public string update_notice(int id, int statusid)
        {
            string message = "";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@nid", id },
                    { "@statusid", statusid }
                };
                sqlHelper.executeNonQuery("tbl_notice_updatestatus", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }


        public string delete_notice(int id)
        {
            string message;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", id }
                };
                message = sqlHelper.executeNonQueryWMessage("tbl_notice_removenotice", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }

    }
}