﻿using hrm.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class accountservices
    {
        public int validateLogin(LoginModel _login, ref string message, ref int roleid, ref bool isresetneeded, ref bool _status, ref bool accepttermsrqud, ref int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            Crypto _crypt = new Crypto();
            SortedList sortedLists = new SortedList();
            isresetneeded = false;
            _status = true;
            int id = 0;
            roleid = 0;
            int statusid;
            message = "Error : Password not match";
            try
            {
                string passowrd = string.Empty;
                sortedLists.Add("@userName", _login.UserName);
                sortedLists.Add("@workspacename", _login.Workspace);
                var dt = sqlHelper.fillDataTable("tbl_member_getlogin_v2", string.Empty, sortedLists);
                sqlHelper.Dispose();
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        passowrd = _crypt.DecryptStringAES(dt.Rows[i]["Password"].ToString());
                        if (passowrd == _login.Password)
                        {
                            id = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                            statusid = Convert.ToInt32(dt.Rows[i]["member_status_id"].ToString());
                            roleid = Convert.ToInt32(dt.Rows[i]["roleid"].ToString());
                            isresetneeded = Convert.ToBoolean(dt.Rows[i]["resetneeded"].ToString());
                            _status = statusid == 1007 ? Convert.ToBoolean(dt.Rows[i]["isprofilecompleted"].ToString()) : true;
                            accepttermsrqud = Convert.ToBoolean(dt.Rows[i]["accepttermsrqud"].ToString());
                            workspaceid = Convert.ToInt32(dt.Rows[i]["workspaceid"].ToString());
                            break;
                        }
                    }
                }
                else
                {
                    message = "Error : Password not match";
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                message = "Error :" + exception.Message;
                id = 0; sqlHelper.Dispose();
            }
            return id;
        }

        /// <summary>
        /// Tuple<Message,UserId,RoleId,WorkSpaceId,Type,IsresetNeeded>
        /// </summary>
        /// <param name="_login"></param>
        /// <returns></returns>
        public Tuple<string, int, int, int, string, bool> validateLogin(LoginModel _login)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            Crypto _crypt = new Crypto();
            var result = Tuple.Create("Error : Password not match", 0, 0, 0, "E", false);
            try
            {
                string passowrd = string.Empty;
                sortedLists.Add("@userName", _login.UserName);
                sortedLists.Add("@workspacename", _login.Workspace);
                var dt = sqlHelper.fillDataTable("ValidateLogin", string.Empty, sortedLists);
                sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    passowrd = _crypt.DecryptStringAES(dt.Rows[i]["Password"].ToString());
                    if (passowrd == _login.Password)
                    {
                        result = Tuple.Create(string.Empty, Convert.ToInt32(dt.Rows[i]["id"].ToString()), Convert.ToInt32(dt.Rows[i]["roleid"].ToString()), Convert.ToInt32(dt.Rows[i]["workspaceid"].ToString()), dt.Rows[i]["membertype"].ToString(), Convert.ToBoolean(dt.Rows[i]["resetneeded"].ToString()));
                        break;
                    }
                }
                dt.Dispose();
            }
            catch (Exception ex)
            {
                result = Tuple.Create("Error : " + ex.Message, 0, 0, 0, "E", false);
                sqlHelper.Dispose();
            }
            return result;
        }


        public string UpdateBilling(billingModal _model, int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string _res = "Account updated successfully";
            try
            {
                sortedLists.Add("@workspaceid", workspaceid);
                sortedLists.Add("@Address", _model.Address);
                sortedLists.Add("@Email", _model.Email);
                sortedLists.Add("@Primarycontact", _model.Primarycontact);
                sortedLists.Add("@Name", _model.Name);
                sortedLists.Add("@Telephone", _model.Telephone);
                _res = sqlHelper.executeNonQueryWMessage("update_Billing", string.Empty, sortedLists).ToString();
                sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _res;
        }

        public string Updatepacjkage(int workspaceid, int smsCount, int emailcount, string subscriptionId, string CustomerId, string PlanName)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string _res = "Account updated successfully";
            try
            {
                sortedLists.Add("@workspaceid", workspaceid);
                sortedLists.Add("@Count", smsCount);
                sortedLists.Add("@CountType", emailcount);
                sortedLists.Add("@subscriptionId", subscriptionId);
                sortedLists.Add("@CustomerId", CustomerId);
                sortedLists.Add("@PlanName", PlanName);
                _res = sqlHelper.executeNonQueryWMessage("addnewsubscription", string.Empty, sortedLists).ToString();
                sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _res;
        }

        public string UpdateBillingwithupgrade(string accounttype, int workspaceid, int email, int sms, string customerid, string subscriptionid, int planid, int userid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string _res = "Account updated successfully";
            try
            {
                sortedLists.Add("@workspaceid", workspaceid);
                sortedLists.Add("@accounttype", accounttype);
                sortedLists.Add("@emailcount", email);
                sortedLists.Add("@smscount", sms);
                sortedLists.Add("@customerid", customerid);
                sortedLists.Add("@subscriptionid", subscriptionid);
                sortedLists.Add("@planid", planid);
                sortedLists.Add("@userid", userid);
                _res = sqlHelper.executeNonQueryWMessage("update_Billing_upgradeaccount", string.Empty, sortedLists).ToString(); sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _res;
        }

        public string deactivateaccount(int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string _res = "Account updated successfully";
            try
            {
                sortedLists.Add("@workspaceid", workspaceid);
                _res = sqlHelper.executeNonQueryWMessage("Deactivateaccount", string.Empty, sortedLists).ToString(); sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _res;
        }

        public string getaccounttype(int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string _res = "paid";
            try
            {
                sortedLists.Add("@workspaceid", workspaceid);
                _res = sqlHelper.executeNonQueryWMessage("getaccounttype", string.Empty, sortedLists).ToString(); sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _res;
        }

        public billingModal getBilling(int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            billingModal _model = new billingModal();
            List<subscriptions> pcks = new List<subscriptions>();
            try
            {
                sortedLists.Add("@workspaceid", workspaceid);
                var ds = sqlHelper.fillDataSet("GetBilling", string.Empty, sortedLists); sqlHelper.Dispose();
                var dt1 = ds.Tables[1];
                var dt = ds.Tables[0];
                ds.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _model.AccountNumber = dt.Rows[i]["AccountNumber"].ToString();
                    _model.AccountType = dt.Rows[i]["ServiceName"].ToString();
                    _model.Address = dt.Rows[i]["Address"].ToString();
                    _model.Email = dt.Rows[i]["Email"].ToString();
                    _model.EmailCount = Convert.ToInt32(dt.Rows[i]["EmailCount"].ToString());
                    _model.smsCount = Convert.ToInt32(dt.Rows[i]["SmsCount"].ToString());
                    _model.Primarycontact = dt.Rows[i]["Primarycontact"].ToString();
                    _model.Name = dt.Rows[i]["Name"].ToString();
                    _model.Telephone = dt.Rows[i]["phonenumber"].ToString();
                }
                dt.Dispose();
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    subscriptions _p = new subscriptions
                    {
                        smsCount = Convert.ToInt32(dt1.Rows[i]["Count"].ToString()),
                        emailcount = Convert.ToInt32(dt1.Rows[i]["CountType"].ToString()),
                        subscriptionId = dt1.Rows[i]["subscriptionId"].ToString(),
                        Planname = dt1.Rows[i]["PlanName"].ToString()
                    };
                    pcks.Add(_p);
                }
                dt1.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            _model.packages = pcks;
            return _model;
        }
        public bool validate_email(string email, int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            bool isexist = false;
            try
            {
                sortedLists.Add("@workspaceid", workspaceid);
                sortedLists.Add("@email", email);
                var dt = sqlHelper.fillDataTable("tbl_member_validate_email", string.Empty, sortedLists); sqlHelper.Dispose();
                if (dt.Rows.Count > 0)
                {
                    isexist = true;
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return isexist;
        }

        public bool validate_memberid(string memberid, int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            bool isexist = false;
            try
            {
                sortedLists.Add("@workspaceid", workspaceid);
                sortedLists.Add("@memberid", memberid);
                var dt = sqlHelper.fillDataTable("tbl_member_validate_memberid", string.Empty, sortedLists); sqlHelper.Dispose();
                if (dt.Rows.Count > 0)
                {
                    isexist = true;
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return isexist;
        }

        public bool validate_username(string username, int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            bool isexist = false;
            try
            {
                sortedLists.Add("@workspaceid", workspaceid);
                sortedLists.Add("@username", username);
                var dt = sqlHelper.fillDataTable("tbl_member_validate_username", string.Empty, sortedLists); sqlHelper.Dispose();
                if (dt.Rows.Count > 0)
                {
                    isexist = true;
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return isexist;
        }


        public string reset_password(string username, string tempasswors, ref string name, ref string email)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string isexist = string.Empty;
            try
            {
                sortedLists.Add("@username", username);
                sortedLists.Add("@temppassword", tempasswors);
                var dt = sqlHelper.fillDataTable("tbl_member_reset_password", string.Empty, sortedLists); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    isexist = dt.Rows[i]["message"].ToString();
                    email = dt.Rows[i]["EMail"].ToString();
                    name = dt.Rows[i]["name"].ToString();
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return isexist;
        }

        public string update_password(int memberid, string password)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            string isexist;
            try
            {
                sortedLists.Add("@memberid", memberid);
                sortedLists.Add("@password", password);
                isexist = sqlHelper.executeNonQueryWMessage("tbl_member_update_password", string.Empty, sortedLists).ToString(); sqlHelper.Dispose();
            }
            catch (Exception ex)
            {
                isexist = "Error:" + ex.Message; sqlHelper.Dispose();
            }
            return isexist;
        }

        public string validatepassword(string c_password, string n_password, int memberid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList(); 
            string message = "Error : Current password is not match";
            try
            {
                string passowrd = string.Empty;
                sortedLists.Add("@id", memberid);
                var dt = sqlHelper.fillDataTable("tbl_member_get_passordbyId", string.Empty, sortedLists); sqlHelper.Dispose();
                if (dt.Rows.Count > 0)
                {
                    Crypto _crypt = new Crypto();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        passowrd = _crypt.DecryptStringAES(dt.Rows[i]["Password"].ToString());
                        if (passowrd == c_password)
                        {
                            n_password = _crypt.EncryptStringAES(n_password);
                            sortedLists.Add("@password", n_password);
                            sqlHelper = new SqlHelper();
                            sqlHelper.executeNonQuery("tbl_member_update_passordbyId", "", sortedLists);
                            sqlHelper.Dispose();
                            message = "Password Updated Successfully";
                        }
                    }
                }
                else
                {
                    message = "Error : Current password is not match";
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                 message = "Error :" + exception.Message; sqlHelper.Dispose();
            }
            return message;
        }

    }
}