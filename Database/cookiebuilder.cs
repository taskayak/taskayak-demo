﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class CookieBuilder
    {
        /// <summary>
        /// Create a cookie
        /// </summary>
        /// <param name="_key">Name of cookie</param>
        /// <param name="_value">Value which need to save</param>
        /// <param name="_islocal">Creating cookie for local or live url</param>
        /// <param name="_year">Cookie life time in years</param>
        public void Set(string _key, string _value, bool _islocal, int _year = 1)
        {
            HttpCookie cookie = GetCookie(_islocal, _key);
            cookie[_key] = _value;
            cookie.Expires = DateTime.Now.AddYears(_year);
            if (!_islocal)
            {
                cookie.Domain = "app.taskayak.com";
            }
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        /// <summary>
        /// Get a cookie by key
        /// </summary>
        /// <param name="_key">Name of cookie</param>
        /// <param name="_islocal">Getting cookie for local or live url</param>
        /// <returns>Value of cookie base of passed key</returns>
        public string Get(string _key, bool _islocal)
        {
            HttpCookie cookie = GetCookie(_islocal, _key);
            if (cookie[_key] != null)
            {
                return cookie[_key];
            }
            return "";
        }

        private HttpCookie GetCookie(bool _islocal, string _key)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[_key];
            if (cookie == null)
            {
                cookie = new HttpCookie(_key);
                if (!_islocal)
                {
                    cookie.Domain = "app.taskayak.com";
                }
                HttpContext.Current.Request.Cookies.Add(cookie);
            }
            return cookie;
        }
    }
}