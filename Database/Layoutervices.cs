﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class Layoutervices
    {
        public List<int> GetMenuPermission()
        {
            List<int> _list = new List<int>();
            string PList = HttpContext.Current.Request.Cookies["_plist"] == null ? "0" : HttpContext.Current.Request.Cookies["_plist"].Value.ToString();
            if (PList == "0")
            {
                int roleid = HttpContext.Current.Request.Cookies["_pid"] == null ? 0 : Convert.ToInt32(HttpContext.Current.Request.Cookies["_pid"].Value.ToString());
                int userid = HttpContext.Current.Request.Cookies["_xid"] == null ? 0 : Convert.ToInt32(HttpContext.Current.Request.Cookies["_xid"].Value.ToString());
                helper _helper = new helper();
                string type = "";
                _list = new PermissionServices().GetAllMenuPermissionsByPermissionRoleId(roleid, userid, ref type);
                var p = JsonConvert.SerializeObject(_list);
                _helper.CreateCookie<string>(type, "_slist", 8760);
                _helper.CreateCookie<string>(p, "_plist", 8760);
            }
            else
            {
                _list = JsonConvert.DeserializeObject<List<int>>(PList);
            }
            return _list;
        }

    }
}