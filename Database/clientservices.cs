﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using hrm.Models;

namespace hrm.Database
{
    public class clientservices
    {
        public string GetLatestClientId()
        {
            SqlHelper sqlHelper = new SqlHelper();
            string clientId = string.Empty;
            try
            {
                clientId = sqlHelper.executeNonQueryWMessage("GetLatestClientId", string.Empty, null).ToString(); sqlHelper.Dispose();
                clientId = clientId.Count() < 5 ? "0" + clientId : clientId;
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return clientId;
        }

        public string GetClientRate(int _clientId)
        {
            string message;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@clientId", _clientId);
                message = sqlHelper.executeNonQueryWMessage("GetClientRate", string.Empty, _srt).ToString().Currency(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error: " + exception.Message;
            }
            return message;
        }

        public string CreateClient(clientmodal_item _request, int userid, int workspaceid)
        {
            string message;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@Clientname", _request.name },
                    { "@primarycontact", _request.primarycontactname },
                    { "@rate", string.IsNullOrEmpty(_request.rate) ? "$0" : _request.rate },
                    { "@workspaceid", workspaceid },
                    { "@Address", _request.address },
                    { "@MapValidateAddress", _request.MapValidateAddress },
                    { "@Cityid", _request.city_id },
                    { "@Stateid", _request.state_id },
                    { "@zipcode", _request.zipcode },
                    { "@Suite", _request.suite },
                    { "@Email", _request.email },
                    { "@Phone", _request.phone },
                    { "@createdby", userid }
                };
                message = sqlHelper.executeNonQueryWMessage("CreateClient", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error: " + exception.Message;
            }
            return message;
        }


        public string UpdateClientProfile(clientmodal_item _request)
        {
            string message;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@Clientname", _request.name },
                    { "@primary", _request.primarycontactname },
                    { "@rate", string.IsNullOrEmpty(_request.rate) ? "$0" : _request.rate },
                    { "@Address", _request.address },
                    { "@MapValidateAddress", _request.MapValidateAddress },
                    { "@Cityid", _request.city_id },
                    { "@Stateid", _request.state_id },
                    { "@Suite", _request.suite },
                    { "@zipcode", _request.zipcode },
                    { "@Email", _request.email },
                    { "@Phone", _request.phone },
                    { "@clientid", _request.clientid }
                };
                message = sqlHelper.executeNonQueryWMessage("UpdateClientProfile", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error: " + exception.Message;
            }
            return message;
        }

        public List<TechnicianClientsModel> GetActiveClient(int _workspaceId)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<TechnicianClientsModel> _model = new List<TechnicianClientsModel>();
            SortedList _list = new SortedList
            {
                { "@workspaceId", _workspaceId }
            };
            try
            {
                var dt = sqlHelper.fillDataTable("GetActiveClient", string.Empty, _list); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TechnicianClientsModel _item = new TechnicianClientsModel
                    {
                        ClientName = dt.Rows[i]["Clientname"].ToString().ToTitleCase(),
                        ClientId = Convert.ToInt32(dt.Rows[i]["id"].ToString())
                    };
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _model;
        }

        public Tuple<List<TechnicianClientsModel>, List<int>> GetUserClient(int _workspaceId, int userId)
        {
            List<int> clientIds = new List<int>();
            SqlHelper sqlHelper = new SqlHelper();
            List<TechnicianClientsModel> _model = new List<TechnicianClientsModel>();
            SortedList _list = new SortedList
            {
                { "@workspaceId", _workspaceId },
                 { "@userId", userId }
            };
            try
            {
                var ds = sqlHelper.fillDataSet("GetUserClient", string.Empty, _list); sqlHelper.Dispose();
                var dt = ds.Tables[0];
                var dt1 = ds.Tables[1];
                ds.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    TechnicianClientsModel _item = new TechnicianClientsModel
                    {
                        ClientName = dt.Rows[i]["Clientname"].ToString().ToTitleCase(),
                        ClientId = Convert.ToInt32(dt.Rows[i]["id"].ToString())
                    };
                    _model.Add(_item);
                }
                dt.Dispose();
                if (dt1.Rows.Count > 0)
                {
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        clientIds.Add(Convert.ToInt32(dt1.Rows[i][0].ToString()));
                    }
                }
                dt1.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return Tuple.Create(_model, clientIds);
        }

        public List<clientmodal_item> GetAllActiveClientWithPaging(ref int _total, int _startIndex = 0, int _endIndex = 25, string _search = "", int _workspaceId = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<clientmodal_item> _model = new List<clientmodal_item>();
            SortedList _srtlist = new SortedList
            {
                { "@startindex", _startIndex },
                { "@endIndex", _endIndex },
                { "@workspaceid", _workspaceId }
            };
            if (!string.IsNullOrEmpty(_search))
            {
                _srtlist.Add("@search", _search);
            }
            try
            {
                var dt = sqlHelper.fillDataTable("GetAllActiveClientWithPaging", string.Empty, _srtlist); sqlHelper.Dispose();
                if (dt.Rows.Count > 0)
                {
                    Crypto _crypt = new Crypto();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        clientmodal_item _item = new clientmodal_item
                        {
                            name = dt.Rows[i]["Clientname"].ToString().ToTitleCase(),
                            primarycontactname = dt.Rows[i]["primarycontact"].ToString().ToTitleCase(),
                            rate = dt.Rows[i]["rate"].ToString().Currency(),
                            phone = dt.Rows[i]["Phone"].ToString().PhoneNumber(),
                            email = dt.Rows[i]["Email"].ToString(),
                            index_id = dt.Rows[i]["clientIndexId"].ToString(),
                            clientid = Convert.ToInt32(dt.Rows[i]["id"].ToString()),
                            token = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString())
                        };
                        _total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                        _model.Add(_item);
                    }
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _model;
        }

        public clientmodal_item GetClientProfileByClientId(int _clientId)
        {
            SqlHelper sqlHelper = new SqlHelper();
            clientmodal_item _item = new clientmodal_item();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@clientid", _clientId }
                };
                var dt = sqlHelper.fillDataTable("GetClientProfileByClientId", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _item.name = dt.Rows[i]["Clientname"].ToString().ToTitleCase();
                    _item.primarycontactname = dt.Rows[i]["primarycontact"].ToString().ToTitleCase();
                    _item.rate = dt.Rows[i]["rate"].ToString().Currency();
                    _item.phone = dt.Rows[i]["Phone"].ToString().PhoneNumber();
                    _item.email = dt.Rows[i]["Email"].ToString();
                    _item.suite = dt.Rows[i]["SuiteNumber"].ToString();
                    _item.MapValidateAddress = dt.Rows[i]["MapValidateAddress"].ToString();
                    _item.clientid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _item.error_text = "Error:" + exception.Message;
            }
            return _item;
        }

        public string DeleteClient(int _clientid)
        {
            string message = "Client Deleted Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@clientid", _clientid }
                };
                sqlHelper.executeNonQuery("DeleteClient", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }
    }
}