﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Web;

namespace hrm.Database
{
    public class helper
    {
        /// <summary>
        /// Process Result to show alert on page
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_entity">Any model class</param>
        /// <param name="_message">Body text to process and show in alert</param>
        /// <returns>Model class Of type T</returns>
        public T GenerateError<T>(T _entity, string _message)
        {
            _message = _message.ToLower().Contains("succesfully") ? _message.ToLower().Replace("succesfully", "successfully") : _message;
            if (!string.IsNullOrEmpty(_message))
            {
                string FirstCharacter = _message.Substring(0, 1);
                _message = FirstCharacter.ToUpper() + _message.ToLower().Substring(1);
                if (_message.ToLower().Contains("error"))
                {
                    typeof(T).GetProperty("alertclass").SetValue(_entity, "error");
                }
                else
                {
                    typeof(T).GetProperty("alertclass").SetValue(_entity, "success");
                }
                typeof(T).GetProperty("error_text").SetValue(_entity, _message);
            }
            return _entity;
        }

        /// <summary>
        /// Convert Enumerable list of Model class into Datatable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_entity">Enumerable list of Model class</param>
        /// <returns>DataTabel of type T</returns>
        public DataTable ConvertToDataTable<T>(List<T> _entity)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable dataTable = new DataTable();
            for (int i = 0; i < properties.Count; i++)
            {
                PropertyDescriptor item = properties[i];
                if ((!item.PropertyType.IsGenericType ? true : item.PropertyType.GetGenericTypeDefinition() != typeof(Nullable<>)))
                {
                    dataTable.Columns.Add(item.Name, item.PropertyType);
                }
                else
                {
                    dataTable.Columns.Add(item.Name, item.PropertyType.GetGenericArguments()[0]);
                }
            }

            object[] row = new object[properties.Count];
            foreach (T item in _entity)
            {
                for (int j = 0; j < row.Length; j++)
                {
                    row[j] = properties[j].GetValue(item);
                }
                dataTable.Rows.Add(row);
            }
            return dataTable;
        }

        /// <summary>
        /// Read data from file and convert it into Datatable
        /// </summary>
        /// <param name="_filePath">File file  to read data</param>
        /// <param name="_numberOfColumns">Number of columns exist in file to be convert in Datatable</param>
        /// <param name="_isFirstRowHeader">Should take first row of file as header or not (header will show in Datatable is set true else all rows taken as table)</param>
        /// <returns>DataTabel of type T</returns>
        public DataTable ConvertToDataTable(string _filePath, int _numberOfColumns, bool _isFirstRowHeader = true)
        {
            DataTable dataTable = new DataTable();
            for (int i = 0; i < _numberOfColumns; i++)
            {
                int num = i + 1;
                dataTable.Columns.Add(new DataColumn(string.Concat("Column", num.ToString())));
            }
            string[] strArrays = File.ReadAllLines(_filePath);
            for (int j = _isFirstRowHeader ? 1 : 0; j < strArrays.Length; j++)
            {
                string str = strArrays[j];
                string[] strArrays1 = str.Split(new char[] { ',' });
                if (!string.IsNullOrEmpty(strArrays1[0]))
                {
                    DataRow dataRow = dataTable.NewRow();
                    for (int k = 0; k < _numberOfColumns; k++)
                    {
                        dataRow[k] = strArrays1[k];
                    }
                    dataTable.Rows.Add(dataRow);
                }
            }
            return dataTable;
        }

        /// <summary>
        /// Generate random string with alpha numeric characters
        /// </summary>
        /// <param name="_charcount">Number of characters need for random string</param>
        /// <returns>Random string  with alpha numeric characters</returns>
        public string GetRandomAlphaNumeric(int? _charCount)
        {
            string FinalString = "convo";
            try
            {
                string Numerics = "0123456789";
                string Alphabets = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
                Random Random = new Random();
                int Count = 2;
                if (_charCount.HasValue)
                {
                    _charCount = new int?(_charCount.GetValueOrDefault() / 2);
                    Count = Convert.ToInt32(_charCount);
                }
                char[] ChrArray = new char[Count];
                for (int i = 0; i < ChrArray.Length; i++)
                {
                    ChrArray[i] = Alphabets[Random.Next(Alphabets.Length)];
                }
                char[] ChrArray1 = new char[Count];
                Random = new Random();
                for (int j = 0; j < ChrArray1.Length; j++)
                {
                    ChrArray1[j] = Numerics[Random.Next(Numerics.Length)];
                }
                FinalString = string.Concat(new string(ChrArray), new string(ChrArray1));
            }
            catch (Exception)
            {

            }
            return FinalString;
        }

        /// <summary>
        /// Generate 5 digit random number
        /// </summary>
        /// <returns>5 digit random string</returns>
        public string Get5RandomDigit()
        {
            int RandomNumber = new Random().Next(111111, 999999);
            return RandomNumber.ToString("D6");
        }

        /// <summary>
        /// Generate 9 digit random number
        /// </summary>
        /// <returns>9 digit random string</returns>
        public string Get9RandomDigit()
        {
            Random RandomObject = new Random();
            int RandomNumber1 = RandomObject.Next(11111, 99999);
            int RandomNumber2 = RandomObject.Next(22222, 99999);
            return RandomNumber1.ToString("D5") + RandomNumber2.ToString("D4");
        }

        /// <summary>
        /// Create cookie  and set it to allow access on subdomain
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="_encyrptedValue">Encrypted value</param>
        /// <param name="_key">Cookie key</param>
        /// <param name="_hour">Life time of a cookie in hour</param>
        public void CreateCookie<T>(T _encyrptedValue, string _key, int _hour = 5)
        {
            HttpCookie HttpCookie = new HttpCookie(_key, _encyrptedValue.ToString())
            {
                Expires = DateTime.Now.AddHours(_hour)
            };
            if (!HttpContext.Current.Request.IsLocal)
            {
                HttpCookie.Domain = ".taskayak.com";
            }
            HttpContext.Current.Response.Cookies.Add(HttpCookie);
        }

        /// <summary>
        /// Send email using AWS pinpoint
        /// </summary>
        /// <param name="_receiverEmail">Email address whom email is going out</param>
        /// <param name="_body">Content of email</param>
        /// <param name="_subject">Subject of email</param>
        /// <param name="_receiverName">Name whom email is going out</param>
        /// <param name="_senderName">Email sender name showing as from name in inbox</param>
        /// <param name="_senderAddress">Email sender email address showing as from in inbox</param>
        /// <returns>Email status </returns>
        /// <exception cref="SmtpException"></exception>
        ///  /// <exception cref="WebException"></exception>
        public string SendEmailWithAWS(string _receiverEmail, string _body, string _subject, string _receiverName, string _senderName = "Taskayak", string _senderAddress = "no-reply@taskayak.com")
        {
            string str = "email sent";
            try
            {
                string SmtpEndPoint = "email-smtp.us-east-1.amazonaws.com";
                int port = 587;
                string SmtpUserName = "AKIAX27HMUUWXFPPOYVW";
                string SmtpPassword = "BGQS+WegjbxpDbrQ6QkDwYHu606Jb1lXf5u61+M0G/Av";
                AlternateView HtmlBody = AlternateView.CreateAlternateViewFromString(_body, null, "text/html");
                MailMessage Message = new MailMessage
                {
                    From = new MailAddress(_senderAddress, _senderName)
                };
                if (!string.IsNullOrEmpty(_receiverName))
                {
                    Message.To.Add(new MailAddress(_receiverEmail, _receiverName));
                }
                else
                {
                    Message.To.Add(new MailAddress(_receiverEmail));
                }
                Message.Subject = _subject;
                Message.AlternateViews.Add(HtmlBody);
                using (var client = new SmtpClient(SmtpEndPoint, port))
                {
                    client.Credentials = new NetworkCredential(SmtpUserName, SmtpPassword);
                    client.EnableSsl = true;
                    try
                    {
                        client.Send(Message);
                    }
                    catch (Exception)
                    { }
                }
            }
            catch (SmtpException smtpException)
            {
                str = string.Concat("Error : ", smtpException.Message);
            }
            catch (WebException webException)
            { 
                using (WebResponse response = webException.Response)
                {
                    HttpWebResponse httpWebResponse = (HttpWebResponse)response;
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        using (StreamReader streamReader = new StreamReader(responseStream))
                        {
                            str = string.Concat("Error : ", streamReader.ReadToEnd());
                        }
                    }
                }
            }
            return str;
        }



    }
}