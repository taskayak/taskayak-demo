﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using hrm.Models;

namespace hrm.Database
{
    public class ContractorServices
    {
        public string AddNewContractor(ContractorModal_Item Contractor, int userid, int workspaceid)
        {
            string message;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@Clientname", Contractor.name },
                    { "@workspaceid", workspaceid },
                    { "@primarycontact", Contractor.primarycontactname },
                    { "@rate", string.IsNullOrEmpty(Contractor.rate) ? "$0" : Contractor.rate },
                    { "@Address", Contractor.MapValidateAddress },
                    // _srt.Add("@MapValidateAddress", client.MapValidateAddress);
                    // _srt.Add("@cityId", client.city_id);
                    //_srt.Add("@stateId", client.state_id);
                    { "@website", Contractor.website },
                    { "@suite", Contractor.suite },
                    { "@Email", Contractor.email },
                    { "@Phone", Contractor.phone },
                    { "@createdby", userid }
                };
                message = sqlHelper.executeNonQueryWMessage("AddNewContractor", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }
        public string add_new_companywithmember(ContractorModal_Item Contractor, int userid)
        {
            string message;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@company", Contractor.name },
                    { "@primaryname", Contractor.primarycontactname },
                    { "@hourlyrate", string.IsNullOrEmpty(Contractor.rate) ? "$0" : Contractor.rate },
                    { "@address", Contractor.address },
                    { "@cityid", Contractor.city_id },
                    { "@stateid", Contractor.state_id },
                    { "@website", Contractor.website },
                    { "@zip", Contractor.zipcode },
                    { "@email", Contractor.email },
                    { "@phone", Contractor.phone },
                    { "@userId", userid }
                };
                message = sqlHelper.executeNonQueryWMessage("tbl_company_addnew", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }
        public string DeleteContractor(int ContractorId)
        {
            string message = "Contractor deleted successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@ContractorId", ContractorId }
                };
                sqlHelper.executeNonQuery("DeleteContractor", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }
        public string delete_member(int member)
        {
            string message = "User deactivate successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@memberid", member }
                };
                sqlHelper.executeNonQuery("tbl_member_delete_company", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }
        public string updatnotification(int memberid)
        {
            string message = "Member deactivate successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@memberid", memberid }
                };
                sqlHelper.executeNonQuery("tbl_genralmessage_update", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }

        public string updatRemindernotification(int notification, int c)
        {
            string message = "Confirmation done successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@notification", notification },
                    { "@c", c }
                };
                sqlHelper.executeNonQuery("UpdateNotification", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }

        public ContractorModal_Item GetAllActiveContractorById(int id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            ContractorModal_Item _item = new ContractorModal_Item();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@ContractorById", id }
                };
                var dt = sqlHelper.fillDataTable("GetAllActiveContractorById", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _item.website = dt.Rows[i]["website"].ToString();
                    _item.name = dt.Rows[i]["Contractorname"].ToString().ToTitleCase();
                    _item.primarycontactname = dt.Rows[i]["primarycontact"].ToString().ToTitleCase();
                    _item.rate = dt.Rows[i]["rate"].ToString().Currency();
                    _item.phone = dt.Rows[i]["Phone"].ToString().PhoneNumber();
                    _item.email = dt.Rows[i]["Email"].ToString();
                    _item.suite = dt.Rows[i]["SuiteNumber"].ToString();
                    _item.MapValidateAddress = dt.Rows[i]["Address"].ToString();
                    _item.Contractorid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _item.error_text = "Error:" + exception.Message;
            }
            return _item;
        }

        public Dictionary<int,string> GetAllActiveCOntractor(int id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            Dictionary<int, string> _item = new Dictionary<int, string>();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@WorkspaceId", id }
                };
                var dt = sqlHelper.fillDataTable("GetDropoDownContractorById", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _item.Add(Convert.ToInt32(dt.Rows[i]["id"].ToString()), dt.Rows[i]["Contractorname"].ToString().ToTitleCase()); 
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                
            }
            return _item;
        }

        public string UpdateContractor(ContractorModal_Item Contractor)
        {
            string message;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@Contractorname", Contractor.name },
                    { "@website", Contractor.website },
                    { "@primary", Contractor.primarycontactname },
                    { "@rate", string.IsNullOrEmpty(Contractor.rate) ? "$0" : Contractor.rate },
                    { "@MapAddress", Contractor.MapValidateAddress },
                    { "@Suite", Contractor.suite },
                    { "@Email", Contractor.email },
                    { "@Phone", Contractor.phone },
                    { "@Contractorid", Contractor.Contractorid },
                    { "@cityid", Contractor.city_id },
                    { "@state", Contractor.state_id },
                    { "@zipcode", Contractor.zipcode },
                    { "@Address", Contractor.address }
                };
                message = sqlHelper.executeNonQueryWMessage("UpdateContractor", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }
        public List<ContractorModal_Item> GetAllActiveContractorByPaging(ref int total, int startindex = 0, int endindex = 25, string search = "", int workspaceid = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<ContractorModal_Item> _model = new List<ContractorModal_Item>();
            SortedList _srtlist = new SortedList
            {
                { "@startindex", startindex },
                { "@endIndex", endindex },
                { "@workspaceid", workspaceid }
            };
            if (!string.IsNullOrEmpty(search))
            {
                _srtlist.Add("@search", search);
            }
            try
            {
                var dt = sqlHelper.fillDataTable("GetAllActiveContractorByPaging", string.Empty, _srtlist); sqlHelper.Dispose();
                if (dt.Rows.Count > 0)
                {
                    Crypto _crypt = new Crypto();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        ContractorModal_Item _item = new ContractorModal_Item
                        {
                            name = dt.Rows[i]["ContractorName"].ToString().ToTitleCase(),
                            primarycontactname = dt.Rows[i]["primarycontact"].ToString().ToTitleCase(),
                            rate = dt.Rows[i]["rate"].ToString().Currency(),
                            phone = dt.Rows[i]["Phone"].ToString().PhoneNumber(),
                            email = dt.Rows[i]["Email"].ToString(),
                            Contractorid = Convert.ToInt32(dt.Rows[i]["id"].ToString()),
                            token = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString())
                        };
                        total = Convert.ToInt32(dt.Rows[i]["total"].ToString()); 
                        _model.Add(_item);
                    }
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _model;
        }
        public string getalterntivephone(int clientid, ref string companyname)
        {
            SqlHelper sqlHelper = new SqlHelper();
            string _model = string.Empty;
            SortedList _srt = new SortedList
            {
                { "@id", clientid }
            };
            companyname = string.Empty;
            try
            {
                var dt = sqlHelper.fillDataTable("getalternativebyclientid", string.Empty, _srt); sqlHelper.Dispose();
                if (dt.Rows.Count > 0)
                {
                    _model = dt.Rows[0]["Phone"].ToString();
                    companyname = dt.Rows[0]["company"].ToString();
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _model;

        }
    }
}