﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class domainservices
    {
        public int validate_domain(string name)
        {
            SqlHelper sqlHelper = new SqlHelper();
            int domainID = 0;
            try
            {
                SortedList sortedLists = new SortedList
                {
                    { "@name", name }
                };
                domainID = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("getworkspaceidByname", string.Empty, sortedLists).ToString()); sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return domainID;
        }
        public int createnewworkspace(string name)
        {
            SqlHelper sqlHelper = new SqlHelper();
            int _model = 0;
            SortedList _srt = new SortedList
            {
                { "@name", name }
            };
            try
            {
                switch (name.ToLower())
                {
                    case "portal":
                    case "www":
                    case "taskayak":
                    case "app":
                        _model = 0;
                        break;
                    default:
                        _model = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("Addnewwebspace", string.Empty, _srt).ToString()); sqlHelper.Dispose();
                        break;
                }
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _model;

        }

    }
}