﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using hrm.Models;

namespace hrm.Database
{
    public class partnerService
    {
        public string add_new_partner(partnermodal_item partner, int userid, int workspaceid)
        {
            string message;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@partnername", partner.name },
                    { "@workspaceid", workspaceid },
                    { "@primarycontact", partner.primarycontactname },
                    { "@rate", string.IsNullOrEmpty(partner.rate) ? "$0" : partner.rate },
                    { "@Address", partner.MapValidateAddress },
                    //  _srt.Add("@Cityid", partner.city_id);
                    //  _srt.Add("@Stateid", partner.state_id);
                    { "@suite", partner.suite },
                    // _srt.Add("@Pincode", partner.zipcode);
                    { "@Email", partner.email },
                    { "@Phone", partner.phone },
                    { "@createdby", userid }
                };
                message = sqlHelper.executeNonQueryWMessage("add_new_partner", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }
        public string update_partners(partnermodal_item partner)
        {
            string message;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@partnername", partner.name },
                    { "@primary", partner.primarycontactname },
                    { "@rate", string.IsNullOrEmpty(partner.rate) ? "$0" : partner.rate },
                    { "@Address", partner.MapValidateAddress },
                    //_srt.Add("@Cityid", partner.city_id);
                    //_srt.Add("@Stateid", partner.state_id);
                    { "@suite", partner.suite },
                    // _srt.Add("@Pincode", partner.zipcode);
                    { "@Email", partner.email },
                    { "@Phone", partner.phone },
                    { "@partnerid", partner.partnerid }
                };
                message = sqlHelper.executeNonQueryWMessage("update_partners", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }

        public List<partnermodal_item> get_all_active_partners_bypaging(ref int total, int startindex = 0, int endindex = 25, string search = "", int workspaceid = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<partnermodal_item> _model = new List<partnermodal_item>();
            Crypto _crypt = new Crypto();
            SortedList _srtlist = new SortedList
            {
                { "@startindex", startindex },
                { "@endIndex", endindex },
                { "@workspaceid", workspaceid }
            };
            if (!string.IsNullOrEmpty(search))
            {
                _srtlist.Add("@search", search);
            }
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_partners_getallpartnerss_by_paging", string.Empty, _srtlist); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    partnermodal_item _item = new partnermodal_item
                    {
                        name = dt.Rows[i]["Partnername"].ToString().ToTitleCase(),
                        primarycontactname = dt.Rows[i]["primarycontact"].ToString().ToTitleCase(),
                        rate = dt.Rows[i]["rate"].ToString().Currency(),
                        phone = dt.Rows[i]["Phone"].ToString().PhoneNumber(),
                        email = dt.Rows[i]["Email"].ToString(),
                        partnerid = Convert.ToInt32(dt.Rows[i]["id"].ToString())
                    };
                    total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _item.token = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _model;
        }

        public partnermodal_item get_all_active_partners_byid(int id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            partnermodal_item _item = new partnermodal_item();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@partnerid", id }
                };
                var dt = sqlHelper.fillDataTable("get_all_active_partners_byid", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _item.name = dt.Rows[i]["Partnername"].ToString().ToTitleCase();
                    _item.primarycontactname = dt.Rows[i]["primarycontact"].ToString().ToTitleCase();
                    _item.rate = dt.Rows[i]["rate"].ToString().Currency();
                    _item.phone = dt.Rows[i]["Phone"].ToString().PhoneNumber();
                    _item.email = dt.Rows[i]["Email"].ToString();
                    _item.suite = dt.Rows[i]["SuiteNumber"].ToString();
                    _item.MapValidateAddress = dt.Rows[i]["Address"].ToString();
                    _item.partnerid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _item.error_text = "Error:" + exception.Message;
            }
            return _item;
        }

        public string delete_partner(int partnerid)
        {
            string message = "Partner Deleted Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@partnerid", partnerid }
                };
                sqlHelper.executeNonQuery("tbl_partners_delete_partners", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                 message = "Error:" + exception.Message;
            } 
            return message;
        }
    }
}