﻿using hrm.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class documentservices
    {
        public List<DocumentTypeItems>  AllDocumentType(int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<DocumentTypeItems> _mdl = new List<DocumentTypeItems>();
            SortedList _srt = new SortedList
            {
                { "@workspaceid", workspaceid }
            };
            try
            {   var dt = sqlHelper.fillDataTable("tbl_doctype_getalltype", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DocumentTypeItems _item = new DocumentTypeItems
                    {
                        DocumentName = dt.Rows[i]["name"].ToString(),
                        DocumentTypeId = Convert.ToInt32(dt.Rows[i]["id"].ToString()),
                        ViewOrder = Convert.ToInt32(dt.Rows[i]["mainorder"].ToString())  
                    };
                    _mdl.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
                           } 
            return _mdl;
        }

        public int getmaxorder(int workspaceid)
        {
            int order = 1;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@workspaceid", workspaceid }
                };
                order =Convert.ToInt32( sqlHelper.executeNonQueryWMessage("tbl_doctypetype_getmaxdept", string.Empty, _srt).ToString()); sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return order;
        }

        public string add_new_type(string name, int userid,int order,int workspaceid)
        {
            string message ;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@deptName", name },
                    { "@workspaceid", workspaceid },
                    { "@createdby", userid },
                    { "@mainorder", order }
                };
                message = sqlHelper.executeNonQueryWMessage("tbl_doctypetype_addtype_v2", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose(); message = "Error:" + exception.Message;
            }
            return message;
        }

        public string update_type(string deptname, int deptid, int userid,int mainorder)
        {
            string message ;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@deptName", deptname },
                    { "@createdby", userid },
                    { "@deptid", deptid },
                    { "@mainorder", mainorder }
                };
                message = sqlHelper.executeNonQueryWMessage("tbl_doctype_updatetype_v2", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
               message = "Error:" + exception.Message;
            }
            return message;
        }

        public string delete_type(int deptid, int userid)
        {
            string message ;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@deptid", deptid },
                    { "@createdby", userid }
                };
                message = sqlHelper.executeNonQueryWMessage("tbl_doctype_removetype", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();message = "Error:" + exception.Message;
            }
            return message;
        }
    }
}