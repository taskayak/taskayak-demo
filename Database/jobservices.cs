﻿using hrm.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class Jobservices
    {
        public string AddNewJob(job _mdl, int userid, string tecrate, string clientrate, string MapValidateAddress, string zip, int workspaceid, int projectId)
        {
            string message;
            SqlHelper sqlHelper = new SqlHelper();
            string _sdate = _mdl.sdate + " " + _mdl.stime;
            string _edate = _mdl.sdate + " " + _mdl.etime;
            TimeSpan span = Convert.ToDateTime(_edate) - Convert.ToDateTime(_sdate);
            var estimhour = string.Format("{0}.{1}", span.Hours, span.Minutes > 0 ? (int)Math.Round(Convert.ToDouble(span.Minutes) * 1.66) : span.Minutes);
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@tecrate", tecrate);
                _srt.Add("@clientrate", clientrate);
                _srt.Add("@MapValidateAddress", MapValidateAddress);
                _srt.Add("@address", _mdl.street);
                _srt.Add("@cityId", _mdl.city_id);
                _srt.Add("@stateId", _mdl.state_id);
                _srt.Add("@workspaceid", workspaceid);
                _srt.Add("@projectId", projectId);
                _srt.Add("@JobId", _mdl.JobId);
                _srt.Add("@Ttile", _mdl.title);
                _srt.Add("@Client", _mdl.client_id);
                _srt.Add("@Technician", _mdl.Technician);
                _srt.Add("@Dispatcher", _mdl.Dispatcher);
                _srt.Add("@Project_manager", _mdl.mgr_id);//client manager
                _srt.Add("@Project_manager1", _mdl.mgr_id1);
                _srt.Add("@startdate", _sdate);
                _srt.Add("@expense", _mdl.Expense);
                _srt.Add("@enddate", _edate);
                _srt.Add("@est_Hours", estimhour);
                _srt.Add("@Status_id", _mdl.Status);
                _srt.Add("@suite", _mdl.suite);
                _srt.Add("@Description", _mdl.description);
                _srt.Add("@createdby", userid);
                _srt.Add("@Contact", _mdl.Contact);
                _srt.Add("@Phone", _mdl.Phone);
                _srt.Add("@Instruction", _mdl.Instruction);
                message = sqlHelper.executeNonQueryWMessage("AddNewJob_v1", string.Empty, _srt).ToString(); sqlHelper.Dispose();

            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error: " + exception.Message;
            }
            return message;
        }
        public bool validateJobId(string JobId, int workspaceid)
        {
            bool isavailable = true;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", JobId },
                    { "@workspaceid", workspaceid }
                };
                isavailable = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("validate_jobid", string.Empty, _srt).ToString()) <= 0;
                sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return isavailable;
        }
        public bool validateJobId(int Id, string jobid, int workspaceid)
        {
            bool isavailable = true;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", Id },
                    { "@jobid", jobid },
                    { "@workspaceid", workspaceid }
                };
                isavailable = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("validate_jobid_editjob", string.Empty, _srt).ToString()) <= 0;
                sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return isavailable;
        }
        public void update_status(int statusId, int jobid, int userId, int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@statusId", statusId },
                    { "@jobid", jobid }
                };
                sqlHelper.executeNonQuery("tbl_job_update_status", string.Empty, _srt);
                sqlHelper.Dispose();
                if (statusId == 5)
                {
                    job _mdl = GetAllActiveJobDetailsById(jobid);
                    attandanceadmin_items client = new attandanceadmin_items
                    {
                        startdate = _mdl.sdate,
                        starttime = _mdl.stime,
                        endtime = _mdl.etime,
                        _memberrate = _mdl.Pay_rate,
                        expnse = _mdl.Expense,
                        memberid = _mdl.Technician,
                        jobid = _mdl._jobid,
                        comment = "Pay Created By Approved Job"
                    };
                    new attandanceservices().add_new_pay(client, userId, workspaceid: workspaceid);
                }
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
        }
        public string UpdateJob(job _mdl, int userid, int workspaceid, int projectId, ref bool NewTech)
        {
            string message = "Job Updated Successfully";
            NewTech = false;
            SqlHelper sqlHelper = new SqlHelper();
            string _sdate = _mdl.sdate + " " + _mdl.stime;
            string _edate = _mdl.sdate + " " + _mdl.etime;
            TimeSpan span = Convert.ToDateTime(_edate) - Convert.ToDateTime(_sdate);
            var estimhour = Convert.ToDouble(String.Format("{0}.{1}", span.Hours, span.Minutes > 0 ? (int)Math.Round(Convert.ToDouble(span.Minutes) * 1.66) : span.Minutes));
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@tecrate", _mdl.Pay_rate);
                _srt.Add("@clientrate", _mdl.Client_rate);
                _srt.Add("@address", _mdl.address);
                _srt.Add("@MapValidateAddress", _mdl.MapValidateAddress);
                _srt.Add("@cityId", _mdl.city_id);
                _srt.Add("@StateId", _mdl.state_id);
                _srt.Add("@jobid", _mdl._jobid);
                _srt.Add("@JobtitleId", _mdl.JobId);
                _srt.Add("@projectId", projectId);
                _srt.Add("@Ttile", _mdl.title);
                _srt.Add("@Client", _mdl.client_id);
                _srt.Add("@Technician", _mdl.Technician);
                _srt.Add("@Dispatcher", _mdl.Dispatcher);
                _srt.Add("@Project_manager", _mdl.mgr_id);//client manager
                _srt.Add("@Project_manager1", _mdl.mgr_id1);
                _srt.Add("@startdate", _sdate);
                _srt.Add("@suite", _mdl.suite);
                _srt.Add("@expense", _mdl.Expense);
                _srt.Add("@enddate", _edate);
                _srt.Add("@est_Hours", estimhour);
                _srt.Add("@Status_id", _mdl.Status);
                _srt.Add("@Description", _mdl.description);
                _srt.Add("@createdby", userid);
                _srt.Add("@clr1", _mdl.clr1);
                _srt.Add("@clr2", _mdl.clr2);
                _srt.Add("@Contact", _mdl.Contact);
                _srt.Add("@Phone", _mdl.Phone);
                _srt.Add("@Instruction", _mdl.Instruction);
                int IsChanged = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("UpdateJobWithTechCheck", string.Empty, _srt).ToString()); sqlHelper.Dispose();
                NewTech = IsChanged == 0 ? false : true;
                if (_mdl.Status == 5)
                {
                    attandanceadmin_items client = new attandanceadmin_items
                    {
                        startdate = _mdl.sdate,
                        starttime = _mdl.stime,
                        endtime = _mdl.etime,
                        _memberrate = _mdl.Pay_rate,
                        expnse = _mdl.Expense,
                        memberid = _mdl.Technician,
                        jobid = _mdl._jobid,
                        comment = "Pay Created By Approved Job"
                    };
                    new attandanceservices().add_new_pay(client, userid, workspaceid: workspaceid);
                }
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }
        public string update_job_byview(job_items1 _mdl, int userid, int projectId, ref bool NewTech)
        {
            string message = "Job Updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                NewTech = false;
                SortedList _srt = new SortedList
                {
                    //_srt.Add("@fname", _mdl.FirstName);
                    { "@JobId", _mdl.JobID },
                    { "@Client_id", _mdl.Client_id },
                    { "@Client_rate", _mdl.Client_rate },
                    { "@Technicianid", _mdl.Technicianid },
                    { "@pay_rate", _mdl.pay_rate },
                    { "@mgr_id", _mdl.mgr_id },
                    { "@mgr_id1", _mdl.mgr_id1 },
                    { "@dispatcher", _mdl.Dispatcherid },
                    { "@expense", _mdl.Expense },
                    { "@Status_id", _mdl.Status },
                    { "@projectid", projectId }
                };
                int IsChanged = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("UpdateJobWithTechStatus", string.Empty, _srt).ToString()); sqlHelper.Dispose();
                NewTech = IsChanged == 0 ? false : true;
                if (_mdl.Status == 5)
                {
                    attandanceadmin_items client = new attandanceadmin_items
                    {
                        startdate = _mdl.sdate,
                        starttime = _mdl.stime,
                        endtime = _mdl.etime,
                        _memberrate = _mdl.pay_rate,
                        expnse = _mdl.Expense,
                        memberid = _mdl.Technicianid,
                        jobid = _mdl.JobID,
                        comment = "Pay Created By Approved Job"
                    };
                    new attandanceservices().add_new_pay(client, userid);
                }
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }
        public string update_job_byviewT(job_items1 _mdl)
        {
            string message = "Job Updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    //_srt.Add("@fname", _mdl.FirstName);
                    { "@JobId", _mdl.JobID },
                    { "@dispatcher", _mdl.Dispatcherid },
                    { "@rate", _mdl.pay_rate },
                    { "@expense", _mdl.Expense },
                    { "@Status_id", _mdl.Status }
                };
                sqlHelper.executeNonQuery("tbl_job_updatenewjobByT", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }
        public string add_new_comment(string comment, int userid, int jobid)
        {
            string message = "Comment Added Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    //_srt.Add("@fname", _mdl.FirstName);
                    { "@JobId", jobid },
                    { "@comment", comment },
                    { "@createdby", userid },
                    { "@createddate", DateTime.UtcNow.AddHours(-6) }
                };
                sqlHelper.executeNonQuery("tbl_notification_tbl_job_comment_add", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }
        public string delete_job(int jobid)
        {
            string message = "Job Deleted Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    //_srt.Add("@fname", _mdl.FirstName);
                    { "@JobId", jobid }
                };
                sqlHelper.executeNonQuery("tbl_job__remove_v2", string.Empty, _srt); sqlHelper.Dispose();

            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }
        public string AddNewDocument(string FileTitle, int userid, int jobid, string Size, string url, int parentid = 0)
        {
            string message = "File Added Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@createddate", DateTime.UtcNow.AddHours(-6) },
                    { "@FileTile", FileTitle },
                    { "@Jobid", jobid },
                    { "@Size", Size },
                    { "@url", url },
                    { "@createdby", userid }
                };
                if (parentid != 0)
                {
                    _srt.Add("@parentid", parentid);
                }
                sqlHelper.executeNonQuery("tbl_jobfiles_add", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }
        public string delete_doc(int id)
        {
            string message = "File Deleted Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", id }
                };
                sqlHelper.executeNonQuery("tbl_jobfiles_remove", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }

        public string delete_docByParent(int id)
        {
            string message = "File Deleted Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", id }
                };
                sqlHelper.executeNonQuery("tbl_jobfiles_removeByParent", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }
        public string assignjob(int Jobid, int memberid, string rate)
        {
            string message;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", Jobid },
                    { "@memberid", memberid },
                    { "@payrate", rate }
                };
                message = sqlHelper.executeNonQueryWMessage("assignjobtonewmember", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }
        public jobmodal GetAllActiveJobsForCalender(string date = "", string Location = "", int cclient = 0, int cTechnician = 0, int cDispatcher = 0, int Status = 0, int workspaceid = 0, int project = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            jobmodal _mdl = new jobmodal();
            SortedList _srt = new SortedList();
            List<job_items> _jbs = new List<job_items>();
            try
            {
                if (!string.IsNullOrEmpty(date))
                {
                    var dates = date.Split('-');
                    _srt.Add("@startdate", dates[0]);
                    _srt.Add("@enddate", dates[1]);
                }
                if (!string.IsNullOrEmpty(Location))
                {
                    _srt.Add("@Location", Location);
                }
                _srt.Add("@workspaceid", workspaceid);
                if (cclient != 0)
                {
                    _srt.Add("@client", cclient);
                }
                if (cTechnician != 0)
                {
                    _srt.Add("@Technician", cTechnician);
                }
                if (project != 0)
                {
                    _srt.Add("@project", project);
                }

                if (cDispatcher != 0)
                {
                    _srt.Add("@Dispatcher", cDispatcher);
                }
                string sp = "GetAllActiveJobsByUserIdForCalender_v1";
                if (Status != 0)
                {
                    // sp = "tbl_job_getalljobs_index_withstatus";
                    _srt.Add("@Status", Status);
                }
                var dt = sqlHelper.fillDataTable(sp, string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    job_items _item = new job_items
                    {
                        Job_ID = dt.Rows[i]["JobId"].ToString(),
                        cityname = dt.Rows[i]["city"].ToString(),
                        statename = dt.Rows[i]["state"].ToString(),
                        Title = dt.Rows[i]["title"].ToString(),
                        Technician = dt.Rows[i]["Technician"].ToString().ToTitleCase(),
                        Dispatcher = dt.Rows[i]["Dispatcher"].ToString().ToTitleCase(),
                        sdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()).ToString("yyyy-MM-dd hh:mm tt"),
                        endate = Convert.ToDateTime(dt.Rows[i]["enddate"].ToString()).ToString("yyyy-MM-dd hh:mm tt"),
                        Pay_rate = dt.Rows[i]["rate"].ToString().Currency(),
                        JobID = Convert.ToInt32(dt.Rows[i]["id"].ToString()),
                        _sdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()),
                        _endate = Convert.ToDateTime(dt.Rows[i]["enddate"].ToString()),
                        ViewContact = dt.Rows[i]["Contact"].ToString(),
                        ViewPhone = dt.Rows[i]["Phone"].ToString(),
                        Viewzipcode = dt.Rows[i]["zipcode"].ToString(),
                        Viewstreet = dt.Rows[i]["street"].ToString(),
                        ViewSuiteNumber = dt.Rows[i]["SuiteNumber"].ToString()
                    };
                    // _item.status_id = Convert.ToInt32(dt.Rows[i]["Status_id"].ToString());
                    //total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _jbs.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _mdl.error_text = "Error:" + exception.Message;
            }
            _mdl._jobs = _jbs;
            return _mdl;
        }
        public List<job_items> GetAllActiveJobsByIndex(ref int total, int startindex, int endindex, string search = "", string date = "", string Location = "", int cclient = 0, int cTechnician = 0, int cDispatcher = 0, int Status = 0, int userid = 0, int notificationUSER = 0, string stype = "T", int workspaceid = 0, int projectId = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList _srt = new SortedList();
            Crypto _crypt = new Crypto();
            List<job_items> _jbs = new List<job_items>();
            try
            {
                if (!string.IsNullOrEmpty(date))
                {
                    var dates = date.Split('-');
                    _srt.Add("@startdate", dates[0]);
                    _srt.Add("@enddate", dates[1]);
                }
                if (!string.IsNullOrEmpty(search))
                {
                    _srt.Add("@search", search);
                }
                if (!string.IsNullOrEmpty(Location))
                {
                    _srt.Add("@Location", Location);
                }
                _srt.Add("@startindex", startindex);
                _srt.Add("@endindex", endindex);
                _srt.Add("@workspaceid", workspaceid);
                if (userid != 0)
                {
                    _srt.Add("@id", userid);
                    stype = new UserService().GetUserType(userid);
                }
                if (projectId != 0)
                {
                    _srt.Add("@projectId", projectId);
                }
                if (stype == "S" || stype == "LT")
                {
                    _srt.Add("@companyid", userid);
                }
                if (cclient != 0)
                {
                    _srt.Add("@client", cclient);
                }
                if (cTechnician != 0)
                {
                    _srt.Add("@Technician", cTechnician);
                }
                if (cDispatcher != 0)
                {
                    _srt.Add("@Dispatcher", cDispatcher);
                }
                string sp = "GetAllActiveJobsByIndexWithoutStatus";
                if (Status != 0)
                {
                    sp = "GetAllActiveJobsByIndexWithStatus";
                    _srt.Add("@Status", Status);
                }
                var dt = sqlHelper.fillDataTable(sp, string.Empty, _srt); sqlHelper.Dispose();
                notificationServices _nser = new notificationServices();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    job_items _item = new job_items
                    {
                        Job_ID = dt.Rows[i]["JobId"].ToString().Count() > 8 ? dt.Rows[i]["JobId"].ToString().Substring(0, 8) : dt.Rows[i]["JobId"].ToString(),
                        cityname = dt.Rows[i]["city"].ToString(),
                        statename = dt.Rows[i]["state"].ToString(),
                        Title = dt.Rows[i]["Ttile"].ToString().Count() > 30 ? dt.Rows[i]["Ttile"].ToString().Substring(0, 30) : dt.Rows[i]["Ttile"].ToString(),
                        Client = dt.Rows[i]["Client"].ToString(),
                        Technician = dt.Rows[i]["Technician"].ToString(),
                        Dispatcher = dt.Rows[i]["Dispatcher"].ToString(),
                        Project_manager = dt.Rows[i]["Project_manager"].ToString(),
                        sdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()).ToString("yyyy-MM-dd"),
                        stime = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()).ToString("hh:mm tt"),
                        etime = Convert.ToDateTime(dt.Rows[i]["enddate"].ToString()).ToString("hh:mm tt"),
                        Client_rate = stype == "ST" ? "XXXX" : dt.Rows[i]["Client_rate"].ToString().Currency(),
                        _sdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()),
                        _endate = Convert.ToDateTime(dt.Rows[i]["enddate"].ToString()),
                        Pay_rate = stype == "ST" ? "XXXX" : dt.Rows[i]["rate"].ToString().Currency(),
                        Est_hours = dt.Rows[i]["est_Hours"].ToString(),
                        hours = dt.Rows[i]["hours"].ToString(),
                        Expense = dt.Rows[i]["Expnese"].ToString().Currency(),
                        Status = dt.Rows[i]["Status"].ToString(),
                        JobID = Convert.ToInt32(dt.Rows[i]["id"].ToString()),
                        token = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString()),
                        status_id = Convert.ToInt32(dt.Rows[i]["Status_id"].ToString()),
                        JobConfirmation = Convert.ToInt32(dt.Rows[i]["jobconfirmation"].ToString())
                    };
                    _item.commentCount = _nser.GetNotificationCountByType(_item.JobID, NotificationType.Job, notificationUSER);
                    _item.confirm = Convert.ToInt32(dt.Rows[i]["Confirmationtype"].ToString());
                    total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _jbs.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _jbs;
        }

        public List<job_items> GetAllActiveAdminJobsByIndex(ref int total, int startindex, int endindex, string search = "", string date = "", string Location = "", int cclient = 0, int cTechnician = 0, int cDispatcher = 0, int Status = 0, int userid = 0, int notificationUSER = 0, string stype = "T", int workspaceid = 0, int projectId = 0, int ClientManager = 0, int ProjectManagerId = 0, int Screening = 0, int[] Skiils = null)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList _srt = new SortedList();
            Crypto _crypt = new Crypto();
            List<job_items> _jbs = new List<job_items>();
            try
            {
                if (!string.IsNullOrEmpty(date))
                {
                    var dates = date.Split('-');
                    _srt.Add("@startdate", dates[0]);
                    _srt.Add("@enddate", dates[1]);
                }
                if (!string.IsNullOrEmpty(search))
                {
                    _srt.Add("@search", search);
                }
                if (!string.IsNullOrEmpty(Location))
                {
                    _srt.Add("@Location", Location);
                }
                _srt.Add("@startindex", startindex);
                _srt.Add("@endindex", endindex);
                _srt.Add("@workspaceid", workspaceid);
                if (userid != 0)
                {
                    _srt.Add("@id", userid);
                    stype = new UserService().GetUserType(userid);
                }
                if (projectId != 0)
                {
                    _srt.Add("@projectId", projectId);
                }
                if (stype == "S" || stype == "LT")
                {
                    _srt.Add("@companyid", userid);
                }
                if (cclient != 0)
                {
                    _srt.Add("@client", cclient);
                }
                if (cTechnician != 0)
                {
                    _srt.Add("@Technician", cTechnician);
                }
                if (cDispatcher != 0)
                {
                    _srt.Add("@Dispatcher", cDispatcher);
                }
                //int ClientManager=0,int ProjectManagerId=0,int Screening=0, int[] Skiils=null
                if (ClientManager != 0)
                {
                    _srt.Add("@ClientManager", ClientManager);
                }
                if (ProjectManagerId != 0)
                {
                    _srt.Add("@ProjectManagerId", ProjectManagerId);
                }
                if (Screening != 0)
                {
                    _srt.Add("@screening", Screening);
                }
                if (Skiils != null)
                {
                    var skill = string.Join<int>(",", Skiils.Where(a => a != 0));
                    if (!string.IsNullOrEmpty(skill))
                    {
                        _srt.Add("@skills", skill);
                    }
                }
                string sp = "GetAllActiveJobsByIndexWithoutStatus";
                if (Status != 0)
                {
                    sp = "GetAllActiveJobsByIndexWithStatus";
                    _srt.Add("@Status", Status);
                }
                var dt = sqlHelper.fillDataTable(sp, string.Empty, _srt); sqlHelper.Dispose();
                notificationServices _nser = new notificationServices();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    job_items _item = new job_items
                    {
                        Job_ID = dt.Rows[i]["JobId"].ToString().Count() > 8 ? dt.Rows[i]["JobId"].ToString().Substring(0, 8) : dt.Rows[i]["JobId"].ToString(),
                        cityname = dt.Rows[i]["city"].ToString(),
                        statename = dt.Rows[i]["state"].ToString(),
                        Title = dt.Rows[i]["Ttile"].ToString().Count() > 30 ? dt.Rows[i]["Ttile"].ToString().Substring(0, 30) : dt.Rows[i]["Ttile"].ToString(),
                        Client = dt.Rows[i]["Client"].ToString(),
                        Technician = dt.Rows[i]["Technician"].ToString(),
                        Dispatcher = dt.Rows[i]["Dispatcher"].ToString(),
                        Project_manager = dt.Rows[i]["Project_manager"].ToString(),
                        sdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()).ToString("yyyy-MM-dd"),
                        stime = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()).ToString("hh:mm tt"),
                        etime = Convert.ToDateTime(dt.Rows[i]["enddate"].ToString()).ToString("hh:mm tt"),
                        Client_rate = stype == "ST" ? "XXXX" : dt.Rows[i]["Client_rate"].ToString().Currency(),
                        _sdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()),
                        _endate = Convert.ToDateTime(dt.Rows[i]["enddate"].ToString()),
                        Pay_rate = stype == "ST" ? "XXXX" : dt.Rows[i]["rate"].ToString().Currency(),
                        Est_hours = dt.Rows[i]["est_Hours"].ToString(),
                        hours = dt.Rows[i]["hours"].ToString(),
                        Expense = dt.Rows[i]["Expnese"].ToString().Currency(),
                        Status = dt.Rows[i]["Status"].ToString(),
                        JobID = Convert.ToInt32(dt.Rows[i]["id"].ToString()),
                        token = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString()),
                        status_id = Convert.ToInt32(dt.Rows[i]["Status_id"].ToString()),
                        JobConfirmation = Convert.ToInt32(dt.Rows[i]["jobconfirmation"].ToString())//
                    };
                    _item.commentCount = _nser.GetNotificationCountByType(_item.JobID, NotificationType.Job, notificationUSER);
                    _item.confirm = Convert.ToInt32(dt.Rows[i]["Confirmationtype"].ToString());
                    total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _jbs.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _jbs;
        }
        public jobmodal GetAllActiveJobsByUserIdForCalender(int id, string date, string Location = "", int cclient = 0, int cTechnician = 0, int cDispatcher = 0, int Status = 0, int userid = 0, string membertype = "T", int workspaceid = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            jobmodal _mdl = new jobmodal();
            SortedList _srt = new SortedList();
            List<job_items> _jbs = new List<job_items>();
            try
            {
                if (!string.IsNullOrEmpty(date))
                {
                    var dates = date.Split('-');
                    _srt.Add("@startdate", dates[0]);
                    _srt.Add("@enddate", dates[1]);
                }
                if (cclient != 0)
                {
                    _srt.Add("@client", cclient);
                }
                if (userid != 0)
                {
                    _srt.Add("@userid", userid);
                }
                _srt.Add("@workspaceid", workspaceid);

                if (cTechnician != 0)
                {
                    _srt.Add("@Technician", cTechnician);
                }
                if (cDispatcher != 0)
                {
                    _srt.Add("@Dispatcher", cDispatcher);
                }
                if (Status != 0)
                {
                    _srt.Add("@Status", Status);
                }
                if (!string.IsNullOrEmpty(Location))
                {
                    _srt.Add("@Location", Location);
                }
                _srt.Add("@id", id);
                var dt = sqlHelper.fillDataTable("GetAllActiveJobsByUserIdForCalender_v1", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    job_items _item = new job_items
                    {
                        Job_ID = dt.Rows[i]["JobId"].ToString(),
                        cityname = dt.Rows[i]["city"].ToString(),
                        statename = dt.Rows[i]["state"].ToString(),
                        Title = dt.Rows[i]["title"].ToString(),
                        Technician = dt.Rows[i]["Technician"].ToString().ToTitleCase(),
                        Dispatcher = dt.Rows[i]["Dispatcher"].ToString().ToTitleCase(),
                        sdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()).ToString("yyyy-MM-dd hh:mm tt"),
                        endate = Convert.ToDateTime(dt.Rows[i]["enddate"].ToString()).ToString("yyyy-MM-dd hh:mm tt"),
                        Pay_rate = dt.Rows[i]["rate"].ToString().Currency(),
                        JobID = Convert.ToInt32(dt.Rows[i]["id"].ToString()),
                        _sdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()),
                        _endate = Convert.ToDateTime(dt.Rows[i]["enddate"].ToString()),
                        ViewContact = dt.Rows[i]["Contact"].ToString(),
                        ViewPhone = dt.Rows[i]["Phone"].ToString(),
                        Viewzipcode = dt.Rows[i]["zipcode"].ToString(),
                        Viewstreet = dt.Rows[i]["street"].ToString(),
                        ViewSuiteNumber = dt.Rows[i]["SuiteNumber"].ToString()

                    };
                    _jbs.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _mdl.error_text = "Error:" + exception.Message;
            }
            _mdl._jobs = _jbs;
            return _mdl;
        }
        public job_items1 GetAllActiveJobById(int id, int NotificationUser, int _workspaceId)
        {
            SqlHelper sqlHelper = new SqlHelper();
            job_items1 _item = new job_items1();
            Crypto _crypt = new Crypto();
            List<commentmodal> _commnts = new List<commentmodal>();
            List<FileModel> _files = new List<FileModel>();
            notificationServices _nser = new notificationServices();
            List<int> ReminderIds = new List<int>();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@jobid", id }
                };
                var ds = sqlHelper.fillDataSet("GetAllActiveJobById", "", _srt); sqlHelper.Dispose();
                var dt = ds.Tables[0];
                var dt1 = ds.Tables[1];
                var dt2 = ds.Tables[2];
                ds.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _item.jobConfirmation = Convert.ToInt32(dt.Rows[0]["jobconfirmation"].ToString());
                    _item.Job_ID = dt.Rows[0]["JobId"].ToString();
                    _item.Title = dt.Rows[0]["Ttile"].ToString();
                    _item.Client = dt.Rows[0]["Client"].ToString();
                    _item.MapValidateAddress = dt.Rows[0]["address"].ToString().GetAddressView(dt.Rows[0]["SuiteNumber"].ToString());
                    _item.token = _crypt.EncryptStringAES(dt.Rows[0]["id"].ToString());
                    _item.distoken = _crypt.EncryptStringAES(dt.Rows[0]["Dispatcherid"].ToString());
                    _item.tectoken = _crypt.EncryptStringAES(dt.Rows[0]["Technicianid"].ToString());
                    _item.Client_id = Convert.ToInt32(dt.Rows[0]["Client"].ToString());
                    _item.projectId = Convert.ToInt32(dt.Rows[0]["Projectid"].ToString());
                    _item.mgr_id = Convert.ToInt32(dt.Rows[0]["Project_manager"].ToString());
                    _item.mgr_id1 = Convert.ToInt32(dt.Rows[0]["Project_manager1"].ToString());
                    _item.Technician = dt.Rows[0]["Technician"].ToString();
                    _item.Dispatcher = dt.Rows[0]["Dispatcher"].ToString();
                    _item.Project_manager = dt.Rows[0]["Project_manager"].ToString();
                    _item.sdate = Convert.ToDateTime(dt.Rows[0]["startdate"].ToString()).ToString("yyyy-MM-dd");
                    _item.stime = Convert.ToDateTime(dt.Rows[0]["startdate"].ToString()).ToString("hh:mm tt");
                    _item.etime = Convert.ToDateTime(dt.Rows[0]["enddate"].ToString()).ToString("hh:mm tt");
                    _item.Client_rate = dt.Rows[0]["Client_rate"].ToString().Currency();
                    _item.Expense = dt.Rows[0]["expense"].ToString().Currency();
                    _item.Est_hours = dt.Rows[0]["est_Hours"].ToString();
                    _item.Status = Convert.ToInt32(dt.Rows[0]["Status_id"].ToString());
                    _item.JobID = Convert.ToInt32(dt.Rows[0]["id"].ToString());
                    _item.description = dt.Rows[0]["description"].ToString();
                    _item.pay_rate = dt.Rows[0]["pay_rate"].ToString().Currency();
                    _item.Technicianid = Convert.ToInt32(dt.Rows[0]["Technicianid"].ToString());
                    _item.Dispatcherid = Convert.ToInt32(dt.Rows[0]["Dispatcherid"].ToString());
                    _item.unreadcount = _nser.GetNotificationCountByType(_item.JobID, NotificationType.Job, NotificationUser);
                    _item.Instruction = dt.Rows[0]["Instruction"].ToString();
                    _item.Contact = dt.Rows[0]["Contact"].ToString();
                    _item.Phone = dt.Rows[0]["Phone"].ToString();
                }
                dt.Dispose();
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    commentmodal _mdl = new commentmodal
                    {
                        Comment = dt1.Rows[i]["Comment"].ToString(),
                        Commentid = Convert.ToInt32(dt1.Rows[i]["Id"].ToString()),
                        createdby = dt1.Rows[i]["createdby"].ToString(),
                        CreatedDate = dt1.Rows[i]["CreatedDate"].ToString(),
                        isread = dt1.Rows[i]["readusers"].ToString().Contains(NotificationUser.ToString()),
                        createdbyId = Convert.ToInt32(dt1.Rows[i]["crtedby"].ToString())//crtedby
                    };
                    if (_mdl.createdbyId == NotificationUser)
                    {
                        _mdl.isread = true;
                    }
                    _commnts.Add(_mdl);
                }
                dt1.Dispose();
                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    FileModel _mdl = new FileModel
                    {
                        FileTitle = dt2.Rows[i]["Filetile"].ToString(),
                        FileId = Convert.ToInt32(dt2.Rows[i]["id"].ToString()),
                        Size = dt2.Rows[i]["Size"].ToString(),
                        Url = dt2.Rows[i]["url"].ToString(),
                        CreatedBy = dt2.Rows[i]["createdby"].ToString(),
                        CreatedDate = dt2.Rows[i]["Createddate"].ToString(),
                        FileToken = _crypt.EncryptStringAES(dt2.Rows[i]["id"].ToString())
                    };
                    _files.Add(_mdl);
                }
                dt2.Dispose();
                _item._Comments = _commnts;
                _item._files = _files;
                ProcessReminders(id, _item.projectId, _workspaceId);
                var reminders = GetReminderSettingForJob(ref ReminderIds, id);
                _item.techreminder = reminders.techReminder;
                List<int> Tools = new List<int>();
                List<int> Skiils = new List<int>();
                _item.dispatcherreminder = reminders.dispatcherReminder;
                GetToolsSkillsByJobId(id, ref Tools, ref Skiils);
                _item.tools = Tools.ToArray();
                _item.skills = Skiils.ToArray();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _item.error_text = "Error:" + exception.Message;
            }
            return _item;
        }

        public List<string> GetFilesUrlbyJobId(int id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<string> _files = new List<string>();
            try
            {
                string path = "";
                SortedList _srt = new SortedList
                {
                    { "@jobid", id }
                };
                var dt = sqlHelper.fillDataTable("GetFilesUrlbyJobId", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    path = HttpContext.Current.Server.MapPath("~" + dt.Rows[i]["url"].ToString());
                    _files.Add(path);
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _files;
        }

        public void GetToolsSkillsByJobId(int _jobId, ref List<int> Tools, ref List<int> Skiils)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@jobId", _jobId }
                };
                var ds = sqlHelper.fillDataSet("GetToolsSkillsByJobId", string.Empty, _srt); sqlHelper.Dispose();
                var toolsdt = ds.Tables[0];
                var skillsdt1 = ds.Tables[1];
                ds.Dispose();
                if (toolsdt.Rows.Count > 0)
                {
                    for (int i = 0; i < toolsdt.Rows.Count; i++)
                    {
                        Tools.Add(Convert.ToInt32(toolsdt.Rows[i][0].ToString()));
                    }
                }
                toolsdt.Dispose();
                if (skillsdt1.Rows.Count > 0)
                {
                    for (int i = 0; i < skillsdt1.Rows.Count; i++)
                    {
                        Skiils.Add(Convert.ToInt32(skillsdt1.Rows[i][0].ToString()));
                    }
                }
                skillsdt1.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
        }

        public List<string> GetToolsNameByJobId(int _jobId)
        {
            List<string> ToolsName = new List<string>();
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@jobId", _jobId }
                };
                var toolsdt = sqlHelper.fillDataTable("GetToolsNameByJobId", string.Empty, _srt); sqlHelper.Dispose();
                if (toolsdt.Rows.Count > 0)
                {
                    for (int i = 0; i < toolsdt.Rows.Count; i++)
                    {
                        ToolsName.Add(toolsdt.Rows[i][0].ToString());
                    }
                }
                toolsdt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return ToolsName;
        }

        public void ProcessReminders(int _jobId, int _projectId, int _workSpaceId)
        {
            List<int> _reminderIds = new List<int>();
            SqlHelper sqlHelper = new SqlHelper();
            List<int> ProcessReminderIds = new List<int>();
            int Id;
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@projectId", _projectId },
                    { "@workSpaceId", _workSpaceId },
                    { "@jobId", _jobId }
                };
                var ds = sqlHelper.fillDataSet("ProcessReminders", string.Empty, _srt); sqlHelper.Dispose();
                var dt0 = ds.Tables[0];
                var dt = ds.Tables[1];
                ds.Dispose();
                for (int i = 0; i < dt0.Rows.Count; i++)
                {
                    Id = Convert.ToInt32(dt0.Rows[i]["parentid"].ToString());
                    if (!_reminderIds.Contains(Id)) { _reminderIds.Add(Id); }
                }
                dt0.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Id = Convert.ToInt32(dt.Rows[i]["Id"].ToString());
                    if (!_reminderIds.Contains(Id)) { ProcessReminderIds.Add(Id); }
                }
                dt.Dispose();
                if (ProcessReminderIds.Count > 0)
                {
                    sqlHelper = new SqlHelper();
                    _srt.Clear();
                    _srt.Add("@jobId", _jobId);
                    _srt.Add("@ProcessReminderIds", ProcessReminderIds.ToArray().ToDelimitedString());
                    sqlHelper.executeNonQuery("ProcessedReminders", string.Empty, _srt);
                    sqlHelper.Dispose();
                }
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
        }

        public job GetAllActiveJobDetailsById(int id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            Crypto _crypt = new Crypto();
            job _item = new job();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@jobid", id }
                };
                var dt = sqlHelper.fillDataTable("GetAllActiveJobDetailsById", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _item.JobId = dt.Rows[0]["JobId"].ToString();
                    _item.title = dt.Rows[0]["Ttile"].ToString();
                    _item.client_id = Convert.ToInt32(dt.Rows[0]["Client"].ToString());
                    _item._projectid = Convert.ToInt32(dt.Rows[0]["projectId"].ToString());
                    _item.mgr_id = Convert.ToInt32(dt.Rows[0]["Project_manager"].ToString());
                    _item.mgr_id1 = Convert.ToInt32(dt.Rows[0]["Project_manager1"].ToString());
                    _item.sdate = Convert.ToDateTime(dt.Rows[0]["startdate"].ToString()).ToString("yyyy-MM-dd");
                    _item.edate = Convert.ToDateTime(dt.Rows[0]["enddate"].ToString()).ToString("yyyy-MM-dd");
                    _item.stime = Convert.ToDateTime(dt.Rows[0]["startdate"].ToString()).ToString("hh:mm tt");
                    _item.etime = Convert.ToDateTime(dt.Rows[0]["enddate"].ToString()).ToString("hh:mm tt");
                    _item.Est_hours = Convert.ToDouble(dt.Rows[0]["est_Hours"].ToString());
                    _item.Status = Convert.ToInt32(dt.Rows[0]["Status_id"].ToString());
                    _item._jobid = id;
                    _item.token = _crypt.EncryptStringAES(id.ToString());
                    _item.description = dt.Rows[0]["Description"].ToString();
                    _item.Technician = Convert.ToInt32(dt.Rows[0]["Technician"].ToString());
                    _item.Dispatcher = Convert.ToInt32(dt.Rows[0]["Dispatcher"].ToString());
                    _item.Expense = dt.Rows[0]["Expnese"].ToString().Currency();
                    _item.Client_rate = dt.Rows[0]["Client_rate"].ToString().Currency();
                    _item.Pay_rate = dt.Rows[0]["payrate"].ToString().Currency();
                    _item.MapValidateAddress = dt.Rows[0]["address"].ToString();
                    _item.suite = dt.Rows[0]["SuiteNumber"].ToString();
                    _item.Contact = dt.Rows[0]["Contact"].ToString();
                    _item.Phone = dt.Rows[0]["Phone"].ToString();
                    _item.Instruction = dt.Rows[0]["Instruction"].ToString();
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _item.error_text = "Error:" + exception.Message;
            }
            return _item;
        }
        public UserModel GetMemberProfileForTemplateById(int id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            UserModel _item = new UserModel();
            SortedList _srt = new SortedList
            {
                { "@id", id }
            };
            try
            {
                var ds = sqlHelper.fillDataSet("GetMemberProfileForTemplateById", string.Empty, _srt); sqlHelper.Dispose();
                var dt = ds.Tables[0];
                ds.Dispose();
                if (dt.Rows.Count > 0)
                {
                    Crypto _crypt = new Crypto();
                    _item.Zipcode = dt.Rows[0]["Zip_code"].ToString();
                    _item.Address = dt.Rows[0]["address"].ToString();
                    _item.FirstName = dt.Rows[0]["f_name"].ToString().ToTitleCase();
                    _item.LastName = dt.Rows[0]["l_name"].ToString().ToTitleCase();
                    _item.CityNameMailMerge = dt.Rows[0]["city"].ToString();
                    _item.StateNameMailMerge = dt.Rows[0]["state"].ToString();
                    _item.UserId = dt.Rows[0]["member_id"].ToString();
                    _item.Password = _crypt.DecryptStringAES(dt.Rows[0]["Password"].ToString());
                    _item.UserName = dt.Rows[0]["Username"].ToString();
                    _item.EmailAddress = dt.Rows[0]["EMail"].ToString();
                    _item.HourlyRate = dt.Rows[0]["rate"].ToString().Currency();
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _item.error_text = "Error:" + exception.Message;
            }
            return _item;
        }
        public job GetJobProfileByJobIdForNotification(int id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            job _item = new job();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@jobid", id }
                };
                var dt = sqlHelper.fillDataTable("GetJobProfileByJobIdForNotification", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _item.JobId = dt.Rows[0]["JobId"].ToString();
                    _item.title = dt.Rows[0]["Ttile"].ToString();
                    _item.sdate = Convert.ToDateTime(dt.Rows[0]["startdate"].ToString()).ToString("yyyy-MM-dd");
                    _item.datetime = Convert.ToDateTime(dt.Rows[0]["startdate"].ToString()).ToString("yyyy-MM-dd hh:mm tt");
                    _item.stime = Convert.ToDateTime(dt.Rows[0]["startdate"].ToString()).ToString("hh:mm tt");
                    _item.etime = Convert.ToDateTime(dt.Rows[0]["enddate"].ToString()).ToString("hh:mm tt");
                    _item.description = dt.Rows[0]["Description"].ToString();
                    _item.Pay_rate = dt.Rows[0]["payrate"].ToString().Currency();
                    _item.street = dt.Rows[0]["address"].ToString();
                    _item.city = dt.Rows[0]["cityname"].ToString();
                    _item.state = dt.Rows[0]["statename"].ToString();
                    _item.zip = dt.Rows[0]["zipcode"].ToString();
                    _item.address = _item.street.GetAddressView(dt.Rows[0]["SuiteNumber"].ToString());
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _item.error_text = "Error:" + exception.Message;
            }
            return _item;
        }
        public remindersetting GetReminderSettingForJob(ref List<int> _reminderIds, int jobId = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            remindersetting _mdl = new remindersetting();
            List<reminder> techreminder = new List<reminder>();
            List<reminder> dispatcherreminder = new List<reminder>();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@jobId", jobId }
                };
                var ds = sqlHelper.fillDataSet("GetReminderSettingForJob", string.Empty, _srt); sqlHelper.Dispose();
                var dt6 = ds.Tables[0];
                var dt7 = ds.Tables[1];
                ds.Dispose();
                if (dt6.Rows.Count > 0)
                {
                    for (int i = 0; i < dt6.Rows.Count; i++)
                    {
                        reminder techreminder1 = new reminder
                        {
                            Projectid = Convert.ToInt32(dt6.Rows[i]["Projectid"].ToString()),
                            token = new Crypto().EncryptStringAES(dt6.Rows[i]["id"].ToString()),
                            _reminderId = Convert.ToInt32(dt6.Rows[i]["id"].ToString()),
                            repeatid = Convert.ToInt32(dt6.Rows[i]["repeatid"].ToString()),
                            reapetdays = Convert.ToInt32(dt6.Rows[i]["reapetdays"].ToString()),
                            repeattype = Convert.ToInt32(dt6.Rows[i]["ReminderType"].ToString()),
                            Attachment = dt6.Rows[i]["Attachment"].ToString().StringToArray(","),
                            info = dt6.Rows[i]["info"].ToString().StringToArray(","),
                            type = dt6.Rows[i]["type"].ToString().StringToArray(","),
                            Confirmation = Convert.ToInt32(dt6.Rows[i]["Confirmation"].ToString())
                        };
                        _reminderIds.Add(Convert.ToInt32(dt6.Rows[i]["parentid"].ToString()));
                        techreminder.Add(techreminder1);
                    }

                }
                dt6.Dispose();
                _mdl.techReminder = techreminder;
                if (dt7.Rows.Count > 0)
                {
                    for (int i = 0; i < dt7.Rows.Count; i++)
                    {
                        reminder dispatcherreminder1 = new reminder
                        {
                            Projectid = Convert.ToInt32(dt7.Rows[i]["Projectid"].ToString()),
                            token = new Crypto().EncryptStringAES(dt7.Rows[i]["id"].ToString()),
                            _reminderId = Convert.ToInt32(dt7.Rows[i]["id"].ToString()),
                            repeatid = Convert.ToInt32(dt7.Rows[i]["repeatid"].ToString()),
                            reapetdays = Convert.ToInt32(dt7.Rows[i]["reapetdays"].ToString()),
                            repeattype = Convert.ToInt32(dt7.Rows[i]["ReminderType"].ToString()),
                            Attachment = dt7.Rows[i]["Attachment"].ToString().StringToArray(","),
                            info = dt7.Rows[i]["info"].ToString().StringToArray(","),
                            type = dt7.Rows[i]["type"].ToString().StringToArray(","),
                            Confirmation = Convert.ToInt32(dt7.Rows[i]["Confirmation"].ToString())
                        };
                        _reminderIds.Add(Convert.ToInt32(dt7.Rows[i]["parentid"].ToString()));
                        dispatcherreminder.Add(dispatcherreminder1);
                    }

                }
                dt7.Dispose();
                _mdl.dispatcherReminder = dispatcherreminder;
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _mdl;
        }
        public void decreasesmsemailcount(int workspaceid, int smscount, int emailcount)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srtlist = new SortedList
                {
                    { "@workspaceid", workspaceid },
                    { "@smscount", smscount },
                    { "@emailcount", emailcount }
                };
                sqlHelper.executeNonQuery("updateaccount", string.Empty, _srtlist); sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
        }

        public void SendJobNotification(int UserId, int Jobid, int workspaceid, int sender)
        {
            //bool sms, bool email, string smstext, string emailtext, string subject, int workspaceid, string workspaceName
            Crypto _crypt = new Crypto();
            try
            {
                string domain = string.Empty;
                var template = new templateservices().GetJobNotificationTemplate(workspaceid, ref domain);
                bool sms = false;
                bool email = true;
                var sqlHelper = new SqlHelper();
                var emailtext = template.email;
                var smstext = template.sms;
                var job = GetJobProfileByJobIdForNotification(Jobid);
                string acceptlink = domain + ".taskayak.com/account/acceptjob?token=";
                string declinelink = domain + ".taskayak.com/account/declinejob?token=";
                SortedList _srt = new SortedList();
                string encryptid = _crypt.EncryptStringAES(Jobid.ToString());
                acceptlink += encryptid;
                declinelink += encryptid;
                int offersId = 0;
                int smscount = 0;
                int emailcount = 0;
                var mdl = GetMemberProfileForTemplateById(UserId);
                smstext = sms ? smstext.MailMergeBytext(job.datetime, job.sdate, job.stime, job.etime, job.JobId, job.title, job.Pay_rate, job.description, mdl.FirstName, mdl.LastName, mdl.FirstName + " " + mdl.LastName, mdl.StateNameMailMerge, mdl.CityNameMailMerge, job.city, job.state, job.address, acceptlink, mdl.HourlyRate, mdl.Password, mdl.UserName, mdl.EmailAddress, mdl.UserId, declinelink) : "";
                emailtext = email ? emailtext.MailMergeBytext(job.datetime, job.sdate, job.stime, job.etime, job.JobId, job.title, job.Pay_rate, job.description, mdl.FirstName, mdl.LastName, mdl.FirstName + " " + mdl.LastName, mdl.StateNameMailMerge, mdl.CityNameMailMerge, job.city, job.state, job.address, acceptlink, mdl.HourlyRate, mdl.Password, mdl.UserName, mdl.EmailAddress, mdl.UserId, declinelink) : "";
                _srt.Clear();
                _srt.Add("@memberid", UserId);
                _srt.Add("@pref", "Manual");
                _srt.Add("@offerId", Jobid);
                _srt.Add("@sms", smstext);
                _srt.Add("@email", emailtext);
                _srt.Add("@subject", "You have assigned a new job");
                _srt.Add("@sender", sender);
                offersId = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("AddJobNotification", string.Empty, _srt).ToString());
                smscount += sms ? 1 : 0;
                emailcount += email ? 1 : 0;
                decreasesmsemailcount(workspaceid, smscount, emailcount);
            }
            catch (Exception)
            {

            }
        }
    }
}