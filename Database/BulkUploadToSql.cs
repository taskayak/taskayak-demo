﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class BulkUploadToSql
    {
        // public IList<T> InternalStore { get; set; }
        public DataTable InternalStore_dt { get; set; }
        public string TableName { get; set; }
        public int CommitBatchSize { get; set; } = 1000;
        public string ConnectionString { get; set; }
        
        //public void Commit_list()
        //{
        //    if (InternalStore.Count > 0)
        //    {
        //        DataTable dt;
        //        int numberOfPages = (InternalStore.Count / CommitBatchSize) + (InternalStore.Count % CommitBatchSize == 0 ? 0 : 1);
        //        for (int pageIndex = 0; pageIndex < numberOfPages; pageIndex++)
        //        {
        //            dt = InternalStore.Skip(pageIndex * CommitBatchSize).Take(CommitBatchSize).ToDataTable();
        //            BulkInsert(dt);
        //        }
        //    }
        //}


        public void Commit_datatable(string spcommit = "",int workspaceid=0)
        {
            SortedList _srtlist = new SortedList();
            if (InternalStore_dt.Rows.Count > 0)
            {
                DataTable dt;
                int numberOfPages = (InternalStore_dt.Rows.Count / CommitBatchSize) + (InternalStore_dt.Rows.Count % CommitBatchSize == 0 ? 0 : 1);
                for (int pageIndex = 0; pageIndex < numberOfPages; pageIndex++)
                {
                    dt = InternalStore_dt.AsEnumerable().Skip(pageIndex * CommitBatchSize).Take(CommitBatchSize).CopyToDataTable();
                    BulkInsert(dt);
                }
                if (!String.IsNullOrEmpty(spcommit))
                {
                    _srtlist.Clear();
                    _srtlist.Add("@workspaceid", workspaceid);
                    new SqlHelper().executeNonQuery(spcommit, "", _srtlist);
                }
            }
        }

        public int Commit_datatableWithretrun(string spcommit = "", int workspaceid = 0)
        {
            int value = 0;
            SortedList _srtlist = new SortedList();
            if (InternalStore_dt.Rows.Count > 0)
            {
                DataTable dt;
                int numberOfPages = (InternalStore_dt.Rows.Count / CommitBatchSize) + (InternalStore_dt.Rows.Count % CommitBatchSize == 0 ? 0 : 1);
                for (int pageIndex = 0; pageIndex < numberOfPages; pageIndex++)
                {
                    dt = InternalStore_dt.AsEnumerable().Skip(pageIndex * CommitBatchSize).Take(CommitBatchSize).CopyToDataTable();
                    BulkInsert(dt);
                }
                if (!String.IsNullOrEmpty(spcommit))
                {
                    _srtlist.Clear();
                    _srtlist.Add("@workspaceid", workspaceid);
                    value=Convert.ToInt32(new SqlHelper().executeNonQueryWMessage(spcommit, "", _srtlist).ToString());
                }
            }
            return value;
        }

        public void BulkInsert(DataTable dt)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
                SqlBulkCopy bulkCopy =
                    new SqlBulkCopy
                    (
                    connection,
                    SqlBulkCopyOptions.TableLock |
                    SqlBulkCopyOptions.FireTriggers |
                    SqlBulkCopyOptions.UseInternalTransaction,
                    null
                    );
                bulkCopy.DestinationTableName = TableName;
                connection.Open();
                bulkCopy.WriteToServer(dt);
                connection.Close();
        }

    }

    public static class BulkUploadToSqlHelper
    {
        public static DataTable ToDataTable<T>(this IEnumerable<T> data)
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }
    }
}