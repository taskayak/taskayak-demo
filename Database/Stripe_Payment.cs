﻿using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class Stripe_Payment
    {
        StripeClient _sclient = new StripeClient(apiKey: "sk_test_CFx1BIKKWhmL1OSPhY9YPbJJ", clientId: "acct_1BHKqYC0JOcJHmzl");
        //---------------------------------test code
        public bool PaymentWithSubscription(string token, string description, string email, string invoicePrefix, string name, string phone, string planid, string subscriptionInterval, decimal amount, string productid)
        {
            CustomerService _cser = new CustomerService(_sclient);
            CustomerListOptions _clist = new CustomerListOptions();
            _clist.Email = email;
            var _list = _cser.List(_clist);
            var id = "";
            foreach (var item in _list)
            {
                id = item.Id;
            }
            if (string.IsNullOrEmpty(id))
            {
                CustomerCreateOptions _coptions = new CustomerCreateOptions();
                _coptions.Source = token;
                _coptions.Description = description;
                _coptions.Email = email;
                _coptions.InvoicePrefix = invoicePrefix;
                _coptions.Name = name;
                _coptions.Phone = phone;
                _coptions.Plan = planid;
                _cser.Create(_coptions);
            }
            else
            {
                CustomerUpdateOptions _cupdate = new CustomerUpdateOptions();
                _cupdate.Email = email;
                _cupdate.Source = token;
                _cser.Update(id, _cupdate);
            }
            //  //-------------------Payment------------------------------------
            //  ChargeCreateOptions _chrgoptions = new ChargeCreateOptions();
            //  _chrgoptions.Capture = true;
            //  _chrgoptions.Currency = "usd";
            // _chrgoptions.Customer = id;
            //  _chrgoptions.Description = description;
            //  _chrgoptions.ReceiptEmail = email;
            ////  _chrgoptions.Source = token;
            //  _chrgoptions.StatementDescriptor = description;
            //  _chrgoptions.StatementDescriptorSuffix = description;
            //  _chrgoptions.Amount = Convert.ToInt64(amount * 100);
            //  ChargeService _chrgservice = new ChargeService(_sclient);
            //  _chrgservice.Create(_chrgoptions);

            //-------------------subscription--------------------------------
            var Items = new List<SubscriptionItemOptions>
                {
                    new SubscriptionItemOptions
                    {
                        PriceData = new SubscriptionItemPriceDataOptions
                        {
                            Currency = "usd",
                            Product = productid,
                            Recurring = new SubscriptionItemPriceDataRecurringOptions
                            {
                                Interval = subscriptionInterval,
                                IntervalCount = 1,
                            },
                            UnitAmountDecimal =amount*100, // Ensure decimals work
                        },
                        Quantity = 1,
                    },
                };

            SubscriptionCreateOptions _scoptions = new SubscriptionCreateOptions();
            _scoptions.Customer = id;
            _scoptions.Items = Items;
            _scoptions.CollectionMethod = "charge_automatically";
            SubscriptionService _subservoce = new SubscriptionService(_sclient);
            _subservoce.Create(_scoptions);
            return false;
        }
        //---------test code  

        #region Register Plans
        public bool StarterWithMonthly(string token, string description, string email, string name, string phone, ref string customerid, ref string subscriptionid)
        {
            CustomerService _cser = new CustomerService(_sclient);
            CustomerListOptions _clist = new CustomerListOptions();
            _clist.Email = email;
            var _list = _cser.List(_clist);
            var id = "";
            foreach (var item in _list)
            {
                id = item.Id;
            }
            if (string.IsNullOrEmpty(id))
            {
                CustomerCreateOptions _coptions = new CustomerCreateOptions();
                _coptions.Source = token;
                _coptions.Description = description;
                _coptions.Email = email;
                _coptions.Name = name;
                _coptions.Phone = phone;
                _cser.Create(_coptions);
            }
            //-------------------subscription--------------------------------
          
            var Items = new List<SubscriptionItemOptions>
                {
                    new SubscriptionItemOptions
                    {
                        Quantity = 1,
                        Plan="plan_HJzZrQN6Fd7uhE",
                     },
                };
            SubscriptionCreateOptions _scoptions = new SubscriptionCreateOptions();
            _scoptions.Customer = id;
            customerid = id;
            _scoptions.Items = Items;
            _scoptions.CollectionMethod = "charge_automatically";
            SubscriptionService _subservoce = new SubscriptionService(_sclient);
            var subscription = _subservoce.Create(_scoptions);
            subscriptionid = subscription.Id;
            return false;
        }

        public bool StarterWithdaily(string token, string description, string email, string name, string phone, ref string customerid, ref string subscriptionid)
        {
            CustomerService _cser = new CustomerService(_sclient);
            CustomerListOptions _clist = new CustomerListOptions();
            _clist.Email = email;
            var _list = _cser.List(_clist);
            var id = "";
            foreach (var item in _list)
            {
                id = item.Id;
            }
            if (string.IsNullOrEmpty(id))
            {
                CustomerCreateOptions _coptions = new CustomerCreateOptions();
                _coptions.Source = token;
                _coptions.Description = description;
                _coptions.Email = email;
                _coptions.Name = name;
                _coptions.Phone = phone;
                _cser.Create(_coptions);
            }
            //-------------------subscription-------------------------------- 
            var Items = new List<SubscriptionItemOptions>
                {
                    new SubscriptionItemOptions
                    {
                        Quantity = 1,
                        Plan="price_1H4rbqC0JOcJHmzlqXusDFpK",
                     },
                };
            SubscriptionCreateOptions _scoptions = new SubscriptionCreateOptions();
            _scoptions.Customer = id;
            customerid = id;
            _scoptions.Items = Items;
            _scoptions.CollectionMethod = "charge_automatically";
            SubscriptionService _subservoce = new SubscriptionService(_sclient);
            var subscription = _subservoce.Create(_scoptions);
            subscriptionid = subscription.Id;
            return false;
        }


        public bool StarterWithYearly(string token, string description, string email, string name, string phone, ref string customerid, ref string subscriptionid)
        {
            CustomerService _cser = new CustomerService(_sclient);
            CustomerListOptions _clist = new CustomerListOptions();
            _clist.Email = email;
            var _list = _cser.List(_clist);
            var id = "";
            foreach (var item in _list)
            {
                id = item.Id;
            }
            if (string.IsNullOrEmpty(id))
            {
                CustomerCreateOptions _coptions = new CustomerCreateOptions();
                _coptions.Source = token;
                _coptions.Description = description;
                _coptions.Email = email;
                _coptions.Name = name;
                _coptions.Phone = phone;
                _cser.Create(_coptions);
            }

            //-------------------subscription--------------------------------
            var Items = new List<SubscriptionItemOptions>
                {
                    new SubscriptionItemOptions
                    {
                        Quantity = 1,
                        Plan="plan_HJzLPF8IvK7S02",
                    },
                };

            SubscriptionCreateOptions _scoptions = new SubscriptionCreateOptions();
            customerid = id;
            _scoptions.Customer = id;
            _scoptions.Items = Items;
            _scoptions.CollectionMethod = "charge_automatically";
            SubscriptionService _subservoce = new SubscriptionService(_sclient);
            var subscription = _subservoce.Create(_scoptions);
            subscriptionid = subscription.Id;
            return false;
        }

        public bool AdvantageWithMonthly(string token, string description, string email, string name, string phone, ref string customerid, ref string subscriptionid)
        {
            CustomerService _cser = new CustomerService(_sclient);
            CustomerListOptions _clist = new CustomerListOptions();
            _clist.Email = email;
            var _list = _cser.List(_clist);
            var id = "";
            foreach (var item in _list)
            {
                id = item.Id;
            }
            if (string.IsNullOrEmpty(id))
            {
                CustomerCreateOptions _coptions = new CustomerCreateOptions();
                _coptions.Source = token;
                _coptions.Description = description;
                _coptions.Email = email;
                _coptions.Name = name;
                _coptions.Phone = phone;
                _cser.Create(_coptions);
            }
            //-------------------subscription--------------------------------
            var Items = new List<SubscriptionItemOptions>
                {
                    new SubscriptionItemOptions
                    {
                        
                        Quantity = 1,
                       Plan= "plan_HJzcdThd8pC9c6",
        },
                };

            SubscriptionCreateOptions _scoptions = new SubscriptionCreateOptions();
            customerid = id;
            _scoptions.Items = Items;
            _scoptions.Customer = id;
            _scoptions.CollectionMethod = "charge_automatically";
            SubscriptionService _subservoce = new SubscriptionService(_sclient);
            var subscription = _subservoce.Create(_scoptions);
            subscriptionid = subscription.Id;
            return false;
        }

        public bool AdvantageWithYearly(string token, string description, string email, string name, string phone, ref string customerid, ref string subscriptionid)
        {
            CustomerService _cser = new CustomerService(_sclient);
            CustomerListOptions _clist = new CustomerListOptions();
            _clist.Email = email;
            var _list = _cser.List(_clist);
            var id = "";
            foreach (var item in _list)
            {
                id = item.Id;
            }
            if (string.IsNullOrEmpty(id))
            {
                CustomerCreateOptions _coptions = new CustomerCreateOptions();
                _coptions.Source = token;
                _coptions.Description = description;
                _coptions.Email = email;
                _coptions.Name = name;
                _coptions.Phone = phone;
                _cser.Create(_coptions);
            }

            //-------------------subscription--------------------------------
            var Items = new List<SubscriptionItemOptions>
                {
                    new SubscriptionItemOptions
                    {
                       
                        Quantity = 1,
                        Plan="plan_HJzUVdVDSjWZDW",
                    },
                };

            SubscriptionCreateOptions _scoptions = new SubscriptionCreateOptions();
            customerid = id;
            _scoptions.Items = Items;
            _scoptions.Customer = id;
            _scoptions.CollectionMethod = "charge_automatically";
           
            SubscriptionService _subservoce = new SubscriptionService(_sclient);
            var subscription = _subservoce.Create(_scoptions);
            subscriptionid = subscription.Id;
            return false;
        }

        #endregion

        #region SMS Plans

        public bool SMS25(string token, string description, string email, string name, string phone, ref string customerid, ref string subscriptionid)
        {
            CustomerService _cser = new CustomerService(_sclient);
            CustomerListOptions _clist = new CustomerListOptions();
            _clist.Email = email;
            var _list = _cser.List(_clist);
            var id = "";
            foreach (var item in _list)
            {
                id = item.Id;
            }
            if (string.IsNullOrEmpty(id))
            {
                CustomerCreateOptions _coptions = new CustomerCreateOptions();
                _coptions.Source = token;
                _coptions.Description = description;
                _coptions.Email = email;
                _coptions.Name = name;
                _coptions.Phone = phone;
                _cser.Create(_coptions);
            }

            //-------------------subscription--------------------------------
            var Items = new List<SubscriptionItemOptions>
                {
                    new SubscriptionItemOptions
                    {
                                                Quantity = 1,
                                                Plan= "plan_HJzedgq534jHM9",
        },
                };

            SubscriptionCreateOptions _scoptions = new SubscriptionCreateOptions();
            customerid = id;
            _scoptions.Items = Items;
            _scoptions.Customer = id;
            _scoptions.CollectionMethod = "charge_automatically";
            SubscriptionService _subservoce = new SubscriptionService(_sclient);
            var subscription = _subservoce.Create(_scoptions);
            subscriptionid = subscription.Id;
            return false;
        }

        public bool SMS35(string token, string description, string email, string name, string phone, ref string customerid, ref string subscriptionid)
        {
            CustomerService _cser = new CustomerService(_sclient);
            CustomerListOptions _clist = new CustomerListOptions();
            _clist.Email = email;
            var _list = _cser.List(_clist);
            var id = "";
            foreach (var item in _list)
            {
                id = item.Id;
            }
            if (string.IsNullOrEmpty(id))
            {
                CustomerCreateOptions _coptions = new CustomerCreateOptions();
                _coptions.Source = token;
                _coptions.Description = description;
                _coptions.Email = email;
                _coptions.Name = name;
                _coptions.Phone = phone;
                _cser.Create(_coptions);
            }

            //-------------------subscription--------------------------------
            var Items = new List<SubscriptionItemOptions>
                {
                    new SubscriptionItemOptions
                    {
                       Plan= "plan_HJzfTaPbQ6OeAl",
                        Quantity = 1,
                    },
                };

            SubscriptionCreateOptions _scoptions = new SubscriptionCreateOptions();
            customerid = id;
            _scoptions.Customer = id;
            _scoptions.Items = Items;
            _scoptions.CollectionMethod = "charge_automatically";
            SubscriptionService _subservoce = new SubscriptionService(_sclient);
            var subscription = _subservoce.Create(_scoptions);
            subscriptionid = subscription.Id;
            return false;
        }
        public bool SMS50(string token, string description, string email, string name, string phone, ref string customerid, ref string subscriptionid)
        {
            CustomerService _cser = new CustomerService(_sclient);
            CustomerListOptions _clist = new CustomerListOptions();
            _clist.Email = email;
            var _list = _cser.List(_clist);
            var id = "";
            foreach (var item in _list)
            {
                id = item.Id;
            }
            if (string.IsNullOrEmpty(id))
            {
                CustomerCreateOptions _coptions = new CustomerCreateOptions();
                _coptions.Source = token;
                _coptions.Description = description;
                _coptions.Email = email;
                _coptions.Name = name;
                _coptions.Phone = phone;
                _cser.Create(_coptions);
            }
            //-------------------subscription--------------------------------
            var Items = new List<SubscriptionItemOptions>
                {
                    new SubscriptionItemOptions
                    {
                        Plan="plan_HK0HAiwvgG5w3z",
            Quantity = 1,
                    },
                };

            SubscriptionCreateOptions _scoptions = new SubscriptionCreateOptions();
            customerid = id;
            _scoptions.Customer = id;
            _scoptions.Items = Items;
            _scoptions.CollectionMethod = "charge_automatically";
            SubscriptionService _subservoce = new SubscriptionService(_sclient);
            var subscription = _subservoce.Create(_scoptions);
            subscriptionid = subscription.Id;
            return false;
        }

        public bool SMS100(string token, string description, string email, string name, string phone, ref string customerid, ref string subscriptionid)
        {
            CustomerService _cser = new CustomerService(_sclient);
            CustomerListOptions _clist = new CustomerListOptions();
            _clist.Email = email;
            var _list = _cser.List(_clist);
            var id = "";
            foreach (var item in _list)
            {
                id = item.Id;
            }
            if (string.IsNullOrEmpty(id))
            {
                CustomerCreateOptions _coptions = new CustomerCreateOptions();
                _coptions.Source = token;
                _coptions.Description = description;
                _coptions.Email = email;
                _coptions.Name = name;
                _coptions.Phone = phone;

                _cser.Create(_coptions);
            }
            //-------------------subscription--------------------------------
            var Items = new List<SubscriptionItemOptions>
                {
                    new SubscriptionItemOptions
                    {
                        Quantity = 1,
                        Plan="plan_HK0IAdyufrlvrU",
                    },
                };

            SubscriptionCreateOptions _scoptions = new SubscriptionCreateOptions();
            customerid = id;
            _scoptions.Customer = id;
            _scoptions.Items = Items;
            _scoptions.CollectionMethod = "charge_automatically";
            SubscriptionService _subservoce = new SubscriptionService(_sclient);
            var subscription = _subservoce.Create(_scoptions);
            subscriptionid = subscription.Id;
            return false;
        }

        public bool SMS200(string token, string description, string email, string name, string phone, ref string customerid, ref string subscriptionid)
        {
            CustomerService _cser = new CustomerService(_sclient);
            CustomerListOptions _clist = new CustomerListOptions();
            _clist.Email = email;
            var _list = _cser.List(_clist);
            var id = "";
            foreach (var item in _list)
            {
                id = item.Id;
            }
            if (string.IsNullOrEmpty(id))
            {
                CustomerCreateOptions _coptions = new CustomerCreateOptions();
                _coptions.Source = token;
                _coptions.Description = description;
                _coptions.Email = email;
                _coptions.Name = name;
                _coptions.Phone = phone;
                var customer = _cser.Create(_coptions);
                id = customer.Id;
            }
            //-------------------subscription--------------------------------
            var Items = new List<SubscriptionItemOptions>
                {
                    new SubscriptionItemOptions
                    {
                        Quantity = 1,
                        Plan="plan_HK0Ih3LB6yJMYU"
                    },

                };

            SubscriptionCreateOptions _scoptions = new SubscriptionCreateOptions();
            customerid = id;
            _scoptions.Items = Items;
            _scoptions.Customer = id;
            _scoptions.CollectionMethod = "charge_automatically";
            SubscriptionService _subservoce = new SubscriptionService(_sclient);
            var subscription = _subservoce.Create(_scoptions);
            subscriptionid = subscription.Id;
            return false;
        }

        //




        #endregion

    }
}