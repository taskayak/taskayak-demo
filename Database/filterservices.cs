﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class filterservices
    {
        public T getfilter<T>(T t, int userId, int pageId)
        {
            typeof(T).GetProperty("isActiveFilter").SetValue(t, false);
            SqlHelper sqlHelper = new SqlHelper(); 
            try
            {
                SortedList sortedLists = new SortedList
                {
                    { "@userId", userId },
                    { "@pageId", pageId }
                };
                var dt = sqlHelper.fillDataTable("getFilterByPageId", string.Empty, sortedLists); sqlHelper.Dispose();
                if (dt.Rows.Count > 0)
                {
                    var search = dt.Rows[0]["search"].ToString();
                    t = deserlizeJson<T>(search);
                    typeof(T).GetProperty("isActiveFilter").SetValue(t,true);
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            } 
            return t;
        }

        public void delete_filter(int userid,int pageId)
        { 
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@pageId", pageId },
                    { "@userid", userid }
                };
                sqlHelper.executeNonQuery("remove_filte", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose(); 
            }
           
        }

        public void save_filter<T>(T t, int userid, int pageId)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@pageId", pageId },
                    { "@userid", userid },
                    { "@search", serlizeJson<T>(t) }
                };
                sqlHelper.executeNonQuery("Addfilter", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
                          } 
        } 
        //---------------------------------------
        public T deserlizeJson<T>(string json)
        {
           var t = JsonConvert.DeserializeObject<T>(json);
            return t;
        }

        public string serlizeJson<T>(T t)
        { 
            string st = JsonConvert.SerializeObject(t);
            return st;
        }
    }
}