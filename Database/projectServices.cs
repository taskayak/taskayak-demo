﻿using hrm.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class projectServices
    {
        public string GetLatestProjectId(int _workspaceId)
        {
            string ProjectId = "0001";
            SqlHelper _SqlHelper = new SqlHelper();
            try
            {
                SortedList _SortedList = new SortedList
                {
                    { "@workspaceId", _workspaceId }
                };
                ProjectId = _SqlHelper.executeNonQueryWMessage("GetLatestProjectId", string.Empty, _SortedList).ToString(); _SqlHelper.Dispose();
                ProjectId = ProcessProjectId4Digit(ProjectId);
            }
            catch (Exception)
            {
                _SqlHelper.Dispose();
            }
            return ProjectId;
        }

        public string ProcessProjectId4Digit(string _projectId)
        {
            int Length = _projectId.Length;
            switch (Length)
            {
                case 1:
                    _projectId = "000" + _projectId;
                    break;
                case 2:
                    _projectId = "00" + _projectId;
                    break;
                case 3:
                    _projectId = "0" + _projectId;
                    break;
            }
            return _projectId;
        }

        public bool validateProjectId(string ProjectId, int workspaceid)
        {
            bool isavailable = true;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", ProjectId },
                    { "@workspaceid", workspaceid }
                };
                isavailable = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("validate_projectid", string.Empty, _srt).ToString()) <= 0; sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return isavailable;
        }

        public _projectdetails GetProjectdetails(int projectId)
        {
            _projectdetails _model = new _projectdetails();
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", projectId }
                };
                var dt = sqlHelper.fillDataTable("GetProjectDetailsById", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _model.clientId = Convert.ToInt32(dt.Rows[0]["clientId"].ToString());
                    _model.ClientManagerId = Convert.ToInt32(dt.Rows[0]["ClientManagerId"].ToString());
                    _model.projectManagerId = Convert.ToInt32(dt.Rows[0]["ProjectManagerId"].ToString());
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _model;
        }

        public void deleteproject(int ProjectId)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", ProjectId }
                };
                sqlHelper.executeNonQuery("removeProject", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
        }

        public void update_status(int ProjectId, int statusid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", ProjectId },
                    { "@statusid", statusid }
                };
                sqlHelper.executeNonQuery("updateProject", string.Empty, _srt);
                sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
        }

        public string CreateProject(ProjectModal _mdl, int workspaceid)
        {
            string message = "Project Added Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@ID", _mdl.ProjectId },
                    { "@clientId", _mdl.clientId },
                    { "@clientManagerId", _mdl.clientManagerId },
                    { "@workspaceid", workspaceid },
                    { "@description", _mdl.description },
                    { "@specialInstruction", _mdl.specialInstruction },
                    { "@email", _mdl.email },
                    { "@projectManagerId", _mdl.projectManagerId },
                    { "@status", _mdl.status },
                    { "@title", _mdl.title }
                };
                if (_mdl.skills != null)
                {
                    _srt.Add("@skills", string.Join<int>(",", _mdl.skills));
                }
                if (_mdl.tools != null)
                {
                    _srt.Add("@tools", string.Join<int>(",", _mdl.tools));
                }
                if (_mdl.Screening != null)
                {
                    _srt.Add("@screening", string.Join<int>(",", _mdl.Screening));
                }
                sqlHelper.executeNonQuery("AddnewProject", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }

        public List<_projectitems> GetActiveProjectByIndex(ref int total, int startindex, int endindex, string search = "", string date = "", int cclient = 0, int cmanager = 0, int pmanager = 0, int Status = 0, int workspaceid = 0,int Screening=0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList _srt = new SortedList();
            Crypto _crypt = new Crypto();
            List<_projectitems> _jbs = new List<_projectitems>();
            int jobcount = 0;
            int offercount = 0;
            try
            {
                if (!string.IsNullOrEmpty(date))
                {
                    var dates = date.Split('-');
                    _srt.Add("@startdate", dates[0]);
                    _srt.Add("@enddate", dates[1]);
                }
                if (!string.IsNullOrEmpty(search))
                {
                    _srt.Add("@search", search);
                }
                _srt.Add("@startindex", startindex);
                _srt.Add("@endindex", endindex);
                _srt.Add("@workspaceid", workspaceid);
                if (cclient != 0)
                {
                    _srt.Add("@client", cclient);
                }
                if (cmanager != 0)
                {
                    _srt.Add("@cmanager", cmanager);
                }
                if (pmanager != 0)
                {
                    _srt.Add("@pmanager", pmanager);
                }
                if (Screening != 0)
                {
                    _srt.Add("@Screening", Screening);
                }
                string sp = "tbl_projects_getallproject_index_withnostatus_v2";
                if (Status != 0)
                {
                    sp = "tbl_projects_getallproject_index_withstatus_v2";
                    _srt.Add("@Status", Status);
                }
                var dt = sqlHelper.fillDataTable(sp, string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    jobcount = 0;
                    offercount = 0;
                    _projectitems _item = new _projectitems
                    {
                        IsDefaultProject = Convert.ToInt32(dt.Rows[i]["Isdefault"].ToString()),//
                        project_ID = dt.Rows[i]["IndexId"].ToString().Count() > 8 ? dt.Rows[i]["IndexId"].ToString().Substring(0, 8) : dt.Rows[i]["IndexId"].ToString(),
                        Title = dt.Rows[i]["ProjectName"].ToString().Count() > 30 ? dt.Rows[i]["ProjectName"].ToString().Substring(0, 30) : dt.Rows[i]["ProjectName"].ToString(),
                        Client = dt.Rows[i]["Client"].ToString().ToTitleCase(),
                        token = _crypt.EncryptStringAES(dt.Rows[i]["ProjectId"].ToString()),
                        Status = Convert.ToInt32(dt.Rows[i]["Status_id"].ToString()) == 1 ? "Open" : "Close",
                        status_id = Convert.ToInt32(dt.Rows[i]["Status_id"].ToString()),
                        ID = Convert.ToInt32(dt.Rows[i]["ProjectId"].ToString()),
                        clientmanager = dt.Rows[i]["ClientManager"].ToString().ToTitleCase(),
                        Project_manager = dt.Rows[i]["ProjectManager"].ToString().ToTitleCase(),
                        reminderCount = GetReminderJobCount(Convert.ToInt32(dt.Rows[i]["ProjectId"].ToString()))
                    };
                    GetofferJobCount(Convert.ToInt32(dt.Rows[i]["ProjectId"].ToString()), workspaceid, ref offercount, ref jobcount);
                    _item.offerCount = offercount;
                    _item.jobCount = jobcount;
                    _item.commentCount = 0;
                    total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _jbs.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _jbs;
        }

        public void GetofferJobCount(int projectid, int workspaceId, ref int offercount, ref int jobcount)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@projectid", projectid },
                    { "@workspaceId", workspaceId }
                };
                var dt = sqlHelper.fillDataTable("GetjobOffercount", string.Empty, _srt); sqlHelper.Dispose();
                offercount = Convert.ToInt32(dt.Rows[0][0].ToString());
                jobcount = Convert.ToInt32(dt.Rows[0][1].ToString());
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
        }
        public int GetReminderJobCount(int projectId)
        {
            int remindercount = 0;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", projectId }
                };
                var dt = sqlHelper.fillDataTable("getReminderCount", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    remindercount = Convert.ToInt32(dt.Rows[0][0].ToString());
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return remindercount;
        }
        public string UpdateProject(ProjectModal _mdl, int ProjectId)
        {
            string message = "Project Updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@ID", ProjectId },
                    { "@clientId", _mdl.clientId },
                    { "@clientManagerId", _mdl.clientManagerId },
                    { "@email", _mdl.email },
                    { "@projectManagerId", _mdl.projectManagerId },
                    { "@status", _mdl.status },
                    { "@title", _mdl.title },
                    { "@description", _mdl.description },
                    { "@specialInstruction", _mdl.specialInstruction }
                };
                if (_mdl.skills != null)
                {
                    _srt.Add("@skills", string.Join<int>(",", _mdl.skills));
                }
                if (_mdl.tools != null)
                {
                    _srt.Add("@tools", string.Join<int>(",", _mdl.tools));
                }
                if (_mdl.Screening != null)
                {
                    _srt.Add("@screening", string.Join<int>(",", _mdl.Screening));
                }
                sqlHelper.executeNonQuery("UpdateProjectDetails_v2", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }
        public string update_new_Project(ProjectModal _mdl, int ProjectId)
        {
            string message = "Project Updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@ID", ProjectId },
                    { "@clientId", _mdl.clientId },
                    { "@clientManagerId", _mdl.clientManagerId },
                    { "@email", _mdl.email },
                    { "@projectManagerId", _mdl.projectManagerId },
                    { "@status", _mdl.status },
                    { "@title", _mdl.title }
                };
                if (_mdl.Screening != null)
                {
                    _srt.Add("@screening", string.Join<int>(",", _mdl.Screening));
                }
                sqlHelper.executeNonQuery("UpdateProjectDetails", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }

        public string update_skills(int[] skills, int ProjectId)
        {
            string message = "Skills Updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@ID", ProjectId }
                };
                if (skills != null)
                {
                    _srt.Add("@skills", string.Join<int>(",", skills));
                }
                sqlHelper.executeNonQuery("UpdateProjectskilldDetails_v2", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose(); message = "Error:" + exception.Message;
            }
            return message;
        }

        public string UpdateSkillsByJob(int[] skill, int _jobId)
        {
            string message = "Skills Updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@jobId", _jobId }
                };
                if (skill != null)
                {
                    _srt.Add("@skills", string.Join<int>(",", skill));
                }
                sqlHelper.executeNonQuery("UpdateJobkilldDetails", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }

        public string UpdateSkillsByOfferId(int[] skill, int _OfferId)
        {
            string message = "Skills Updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@OfferId", _OfferId }
                };
                if (skill != null)
                {
                    _srt.Add("@skills", string.Join<int>(",", skill));
                }
                sqlHelper.executeNonQuery("UpdateOfferskillsDetails", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }

        public string UpdateToolsByJob(int[] tools, int _jobId)
        {
            string message = "Tools Updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@jobId", _jobId }
                };
                if (tools != null)
                {
                    _srt.Add("@tools", string.Join<int>(",", tools));
                }
                sqlHelper.executeNonQuery("UpdateJobtoolsDetails", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }

        public string UpdateToolsByOffer(int[] tools, int _offerId)
        {
            string message = "Tools Updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@offerId", _offerId }
                };
                if (tools != null)
                {
                    _srt.Add("@tools", string.Join<int>(",", tools));
                }
                sqlHelper.executeNonQuery("UpdateoffertoolsDetails", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }

        public string update_tools(int[] tools, int ProjectId)
        {
            string message = "Tools Updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@ID", ProjectId }
                };
                if (tools != null)
                {
                    _srt.Add("@tools", string.Join<int>(",", tools));
                }
                sqlHelper.executeNonQuery("UpdateProjecttoolsDetails_v2", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }

        public string addprojectComment(string Comment, int ProjectId, int userid, ref List<int> jobs, ref List<int> offers)
        {
            string message = "Project Updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@ProjectId", ProjectId },
                    { "@comment", Comment },
                    { "@createdby", userid },
                    { "@createddate", DateTime.UtcNow.AddHours(-6) }
                };
                sqlHelper.executeNonQuery("AddProjectComment", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            jobs = getJobListbyProjectId(ProjectId);
            offers = getofferListbyProjectId(ProjectId);
            return message;
        }

        public string AddNewDocument(string FileTitle, int userid, int ProjectId, string Size, string url, ref List<int> jobs, ref List<int> offers, ref int parentid)
        {
            string message = "File Added Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@createddate", DateTime.UtcNow.AddHours(-6) },
                    { "@FileTitle", FileTitle },
                    { "@ProjectId", ProjectId },
                    { "@Size", Size },
                    { "@url", url },
                    { "@createdby", userid }
                };
                parentid = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("tbl_projectfiles_add", string.Empty, _srt).ToString()); sqlHelper.Dispose();
                jobs = getJobListbyProjectId(ProjectId);
                offers = getofferListbyProjectId(ProjectId);
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }
        public string delete_doc(int id, int ProjectId, ref List<int> jobs, ref List<int> offers)
        {
            string message = "File Deleted Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", id }
                };
                sqlHelper.executeNonQuery("tbl_projectfiles_remove", string.Empty, _srt); sqlHelper.Dispose();
                jobs = getJobListbyProjectId(ProjectId);
                offers = getofferListbyProjectId(ProjectId);
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }
        public List<int> getJobListbyProjectId(int projectid)
        {
            List<int> _list = new List<int>();
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@projectid", projectid }
                };
                var dt = sqlHelper.fillDataTable("GetJobsByProjectId", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _list.Add(Convert.ToInt32(dt.Rows[i][0].ToString()));
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _list;

        }
        public List<int> getofferListbyProjectId(int projectid)
        {
            List<int> _list = new List<int>();
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@projectid", projectid }
                };
                var dt = sqlHelper.fillDataTable("GetofferByProjectId", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _list.Add(Convert.ToInt32(dt.Rows[i][0].ToString()));
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _list;

        }
        public ProjectModal GetProjectDetailById(int projectId)
        { 
            SqlHelper sqlHelper = new SqlHelper();
            ProjectModal _mdl = new ProjectModal();
            List<int> _skills = new List<int>();
            List<int> _tools = new List<int>();
            List<int> _screening = new List<int>();
            List<commentmodal> _cmt = new List<commentmodal>();
            List<FileModel> _files = new List<FileModel>();
            List<reminder> techreminder = new List<reminder>();
            List<reminder> dispatcherreminder = new List<reminder>();
            List<string> technicians = new List<string>();
            List<string> dispatchers = new List<string>();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@ID", projectId }
                };
                var ds = sqlHelper.fillDataSet("Getprojectbyid", string.Empty, _srt); sqlHelper.Dispose();
                var dt = ds.Tables[0];
                var dt1 = ds.Tables[1];
                var dt2 = ds.Tables[2];
                var dt3 = ds.Tables[3];
                var dt4 = ds.Tables[4];
                var dt5 = ds.Tables[5];
                ds.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _mdl.Id = Convert.ToInt32(dt.Rows[0]["ProjectId"].ToString());
                    _mdl.clientId = Convert.ToInt32(dt.Rows[0]["clientId"].ToString());
                    _mdl.ProjectId = dt.Rows[0]["IndexId"].ToString();
                    _mdl.clientManagerId = Convert.ToInt32(dt.Rows[0]["ClientManagerId"].ToString());
                    _mdl.description = dt.Rows[0]["Description"].ToString();
                    _mdl.specialInstruction = dt.Rows[0]["SpecialInstruction"].ToString();
                    _mdl.projectManagerId = Convert.ToInt32(dt.Rows[0]["ProjectManagerId"].ToString());
                    _mdl.email = dt.Rows[0]["ReminderEmail"].ToString();
                    _mdl.status = Convert.ToInt32(dt.Rows[0]["Status"].ToString());
                    _mdl.title = dt.Rows[0]["ProjectName"].ToString();
                    _mdl.token = new Crypto().EncryptStringAES(dt.Rows[i]["ProjectId"].ToString());
                    _mdl._client = dt.Rows[0]["clientName"].ToString();
                    _mdl._clientManager = dt.Rows[0]["ClientManager"].ToString();
                    _mdl._projectManager = dt.Rows[0]["projectManager"].ToString();
                }
                if (dt1.Rows.Count > 0)
                {
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        _skills.Add(Convert.ToInt32(dt1.Rows[i][0].ToString()));
                    }
                    _mdl.skills = _skills.ToArray();
                }

                if (dt2.Rows.Count > 0)
                {
                    for (int i = 0; i < dt2.Rows.Count; i++)
                    {
                        _tools.Add(Convert.ToInt32(dt2.Rows[i][0].ToString()));
                    }
                    _mdl.tools = _tools.ToArray();
                }

                if (dt3.Rows.Count > 0)
                {
                    for (int i = 0; i < dt3.Rows.Count; i++)
                    {
                        _screening.Add(Convert.ToInt32(dt3.Rows[i][0].ToString()));
                    }
                    _mdl.Screening = _screening.ToArray();
                }
                if (dt4.Rows.Count > 0)
                {
                    for (int i = 0; i < dt4.Rows.Count; i++)
                    {
                        commentmodal _ml = new commentmodal
                        {
                            Comment = dt4.Rows[i]["Comment"].ToString(),
                            createdby = dt4.Rows[i]["name"].ToString(),
                            CreatedDate = dt4.Rows[i]["Createddate"].ToString()
                        };
                        _cmt.Add(_ml);
                    }

                }
                _mdl._commnts = _cmt;

                if (dt5.Rows.Count > 0)
                {
                    for (int i = 0; i < dt5.Rows.Count; i++)
                    {
                        FileModel _fmdl = new FileModel
                        {
                            FileTitle = dt5.Rows[i]["FileTile"].ToString(),
                            FileId = Convert.ToInt32(dt5.Rows[i]["id"].ToString()),
                            Size = dt5.Rows[i]["Size"].ToString(),
                            Url = dt5.Rows[i]["url"].ToString(),
                            CreatedBy = dt5.Rows[i]["name"].ToString(),
                            CreatedDate = dt5.Rows[i]["Createddate"].ToString(),
                            FileToken = new Crypto().EncryptStringAES(dt5.Rows[i]["id"].ToString())
                        };
                        _files.Add(_fmdl);
                    }
                }
                _mdl._files = _files;
                var dt6 = ds.Tables[6];
                var dt7 = ds.Tables[7];
                if (dt6.Rows.Count > 0)
                {
                    for (int i = 0; i < dt6.Rows.Count; i++)
                    {
                        reminder techreminder1 = new reminder
                        {
                            Projectid = Convert.ToInt32(dt6.Rows[i]["Projectid"].ToString()),
                            token = new Crypto().EncryptStringAES(dt6.Rows[i]["id"].ToString()),
                            _reminderId = Convert.ToInt32(dt6.Rows[i]["id"].ToString()),
                            repeatid = Convert.ToInt32(dt6.Rows[i]["repeatid"].ToString()),
                            reapetdays = Convert.ToInt32(dt6.Rows[i]["reapetdays"].ToString()),
                            repeattype = Convert.ToInt32(dt6.Rows[i]["ReminderType"].ToString()),
                            Attachment = dt6.Rows[i]["Attachment"].ToString().StringToArray(","),
                            info = dt6.Rows[i]["info"].ToString().StringToArray(","),
                            type = dt6.Rows[i]["type"].ToString().StringToArray(","),
                            Confirmation = Convert.ToInt32(dt6.Rows[i]["Confirmation"].ToString())
                        };
                        techreminder.Add(techreminder1);
                    }
                }
                _mdl.techreminder = techreminder;
                if (dt7.Rows.Count > 0)
                {
                    for (int i = 0; i < dt7.Rows.Count; i++)
                    {
                        reminder dispatcherreminder1 = new reminder();
                        dispatcherreminder1.Projectid = Convert.ToInt32(dt7.Rows[i]["Projectid"].ToString());
                        dispatcherreminder1.token = new Crypto().EncryptStringAES(dt7.Rows[i]["id"].ToString());
                        dispatcherreminder1._reminderId = Convert.ToInt32(dt7.Rows[i]["id"].ToString());
                        dispatcherreminder1.repeatid = Convert.ToInt32(dt7.Rows[i]["repeatid"].ToString());
                        dispatcherreminder1.reapetdays = Convert.ToInt32(dt7.Rows[i]["reapetdays"].ToString());
                        dispatcherreminder1.repeattype = Convert.ToInt32(dt7.Rows[i]["ReminderType"].ToString());
                        dispatcherreminder1.Attachment = dt7.Rows[i]["Attachment"].ToString().StringToArray(",");
                        dispatcherreminder1.info = dt7.Rows[i]["info"].ToString().StringToArray(",");
                        dispatcherreminder1.type = dt7.Rows[i]["type"].ToString().StringToArray(",");
                        dispatcherreminder1.Confirmation = Convert.ToInt32(dt7.Rows[i]["Confirmation"].ToString());
                        dispatcherreminder.Add(dispatcherreminder1);
                    }
                }
                _mdl.dispatcherreminder = dispatcherreminder;
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            _mdl.jobOffer = getJobofferprojectId(projectId, ref technicians, ref dispatchers);
            _mdl.technicians = technicians;
            _mdl.dispatchers = dispatchers;
            return _mdl;
        }


        public remindersetting GetReminders(int _workspaceId)
        {
            SqlHelper sqlHelper = new SqlHelper();
            remindersetting _mdl = new remindersetting();
            List<reminder> techreminder = new List<reminder>();
            List<reminder> dispatcherreminder = new List<reminder>();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@WorkspaceId", _workspaceId }
                };
                var ds = sqlHelper.fillDataSet("GetRemindersetting",string.Empty, _srt); sqlHelper.Dispose();
                ds.Dispose();
                var dt6 = ds.Tables[0];
                var dt7 = ds.Tables[1];
                if (dt6.Rows.Count > 0)
                {
                    for (int i = 0; i < dt6.Rows.Count; i++)
                    {
                        reminder techreminder1 = new reminder
                        {
                            Projectid = Convert.ToInt32(dt6.Rows[i]["Projectid"].ToString()),
                            token = new Crypto().EncryptStringAES(dt6.Rows[i]["id"].ToString()),
                            _reminderId = Convert.ToInt32(dt6.Rows[i]["id"].ToString()),
                            repeatid = Convert.ToInt32(dt6.Rows[i]["repeatid"].ToString()),
                            reapetdays = Convert.ToInt32(dt6.Rows[i]["reapetdays"].ToString()),
                            repeattype = Convert.ToInt32(dt6.Rows[i]["ReminderType"].ToString()),
                            Attachment = dt6.Rows[i]["Attachment"].ToString().StringToArray(","),
                            info = dt6.Rows[i]["info"].ToString().StringToArray(","),
                            type = dt6.Rows[i]["type"].ToString().StringToArray(","),
                            Confirmation = Convert.ToInt32(dt6.Rows[i]["Confirmation"].ToString())
                        };
                        techreminder.Add(techreminder1);
                    }

                }
                _mdl.techReminder = techreminder;
                if (dt7.Rows.Count > 0)
                {
                    for (int i = 0; i < dt7.Rows.Count; i++)
                    {
                        reminder dispatcherreminder1 = new reminder
                        {
                            Projectid = Convert.ToInt32(dt7.Rows[i]["Projectid"].ToString()),
                            token = new Crypto().EncryptStringAES(dt7.Rows[i]["id"].ToString()),
                            _reminderId = Convert.ToInt32(dt7.Rows[i]["id"].ToString()),
                            repeatid = Convert.ToInt32(dt7.Rows[i]["repeatid"].ToString()),
                            reapetdays = Convert.ToInt32(dt7.Rows[i]["reapetdays"].ToString()),
                            repeattype = Convert.ToInt32(dt7.Rows[i]["ReminderType"].ToString()),
                            Attachment = dt7.Rows[i]["Attachment"].ToString().StringToArray(","),
                            info = dt7.Rows[i]["info"].ToString().StringToArray(","),
                            type = dt7.Rows[i]["type"].ToString().StringToArray(","),
                            Confirmation = Convert.ToInt32(dt7.Rows[i]["Confirmation"].ToString())
                        };
                        dispatcherreminder.Add(dispatcherreminder1);
                    } 
                }
                _mdl.dispatcherReminder = dispatcherreminder;
                dt6.Dispose();
                dt7.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            } 
            return _mdl;
        }

        public string addNewReminder(int usertype, reminder item, int projectId, int workspaceid, int isprojectlevel, ref int reminderId)
        {
            string message = "Reminder Added Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            string Attachment = (item.Attachment != null && item.Attachment.Count() > 0) ? item.Attachment.ToDelimitedString() : "";
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@usertype", usertype },
                    { "@remindertype", item.repeattype },
                    { "@projectId", projectId },
                    { "@repeatid", item.repeatid },
                    { "@reapetdays", item.reapetdays },
                    { "@workspaceid", workspaceid },
                    { "@isprojectlevel", isprojectlevel },
                    { "@Attachment", Attachment },
                    { "@info", item.info.ToDelimitedString() },
                    { "@type", item.type.ToDelimitedString() },
                    { "@Confirmation", item.Confirmation }
                };
                reminderId = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("AddReminderfromsetting", string.Empty, _srt).ToString()); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            } 
            return message;
        } 
        public Tuple<string,int>  AddNewReminder(int usertype, reminder item, int workspaceid, int isprojectlevel)
        {
            int reminderId = 0;
            string Attachment = (item.Attachment != null && item.Attachment.Count() > 0) ? item.Attachment.ToDelimitedString() : "";
            string message = "Reminder Added Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@usertype", usertype },
                    { "@remindertype", item.repeattype },
                    { "@projectId", 0 },
                    { "@repeatid", item.repeatid },
                    { "@reapetdays", item.reapetdays },
                    { "@workspaceid", workspaceid },
                    { "@isprojectlevel", isprojectlevel },
                    { "@Attachment", Attachment },
                    { "@info", item.info.ToDelimitedString() },
                    { "@type", item.type.ToDelimitedString() },
                    { "@Confirmation", item.Confirmation }
                };
                reminderId = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("AddReminderfromsetting", string.Empty, _srt).ToString()); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                 message = "Error:" + exception.Message;
            } 
            return Tuple.Create(message, reminderId);
        } 
        public string AddNewReminderWithJobId(int usertype, reminder item, int workspaceid, int isprojectlevel, int _jobId, ref int reminderId)
        {
            string Attachment = (item.Attachment != null && item.Attachment.Count() > 0) ? item.Attachment.ToDelimitedString() : "";
            string message = "Reminder Added Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@usertype", usertype },
                    { "@remindertype", item.repeattype },
                    { "@projectId", 0 },
                    { "@repeatid", item.repeatid },
                    { "@reapetdays", item.reapetdays },
                    { "@workspaceid", workspaceid },
                    { "@isprojectlevel", isprojectlevel },
                    { "@Attachment", Attachment },
                    { "@info", item.info.ToDelimitedString() },
                    { "@type", item.type.ToDelimitedString() },
                    { "@Confirmation", item.Confirmation },
                    { "@jobId", _jobId }
                };
                reminderId = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("AddNewReminderWithJobId", string.Empty, _srt).ToString()); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            } 
            return message;
        }

        public string DeleteReminder(int reminderid)
        {
            string message = "Reminder deleted Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@reminderid", reminderid }
                };
                sqlHelper.executeNonQuery("deleteeReminder", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                 message = "Error:" + exception.Message;
            } 
            return message;
        }


        public string DeactivateReminder(int _reminderId)
        {
            string message = "Reminder deleted Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@reminderid", _reminderId }
                };
                sqlHelper.executeNonQuery("DeactivateReminder", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }  
            return message;
        }


        public string UpdateReminder(int usertype, reminder item, int projectId, int reminderid)
        {
            string Attachment = (item.Attachment != null && item.Attachment.Count() > 0) ? item.Attachment.ToDelimitedString() : ""; 
            string message = "Reminder updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@usertype", usertype },
                    { "@remindertype", item.repeattype },
                    { "@projectId", projectId },
                    { "@repeatid", item.repeatid },
                    { "@reapetdays", item.reapetdays },
                    { "@Attachment", Attachment },
                    { "@info", item.info.ToDelimitedString() },
                    { "@type", item.type.ToDelimitedString() },
                    { "@reminderid", reminderid },
                    { "@Confirmation", item.Confirmation }
                };
                sqlHelper.executeNonQuery("updateReminder", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                 message = "Error:" + exception.Message;
            } 
            return message;
        }

        public List<Projectjobitems> getJobofferprojectId(int projectId, ref List<string> techs, ref List<string> dispatchers)
        {
            Crypto _crypt = new Crypto();
            SqlHelper sqlHelper = new SqlHelper();
            List<Projectjobitems> _mdl = new List<Projectjobitems>();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@ID", projectId }
                };
                var dt = sqlHelper.fillDataTable("GetjobsofferByProjectid", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Projectjobitems _items = new Projectjobitems
                    {
                        date = dt.Rows[i]["startdate"].ToString(),
                        dispatcher = dt.Rows[i]["dispatcher"].ToString().Length > 20 ? dt.Rows[i]["dispatcher"].ToString().Substring(0, 20) : dt.Rows[i]["dispatcher"].ToString(),
                        id = Convert.ToInt32(dt.Rows[i]["id"].ToString()),
                        JobId = dt.Rows[i]["JobId"].ToString().Count() > 20 ? dt.Rows[i]["JobId"].ToString().Substring(0, 20) : dt.Rows[i]["JobId"].ToString(),
                        token = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString()),
                        Location = dt.Rows[i]["cityname"].ToString() + ", " + dt.Rows[i]["statename"].ToString(),
                        tech = dt.Rows[i]["techname"].ToString().Length > 20 ? dt.Rows[i]["techname"].ToString().Substring(0, 20) : dt.Rows[i]["techname"].ToString()
                    };
                    if (!string.IsNullOrEmpty(_items.tech) && !techs.Contains(_items.tech))
                    {
                        techs.Add(_items.tech);
                    }
                    if (!string.IsNullOrEmpty(_items.dispatcher) && !dispatchers.Contains(_items.dispatcher))
                    {
                        dispatchers.Add(_items.dispatcher);
                    } 
                    _items.title = dt.Rows[i]["Ttile"].ToString().Length > 20 ? dt.Rows[i]["Ttile"].ToString().Substring(0, 20) : dt.Rows[i]["Ttile"].ToString();
                    _items.Type = dt.Rows[i]["type"].ToString();
                    _mdl.Add(_items);
                }
            }
            catch (Exception)
            {
                sqlHelper.Dispose(); 
            }
            return _mdl;
        }
    }
}