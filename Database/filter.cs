﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class filter
    { public bool isActiveFilter { get; set; }
    } 
    public class Payfilter: filter
    { public int  PaymentStatus { get; set; } 
        public string JobStatus { get; set; }
        public int MemberId { get; set; }
        public string Location { get; set; }//Can be a adress ,a city, a state or all three
        public int ProjectId { get; set; }
        public int ManagerId { get; set; }
        public int? length { get; set; }
    } 
    public class memberfilter : filter
    {    public string membertype { get; set; }
        public string amount { get; set; } 
        public string skills { get; set; } 
        public string drug { get; set; }
        public string background { get; set; }
        public int? status { get; set; }
        public string Location { get; set; }//Can be a adress ,a city, a state or all three
        public int? workspace { get; set; }
        public int? Screening { get; set; }
        public int? Role { get; set; }
        public int? Contractor { get; set; }
        public int? length { get; set; }
    } 
    public class jobfilter : filter
    {
        public string Location { get; set; }//Can be a adress ,a city, a state or all three
        public int? project { get; set; } 
        public string date { get; set; }
        public int? cclient { get; set; }
        public int? cTechnician { get; set; }
        public int? cDispatcher { get; set; }
        public int? Status { get; set; }
        public int? state_filter_id { get; set; }
        public int? city_filter_id { get; set; }
        public int? PmanagerId { get; set; }
        public int? cmanagerId { get; set; }
        public int? screening { get; set; }
        public string Skiil { get; set; }
        public int? length { get; set; }
    } 
    public class projectfilter : filter
    { 
        public string date { get; set; }
        public int? cclient { get; set; }
        public int? cmanager { get; set; }
        public int? pmanager { get; set; }
        public int? Status { get; set; }
        public int? Screening { get; set; }
        public int? length { get; set; }
    } 
}