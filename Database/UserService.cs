﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using hrm.Models;
using Flurl;
using System.Web.Mvc;

namespace hrm.Database
{
    public class UserService
    {    /// <summary>
         /// Create a enquiry
         /// </summary>
         /// <returns>Returend message</returns>
        public string AddEquiry(string CompanyName, string Email, string PhoneNumber, string ContactName, int Employees, string Message, string EmailBody, string website)
        {
            SqlHelper _SqlHelper = new SqlHelper();
            try
            {
                SortedList _SortedList = new SortedList
                {
                    { "@CompanyName", CompanyName},
                    { "@Email", Email },
                    { "@PhoneNumber", PhoneNumber },
                    { "@ContactName", ContactName },
                    { "@Employees", Employees },
                    { "@Message", Message},
                     { "@website", website},
                    { "@EmailBody",EmailBody}
                };
                string ReturnMessage = _SqlHelper.executeNonQueryWMessage("AddEnquiry", string.Empty, _SortedList).ToString();
                _SqlHelper.Dispose();
                return ReturnMessage;
            }
            catch (Exception exception)
            {
                _SqlHelper.Dispose();
                return "Error:" + exception.Message;
            }
        }

        /// <summary>
        /// Get Enquiry model with id
        /// </summary> 
        /// <returns>New Enquiry</returns>
        public Enquiry GetEnquirymodel(int EnquiryId)
        {
            SqlHelper _SqlHelper = new SqlHelper();
            Enquiry _model = new Enquiry();
            try
            {
                SortedList _SortedList = new SortedList
                {
                    { "@EnquiryId", EnquiryId }
                };
                var dt = _SqlHelper.fillDataTable("GetEnquiry", string.Empty, _SortedList);
                _SqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _model.CompanyName = dt.Rows[0]["CompanyName"].ToString();
                    _model.ContactName = dt.Rows[0]["ContactName"].ToString();
                    _model.Email = dt.Rows[0]["Email"].ToString();
                    _model.EmailBody = dt.Rows[0]["EmailBody"].ToString();
                    _model.Employees = Convert.ToInt32(dt.Rows[0]["Employees"].ToString());
                    _model.Message = dt.Rows[0]["Message"].ToString();
                    _model.PhoneNumber = dt.Rows[0]["PhoneNumber"].ToString();
                    _model.website = dt.Rows[0]["Website"].ToString();
                    _model.IsValidate = Convert.ToBoolean(dt.Rows[0]["IsValidted"].ToString());
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                _SqlHelper.Dispose();
            }
            return _model;
        }
        //Enquiry

        /// <summary>
        /// Create New userid based on workspace
        /// </summary>
        /// <param name="_workspaceid">workspaceid</param>
        /// <returns>New userid</returns>
        public string GetLatestUserId(int _workspaceid)
        {
            SqlHelper _SqlHelper = new SqlHelper();
            string UserId = "";
            try
            {
                SortedList _SortedList = new SortedList
                {
                    { "@Workspaceid", _workspaceid }
                };
                UserId = _SqlHelper.executeNonQueryWMessage("GetLatestUserId", string.Empty, _SortedList).ToString(); _SqlHelper.Dispose();
                UserId = UserId.Count() < 6 ? "0" + UserId : UserId;
            }
            catch (Exception)
            {
                _SqlHelper.Dispose();
            }
            return UserId;
        }

        public string GetNewUserId(int _workspaceid)
        {
            SqlHelper _SqlHelper = new SqlHelper();
            string UserId = "";
            try
            {
                SortedList _SortedList = new SortedList
                {
                    { "@Workspaceid", _workspaceid }
                };
                UserId = _SqlHelper.executeNonQueryWMessage("GetNewUserId", string.Empty, _SortedList).ToString();
                _SqlHelper.Dispose();
                UserId = UserId.Count() < 6 ? "0" + UserId : UserId;

            }
            catch (Exception)
            {
                _SqlHelper.Dispose();
            }
            return UserId;
        }

        /// <summary>
        /// Get Role id of Technician type based on workspaceid
        /// </summary>
        /// <param name="_workspaceid">workspaceid</param>
        /// <returns>Role  id of Technician type</returns>
        public int GetTechRoleIdByWorkSpaceId(int _workspaceid)
        {
            SqlHelper _SqlHelper = new SqlHelper();
            int RoleId = 1032;
            try
            {
                SortedList _SortedList = new SortedList
                {
                    { "@Workspaceid", _workspaceid }
                };
                RoleId = Convert.ToInt32(_SqlHelper.executeNonQueryWMessage("tbl_member_getroleid", string.Empty, _SortedList).ToString()); _SqlHelper.Dispose();
            }
            catch (Exception)
            {
                _SqlHelper.Dispose();
            }
            return RoleId;
        }
        /// <summary>
        /// Get RtoleId based on position
        /// </summary>
        /// <param name="_workspaceid">workspaceid</param>
        /// <returns>Role  id of new registerd user</returns>
        public int GetRoleIdByPositionType(int _workspaceid, string _position)
        {
            SqlHelper _SqlHelper = new SqlHelper();
            int RoleId = 0;
            try
            {
                SortedList _SortedList = new SortedList
                {
                    { "@Workspaceid", _workspaceid },
                    { "@position", _position }
                };
                RoleId = Convert.ToInt32(_SqlHelper.executeNonQueryWMessage("GetRoleIdByPositionType", string.Empty, _SortedList).ToString()); _SqlHelper.Dispose();
            }
            catch (Exception)
            {
                _SqlHelper.Dispose();
            }
            return RoleId;
        }
        /// <summary>
        /// Notify admin on new technician Register
        /// </summary>
        /// <param name="_userid">Technician userid</param>
        public void AddUserSignupNotification(int _userid)
        {
            SqlHelper _SqlHelper = new SqlHelper();
            SortedList _SortedList = new SortedList
            {
                { "@memberid", _userid }
            };
            try
            {
                _SqlHelper.executeNonQuery("AddtechsignupNotification", string.Empty, _SortedList); _SqlHelper.Dispose();
            }
            catch (Exception)
            {
                _SqlHelper.Dispose();
            }
        }

        /// <summary>
        /// Update user status
        /// </summary>
        /// <param name="_userid">User id </param>
        /// <param name="_statusid"> Status id</param>
        public void UpdateUserStatus(int _userid, int _statusid)
        {
            SqlHelper _SqlHelper = new SqlHelper();
            try
            {
                SortedList _SortedList = new SortedList
                {
                    { "@id", _userid },
                    { "@statusid", _statusid }
                };
                _SqlHelper.executeNonQuery("tbl_member_update_status", string.Empty, _SortedList); _SqlHelper.Dispose();
            }
            catch (Exception)
            {
                _SqlHelper.Dispose();
            }
        }

        public void UpdateAccountStatus(int _accountId, int _statusid)
        {
            SqlHelper _SqlHelper = new SqlHelper();
            try
            {
                SortedList _SortedList = new SortedList
                {
                    { "@id", _accountId },
                    { "@statusid", _statusid }
                };
                _SqlHelper.executeNonQuery("UpdateAccountStatus", string.Empty, _SortedList); _SqlHelper.Dispose();
            }
            catch (Exception)
            {
                _SqlHelper.Dispose();
            }
        }
        /// <summary>
        /// Get user profile , notifications from userid
        /// </summary>
        /// <param name="_userid">User id of profile</param>
        /// <param name="_isadmin">send loggedin user having admin role or not</param>
        /// <returns> user profile ,Job notifications,Offer notifications,Pay notifications,General message </returns>
        public Profilemodel GetUserProfile(int _userid, bool _isadmin)
        {
            Crypto _Crypto = new Crypto();
            SqlHelper _SqlHelper = new SqlHelper();
            Profilemodel _Profilemodel = new Profilemodel();
            SortedList _SortedList = new SortedList();
            List<JobNotificationModel> Offer = new List<JobNotificationModel>();
            List<JobNotificationModel> Job = new List<JobNotificationModel>();
            List<string> _GeneralNotification = new List<string>();
            List<JobNotificationModel> _confirmation = new List<JobNotificationModel>();
            bool Autoread = false;
            try
            {
                _SortedList.Add("@id", _userid);
                var ds = _SqlHelper.fillDataSet("tbl_member_getprofile_v2", string.Empty, _SortedList);
                _SqlHelper.Dispose();
                var dt = ds.Tables[0];//user profile
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _Profilemodel.Image = dt.Rows[i]["image"].ToString();
                    _Profilemodel.Name = dt.Rows[i]["name"].ToString();
                    _Profilemodel.AccountNumber = dt.Rows[i]["companyid"].ToString();
                    _Profilemodel.AccountName = dt.Rows[i]["companyname"].ToString();
                }
                var dt1 = ds.Tables[1];//offer
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    JobNotificationModel _item = new JobNotificationModel();
                    _item.JobId = Convert.ToInt32(dt1.Rows[i]["offerid"].ToString());
                    _item.NotificationId = Convert.ToInt32(dt1.Rows[i]["id"].ToString());
                    _item.Message = dt1.Rows[i]["body"].ToString();
                    _item.Name = dt1.Rows[i]["sender"].ToString().ToTitleCase();
                    _item.JobIndexId = dt1.Rows[i]["offerindexid"].ToString();
                    _item.Time = Convert.ToDateTime(dt1.Rows[i]["Createddate"]).ToString("hh:mm tt");
                    _item.Url = "/offer/view?type=2&token=" + _Crypto.EncryptStringAES(dt1.Rows[i]["offerid"].ToString());
                    Offer.Add(_item);
                }
                var dt2 = ds.Tables[2];//job
                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    JobNotificationModel _item = new JobNotificationModel();
                    _item.JobId = Convert.ToInt32(dt2.Rows[i]["jobid"].ToString());
                    _item.NotificationId = Convert.ToInt32(dt2.Rows[i]["id"].ToString());
                    _item.Message = dt2.Rows[i]["body"].ToString();
                    _item.Name = dt2.Rows[i]["sender"].ToString().ToTitleCase();
                    _item.JobIndexId = dt2.Rows[i]["jobindexid"].ToString();
                    _item.Time = Convert.ToDateTime(dt2.Rows[i]["Createddate"]).ToString("hh:mm tt");
                    Autoread = Convert.ToBoolean(dt2.Rows[i]["jobconfirmation"]);  //
                    _item.Url = "/jobs/view?type=2&read="+(Autoread? _item.NotificationId : 0 )+"&token=" + _Crypto.EncryptStringAES(dt2.Rows[i]["jobid"].ToString());
                    Job.Add(_item);
                }
                var dt3 = ds.Tables[3];//Admin message 
                for (int i = 0; i < dt3.Rows.Count; i++)
                {
                    _GeneralNotification.Add(dt3.Rows[i]["message"].ToString());
                }
                var dt4 = ds.Tables[4];//job
                for (int i = 0; i < dt4.Rows.Count; i++)
                {
                    JobNotificationModel _item = new JobNotificationModel();
                    _item.JobId = Convert.ToInt32(dt4.Rows[i]["jobid"].ToString());
                    _item.NotificationId = Convert.ToInt32(dt4.Rows[i]["id"].ToString());
                    _item.JobIndexId = dt4.Rows[i]["jobindexid"].ToString();
                    _item.Name = dt4.Rows[i]["title"].ToString();
                    _item.ConfirmUrl = "/jobs/index?type=1&c=2&Nid=" + Convert.ToInt32(dt4.Rows[i]["id"].ToString());
                    _item.DeclineUrl = "/jobs/view?type=2&c=1&token=" + _Crypto.EncryptStringAES(dt4.Rows[i]["jobid"].ToString()) + "&Nid=" + Convert.ToInt32(dt4.Rows[i]["id"].ToString());
                    _confirmation.Add(_item);
                }
                _Profilemodel.GeneralMessage = _GeneralNotification;
                ds.Dispose();
                dt.Dispose();
                dt1.Dispose();
                dt2.Dispose();
                dt4.Dispose();
            }
            catch (Exception)
            {
                _SqlHelper.Dispose();
            }
            _Profilemodel.OfferMessage = Offer;
            _Profilemodel.Confirmation = _confirmation;
            _Profilemodel.JobMessage = Job;
            _Profilemodel.PayMessage = GetPayNotification(_isadmin ? 0 : _userid);
            return _Profilemodel;
        }

        /// <summary>
        /// Get pay notification by user id on page top notification section
        /// </summary>
        /// <param name="_userid">User id </param>
        /// <returns>List of pay notification</returns>
        public List<PayNotificationModel> GetPayNotification(int _userid = 0)
        {
            SqlHelper _SqlHelper = new SqlHelper();
            List<PayNotificationModel> model = new List<PayNotificationModel>();
            SortedList _SortedList = new SortedList();
            Crypto _Crypto = new Crypto();
            if (_userid != 0)
            {
                _SortedList.Add("@id", _userid);
            }
            try
            {
                var dt = _SqlHelper.fillDataTable("tbl_notification_getalladminpay", string.Empty, _SortedList);
                _SqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    PayNotificationModel _item = new PayNotificationModel
                    {
                        Hours = dt.Rows[i]["Hoursworked"].ToString(),
                        Amount = dt.Rows[i]["paymentamount"].ToString().Currency(),
                        JobIndexId = dt.Rows[i]["jobid"].ToString(),
                        Name = dt.Rows[i]["member"].ToString().ToTitleCase(),
                        PayId = Convert.ToInt32(dt.Rows[i]["id"].ToString()),
                        Rate = dt.Rows[i]["memberrate"].ToString().Currency()
                    };
                    ;
                    _item.Url = new UrlHelper().Action("edit", "pay", new { token = _Crypto.EncryptStringAES(dt.Rows[i]["id"].ToString()) });
                    model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                _SqlHelper.Dispose();
            }
            return model;
        }

        /// <summary>
        /// Get list of upload files in profile by userid
        /// </summary>
        /// <param name="_userid">Userid of selected user</param>
        /// <returns>List of files</returns>
        public List<FileModel> GetDocumentByUserid(int _userid, int WorkspaceId)
        {
            SqlHelper _SqlHelper = new SqlHelper();
            List<FileModel> model = new List<FileModel>();
            SortedList _SortedList = new SortedList();
            Crypto _Crypto = new Crypto();
            _SortedList.Add("@id", _userid);
            _SortedList.Add("@workspaceId", WorkspaceId);
            try
            {
                var dt = _SqlHelper.fillDataTable("GetDocumentByUserid", string.Empty, _SortedList);
                _SqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    FileModel _item = new FileModel
                    {
                        FileTitle = dt.Rows[i]["filename"].ToString(),
                        Url = dt.Rows[i]["url"].ToString(),
                        FileId = Convert.ToInt32(dt.Rows[i]["id"].ToString()),
                        FileTypeName = dt.Rows[i]["Filetypename"].ToString(),
                        FileTypeId = Convert.ToInt32(dt.Rows[i]["Filetypeid"].ToString()),
                        MainOrder = Convert.ToInt32(dt.Rows[i]["mainorder"].ToString()),
                        FileToken = _Crypto.EncryptStringAES(dt.Rows[i]["id"].ToString())
                    };
                    model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                _SqlHelper.Dispose();
            }
            return model;
        }

        /// <summary>
        /// Get user type on base of userid
        /// </summary>
        /// <param name="_userid">userid of selected user</param>
        /// <returns>User Type</returns>
        public string GetUserType(int _userid)
        {
            SqlHelper _SqlHelper = new SqlHelper();
            string Type = "m";
            SortedList _SortedList = new SortedList
            {
                { "@id", _userid }
            };
            try
            {
                var dt = _SqlHelper.fillDataTable("GetMembertype", string.Empty, _SortedList); _SqlHelper.Dispose();
                if (dt.Rows.Count > 0)
                {
                    Type = dt.Rows[0]["type"].ToString();
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                _SqlHelper.Dispose();
            }
            return Type;
        }

        /// <summary>
        /// Get Contractor id from user profile by userid
        /// </summary>
        /// <param name="_userid">Userid </param>
        /// <returns>Contractor id user</returns> 
        public int GetContractorIdByUserId(int _userid)
        {
            SqlHelper _SqlHelper = new SqlHelper();
            int CompanyId = 0;
            SortedList _SortedList = new SortedList
            {
                { "@id", _userid }
            };
            try
            {
                var dt = _SqlHelper.fillDataTable("getcompanyId", string.Empty, _SortedList); _SqlHelper.Dispose();
                if (dt.Rows.Count > 0)
                {
                    CompanyId = Convert.ToInt32(dt.Rows[0]["companyid"].ToString());
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                _SqlHelper.Dispose();
            }
            return CompanyId;
        }

        /// <summary>
        /// Get Identity column id of user table from userid, Auto generated id from sql server.
        /// </summary>
        /// <param name="_userid">User id</param>
        /// <returns>Identity column id</returns>
        public int GetUserTableRowId(string _userid)
        {
            SqlHelper _SqlHelper = new SqlHelper();
            int TableRowId = 0;
            SortedList _SortedList = new SortedList
            {
                { "@id", _userid }
            };
            try
            {
                var dt = _SqlHelper.fillDataTable("getmemberidbym_id", string.Empty, _SortedList); _SqlHelper.Dispose();
                if (dt.Rows.Count > 0)
                {
                    TableRowId = Convert.ToInt32(dt.Rows[0]["id"].ToString());
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                _SqlHelper.Dispose();
            }
            return TableRowId;
        }


        /// <summary>
        /// Create a new user
        /// </summary>
        /// <param name="model">User details (UserModel class)</param>
        /// <param name="_createdby">Loggedin user</param>
        /// <param name="_workspaceid">Workspace id</param>
        /// <param name="_isshow">"Y" if want to show new user in user list ,"N" no one can see user in user list</param>
        /// <returns>Returend message</returns>
        public string AddNewUser(UserModel model, int _createdby, int _workspaceid = 0, string _isshow = "Y")
        {
            string Message = "Account created successfully";
            SqlHelper _SqlHelper = new SqlHelper();
            Crypto _Crypto = new Crypto();
            try
            {
                SortedList _SortedList = new SortedList
                {
                    { "@fname", model.FirstName },
                    { "@isshow", _isshow },
                    { "@termsaccepted", false },
                    { "@termsrequired", false },
                    { "@checkbucontracter", false },
                    { "@lname", model.LastName }
                };
                if (model.CompanyId != 0)
                {
                    _SortedList.Add("@companyid", model.CompanyId);
                }
                _SortedList.Add("@memberid", model.UserId);
                _SortedList.Add("@username", model.EmailAddress);
                _SortedList.Add("@type", model.UserType);
                _SortedList.Add("@password", _Crypto.EncryptStringAES(model.Password));
                _SortedList.Add("@hourlyrate", model.HourlyRate);
                _SortedList.Add("@tax", model.Tax);
                _SortedList.Add("@statusid", model.StatusId);
                _SortedList.Add("@deptid", 0);
                _SortedList.Add("@desgntid", 0);
                _SortedList.Add("@position", model.Position);
                _SortedList.Add("@roleid", model.RoleId);
                _SortedList.Add("@managerid", model.ManagerId);
                _SortedList.Add("@dob", DateTime.Now);
                _SortedList.Add("@gender", model.Gender);
                _SortedList.Add("@bckground", model.Background);
                _SortedList.Add("@drug", model.DrugTested);
                _SortedList.Add("@email", model.EmailAddress);
                _SortedList.Add("@phone", model.PhoneNumber);
                _SortedList.Add("@alternativephone", model.AlertnativePhoneNumber);
                _SortedList.Add("@stateid", model.StateId);
                _SortedList.Add("@cityid", model.CityId);
                _SortedList.Add("@zip", model.Zipcode);
                _SortedList.Add("@address", model.Address);
                _SortedList.Add("@suite", model.SuiteNumber);
                _SortedList.Add("@MapValidateAddress", model.MapValidateAddress);
                _SortedList.Add("@createdby", _createdby);
                _SortedList.Add("@company", model.CompanyName);
                _SortedList.Add("@dd", model.DrivingDistance);
                _SortedList.Add("@workspaceid", _workspaceid);
                if (model.Skills != null)
                {
                    _SortedList.Add("@skills", string.Join<int>(",", model.Skills));
                }
                _SqlHelper.executeNonQueryWMessage("AddNewUser", string.Empty, _SortedList).ToString(); _SqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                _SqlHelper.Dispose();
                Message = "Error:" + exception.Message;
            }
            return Message;
        }

        /// <summary>
        /// Add new user from company view page or from form/index page
        /// </summary>
        /// <param name="model">User details (UserModel class)</param>
        /// <param name="_createdby">Loggedin user</param>
        /// <param name="_workspaceid">Workspace id</param>
        /// <returns>Returend User table id</returns>
        public int AddNewUserWithoutRegister(UserModel model, int _createdby, int _workspaceid = 0)
        {
            int UserId = 0;
            SqlHelper _SqlHelper = new SqlHelper();
            Crypto _Crypto = new Crypto();
            try
            {
                SortedList _SortedList = new SortedList
                {
                    { "@fname", model.FirstName },
                    { "@termsaccepted", false },
                    { "@termsrequired", false },
                    { "@checkbucontracter", false },
                    { "@lname", model.LastName }
                };
                if (model.CompanyId != 0)
                {
                    _SortedList.Add("@companyid", model.CompanyId);
                }
                _SortedList.Add("@memberid", model.UserId);
                _SortedList.Add("@username", model.EmailAddress);
                _SortedList.Add("@type", model.UserType);
                _SortedList.Add("@password", _Crypto.EncryptStringAES(model.Password));
                _SortedList.Add("@hourlyrate", model.HourlyRate);
                _SortedList.Add("@tax", model.Tax);
                _SortedList.Add("@statusid", model.StatusId);
                _SortedList.Add("@deptid", 0);
                _SortedList.Add("@desgntid", 0);
                _SortedList.Add("@position", model.Position);
                _SortedList.Add("@roleid", model.RoleId);
                _SortedList.Add("@managerid", model.ManagerId);
                _SortedList.Add("@dob", DateTime.Now);
                _SortedList.Add("@gender", model.Gender);
                _SortedList.Add("@bckground", model.Background);
                _SortedList.Add("@drug", model.DrugTested);
                _SortedList.Add("@email", model.EmailAddress);
                _SortedList.Add("@phone", model.PhoneNumber);
                _SortedList.Add("@alternativephone", model.AlertnativePhoneNumber);
                _SortedList.Add("@stateid", model.StateId);
                _SortedList.Add("@MapValidateAddress", model.MapValidateAddress);
                _SortedList.Add("@cityid", model.CityId);
                _SortedList.Add("@zip", model.Zipcode);
                _SortedList.Add("@address", model.Address);
                _SortedList.Add("@createdby", _createdby);
                _SortedList.Add("@company", model.CompanyName);
                _SortedList.Add("@dd", model.DrivingDistance);
                _SortedList.Add("@workspaceid", _workspaceid);
                _SortedList.Add("@suite", model.SuiteNumber);
                if (model.Skills != null)
                {
                    _SortedList.Add("@skills", string.Join<int>(",", model.Skills));
                }
                UserId = Convert.ToInt32(_SqlHelper.executeNonQueryWMessage("tbl_member_allnew", string.Empty, _SortedList).ToString()); _SqlHelper.Dispose();
            }
            catch (Exception)
            {
                _SqlHelper.Dispose();
            }
            return UserId;
        }

        /// <summary>
        /// Upload new document in user profile
        /// </summary>
        /// <param name="_filetitle">File name</param>
        /// <param name="_createdby">Loggedin user id</param>
        /// <param name="_fileurl">File saved path</param>
        /// <param name="_documenttypeId">Document type id</param>
        /// <returns>Result after process upload</returns>
        public string AddUserDocument(string _filetitle, int _createdby, string _fileurl, int _documenttypeId = 0)
        {
            string Message = "Document added successfully";
            SqlHelper _SqlHelper = new SqlHelper();
            try
            {
                SortedList _SortedList = new SortedList
                {
                    { "@FileTitle", _filetitle },
                    { "@ispopupform", false },
                    { "@url", _fileurl },
                    { "@createdby", _createdby },
                    { "@_dctype", _documenttypeId }
                };
                _SqlHelper.executeNonQuery("tbl_member_doc_add_v2", string.Empty, _SortedList); _SqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                _SqlHelper.Dispose();
                Message = "Error:" + exception.Message;
            }
            return Message;
        }

        /// <summary>
        /// Add new entry in account table with stripe details
        /// </summary>
        /// <param name="_workspaceid">Workspace id</param>
        /// <param name="_planType">Free,Starter,Advantage,Professional</param>
        /// <param name="_numberofsms">Number of sms provided in plan</param>
        /// <param name="_numberofemails">Number of emails provided in plan</param>
        /// <param name="_accountname">Company name</param>
        /// <param name="_accountemail">Company email</param>
        /// <param name="_accountaddress">Company address</param>
        /// <param name="_accountphone">Company phone number</param>
        /// <param name="_primaryContact">Primary contact person phone number</param>
        /// <param name="_stripesubscriptionid">Stripe subscription id ,0 if free account</param>
        /// <param name="_stripecustomerid">customer id returned by stripe after paymwnt, 0 if account is free</param>
        /// <param name="_planid">Select plan id</param> 
        public void CreateNewSubscriptionAccount(int _workspaceid, string _planType, int _numberofsms, int _numberofemails, string _accountname, string _accountemail, string _accountaddress, string _accountphone, string _primaryContact, string _stripesubscriptionid, string _stripecustomerid, int _planid)
        {
            SqlHelper _SqlHelper = new SqlHelper();
            try
            {
                SortedList _SortedList = new SortedList
                {
                    { "@ServiceName", _planType },
                    { "@SmsCount", _numberofsms },
                    { "@EmailCount", _numberofemails },
                    { "@Name", _accountname },
                    { "@Email", _accountemail },
                    { "@Address", _accountaddress },
                    { "@phonenumber", _accountphone },
                    { "@Primarycontact", _primaryContact },
                    { "@Workspaceid", _workspaceid },
                    { "@planId", _planid },
                    { "@customerid", _stripecustomerid },
                    { "@subscriptionId", _stripesubscriptionid }
                };
                _SqlHelper.executeNonQuery("AddNewAccount", string.Empty, _SortedList); _SqlHelper.Dispose();
            }
            catch (Exception)
            {
                _SqlHelper.Dispose();
            }
        }

        /// <summary>
        /// Delete User profile document
        /// </summary>
        /// <param name="_documentid"></param>
        /// <returns></returns>
        public string DeleteUserDocumentById(int _documentid)
        {
            string Message = "Document deleted successfully";
            SqlHelper _SqlHelper = new SqlHelper();
            try
            {
                SortedList _SortedList = new SortedList
                {
                    { "@docid", _documentid }
                };
                _SqlHelper.executeNonQuery("tbl_member_doc_delete", string.Empty, _SortedList); _SqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                _SqlHelper.Dispose();
                Message = "Error:" + exception.Message;
            }
            return Message;
        }

        /// <summary>
        /// Get Technician rate by userid
        /// </summary>
        /// <param name="_userid"></param>
        /// <returns></returns>
        public string GetUserRateById(int _userid)
        {
            string Rate;
            SqlHelper _SqlHelper = new SqlHelper();
            try
            {
                SortedList _SortedList = new SortedList
                {
                    { "@id", _userid }
                };
                Rate = _SqlHelper.executeNonQueryWMessage("tbl_member_getrate", "", _SortedList).ToString().Currency(); _SqlHelper.Dispose();
            }
            catch (Exception)
            {
                _SqlHelper.Dispose();
                Rate = "$0.00";
            }
            return Rate;
        }



        /// <summary>
        /// Delete User Bank from profile
        /// </summary>
        /// <param name="_bankid"></param>
        /// <returns></returns>
        public string DeleteUserBank(int _bankid)
        {
            string Message = "Bank details deleted successfully";
            SqlHelper _SqlHelper = new SqlHelper();
            try
            {
                SortedList _SortedList = new SortedList
                {
                    { "@bankid", _bankid }
                };
                _SqlHelper.executeNonQuery("tbl_member_bank_delete", string.Empty, _SortedList); _SqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                _SqlHelper.Dispose();
                Message = "Error:" + exception.Message;
            }
            return Message;
        }

        /// <summary>
        /// Upload user profile image
        /// </summary>
        /// <param name="_imageurl"></param>
        /// <param name="_userId"></param>
        /// <returns></returns>
        public string AddUserProfileImage(string _imageurl, int _userId)
        {
            string Message = "Profile image updated successfully";
            SqlHelper _SqlHelper = new SqlHelper();
            try
            {
                SortedList _SortedList = new SortedList
                {
                    { "@image", _imageurl },
                    { "@id", _userId }
                };
                _SqlHelper.executeNonQueryWMessage("tbl_member_allimageupdate", string.Empty, _SortedList).ToString(); _SqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                _SqlHelper.Dispose();
                Message = "Error:" + exception.Message;
            }
            return Message;
        }
        /// <summary>
        /// Delete user parmanently
        /// </summary>
        /// <param name="_userid"></param>
        /// <returns></returns>
        public string DeleteUser(int _userid)
        {
            string Message = "User Deleted Successfully";
            SqlHelper _SqlHelper = new SqlHelper();
            try
            {
                SortedList _SortedList = new SortedList
                {
                    { "@id", _userid }
                };
                _SqlHelper.executeNonQueryWMessage("tbl_member_remove", string.Empty, _SortedList).ToString(); _SqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                _SqlHelper.Dispose();
                Message = "Error:" + exception.Message;
            }
            return Message;
        }

        /// <summary>
        /// Delete user temporary
        /// </summary>
        /// <param name="_userid"></param>
        /// <returns></returns>
        public string InactiveUser(int _userid)
        {
            string Message = "User status Inactive Successfully";
            SqlHelper _SqlHelper = new SqlHelper();
            try
            {
                SortedList _SortedList = new SortedList
                {
                    { "@id", _userid }
                };
                _SqlHelper.executeNonQueryWMessage("tbl_member_remove_status", string.Empty, _SortedList).ToString(); _SqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                _SqlHelper.Dispose();
                Message = "Error:" + exception.Message;
            }
            return Message;
        }
        /// <summary>
        /// Update user profile
        /// </summary>
        /// <param name="model"></param>
        /// <param name="_updatedby"></param>
        /// <returns></returns>
        public string UpdateUserProfile(UserModel model, int _updatedby, int _workspaceid)
        {
            string message = "User Updated Successfully";
            SqlHelper _SqlHelper = new SqlHelper();
            Crypto _Crypto = new Crypto();
            try
            {
                model.RoleId = model.RoleId != 0 ? model.RoleId : GetRoleIdByPositionType(_workspaceid, model.UserType);
                SortedList _SortedList = new SortedList
                {
                    { "@fname", model.FirstName },
                    { "@lname", model.LastName },
                    { "@checkcompany", false },
                    { "@memberid", model.UserId },
                    { "@type", model.UserType },
                    { "@username", model.EmailAddress },
                    { "@password", _Crypto.EncryptStringAES(model.Password) },
                    { "@hourlyrate", model.HourlyRate },
                    { "@tax", model.Tax },
                    { "@statusid", model.StatusId },
                    { "@userid", model.UserTableId },
                    { "@deptid", 0 },
                    { "@desgntid", 0 },
                    { "@roleid", model.RoleId },
                    { "@managerid", model.ManagerId },
                    { "@dob", "" },
                    { "@gender", "M" },
                    { "@bckground", model.Background },
                    { "@drug", model.DrugTested },
                    { "@email", model.EmailAddress },
                    { "@phone", model.PhoneNumber },
                    { "@alternativephone", model.AlertnativePhoneNumber },
                    { "@stateid", model.StateId },
                    { "@suite", model.SuiteNumber },
                    { "@cityid", model.CityId },
                    { "@zip", model.Zipcode },
                    { "@address", model.Address },
                    { "@MapValidateAddress", model.MapValidateAddress },
                    { "@company", model.CompanyName },
                    { "@dd", model.DrivingDistance },
                    { "@createdby", _updatedby }
                };
                _SqlHelper.executeNonQueryWMessage("UpdateUserProfile", string.Empty, _SortedList).ToString(); _SqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                _SqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }

        /// <summary>
        /// Update Ownn profile after login
        /// </summary>
        /// <param name="model"></param>
        /// <param name="_usertableid"> Identity column id of user table</param>
        /// <returns></returns>
        public string UpdateProfile(UserModel model, int _usertableid)
        {
            string Message = "Profile Updated Successfully";
            SqlHelper _SqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@fname", model.FirstName },
                    { "@lname", model.LastName },
                    { "@hourlyrate", model.HourlyRate },
                    { "@tax", model.Tax },
                    { "@userid", _usertableid },
                    { "@dob", DateTime.Now },
                    { "@gender", model.Gender },
                    { "@email", model.EmailAddress },
                    { "@phone", model.PhoneNumber },
                    { "@alternativephone", model.AlertnativePhoneNumber },
                    { "@stateid", model.StateId },
                    { "@cityid", model.CityId },
                    { "@zip", model.Zipcode },
                    { "@address", model.Address },
                    { "@createdby", _usertableid },
                    { "@company", model.CompanyName },
                    { "@MapValidateAddress", model.MapValidateAddress },
                    { "@suite", model.SuiteNumber },
                       { "@background", model.Background },
                          { "@Drugtest", model.DrugTested },
                    { "@drivingdistance", model.DrivingDistance }
                };
                if (model.Skills != null)
                {
                    _srt.Add("@skills", string.Join<int>(",", model.Skills));
                }
                _SqlHelper.executeNonQueryWMessage("tbl_member_allupdate_byuser", string.Empty, _srt).ToString(); _SqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                _SqlHelper.Dispose();
                Message = "Error:" + exception.Message;
            }
            return Message;
        }

        /// <summary>
        /// Assign a existing technician under a company
        /// </summary>
        /// <param name="_userid"></param>
        /// <param name="_usertype"></param>
        /// <param name="_firstname"></param>
        /// <param name="_companyid"></param>
        /// <param name="_lastname"></param>
        /// <param name="_email"></param>
        /// <param name="_phonenumber"></param>
        /// <param name="_address"></param>
        /// <param name="_zipcode"></param>
        /// <param name="_position"></param>
        /// <param name="_managerid"></param>
        /// <param name="_createdby"></param>
        /// <param name="_cityid"></param>
        /// <param name="_stateid"></param>
        /// <param name="_roleid"></param>
        /// <returns></returns>
        public string AssignTechnicianToCompany(int _userid, string _usertype, string _firstname, int _companyid, string _lastname, string _email, string _phonenumber, string _address, string _zipcode, string _position, int _managerid, int _createdby, int _cityid, int _stateid, int _roleid = 0, string MapvalidateAddress = "", string suitenumber = "")
        {
            string Message = "User details updated successfully";
            SqlHelper _SqlHelper = new SqlHelper();
            try
            {
                SortedList _SortedList = new SortedList
                {
                    { "@member_id", _userid },
                    { "@MapvalidateAddress", MapvalidateAddress },
                    { "@membertype", _usertype },
                    { "@fname", _firstname },
                    { "@clientid", _companyid },
                    { "@lname", _lastname },
                    { "@email", _email },
                    { "@phone", _phonenumber },
                    { "@createdby", _createdby },
                    { "@address", _address },
                    { "@zip", _zipcode },
                    { "@Position", _position },
                    { "@manager_id", _managerid },
                    { "@city", _cityid },
                    { "@state", _stateid },
                    { "@suite", suitenumber }
                };
                if (_roleid != 0)
                {
                    _SortedList.Add("@roleid", _roleid);
                }
                _SqlHelper.executeNonQueryWMessage("tbl_member_allupdate_bycompany", string.Empty, _SortedList).ToString(); _SqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                _SqlHelper.Dispose();
                Message = "Error:" + exception.Message;
            }
            return Message;
        }


        /// <summary>
        /// Update skills in profile section of user
        /// </summary>
        /// <param name="_skillsidarrry"></param>
        /// <param name="_userid"></param>
        /// <returns></returns>
        public string UpdateUserSkills(int[] _skillsidarrry, int _userid)
        {
            string Message = "Skills updated successfully";
            SqlHelper _SqlHelper = new SqlHelper();
            try
            {
                SortedList _SortedList = new SortedList
                {
                    { "@userid", _userid },
                    { "@skills", string.Join<int>(",", _skillsidarrry) }
                };
                _SqlHelper.executeNonQueryWMessage("tbl_member_allupdateskills_byuser_v2", string.Empty, _SortedList).ToString(); _SqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                _SqlHelper.Dispose();
                Message = "Error:" + exception.Message;
            }
            return Message;
        }

        /// <summary>
        /// Update client list in profile section of a technician , whom jobs a technician can see
        /// </summary>
        /// <param name="_clientsidarray"></param>
        /// <param name="_userid"></param>
        /// <returns></returns>
        public string UpdateAllowedClientsList(int[] _clientsidarray, int _userid)
        {
            string Message = "clients updated successfully";
            SqlHelper _SqlHelper = new SqlHelper();
            try
            {
                SortedList _SortedList = new SortedList
                {
                    { "@userid", _userid },
                    { "@clients", string.Join<int>(",", _clientsidarray) }
                };
                _SqlHelper.executeNonQueryWMessage("tbl_user_allupdateclients_byuser_v2", string.Empty, _SortedList).ToString(); _SqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                _SqlHelper.Dispose();
                Message = "Error:" + exception.Message;
            }
            return Message;
        }

        /// <summary>
        /// update tool list in technician profile section
        /// </summary>
        /// <param name="_toolsidarray"></param>
        /// <param name="_userid"></param>
        /// <returns></returns>
        public string UpdateTools(int[] _toolsidarray, int _userid)
        {
            string Message = "Tools updated successfully";
            SqlHelper _SqlHelper = new SqlHelper();
            try
            {
                SortedList _SortedList = new SortedList
                {
                    { "@userid", _userid },
                    { "@skills", string.Join<int>(",", _toolsidarray) }
                };
                _SqlHelper.executeNonQueryWMessage("tbl_member_allupdatetools_byuser_v2", string.Empty, _SortedList).ToString(); _SqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                _SqlHelper.Dispose();
                Message = "Error:" + exception.Message;
            }
            return Message;
        }
        /// <summary>
        /// Get userid list with rate range
        /// </summary>
        /// <param name="_raterangeid"></param>
        /// <param name="_workspaceid"></param>
        /// <returns></returns>
        public List<int> GetUserIdsByRateRange(int _raterangeid, int _workspaceid)
        {
            List<int> UserIds = new List<int>();
            SqlHelper _SqlHelper = new SqlHelper();
            SortedList _SortedList = new SortedList();
            string MinAmount = "0"; string MaxAmount = "999";
            try
            {
                switch (_raterangeid)
                {
                    case 1:
                        MinAmount = "0";
                        MaxAmount = "40";
                        break;
                    case 2:
                        MinAmount = "41";
                        MaxAmount = "60";
                        break;
                    case 3:
                        MinAmount = "61";
                        MaxAmount = "80";
                        break;
                    case 4:
                        MinAmount = "81";
                        MaxAmount = "100";
                        break;
                    case 5:
                        MinAmount = "101";
                        MaxAmount = "999";
                        break;
                }
                _SortedList.Add("@min", MinAmount);
                _SortedList.Add("@max", MaxAmount);
                if (_workspaceid != 0)
                {
                    _SortedList.Add("@workspaceid", _workspaceid);
                }
                var dt = _SqlHelper.fillDataTable("tbl_member_getallmembersbyrate", string.Empty, _SortedList); _SqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    UserIds.Add(Convert.ToInt32(dt.Rows[i]["Id"].ToString()));
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                _SqlHelper.Dispose();
            }
            return UserIds;
        }


        //public Users get_all_active_members(string delimater = ",", int state_id = 0, int[] city_id = null, int[] amount = null, string background = "", string drug = "", int status = 0, int[] skills = null)
        //{
        //    SqlHelper sqlHelper = new SqlHelper();
        //    Users _mdl = new Users();
        //    List<member_item> _model = new List<member_item>();
        //    List<int> id = new List<int>();
        //    //getMembersByratePayid
        //    if (amount == null)
        //    {
        //        // id = getMembersByratePayid(0);
        //    }
        //    else
        //    {
        //        foreach (var item in amount)
        //        {
        //            // id.AddRange(getMembersByratePayid(item));
        //        }
        //    }
        //    try
        //    {
        //        SortedList _srt = new SortedList();
        //        if (state_id != 0)
        //        {
        //            _srt.Add("@state", state_id);
        //        }
        //        if (status != 0)
        //        {
        //            _srt.Add("@status", status);
        //        }
        //        if (!string.IsNullOrEmpty(background) && background != "all")
        //        {
        //            _srt.Add("@background", background);
        //        }
        //        if (!string.IsNullOrEmpty(drug) && drug != "all")
        //        {
        //            _srt.Add("@drug", drug);
        //        }
        //        if (city_id != null)
        //        {
        //            var city = string.Join<int>(",", city_id);
        //            _srt.Add("@city", city);
        //        }
        //        if (skills != null)
        //        {
        //            var skill = string.Join<int>(",", skills);
        //            _srt.Add("@skills", skill);
        //        }
        //        var dt = sqlHelper.fillDataTable("tbl_member_getallmembers", "", _srt);
        //        for (int i = 0; i < dt.Rows.Count; i++)
        //        {
        //            member_item _item = new member_item();
        //            _item.memder_p_id = Convert.ToInt32(dt.Rows[i]["Id"].ToString());
        //            _item.memberid = dt.Rows[i]["member_id"].ToString();
        //            _item.fname = dt.Rows[i]["f_name"].ToString().ToTitleCase();
        //            _item.lname = dt.Rows[i]["l_name"].ToString().ToTitleCase();
        //            _item.city = dt.Rows[i]["cityname"].ToString();
        //            _item.state = dt.Rows[i]["statename"].ToString();
        //            _item.phone = dt.Rows[i]["phone"].ToString().PhoneNumber();
        //            _item.email = dt.Rows[i]["EMail"].ToString();
        //            _item.hourlyrate = dt.Rows[i]["rate"].ToString().Currency();
        //            _item.background = dt.Rows[i]["Background"].ToString();
        //            _item.drug = dt.Rows[i]["drug_tested"].ToString();
        //            _item.role = dt.Rows[i]["rolename"].ToString();
        //            _item.staus = dt.Rows[i]["name"].ToString();
        //            _item.skills = "n/a";
        //            var items = GetUserSkillList(_item.memder_p_id);
        //            if (items.Count > 0)
        //            {
        //                _item.skills = string.Join(delimater, items.Select(k => k.name).ToArray());
        //            }
        //            if (id.Contains(_item.memder_p_id))
        //            {
        //                _model.Add(_item);
        //            }
        //        }
        //        dt.Dispose();
        //    }
        //    catch (Exception)
        //    {
        //        //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
        //        _mdl.error_text = "Error:" + exception.Message;
        //    }
        //    finally
        //    {
        //        sqlHelper.Dispose();

        //    }
        //    _mdl._members = _model;
        //    return _mdl;
        //}

        /// <summary>
        /// Get job clientid,Stateid and city id
        /// </summary>
        /// <param name="_jobid"></param>
        /// <returns></returns>
        public PopUpModel GetJobClientWithCityState(int _jobid)
        {
            PopUpModel model = new PopUpModel();
            SqlHelper _SqlHelper = new SqlHelper();
            SortedList _SortedList = new SortedList
            {
                { "@jobid", _jobid }
            };
            try
            {
                var dt = _SqlHelper.fillDataTable("getjobdetailbyid", string.Empty, _SortedList); _SqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    model.CityId = Convert.ToInt32(dt.Rows[i]["city_id"].ToString());
                    model.StateId = Convert.ToInt32(dt.Rows[i]["State_id"].ToString());
                    model.ClientId = Convert.ToInt32(dt.Rows[i]["client"].ToString());
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                _SqlHelper.Dispose();
            }
            return model;
        }


        /// <summary>
        /// Get offer clientid,Stateid and city id
        /// </summary>
        /// <param name="_offerid"></param>
        /// <returns></returns>
        public PopUpModel GetOfferClientWithCityState(int _offerid)
        {
            PopUpModel model = new PopUpModel();
            SqlHelper _SqlHelper = new SqlHelper();
            SortedList _SortedList = new SortedList
            {
                { "@jobid", _offerid }
            };
            try
            {
                var dt = _SqlHelper.fillDataTable("getofferdetailbyid", string.Empty, _SortedList); _SqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    model.CityId = Convert.ToInt32(dt.Rows[i]["city_id"].ToString());
                    model.StateId = Convert.ToInt32(dt.Rows[i]["State_id"].ToString());
                    model.ClientId = Convert.ToInt32(dt.Rows[i]["client"].ToString());
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                _SqlHelper.Dispose();
            }
            return model;
        }

        public List<UserItem> GetAllActiveUsersByIndex(ref int total, int startIndex, int endIndex, string search = "", string delimater = ",", string Location = "", int[] amount = null, string background = "", string drug = "", int status = 0, int[] skills = null, bool phone = true, bool email = true, bool rate = true, string type = "S", int userid = 0, string userType = "M", int workspaceid = 0,int contarctor=0,int Role=0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<UserItem> _model = new List<UserItem>();
            List<int> id = new List<int>();
            Crypto _crypt = new Crypto();
            bool icontactor = type == "S" || type == "LT";
            bool checkId = false;
            if (amount.Where(a => a != 0).Count() == 0)
            {
                checkId = false;
            }
            else
            {
                checkId = true;
                foreach (var item in amount.Where(a => a != 0))
                {
                    id.AddRange(GetUserIdsByRateRange(item, workspaceid));
                }
            }
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@startIndex", startIndex },
                    { "@endIndex", endIndex }
                };
                if (!string.IsNullOrEmpty(search))
                {
                    _srt.Add("@search", search);
                }
                if (!string.IsNullOrEmpty(Location))
                {
                    _srt.Add("@Location", Location);
                }
                _srt.Add("@workspaceid", workspaceid);
                if (!string.IsNullOrEmpty(userType) && userType != "M")
                {
                    _srt.Add("@membertype", userType);
                }
                if (type == "S" || type == "LT")
                {
                    _srt.Add("@companyid", userid);
                }
                string sp = "GetAllActiveUsersByIndex";// "tbl_member_getallmembers_byindexwotstatus";
                if (status != 0)
                {
                    //sp = "tbl_member_getallmembers_byindex";
                    _srt.Add("@status", status);
                }
                if (!string.IsNullOrEmpty(background) && background != "all")
                {
                    _srt.Add("@background", background);
                }
                if (!string.IsNullOrEmpty(drug) && drug != "all")
                {
                    _srt.Add("@drug", drug);
                }
                if (Role!=0)
                {
                    _srt.Add("@Role", Role);
                }  
                if (skills != null)
                {
                    var skill = string.Join<int>(",", skills.Where(a => a != 0));
                    if (!string.IsNullOrEmpty(skill))
                    {
                        _srt.Add("@skills", skill);
                    }
                }
                if(contarctor!=0)
                {
                    _srt.Add("@contarctor", contarctor);
                }
                var dt = sqlHelper.fillDataTable(sp, string.Empty, _srt); sqlHelper.Dispose();
                int _all = 0;
                string CityName = "";
                string StateName = "";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    UserItem _item = new UserItem();
                    GetCityStateName(Convert.ToInt32(dt.Rows[i]["cityname"].ToString()), Convert.ToInt32(dt.Rows[i]["statename"].ToString()), ref CityName, ref StateName);
                    _item.UserTableId = Convert.ToInt32(dt.Rows[i]["Id"].ToString());
                    _item.UserId = dt.Rows[i]["member_id"].ToString();
                    _item.FirstName = dt.Rows[i]["f_name"].ToString().ToTitleCase();
                    _item.LastName = dt.Rows[i]["l_name"].ToString().ToTitleCase();
                    _item.CityName = CityName;
                    _item.StateName = StateName;
                    _item.PhoneNumber = !phone ? "xxx-xxx-xxxx" : dt.Rows[i]["phone"].ToString().PhoneNumber();
                    _item.EmailAddress = !email ? "xxx" : dt.Rows[i]["EMail"].ToString();
                    _item.HourlyRate = !rate ? "xxx" : dt.Rows[i]["rate"].ToString().Currency();
                    _item.StatusName = Convert.ToInt32(dt.Rows[i]["member_status_id"].ToString()).GetUserStatusById();
                    _item.StatusName = _item.StatusName != "Inactive" ? "Active" : "Inactive";
                    _item.Skills = "--";
                    _item.StatusId = Convert.ToInt32(dt.Rows[i]["member_status_id"].ToString());
                    total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _item.Token = _crypt.EncryptStringAES(dt.Rows[i]["Id"].ToString());
                    if (checkId == true)
                    {
                        if (id.Contains(_item.UserTableId))
                        {
                            _model.Add(_item);
                        }
                        else
                        {
                            _all += 1;
                        }
                    }
                    else
                    {
                        _model.Add(_item);
                    }   
                }
                total -= _all;
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _model;
        }

        public List<UserItem> GetAllAccountUsersByIndex(ref int total, int startIndex, int endIndex, string search = "", string delimater = ",", string Location = "", int[] amount = null, string background = "", string drug = "", int status = 0, int[] skills = null, bool phone = true, bool email = true, bool rate = true, string type = "S", int userid = 0, string userType = "M", int workspaceid = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<UserItem> _model = new List<UserItem>();
            //List<int> id =   GetUserIdsByRateRange(0, workspaceid); 
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@startIndex", startIndex },
                    { "@endIndex", endIndex }
                }; 
                _srt.Add("@workspaceid", workspaceid);
                
                string sp = "GetAllActiveUsersByIndex";// "tbl_member_getallmembers_byindexwotstatus";
                 
                var dt = sqlHelper.fillDataTable(sp, string.Empty, _srt); sqlHelper.Dispose();
                //int _all = 0;
                string CityName = "";
                string StateName = "";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    UserItem _item = new UserItem();
                    _item.UserTableId = Convert.ToInt32(dt.Rows[i]["Id"].ToString());
                    GetCityStateName(Convert.ToInt32(dt.Rows[i]["cityname"].ToString()), Convert.ToInt32(dt.Rows[i]["statename"].ToString()), ref CityName, ref StateName);
                    _item.UserId = dt.Rows[i]["member_id"].ToString();
                    _item.FirstName = dt.Rows[i]["f_name"].ToString().ToTitleCase()+" "+ dt.Rows[i]["l_name"].ToString().ToTitleCase();
                     _item.CityName = CityName+", "+ StateName; 
                    _item.PhoneNumber =   dt.Rows[i]["phone"].ToString().PhoneNumber();
                    _item.EmailAddress =   dt.Rows[i]["EMail"].ToString();
                    _item.HourlyRate =  dt.Rows[i]["rate"].ToString().Currency();
                    _item.StatusName = Convert.ToInt32(dt.Rows[i]["member_status_id"].ToString()).GetUserStatusById();
                    _item.StatusName = _item.StatusName != "Inactive" ? "Active" : "Inactive"; 
                    total = Convert.ToInt32(dt.Rows[i]["total"].ToString()); 
                    //if (id.Contains(_item.UserTableId))
                    //{
                        _model.Add(_item);
                    //}
                    //else
                    //{
                    //    _all += 1;
                    //}
                }
                //total -= _all;
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _model;
        }


        public List<Accounts> GetAllActiveAccountsByIndex(ref int total, int startIndex, int endIndex, string search = "", string Location = "", int status = 0, int workspaceid = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<Accounts> _model = new List<Accounts>();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@startIndex", startIndex },
                    { "@endIndex", endIndex }
                };
                if (!string.IsNullOrEmpty(search))
                {
                    _srt.Add("@search", search);
                }
                if (!string.IsNullOrEmpty(Location))
                {
                    _srt.Add("@Location", Location);
                }
                if (workspaceid != 0)
                {
                    _srt.Add("@workspaceid", workspaceid);
                }
                string sp = "GetAllAccountByIndex";// 
                if (status != 0)
                {
                    _srt.Add("@status", status);
                }
                var dt = sqlHelper.fillDataTable(sp, string.Empty, _srt);
                sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Accounts _item = new Accounts
                    {
                        AccountId = Convert.ToInt32(dt.Rows[i]["Id"].ToString()),
                        AccountNumber = dt.Rows[i]["AccountNumber"].ToString(),
                        City = dt.Rows[i]["City"].ToString(),
                        CompanyName = dt.Rows[i]["Name"].ToString(),
                        Email = dt.Rows[i]["Email"].ToString(),
                        PhoneNumber = dt.Rows[i]["phone"].ToString(),
                        PrimaryContact = dt.Rows[i]["Contact"].ToString(),
                        State = dt.Rows[i]["State"].ToString(),
                        Status = dt.Rows[i]["StatusName"].ToString(),
                        StatusId = Convert.ToInt32(dt.Rows[i]["Status"].ToString()),
                        WorkspaceId = Convert.ToInt32(dt.Rows[i]["Workspaceid"].ToString()),
                        TotalUser = Convert.ToInt32(dt.Rows[i]["TotalUser"].ToString())
                    };
                    total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _model;
        }

        public Dictionary<int, Tuple<string, double>> GetUsersByTechDiscoveryFilter(int[] amount = null, string background = "", string drug = "", int[] skills = null, int[] tools = null, int workspaceid = 0, double distance = 50.0, int _offerid = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            Dictionary<int, Tuple<string, double>> _model = new Dictionary<int, Tuple<string, double>>();
            List<int> id = new List<int>();
            Crypto _crypt = new Crypto();
            if (amount==null|| amount.Where(a => a != 0).Count() == 0)
            {
                id = GetUserIdsByRateRange(0, workspaceid);
            }
            else
            {
                foreach (var item in amount.Where(a => a != 0))
                {
                    id.AddRange(GetUserIdsByRateRange(item, workspaceid));
                }
            }
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@workspaceid", workspaceid);
                _srt.Add("@offerid", _offerid);
                string sp = "GetAllActiveUsersForTechDiscovery";// "tbl_member_getallmembers_byindexwotstatus"; 
                if (!string.IsNullOrEmpty(background) && background != "all")
                {
                    _srt.Add("@background", background);
                }
                if (!string.IsNullOrEmpty(drug) && drug != "all")
                {
                    _srt.Add("@drug", drug);
                }
                if (skills != null)
                {
                    var skill = string.Join<int>(",", skills.Where(a => a != 0));
                    if (!string.IsNullOrEmpty(skill))
                    {
                        _srt.Add("@skills", skill);
                    }
                }
                if (tools != null)
                {
                    var tool = string.Join<int>(",", tools.Where(a => a != 0));
                    if (!string.IsNullOrEmpty(tool))
                    {
                        _srt.Add("@tools", tool);
                    }
                }
                var dt = sqlHelper.fillDataTable(sp, string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (id.Contains(Convert.ToInt32(dt.Rows[i][0].ToString())))
                    {
                        var _dict = new Tuple<string, double>(dt.Rows[i][1].ToString(), Convert.ToDouble(dt.Rows[i][2].ToString()));
                        _model.Add(Convert.ToInt32(dt.Rows[i][0].ToString()), _dict);
                    }
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _model;
        }


        public UserModel GetAllActiveMembersById(int id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            UserModel _item = new UserModel
            {
                UserTableId = id
            };
            SortedList _srt = new SortedList();
            Crypto _crypt = new Crypto();
            List<int> _skills = new List<int>();
            List<int> _skills2 = new List<int>();
            List<int> client_tech = new List<int>();
            _srt.Add("@id", id);
            try
            {
                var ds = sqlHelper.fillDataSet("tbl_member_getmemberbyid", string.Empty, _srt); sqlHelper.Dispose();
                var dt = ds.Tables[0];
                var dt1 = ds.Tables[1];
                var dt2 = ds.Tables[2];
                var dt3 = ds.Tables[3];
                if (dt.Rows.Count > 0)
                {
                    _item.IsContracter = Convert.ToBoolean(dt.Rows[0]["havecompany"].ToString());
                    _item.AccountType = dt.Rows[0]["accounttype"].ToString();
                    _item.Tax = dt.Rows[0]["tax"].ToString().Currency();
                    _item.Token = _crypt.EncryptStringAES(id.ToString());
                    _item.UserType = dt.Rows[0]["type"].ToString();
                    _item.CompanyId = Convert.ToInt32(dt.Rows[0]["companyid"].ToString());
                    _item.StatusId = Convert.ToInt32(dt.Rows[0]["member_status_id"].ToString());
                    _item.RoleId = Convert.ToInt32(dt.Rows[0]["RoleId"].ToString());
                    _item.ManagerId = Convert.ToInt32(dt.Rows[0]["ManagerId"].ToString());
                    _item.CityId = Convert.ToInt32(dt.Rows[0]["city_id"].ToString());
                    _item.CityName = dt.Rows[0]["city"].ToString();
                    _item.SuiteNumber = dt.Rows[0]["SuiteNumber"].ToString();
                    _item.StateId = Convert.ToInt32(dt.Rows[0]["state_id"].ToString());
                    _item.Zipcode = dt.Rows[0]["Zip_code"].ToString();
                    _item.Address = dt.Rows[0]["address"].ToString();
                    _item.MapValidateAddress = dt.Rows[0]["MapValidateAddress"].ToString();
                    _item.Gender = dt.Rows[0]["Gender"].ToString();
                    _item.ImageUrl = dt.Rows[0]["Imageurl"].ToString();
                    _item.Background = dt.Rows[0]["Background"].ToString();
                    _item.DrugTested = dt.Rows[0]["drug_tested"].ToString();
                    _item.EmailAddress = dt.Rows[0]["EMail"].ToString();
                    _item.PhoneNumber = dt.Rows[0]["Phone"].ToString().PhoneNumber();
                    _item.AlertnativePhoneNumber = dt.Rows[0]["Alternative_phone"].ToString().PhoneNumber();
                    _item.HourlyRate = dt.Rows[0]["hrlyrate"].ToString().Currency();
                    _item.UserId = dt.Rows[0]["member_id"].ToString();
                    _item.FirstName = dt.Rows[0]["f_name"].ToString().ToTitleCase();
                    _item.LastName = dt.Rows[0]["l_name"].ToString().ToTitleCase();
                    _item.UserName = dt.Rows[0]["Username"].ToString();
                    _item.Password = _crypt.DecryptStringAES(dt.Rows[0]["Password"].ToString());
                    _item.ConfirmPassword = _crypt.DecryptStringAES(dt.Rows[0]["Password"].ToString());
                    _item.CompanyName = dt.Rows[0]["company"].ToString();
                    _item.DrivingDistance = Convert.ToInt32(dt.Rows[0]["dd"].ToString());
                    if (dt1.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt1.Rows.Count; i++)
                        {
                            _skills.Add(Convert.ToInt32(dt1.Rows[i][0].ToString()));
                        }
                        _item.Skills = _skills.ToArray();
                    }

                    if (dt2.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt2.Rows.Count; i++)
                        {
                            _skills2.Add(Convert.ToInt32(dt2.Rows[i][0].ToString()));
                        }
                        _item.Tools = _skills2.ToArray();
                    }
                    if (dt3.Rows.Count > 0)
                    {
                        for (int i = 0; i < dt3.Rows.Count; i++)
                        {
                            client_tech.Add(Convert.ToInt32(dt3.Rows[i][0].ToString()));
                        }
                        _item.Clients = client_tech.ToArray();
                    }
                }
                dt.Dispose();
                dt1.Dispose();
                dt2.Dispose();
                dt3.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _item.error_text = "Error:" + exception.Message;
            }
            return _item;
        }
        public UserModel GetUserProfileById(int id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            UserModel _item = new UserModel
            {
                UserTableId = id
            };
            SortedList _srt = new SortedList();
            Crypto _crypt = new Crypto();
            //List<int> _skills = new List<int>();
            //List<int> _skills2 = new List<int>();
            //List<int> client_tech = new List<int>();
            _srt.Add("@id", id);
            try
            {
                var ds = sqlHelper.fillDataSet("GetUserProfileById", "", _srt); sqlHelper.Dispose();
                var dt = ds.Tables[0];
                //var dt1 = ds.Tables[1];
                //var dt2 = ds.Tables[2];
                //var dt3 = ds.Tables[3]; 
                ds.Dispose();
                if (dt.Rows.Count > 0)
                {
                    _item.IsContracter = Convert.ToBoolean(dt.Rows[0]["havecompany"].ToString());
                    _item.AccountType = dt.Rows[0]["accounttype"].ToString();
                    _item.Token = _crypt.EncryptStringAES(id.ToString());
                    _item.UserType = dt.Rows[0]["type"].ToString();
                    _item.CompanyId = Convert.ToInt32(dt.Rows[0]["companyid"].ToString());
                    _item.StatusId = Convert.ToInt32(dt.Rows[0]["member_status_id"].ToString());
                    _item.RoleId = Convert.ToInt32(dt.Rows[0]["RoleId"].ToString());
                    _item.ManagerId = Convert.ToInt32(dt.Rows[0]["ManagerId"].ToString());
                    _item.SuiteNumber = dt.Rows[0]["SuiteNumber"].ToString();
                    _item.MapValidateAddress = dt.Rows[0]["address"].ToString();
                    _item.ImageUrl = dt.Rows[0]["Imageurl"].ToString();
                    _item.Background = dt.Rows[0]["Background"].ToString();
                    _item.DrugTested = dt.Rows[0]["drug_tested"].ToString();
                    _item.EmailAddress = dt.Rows[0]["EMail"].ToString();
                    _item.PhoneNumber = dt.Rows[0]["Phone"].ToString().PhoneNumber();
                    _item.AlertnativePhoneNumber = dt.Rows[0]["Alternative_phone"].ToString().PhoneNumber();
                    _item.HourlyRate = dt.Rows[0]["hrlyrate"].ToString().Currency();
                    _item.UserId = dt.Rows[0]["member_id"].ToString();
                    _item.FirstName = dt.Rows[0]["f_name"].ToString().ToTitleCase();
                    _item.LastName = dt.Rows[0]["l_name"].ToString().ToTitleCase();
                    _item.UserName = dt.Rows[0]["Username"].ToString();
                    _item.Password = _crypt.DecryptStringAES(dt.Rows[0]["Password"].ToString());
                    _item.ConfirmPassword = _crypt.DecryptStringAES(dt.Rows[0]["Password"].ToString());
                    _item.CompanyName = dt.Rows[0]["company"].ToString();
                    _item.DrivingDistance = Convert.ToInt32(dt.Rows[0]["dd"].ToString());
                    _item.Tax = dt.Rows[0]["Tax"].ToString();
                    //if (dt1.Rows.Count > 0)
                    //{
                    //    for (int i = 0; i < dt1.Rows.Count; i++)
                    //    {
                    //        _skills.Add(Convert.ToInt32(dt1.Rows[i][0].ToString()));
                    //    }
                    //    _item.Skills = _skills.ToArray();
                    //}

                    //if (dt2.Rows.Count > 0)
                    //{
                    //    for (int i = 0; i < dt2.Rows.Count; i++)
                    //    {
                    //        _skills2.Add(Convert.ToInt32(dt2.Rows[i][0].ToString()));
                    //    }
                    //    _item.Tools = _skills2.ToArray();
                    //}
                    //if (dt3.Rows.Count > 0)
                    //{
                    //    for (int i = 0; i < dt3.Rows.Count; i++)
                    //    {
                    //        client_tech.Add(Convert.ToInt32(dt3.Rows[i][0].ToString()));
                    //    }
                    //    _item.Clients = client_tech.ToArray();
                    //}
                }
                dt.Dispose();
                //dt1.Dispose();
                //dt2.Dispose();
                //dt3.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _item.error_text = "Error:" + exception.Message;
            }
            return _item;
        }
        public void GetCityStateName(int _cityId, int _stateId, ref string _cityName, ref string _stateName)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList _srt = new SortedList
            {
                { "@cityId", _cityId },
                { "@stateId", _stateId }
            };
            try
            {
                var dt = sqlHelper.fillDataTable("GetCityStateName", string.Empty, _srt); sqlHelper.Dispose();
                if (dt.Rows.Count > 0)
                {
                    _cityName = dt.Rows[0]["city"].ToString(); ;
                    _stateName = dt.Rows[0]["state"].ToString(); ;
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
        }

        public UserModel get_members_detail_byid(int id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            UserModel _item = new UserModel
            {
                UserTableId = id
            };
            SortedList _srt = new SortedList();
            _srt.Add("@id", id);
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_member_getmemberdetailbyid", string.Empty, _srt); sqlHelper.Dispose();
                if (dt.Rows.Count > 0)
                {
                    _item.ManagerId = Convert.ToInt32(dt.Rows[0]["ManagerId"].ToString());
                    _item.UserId = dt.Rows[0]["member_id"].ToString();
                    _item.Zipcode = dt.Rows[0]["Zip_code"].ToString();
                    _item.Address = dt.Rows[0]["address"].ToString();
                    _item.EmailAddress = dt.Rows[0]["EMail"].ToString();
                    _item.Position = dt.Rows[0]["position"].ToString();
                    _item.UserType = !string.IsNullOrEmpty(dt.Rows[0]["type"].ToString()) ? dt.Rows[0]["type"].ToString() : "ST";
                    _item.UserType = _item.UserType != "T" ? _item.UserType : "ST";
                    _item.PhoneNumber = dt.Rows[0]["Phone"].ToString().PhoneNumber();
                    _item.FirstName = dt.Rows[0]["f_name"].ToString().ToTitleCase();
                    _item.LastName = dt.Rows[0]["l_name"].ToString().ToTitleCase();
                    _item.CityName = dt.Rows[0]["city"].ToString();
                    _item.StateId = Convert.ToInt32(dt.Rows[0]["state_id"].ToString());
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _item.error_text = "Error:" + exception.Message;
            }
            return _item;
        }
    }
}