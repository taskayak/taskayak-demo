﻿using hrm.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class dashboardservices
    {
        public dashboardmodal GetDashBoardData(int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            dashboardmodal _mdl = new dashboardmodal();
            try
            {
                SortedList _srtlist = new SortedList
                {
                    { "@workspaceid", workspaceid }
                };
                var dt = sqlHelper.fillDataTable("GetDashboard_v2", string.Empty, _srtlist); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _mdl.jobs = Convert.ToInt32(dt.Rows[i]["j"].ToString());
                    _mdl.leads = Convert.ToInt32(dt.Rows[i]["o"].ToString());
                    _mdl.monthuypay = dt.Rows[i]["p"].ToString().Currency();
                    _mdl.monthreveneue = dt.Rows[i]["r"].ToString().Currency(); 
                    _mdl.AccountId = dt.Rows[i]["Id"].ToString();
                    _mdl.AccountName = dt.Rows[i]["Name"].ToString();
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _mdl.error_text = "Error:" + exception.Message;
            }
            return _mdl;
        }

        public List<dashboardjobs> GetRecentJobs(int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<dashboardjobs> _mdl = new List<dashboardjobs>();
            try
            {
                SortedList _srtlist = new SortedList
                {
                    { "@workspaceid", workspaceid }
                };
                var dt = sqlHelper.fillDataTable("GetRecentJobs", string.Empty, _srtlist); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dashboardjobs _itm = new dashboardjobs
                    {
                        clientname = dt.Rows[i]["Clientname"].ToString(),
                        Technician = dt.Rows[i]["Technician"].ToString().ToTitleCase(),
                        startdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()).ToString("dd-MM-yyyy"),
                        rate = dt.Rows[i]["Client_rate"].ToString(),
                        Jobid = dt.Rows[i]["Jobid"].ToString()
                    };
                    _mdl.Add(_itm);
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _mdl;
        }

        public List<dashboardjobs> GetRecentJobs(ref int total, int StartIndex, int EndIndex, int WorkspaceId)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<dashboardjobs> _mdl = new List<dashboardjobs>();
            try
            {
                SortedList _srtlist = new SortedList
                {
                    { "@workspaceid", WorkspaceId },
                    { "@startIndex", StartIndex },
                    { "@endindex", EndIndex }
                };
                var dt = sqlHelper.fillDataTable("GetRecentJobsByIndex", string.Empty, _srtlist); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dashboardjobs _itm = new dashboardjobs
                    {
                        clientname = dt.Rows[i]["Clientname"].ToString(),
                        Technician = dt.Rows[i]["Technician"].ToString().ToTitleCase(),
                        startdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()).ToString("dd-MM-yyyy"),
                        rate = dt.Rows[i]["Client_rate"].ToString(),
                        Jobid = dt.Rows[i]["Jobid"].ToString()

                    };
                    total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _mdl.Add(_itm);
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _mdl;
        }


        public List<ReminderFetch> GetReminders(ref int total, int StartIndex, int EndIndex, int WorkspaceId, string Type = "Email")
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<ReminderFetch> _mdl = new List<ReminderFetch>();
            try
            {
                SortedList _srtlist = new SortedList
                {
                    { "@workspaceid", WorkspaceId },
                    { "@startIndex", StartIndex },
                    { "@Type", Type },
                    { "@endindex", EndIndex }
                };
                var dt = sqlHelper.fillDataTable("GetRemindersByIndex", string.Empty, _srtlist);
                sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ReminderFetch _itm = new ReminderFetch
                    {
                        CreatedDate = Convert.ToDateTime(dt.Rows[i]["CreatedDate"].ToString()).ToString("dd-MM-yyyy"),
                        JobName = dt.Rows[i]["JobName"].ToString(),
                        UserID = dt.Rows[i]["UserIndexId"].ToString(),
                        Name = dt.Rows[i]["Name"].ToString(),
                        NeedConfirmation = Convert.ToBoolean(dt.Rows[i]["NeedConfirmation"].ToString()),
                        SendDate = Convert.ToDateTime(dt.Rows[i]["SendDate"].ToString()).ToString("dd-MM-yyyy"),
                        Status = dt.Rows[i]["Status"].ToString()
                    };
                    total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _mdl.Add(_itm);
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _mdl;
        }

        public List<dashboardoffer> GetRecentOffers(int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<dashboardoffer> _mdl = new List<dashboardoffer>();
            try
            {
                SortedList _srtlist = new SortedList
                {
                    { "@workspaceid", workspaceid }
                };
                var dt = sqlHelper.fillDataTable("GetRecentOffers", string.Empty, _srtlist); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dashboardoffer _itm = new dashboardoffer
                    {
                        clientname = dt.Rows[i]["Clientname"].ToString(),
                        Dispatcher = dt.Rows[i]["dispatcher"].ToString().ToTitleCase(),
                        startdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()).ToString("dd-MM-yyyy"),
                        rate = dt.Rows[i]["Client_rate"].ToString(),
                        Jobid = dt.Rows[i]["Jobid"].ToString()
                    };
                    _mdl.Add(_itm);
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _mdl;
        }

        public dashboardmodaljson get_all_active_dashboard_json(int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            dashboardmodaljson _mdl = new dashboardmodaljson();
            List<int> _js = new List<int>();
            List<int> _ofer = new List<int>();
            int count;
            try
            {
                SortedList _list = new SortedList
                {
                    { "@workspaceid", workspaceid }
                };
                var ds = sqlHelper.fillDataSet("getjobofferdata", string.Empty, _list); sqlHelper.Dispose();
                var dt = ds.Tables[0];
                var dt1 = ds.Tables[1];
                ds.Dispose();
                int startcoulm = 0;
                for (int i = 1; i < 13; i++)
                {
                    if (startcoulm <= dt.Rows.Count)
                    {
                        try
                        {
                            count = Convert.ToInt32(dt.Rows[startcoulm]["months"].ToString());
                            if (count == i)
                            {
                                _js.Add(Convert.ToInt32(dt.Rows[startcoulm]["total"].ToString()));
                                startcoulm = startcoulm + 1;
                            }
                            else
                            {
                                _js.Add(0);
                            }
                        }
                        catch
                        {
                            _js.Add(0);
                        }
                    }
                    else
                    {
                        _js.Add(0);
                    }
                }
                dt.Dispose();
                startcoulm = 0;
                for (int i = 1; i < 13; i++)
                {
                    if (startcoulm <= dt1.Rows.Count)
                    {
                        try
                        {
                            count = Convert.ToInt32(dt1.Rows[startcoulm]["months"].ToString());
                            if (count == i)
                            {
                                _ofer.Add(Convert.ToInt32(dt1.Rows[startcoulm]["total"].ToString()));
                                startcoulm = startcoulm + 1;
                            }
                            else
                            {
                                _ofer.Add(0);
                            }
                        }
                        catch { _ofer.Add(0); }
                    }
                    else
                    {
                        _ofer.Add(0);
                    }
                }
                dt1.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _mdl.error_text = "Error:" + exception.Message;
            }
            _mdl._jobs = _js;
            _mdl._offer = _ofer;
            return _mdl;
        }
    }
}