﻿using HubSpot.NET.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HubSpot;
using HubSpot.NET.Api.Company.Dto;
using hrm.Models;
using HubSpot.NET.Api.Contact.Dto;

namespace hrm.Database
{
    public class hubspot
    {
       private HubSpotApi api = new HubSpotApi("e70ce0ce-f39c-4341-a624-92e8176c7e0c");
        //feb39c09-2517-417a-8e48-7c9426cb420a Umesh Account for testing
        //e70ce0ce-f39c-4341-a624-92e8176c7e0c Saurel Live
        public string CreateHubSpotCompany(HubSpotCOmpanyModel model,string Primarycontact)
        {
            try
            {
                var company = api.Company.Create(model);
                var name =  Primarycontact.Split(' ');
                var contactmodel = new ContactHubSpotModel
                {
                    FirstName = name[0],
                    LastName = name.Length>1? name[1]:string.Empty,
                    AssociatedCompanyId = company.Id,
                    Email = model.Emailaddress,
                    Phone = model.Phonenumber,
                    Website=model.Website 
                };
                return CreateHubSpotContact(contactmodel);
            }
            catch (Exception ex)
            {
                return "Error: " + ex.Message;
            } 
        } 
        public string CreateHubSpotContact(ContactHubSpotModel model)
        {
            try
            {
                var contact = api.Contact.CreateOrUpdate(model);
                return contact.Id.ToString();
            }
            catch (Exception ex)
            {
                return "Error: " + ex.Message;
            }
        } 
    }
}