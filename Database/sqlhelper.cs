﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class SqlHelper : ConnectionHelper, IDisposable
    {
        private DataSet ds;
        private DataTable dt;
        private SqlDataAdapter adapter = new SqlDataAdapter();
        private SqlCommand com = new SqlCommand();
        public void Dispose()
        {
            if (ds != null)
            {
                ds.Dispose();
            }
            if (dt != null)
            {
                dt.Dispose();
            }
            if (com != null)
            {
                com.Dispose();
            }
            if (adapter != null)
            {
                adapter.Dispose();
            }
            con.Dispose();
            GC.SuppressFinalize(this);
        }

        public void executeNonQuery(string procName, string qry, SortedList list)
        {
            com = returnCommand(procName, qry, list);
            com.ExecuteNonQuery();
            con.Close();
        }

        public string executeNonQueryWithTran(string procName, SortedList list, SqlConnection conTran, SqlTransaction tran)
        {
            com = returnCommand(procName, "", list);
            com.Parameters.Add("@xxx", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            com.Connection.Close();
            com.Connection = conTran;
            com.Transaction = tran;
            com.ExecuteNonQuery();
            string str = com.Parameters["@xxx"].Value.ToString();
            con.Close();
            return str;
        }

        public object executeNonQueryWMessage(string procName, string qry, SortedList list)
        {
            com = returnCommand(procName, qry, list);
            SqlParameter sqlParameter = new SqlParameter("@Mes", SqlDbType.VarChar, 8000)
            {
                Direction = ParameterDirection.Output
            };
            com.Parameters.Add(sqlParameter);
            com.ExecuteNonQuery();
            com.Dispose();
            return sqlParameter.Value;
        }

        public string executeNonQueryWTranOutMes(string procName, SortedList list, SqlConnection conTran, SqlTransaction tran)
        {
            com = returnCommand(procName, "", list);
            SqlParameter sqlParameter = new SqlParameter("@Mes", SqlDbType.VarChar, 500)
            {
                Direction = ParameterDirection.Output
            };
            com.Parameters.Add(sqlParameter);
            com.Connection.Close();
            com.Connection = conTran;
            com.Transaction = tran;
            com.ExecuteNonQuery();
            con.Close();
            return sqlParameter.Value.ToString();
        }

        public SqlDataReader executeReader(string procName, string qry, SortedList list)
        {
            com = returnCommand(procName, qry, list);
            return com.ExecuteReader();
        }

        public object executeScaler(string qry)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            com = new SqlCommand(qry, con)
            {
                CommandType = CommandType.Text
            };
            object obj = com.ExecuteScalar();
            con.Close();
            return obj;
        }

        public DataSet fillDataSet(string procName, string qry, SortedList list)
        {
            com = returnCommand(procName, qry, list);
            adapter = new SqlDataAdapter(com);
            ds = new DataSet();
            adapter.Fill(ds);
            con.Close();
            return ds;
        }

        public DataTable fillDataTable(string procName, string qry, SortedList list)
        {
            com = returnCommand(procName, qry, list);
            adapter = new SqlDataAdapter(com);
            dt = new DataTable();
            adapter.Fill(dt);
            con.Close();
            return dt;
        }

        public SqlCommand returnCommand(string procName, string qry, SortedList list)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            if (!string.IsNullOrEmpty(procName))
            {
                com = new SqlCommand(procName, con)
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandTimeout = 0
                };
            }
            if (!string.IsNullOrEmpty(qry))
            {
                com = new SqlCommand(qry, con)
                {
                    CommandType = CommandType.Text
                };
            }
            string str;
            if (list != null)
            {
                foreach (string key in list.Keys)
                {
                    str = (key.Contains("@") ? key : string.Concat("@", key));
                    com.Parameters.AddWithValue(str, list[key] != null ? list[key].ToString() : string.Empty);
                }
            }
            return com;
        }
    }
}