﻿using Flurl.Util;
using hrm.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class templateservices
    {
        public List<templateitem> get_all_active_template_bypaging(ref int total, int startindex = 0, int endindex = 25, string search = "", int workspaceid = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<templateitem> _model = new List<templateitem>();
            SortedList _srtlist = new SortedList
            {
                { "@startindex", startindex },
                { "@endIndex", endindex },
                { "@workspaceid", workspaceid }
            };
            if (!string.IsNullOrEmpty(search))
            {
                _srtlist.Add("@search", search);
            }
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_template_by_paging", string.Empty, _srtlist); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    templateitem _item = new templateitem
                    {
                        email = dt.Rows[i]["email"].ToString(),
                        sms = dt.Rows[i]["sms"].ToString(),
                        isdefault = Convert.ToBoolean(dt.Rows[i]["isdefault"].ToString()),
                        id = Convert.ToInt32(dt.Rows[i]["id"].ToString()),
                        title = dt.Rows[i]["title"].ToString()
                    };
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _model;
        }

        public templateModal get_all_active_template(int? id, int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            templateModal _mdl = new templateModal();
            List<templateitem> _model = new List<templateitem>();
            try
            {
                SortedList _srt = new SortedList();
                if (id.HasValue)
                {
                    _srt.Add("@id", id.Value);
                }
                _srt.Add("@workspaceid", workspaceid);
                var ds = sqlHelper.fillDataSet("tbl_template_getalltemplate", string.Empty, _srt); sqlHelper.Dispose();
                var dt = ds.Tables[0];
                ds.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    templateitem _item = new templateitem
                    {
                        email = dt.Rows[i]["email"].ToString(),
                        sms = dt.Rows[i]["sms"].ToString(),
                        isdefault = Convert.ToBoolean(dt.Rows[i]["isdefault"].ToString()),
                        id = Convert.ToInt32(dt.Rows[i]["id"].ToString()),
                        title = dt.Rows[i]["title"].ToString()
                    };
                    _model.Add(_item);
                }
                dt.Dispose();
              
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                 _mdl.error_text = "Error:" + exception.Message;
            } 
            _mdl._template = _model;
            return _mdl;
        }

        public string add_new_template(templateitem notice, int userid, int workspaceid)
        {
            string message ;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@title", notice.title },
                    { "@sms", notice.sms },
                    { "@workspaceid", workspaceid },
                    { "@email", notice.email },
                    { "@default", notice.isdefault },
                    { "@createdby", userid }
                };
                message = sqlHelper.executeNonQueryWMessage("tbl_template_addnew", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                 message = "Error:" + exception.Message;
            }  
            return message;
        }


        public string update_template(templateitem template)
        {
            string message;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@title", template.title },
                    { "@sms", template.sms },
                    { "@email", template.email },
                    { "@default", template.isdefault },
                    { "@id", template.id }
                };
                message = sqlHelper.executeNonQueryWMessage("tbl_template_update", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                 message = "Error:" + exception.Message;
            } 
            return message;
        }

        public templateitem get_template(int id)
        {
            templateitem _item = new templateitem();
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                int i = 0;
                SortedList _srt = new SortedList
                {
                    { "@id", id }
                };
                var dt = sqlHelper.fillDataTable("tbl_template_getalltemplate", string.Empty, _srt); sqlHelper.Dispose();
                if (dt.Rows.Count > 0)
                {
                    _item.email = dt.Rows[i]["email"].ToString();
                    _item.sms = dt.Rows[i]["sms"].ToString();
                    _item.isdefault = Convert.ToBoolean(dt.Rows[i]["isdefault"].ToString());
                    _item.id = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.title = dt.Rows[i]["title"].ToString();
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                 _item.error_text = "Error:" + exception.Message;
            } 
            return _item;
        }


        public templateitem GetJobNotificationTemplate(int Workspaceid,ref string DomainName)
        {
            templateitem _item = new templateitem();
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                int i = 0;
                SortedList _srt = new SortedList
                {
                    { "@workspaceid", Workspaceid }
                };
                var dt = sqlHelper.fillDataTable("GetJobnotificationTemplate", string.Empty, _srt); sqlHelper.Dispose();
                if (dt.Rows.Count > 0)
                {
                    _item.email = dt.Rows[i]["email"].ToString();
                    _item.sms = dt.Rows[i]["sms"].ToString();
                    _item.isdefault = Convert.ToBoolean(dt.Rows[i]["isdefault"].ToString());
                    _item.id = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item.title = dt.Rows[i]["title"].ToString();
                    DomainName = dt.Rows[i]["domain"].ToString();
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _item.error_text = "Error:" + exception.Message;
            }
            return _item;
        }

        public string update_default(int id, bool status)
        {
            string message = "Template Updated Succesfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", id },
                    { "@status", status }
                };
                sqlHelper.executeNonQuery("tbl_template_updatedefault", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            } 
            return message;
        }

        public string getdomainname(int workspaceid)
        {
            string message = "convo";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@workspaceid", workspaceid }
                };
                message = sqlHelper.executeNonQueryWMessage("Getdomainname", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose(); 
            }
            return message;
        } 

        public string delete_template(int id)
        {
            string message ;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", id }
                };
                message = sqlHelper.executeNonQueryWMessage("tbl_template_removetemplate", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                 message = "Error:" + exception.Message;
            } 
            return message;
        }
    }
}