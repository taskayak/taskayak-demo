﻿using hrm.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class skillservices
    {
        public skill get_all_active_skills(int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            skill _mdl = new skill();
            List<SkillItems> _model = new List<SkillItems>();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@workspaceid", workspaceid }
                };
                var dt = sqlHelper.fillDataTable("tbl_skill_master_getallskills", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    SkillItems _item = new SkillItems
                    {
                        Name = dt.Rows[i]["name"].ToString(),
                        SkillId = Convert.ToInt32(dt.Rows[i]["id"].ToString())
                    };
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
               _mdl.error_text = "Error:" + exception.Message;
            } 
            _mdl._skill = _model;
            return _mdl;
        }
        public Tuple<List<SkillItems>, List<int>> GetUserSkills(int workspaceid, int UserId)
        {
            List<int> Skills = new List<int>();
            SqlHelper sqlHelper = new SqlHelper();
            List<SkillItems> _model = new List<SkillItems>();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@workspaceid", workspaceid },
                    { "@UserId", UserId }
                };
                var ds = sqlHelper.fillDataSet("GetUserSkills", string.Empty, _srt); sqlHelper.Dispose();
                ds.Dispose();
                var dt = ds.Tables[0];
                var dt1 = ds.Tables[1];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    SkillItems _item = new SkillItems
                    {
                        Name = dt.Rows[i]["name"].ToString(),
                        SkillId = Convert.ToInt32(dt.Rows[i]["id"].ToString())
                    };
                    _model.Add(_item);
                }
                if (dt1.Rows.Count > 0)
                {
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        Skills.Add(Convert.ToInt32(dt1.Rows[i][0].ToString()));
                    }
                }
                dt1.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return Tuple.Create(_model, Skills);
        }

        public Tool get_all_active_tools(int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            Tool _mdl = new Tool();
            List<ToolItems> _model = new List<ToolItems>();
            SortedList _srt = new SortedList
            {
                { "@workspaceid", workspaceid }
            };
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_tools_master_getalltools", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ToolItems _item = new ToolItems
                    {
                        Name = dt.Rows[i]["name"].ToString(),
                        TypeName = dt.Rows[i]["type"].ToString(),
                        ToolId = Convert.ToInt32(dt.Rows[i]["id"].ToString()),
                        ToolTypeId = Convert.ToInt32(dt.Rows[i]["typeid"].ToString())
                    };
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                 _mdl.error_text = "Error:" + exception.Message;
            } 
            _mdl._tool = _model;
            return _mdl;
        }

        public Tuple< List<ToolItems>, List<int>> GetUserTools(int workspaceid,int UserId)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<ToolItems> _mdl = new List<ToolItems>();
             List<int> Tools = new List<int>();
            SortedList _srt = new SortedList
            {
                { "@workspaceid", workspaceid },
                { "@UserId", UserId }
            };
            try
            {
                var ds = sqlHelper.fillDataSet("GetUserTools", string.Empty, _srt); sqlHelper.Dispose();
                var dt = ds.Tables[0];
                var dt1 = ds.Tables[1]; ds.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ToolItems _item = new ToolItems
                    {
                        Name = dt.Rows[i]["name"].ToString(),
                        TypeName = dt.Rows[i]["type"].ToString(),
                        ToolId = Convert.ToInt32(dt.Rows[i]["id"].ToString()),
                        ToolTypeId = Convert.ToInt32(dt.Rows[i]["typeid"].ToString())
                    };
                    _mdl.Add(_item);
                }
                dt.Dispose();
                if (dt1.Rows.Count > 0)
                {
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        Tools.Add(Convert.ToInt32(dt1.Rows[i][0].ToString()));
                    } 
                }
                dt1.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose(); 
            }  
            return Tuple.Create(_mdl, Tools);
        }

        public string add_new_skill(string skill, int userid,int workspaceid)
        {
            string message ;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@skill", skill },
                    { "@workspaceid", workspaceid },
                    { "@createdby", userid }
                };
                message = sqlHelper.executeNonQueryWMessage("tbl_skill_master_addskills", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                 message = "Error:" + exception.Message;
            } 
            return message;
        }
        public string update_skill(string skill, int skillid, int userid)
        {
            string message ;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@skill", skill },
                    { "@createdby", userid },
                    { "@skillid", skillid }
                };
                message = sqlHelper.executeNonQueryWMessage("tbl_skill_master_updateskill", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                 message = "Error:" + exception.Message;
            } 
            return message;
        }
        public string delete_skill(int skillid, int userid)
        {
            string message ;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@skillid", skillid },
                    { "@createdby", userid }
                };
                message = sqlHelper.executeNonQueryWMessage("tbl_skill_master_removeskill", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                  message = "Error:" + exception.Message;
            } 
            return message;
        }
    } 
}