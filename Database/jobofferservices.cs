﻿using hrm.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;

namespace hrm.Database
{
    public class jobofferservices
    {
        public string AddNewOffer(job _mdl, int userid, string tecrate, string clientrate, string MapValidateAddress, string zip, int workspaceid, int projectId)
        {
            string message = "Offer Added Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            string _sdate = _mdl.sdate + " " + _mdl.stime;
            string _edate = _mdl.sdate + " " + _mdl.etime;
            TimeSpan span = Convert.ToDateTime(_edate) - Convert.ToDateTime(_sdate);
            var estimhour = String.Format("{0}.{1}", span.Hours, span.Minutes > 0 ? (int)Math.Round(Convert.ToDouble(span.Minutes) * 1.66) : span.Minutes);
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@tecrate", tecrate },
                    { "@clientrate", clientrate },
                    { "@address", _mdl.street },
                    { "@MapValidateAddress", MapValidateAddress },
                    { "@cityId", _mdl.city_id },
                    { "@stateId", _mdl.state_id },
                    { "@projectId", projectId },
                    { "@JobId", _mdl.JobId },
                    { "@Ttile", _mdl.title },
                    { "@Client", _mdl.client_id },
                    { "@Technician", _mdl.Technician },
                    { "@Dispatcher", _mdl.Dispatcher },
                    { "@Project_manager", _mdl.mgr_id },//client manager
                    { "@Project_manager1", _mdl.mgr_id1 },
                    { "@startdate", _sdate },
                    { "@expense", _mdl.Expense },
                    { "@enddate", _edate },
                    { "@est_Hours", estimhour },
                    { "@Status_id", _mdl.Status },
                    { "@suite", _mdl.suite },
                    { "@Description", _mdl.description },
                    { "@createdby", userid },
                    { "@workspaceid", workspaceid },
                    { "@Contact", _mdl.Contact },
                    { "@Phone", _mdl.Phone }
                };
                message = sqlHelper.executeNonQueryWMessage("AddNewOffer", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }


        public void decreasesmsemailcount(int workspaceid, int smscount, int emailcount)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srtlist = new SortedList
                {
                    { "@workspaceid", workspaceid },
                    { "@smscount", smscount },
                    { "@emailcount", emailcount }
                };
                sqlHelper.executeNonQuery("updateaccount", string.Empty, _srtlist); sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
        }

        public string Sendoffers(List<int> data, int pref, int jobofferId, bool sms, bool email, string smstext, string emailtext, string subject, int workspaceid, string workspaceName)
        {
            Crypto _crypt = new Crypto();
            string message = "Offer Added Successfully";
            try
            {
                var sqlHelper = new SqlHelper();
                var emailtemp = emailtext;
                var smstemp = smstext;
                var job = GetMailMergeJobDetailsById(jobofferId);
                string acceptlink = workspaceName + ".taskayak.com/offer/acceptoffer?token=";
                string declinelink = workspaceName + ".taskayak.com/offer/declineoffer?token=";
                SortedList _srt = new SortedList();
                string encryptid = string.Empty;
                int offersId = 0;
                int smscount = 0;
                int emailcount = 0;
                foreach (var item in data)
                {
                    sqlHelper = new SqlHelper();
                    acceptlink = workspaceName + ".taskayak.com/offer/acceptoffer?token=";
                    declinelink = workspaceName + ".taskayak.com/offer/declineoffer?token=";
                    emailtemp = emailtext;
                    smstemp = smstext;
                    var mdl = GetAllActiveMembersById(item);
                    smstemp = sms ? smstemp.MailMergeBytext(job.datetime, job.sdate, job.stime, job.etime, job.JobId, job.title, job.Pay_rate, job.description, mdl.FirstName, mdl.LastName, mdl.FirstName + " " + mdl.LastName, mdl.StateNameMailMerge, mdl.CityNameMailMerge, job.city, job.state, job.address, acceptlink, mdl.HourlyRate, mdl.Password, mdl.UserName, mdl.EmailAddress, mdl.UserId, declinelink) : "";
                    emailtemp = email ? emailtemp.MailMergeBytext(job.datetime, job.sdate, job.stime, job.etime, job.JobId, job.title, job.Pay_rate, job.description, mdl.FirstName, mdl.LastName, mdl.FirstName + " " + mdl.LastName, mdl.StateNameMailMerge, mdl.CityNameMailMerge, job.city, job.state, job.address, acceptlink, mdl.HourlyRate, mdl.Password, mdl.UserName, mdl.EmailAddress, mdl.UserId, declinelink) : "";
                    _srt.Clear();
                    _srt.Add("@memberid", item);
                    _srt.Add("@pref", pref == 1 ? "Auto" : "Manual");
                    _srt.Add("@offerId", jobofferId);
                    _srt.Add("@sms", smstemp);
                    _srt.Add("@email", emailtemp);
                    _srt.Add("@subject", subject);
                    offersId = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("tbl_job_offer_add_v2", string.Empty, _srt).ToString());
                    encryptid = _crypt.EncryptStringAES(offersId.ToString());
                    acceptlink += encryptid;
                    declinelink += encryptid;
                    smstemp = sms ? smstext.MailMergeBytext(job.datetime, job.sdate, job.stime, job.etime, job.JobId, job.title, job.Pay_rate, job.description, mdl.FirstName, mdl.LastName, mdl.FirstName + " " + mdl.LastName, mdl.StateNameMailMerge, mdl.CityNameMailMerge, job.city, job.state, job.address, acceptlink, mdl.HourlyRate, mdl.Password, mdl.UserName, mdl.EmailAddress, mdl.UserId, declinelink) : "";
                    emailtemp = email ? emailtext.MailMergeBytext(job.datetime, job.sdate, job.stime, job.etime, job.JobId, job.title, job.Pay_rate, job.description, mdl.FirstName, mdl.LastName, mdl.FirstName + " " + mdl.LastName, mdl.StateNameMailMerge, mdl.CityNameMailMerge, job.city, job.state, job.address, acceptlink, mdl.HourlyRate, mdl.Password, mdl.UserName, mdl.EmailAddress, mdl.UserId, declinelink) : "";
                    _srt.Clear();
                    _srt.Add("@offerId", offersId);
                    _srt.Add("@sms", smstemp);
                    _srt.Add("@email", emailtemp);
                    sqlHelper.executeNonQuery("tbl_job_offer_updatelink", string.Empty, _srt);
                    sqlHelper.Dispose();
                    smscount += sms ? 1 : 0;
                    emailcount += email ? 1 : 0;
                }
                decreasesmsemailcount(workspaceid, smscount, emailcount);
            }
            catch (Exception exception)
            {
                message = "Error:" + exception.Message;
            }
            return message;
        }

        public string sendoffers2(List<int> data, bool sms, bool email, string smstext, string emailtext, string subject, bool termspopup, int workspaceid)
        {
            string message = "Offer Added Successfully";
            try
            {
                var sqlHelper = new SqlHelper();
                int smscount = 0;
                int emailcount = 0;
                SortedList _srt = new SortedList();
                foreach (var item in data)
                {
                    string emailtemp = emailtext;
                    string smstemp = smstext;
                    var mdl = GetAllActiveMembersById(item);
                    smstemp = sms ? smstemp.MailMergeBytext(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, mdl.FirstName, mdl.LastName, mdl.FirstName + " " + mdl.LastName, mdl.StateNameMailMerge, mdl.CityNameMailMerge, string.Empty, string.Empty, string.Empty, string.Empty, mdl.HourlyRate, mdl.Password, mdl.UserName, mdl.EmailAddress, mdl.UserId) : string.Empty;
                    emailtemp = email ? emailtemp.MailMergeBytext(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, mdl.FirstName, mdl.LastName, mdl.FirstName + " " + mdl.LastName, mdl.StateNameMailMerge, mdl.CityNameMailMerge, string.Empty, string.Empty, string.Empty, string.Empty, mdl.HourlyRate, mdl.Password, mdl.UserName, mdl.EmailAddress, mdl.UserId) : string.Empty;
                    _srt.Clear();
                    _srt.Add("@memberid", item);
                    _srt.Add("@pref", "Auto");
                    _srt.Add("@offerId", 0);
                    _srt.Add("@sms", smstemp);
                    _srt.Add("@email", emailtemp);
                    _srt.Add("@subject", subject);
                    _srt.Add("@termspopup", termspopup);
                    sqlHelper = new SqlHelper();
                    sqlHelper.executeNonQuery("tbl_job_offer_add", string.Empty, _srt);
                    sqlHelper.Dispose();
                    smscount += sms ? 1 : 0;
                    emailcount += email ? 1 : 0;
                }
                decreasesmsemailcount(workspaceid, smscount, emailcount);
            }
            catch (Exception exception)
            {
                message = "Error:" + exception.Message;
            }
            return message;
        }

        public string sendoffers4(List<int> data, bool sms, bool email, string smstext, string emailtext, string subject, int jobid, int workspaceid)
        {
            string message = "Offer Added Successfully";
            try
            {
               
                int smscount = 0;
                int emailcount = 0;
                SortedList _srt = new SortedList();
                var job = GetMailMergeOnlyJobDetailsById(jobid); 
                foreach (var item in data)
                {
                    SqlHelper sqlHelper = new SqlHelper();
                    string emailtemp = emailtext;
                    string smstemp = smstext;
                    var mdl = GetAllActiveMembersById(item);
                    smstemp = sms ? smstemp.MailMergeBytext(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, mdl.FirstName, mdl.LastName, mdl.FirstName + " " + mdl.LastName, mdl.StateNameMailMerge, mdl.CityNameMailMerge, string.Empty, string.Empty, string.Empty, string.Empty, mdl.HourlyRate, mdl.Password, mdl.UserName, mdl.EmailAddress, mdl.UserId) : string.Empty;
                    emailtemp = email ? emailtemp.MailMergeBytext(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, mdl.FirstName, mdl.LastName, mdl.FirstName + " " + mdl.LastName, mdl.StateNameMailMerge, mdl.CityNameMailMerge, string.Empty, string.Empty, string.Empty, string.Empty, mdl.HourlyRate, mdl.Password, mdl.UserName, mdl.EmailAddress, mdl.UserId) : string.Empty;
                    _srt.Clear();
                    _srt.Add("@memberid", item);
                    _srt.Add("@pref", "Auto");
                    _srt.Add("@offerId", 0);
                    _srt.Add("@jobid", jobid);
                    _srt.Add("@sms", smstemp);
                    _srt.Add("@email", emailtemp);
                    _srt.Add("@subject", subject);
                    _srt.Add("@termspopup", false); 
                    sqlHelper.executeNonQuery("tbl_job_offer_add", string.Empty, _srt); 
                    smscount += sms ? 1 : 0;
                    emailcount += email ? 1 : 0;
                    sqlHelper.Dispose();
                }
                decreasesmsemailcount(workspaceid, smscount, emailcount);
            }
            catch (Exception exception)
            {
                message = "Error:" + exception.Message;
            }
            return message;
        }


        public void AddUserSignupNotification(int newmemberid, int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList _list = new SortedList
            {
                { "@memberid", newmemberid },
                { "@workspaceid", workspaceid }
            };
            try
            {
                sqlHelper.executeNonQuery("AddtechsignupNotification", string.Empty, _list); sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
        }
        public string sendoffersimport4(List<string> data, bool sms, bool email, string smstext, string emailtext, string subject, bool termspopup, int workSpaceid)
        {
            string message = "Offer Added Successfully";
            try
            {
                var sqlHelper = new SqlHelper();
                SortedList _srt = new SortedList();
                foreach (var item in data)
                {
                    string emailtemp = emailtext;
                    string smstemp = smstext;
                    var mdl = get_all_active_members_bymemberid(item);
                    smstemp = sms ? smstemp.MailMergeBytext(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, mdl.FirstName, mdl.LastName, mdl.FirstName + " " + mdl.LastName, mdl.StateNameMailMerge, mdl.CityNameMailMerge, string.Empty, string.Empty, string.Empty, string.Empty, mdl.HourlyRate, mdl.Password, mdl.UserName, mdl.EmailAddress, mdl.UserId) : string.Empty;
                    emailtemp = email ? emailtemp.MailMergeBytext(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, mdl.FirstName, mdl.LastName, mdl.FirstName + " " + mdl.LastName, mdl.StateNameMailMerge, mdl.CityNameMailMerge, string.Empty, string.Empty, string.Empty, string.Empty, mdl.HourlyRate, mdl.Password, mdl.UserName, mdl.EmailAddress, mdl.UserId) : string.Empty;
                    _srt.Clear();
                    _srt.Add("@memberid", mdl.UserTableId);
                    _srt.Add("@pref", "Auto");
                    _srt.Add("@offerId", 0);
                    _srt.Add("@sms", smstemp);
                    _srt.Add("@email", emailtemp);
                    _srt.Add("@subject", subject);
                    _srt.Add("@termspopup", termspopup);
                    _srt.Add("@workSpaceid", workSpaceid);
                    sqlHelper = new SqlHelper();
                    sqlHelper.executeNonQuery("tbl_job_offer_add", string.Empty, _srt);
                    sqlHelper.Dispose();
                    AddUserSignupNotification(mdl.UserTableId, workSpaceid);
                }
            }
            catch (Exception exception)
            {
                message = "Error:" + exception.Message;
            }
            return message;
        }
        public UserModel GetAllActiveMembersById(int id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            UserModel _item = new UserModel();
            SortedList _srt = new SortedList
            {
                { "@id", id }
            };
            try
            {
                var ds = sqlHelper.fillDataSet("tbl_job_offer_tbl_member_getmemberbyid", string.Empty, _srt); sqlHelper.Dispose();
                var dt = ds.Tables[0];
                ds.Dispose();
                if (dt.Rows.Count > 0)
                {
                    Crypto _crypt = new Crypto();
                    _item.Zipcode = dt.Rows[0]["Zip_code"].ToString();
                    _item.Address = dt.Rows[0]["address"].ToString();
                    _item.FirstName = dt.Rows[0]["f_name"].ToString().ToTitleCase();
                    _item.LastName = dt.Rows[0]["l_name"].ToString().ToTitleCase();
                    _item.CityNameMailMerge = dt.Rows[0]["city"].ToString();
                    _item.StateNameMailMerge = dt.Rows[0]["state"].ToString();
                    _item.UserId = dt.Rows[0]["member_id"].ToString();
                    _item.Password = _crypt.DecryptStringAES(dt.Rows[0]["Password"].ToString());
                    _item.UserName = dt.Rows[0]["Username"].ToString();
                    _item.EmailAddress = dt.Rows[0]["EMail"].ToString();
                    _item.HourlyRate = dt.Rows[0]["rate"].ToString().Currency();
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _item.error_text = "Error:" + exception.Message;
            }
            return _item;
        }


        public OfferTechFilter GetOfferTechFilter(int id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            OfferTechFilter _item = new OfferTechFilter();
            SortedList _srt = new SortedList
            {
                { "@id", id }
            };
            try
            {
                var dt = sqlHelper.fillDataTable("GetOfferFiltertoprocess", string.Empty, _srt); sqlHelper.Dispose();
                if (dt.Rows.Count > 0)
                {
                    _item.TecF_amount = dt.Rows[0]["Rate"].ToString();
                    _item.TecF_Screening = dt.Rows[0]["Screening"].ToString();
                    _item.TecF_skiils = dt.Rows[0]["Skills"].ToString();
                    _item.TecF_tools = dt.Rows[0]["Tools"].ToString();
                    _item.TecF_Distance = Convert.ToInt32(dt.Rows[0]["Distance"].ToString());
                    _item.TecF_Assign = Convert.ToInt32(dt.Rows[0]["AssignPreference"].ToString());
                    _item.TecF_Distribution = dt.Rows[0]["Messagetype"].ToString();
                    _item.IsProcessed = Convert.ToBoolean(dt.Rows[0]["IsProcessed"].ToString());
                    _item.Template = Convert.ToInt32(dt.Rows[0]["Template"].ToString());
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _item;
        }

        public bool GetOfferTechFilterStatus(int id)
        {
            SqlHelper sqlHelper = new SqlHelper(); 
            SortedList _srt = new SortedList
            {
                { "@id", id }
            };
            try
            {
                bool IsProcessed = false;
                var dt = sqlHelper.fillDataTable("GetOfferTechFilterStatus", string.Empty, _srt); sqlHelper.Dispose();
                if (dt.Rows.Count > 0)
                {
                    IsProcessed = Convert.ToBoolean(dt.Rows[0]["IsProcessed"].ToString()); 
                }
                dt.Dispose();
                return IsProcessed;
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
                return false ;
            } 
        }

        public UserModel get_all_active_members_bymemberid(string id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            UserModel _item = new UserModel();
            SortedList _srt = new SortedList
            {
                { "@id", id }
            };
            try
            {
                var ds = sqlHelper.fillDataSet("tbl_job_offer_tbl_member_getmemberbymemberid", string.Empty, _srt); sqlHelper.Dispose();
                var dt = ds.Tables[0];
                ds.Dispose();
                if (dt.Rows.Count > 0)
                {
                    Crypto _crypt = new Crypto();
                    _item.Zipcode = dt.Rows[0]["Zip_code"].ToString();
                    _item.Address = dt.Rows[0]["address"].ToString();
                    _item.FirstName = dt.Rows[0]["f_name"].ToString().ToTitleCase();
                    _item.LastName = dt.Rows[0]["l_name"].ToString().ToTitleCase();
                    _item.CityNameMailMerge = dt.Rows[0]["city"].ToString();
                    _item.StateNameMailMerge = dt.Rows[0]["state"].ToString();
                    _item.UserId = dt.Rows[0]["member_id"].ToString();
                    _item.UserTableId = Convert.ToInt32(dt.Rows[0]["id"].ToString());
                    _item.Password = _crypt.DecryptStringAES(dt.Rows[0]["Password"].ToString());
                    _item.UserName = dt.Rows[0]["Username"].ToString();
                    _item.EmailAddress = dt.Rows[0]["EMail"].ToString();
                    _item.HourlyRate = dt.Rows[0]["rate"].ToString().Currency();
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _item.error_text = "Error:" + exception.Message;
            }
            return _item;
        }
        public bool validateJobId(string JobId, int workspaceid)
        {
            bool isavailable = true;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", JobId },
                    { "@workspaceid", workspaceid }
                };
                isavailable = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("validate_jobid", string.Empty, _srt).ToString()) > 0 ? false : true;
                sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return isavailable;
        }
        public bool validateJobId(int Id, string jobid)
        {
            bool isavailable = true;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", Id },
                    { "@jobid", jobid }
                };
                isavailable = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("validate_jobid_editjob", string.Empty, _srt).ToString()) > 0 ? false : true; sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return isavailable;
        }
        public void update_status(int statusId, int jobid, int userId)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@statusId", statusId },
                    { "@jobid", jobid }
                };
                sqlHelper.executeNonQuery("tbl_job_offer_update_status", string.Empty, _srt);
                sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
        }
        public string UpdateOffer(job _mdl, int userid)
        {
            string message = "Offer Updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            string _sdate = _mdl.sdate + " " + _mdl.stime;
            string _edate = _mdl.sdate + " " + _mdl.etime;
            TimeSpan span = Convert.ToDateTime(_edate) - Convert.ToDateTime(_sdate);
            var estimhour = Convert.ToDouble(String.Format("{0}.{1}", span.Hours, span.Minutes > 0 ? (int)Math.Round(Convert.ToDouble(span.Minutes) * 1.66) : span.Minutes));
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@tecrate", _mdl.Pay_rate },
                    { "@clientrate", _mdl.Client_rate },
                    { "@address", _mdl.street },
                    { "@MapValidateAddress", _mdl.MapValidateAddress },
                    { "@cityId", _mdl.city_id },
                    { "@StateId", _mdl.state_id },
                    { "@jobid", _mdl._jobid },
                    { "@ProjectId", _mdl._projectid },
                    { "@JobtitleId", _mdl.JobId },
                    { "@Ttile", _mdl.title },
                    { "@Client", _mdl.client_id },
                    { "@Technician", _mdl.Technician },
                    { "@Dispatcher", _mdl.Dispatcher },
                    { "@Project_manager", _mdl.mgr_id },///client manager
                    { "@Project_manager1", _mdl.mgr_id1 },
                    { "@startdate", _sdate },
                    { "@expense", _mdl.Expense },
                    { "@enddate", _edate },
                    { "@est_Hours", estimhour },
                    { "@Status_id", _mdl.Status },
                    { "@suite", _mdl.suite },
                    { "@Description", _mdl.description },
                    { "@Contact", _mdl.Contact },
                    { "@Phone", _mdl.Phone },
                    { "@Instruction", _mdl.Instruction },
                    { "@createdby", userid }
                };
                sqlHelper.executeNonQuery("UpdateOffer", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }
        public string update_job_byview(job_items1 _mdl, int userid)
        {
            string message = "Job Updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    //_srt.Add("@fname", _mdl.FirstName);
                    { "@JobId", _mdl.JobID },
                    { "@projectId", _mdl.projectId },
                    { "@Client_id", _mdl.Client_id },
                    { "@Client_rate", _mdl.Client_rate },
                    { "@Technicianid", _mdl.Technicianid },
                    { "@pay_rate", _mdl.pay_rate },
                    { "@mgr_id", _mdl.mgr_id },
                    { "@mgr_id1", _mdl.mgr_id1 },
                    { "@dispatcher", _mdl.Dispatcherid },
                    { "@expense", _mdl.Expense },
                    { "@Status_id", _mdl.Status }
                };
                sqlHelper.executeNonQuery("tbl_job_offer_updatenewjob", string.Empty, _srt);
                sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }
        public string update_job_byviewT(job_items1 _mdl)
        {
            string message = "Job Updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    //_srt.Add("@fname", _mdl.FirstName);
                    { "@JobId", _mdl.JobID },
                    { "@dispatcher", _mdl.Dispatcherid },
                    { "@rate", _mdl.pay_rate },
                    { "@expense", _mdl.Expense },
                    { "@Status_id", _mdl.Status }
                };
                sqlHelper.executeNonQuery("tbl_job_updatenewjobByT", string.Empty, _srt);
                sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }
        public string add_new_comment(string comment, int userid, int jobid, bool isadminpost = false, int commentId = 0, int workspaceid = 11)
        {
            string message = "Comment Added Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@isadminpost", isadminpost },
                    { "@commentid", commentId },
                    { "@JobId", jobid },
                    { "@comment", comment },
                    { "@createdby", userid },
                    { "@createddate", DateTime.UtcNow.AddHours(-6) }
                }; sqlHelper.executeNonQuery("tbl_job_comment_offer_addv2", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }

        public DateTime add_new_comment2(string comment, int userid, int jobid, ref string createdBy, bool isadminpost = false, int commentId = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            DateTime cst = DateTime.UtcNow.AddHours(-6);
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@isadminpost", isadminpost },
                    { "@commentid", commentId },
                    { "@JobId", jobid },
                    { "@comment", comment },
                    { "@createdby", userid },
                    { "@createddate",cst }
                };
                createdBy = sqlHelper.executeNonQueryWMessage("tbl_job_comment_offer_addv3", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return cst;
        }

        public string delete_job(int jobid)
        {
            string message = "Offer Deleted Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@JobId", jobid }
                };
                sqlHelper.executeNonQuery("tbl_job_offer_remove_v2", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                message = "Error: " + exception.Message;
                sqlHelper.Dispose();
            }
            return message;
        }


        public string delete_submitedoffer(int offerid)
        {
            string message = "Offer removed Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@JobId", offerid }
                };
                sqlHelper.executeNonQuery("tbl_offer_remove_v2", string.Empty, _srt);
                sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error: " + exception.Message;
            }
            return message;
        }
        public string re_submitedoffer(int offerid)
        {
            string message = "Offer re-submit Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@JobId", offerid);
                sqlHelper.executeNonQuery("tbl_offer_resubmit", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = $"Error : {exception.Message}";
            }
            return message;
        }

        public string re_acceptedoffer(int offerid)
        {
            string message = "Offer Accepted Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@JobId", offerid }
                };
                sqlHelper.executeNonQuery("tbl_offer_reaccpeted", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error :" + exception.Message;
            }
            return message;
        }

        public string assign_declineoffer(int offerid)
        {
            string message = "Offer declined Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@JobId", offerid }
                };
                sqlHelper.executeNonQuery("tbl_offer_declinejob", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error :" + exception.Message;
            }
            return message;
        }

        public string assign_submitedoffer(int offerid)
        {
            string message = "Offer assigned Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@JobId", offerid }
                };
                sqlHelper.executeNonQuery("tbl_offer_assignjob", string.Empty, _srt);
                sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error :" + exception.Message;
            }
            return message;
        }
        public string AddNewDocument(string FileTitle, int userid, int jobid, string Size, string url, int parentid = 0)
        {
            string message = "File Added Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@createddate", DateTime.UtcNow.AddHours(-6) },
                    { "@Filetile", FileTitle },
                    { "@Jobid", jobid },
                    { "@Size", Size },
                    { "@url", url },
                    { "@createdby", userid }
                };
                if (parentid != 0)
                {
                    _srt.Add("@parentid", parentid);
                }
                sqlHelper.executeNonQuery("tbl_job_offer_files_add", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }
        public string delete_doc(int id)
        {
            string message = "File Deleted Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", id }
                };
                sqlHelper.executeNonQuery("tbl_job_offerfiles_remove", string.Empty, _srt);
                sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }


        public string delete_docByParent(int id)
        {
            string message = "File Deleted Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", id }
                };
                sqlHelper.executeNonQuery("tbl_job_offerfiles_removeByParent", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }
        public string assignjob(int Jobid, int memberid, string rate)
        {
            string message;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", Jobid },
                    { "@memberid", memberid },
                    { "@payrate", rate }
                };
                message = sqlHelper.executeNonQueryWMessage("assignjobtonewmember", string.Empty, _srt).ToString();
                sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }

        public List<commentModel> getreply(int mainid)
        {
            List<commentModel> message = new List<commentModel>();
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", mainid }
                };
                var dt = sqlHelper.fillDataTable("tbl_notification_getmessagebyparentid", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    commentModel _item = new commentModel
                    {
                        createdBy = dt.Rows[i]["Createdby"].ToString(),
                        text = dt.Rows[i]["comment"].ToString(),
                        timedate = dt.Rows[i]["Createddate"].ToString()
                    };
                    message.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return message;
        }

        public jobmodal GetAllActiveOffersForCalender(string date = "", string Location = "", int cclient = 0, int cTechnician = 0, int cDispatcher = 0, int Status = 0, int workspaceid = 0, int project = 0,int projectmanagerId = 0, int clientManagerId = 0, int screening = 0, int[] _Skiils = null)
        {
            SqlHelper sqlHelper = new SqlHelper();
            jobmodal _mdl = new jobmodal();
            SortedList _srt = new SortedList();
            List<job_items> _jbs = new List<job_items>();
            try
            {
                _srt.Add("@workspaceid", workspaceid);
                if (!string.IsNullOrEmpty(date))
                {
                    var dates = date.Split('-');
                    _srt.Add("@startdate", dates[0]);
                    _srt.Add("@enddate", dates[1]);
                }
                if (!string.IsNullOrEmpty(Location))
                {
                    _srt.Add("@Location", Location);
                }
                if (cclient != 0)
                {
                    _srt.Add("@client", cclient);
                }
                if (project != 0)
                {
                    _srt.Add("@project", project);
                }
                if (clientManagerId != 0)
                {
                    _srt.Add("@clientManagerId", clientManagerId);
                }
                if (projectmanagerId != 0)
                {
                    _srt.Add("@projectmanagerId", projectmanagerId);
                }
                if (screening != 0)
                {
                    _srt.Add("@screening", screening);
                }
                if (_Skiils != null)
                {
                    var skill = string.Join<int>(",", _Skiils.Where(a => a != 0));
                    if (!string.IsNullOrEmpty(skill))
                    {
                        _srt.Add("@skills", skill);
                    }
                }
                if (cDispatcher != 0)
                {
                    _srt.Add("@Dispatcher", cDispatcher);
                }
                string sp = "GetAllActiveOffersForCalenderWithoutStatus";
                if (Status != 0)
                {
                    sp = "GetAllActiveOffersForCalenderWithStatus";
                    _srt.Add("@Status", Status);
                }
                var dt = sqlHelper.fillDataTable(sp, string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    job_items _item = new job_items
                    {
                        Job_ID = dt.Rows[i]["JobId"].ToString(),
                        cityname = dt.Rows[i]["city"].ToString(),
                        statename = dt.Rows[i]["state"].ToString(),
                        Title = dt.Rows[i]["Ttile"].ToString(),
                        Client = dt.Rows[i]["Client"].ToString().ToTitleCase(),
                        Technician = dt.Rows[i]["Technician"].ToString().ToTitleCase(),
                        Dispatcher = dt.Rows[i]["Dispatcher"].ToString().ToTitleCase(),
                        Project_manager = dt.Rows[i]["Project_manager"].ToString().ToTitleCase(),
                        sdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()).ToString("yyyy-MM-dd"),
                        stime = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()).ToString("hh:mm tt"),
                        etime = Convert.ToDateTime(dt.Rows[i]["enddate"].ToString()).ToString("hh:mm tt"),
                        Client_rate = dt.Rows[i]["Client_rate"].ToString().Currency(),
                        _sdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()),
                        _endate = Convert.ToDateTime(dt.Rows[i]["enddate"].ToString()),
                        Pay_rate = dt.Rows[i]["rate"].ToString().Currency(),
                        Est_hours = dt.Rows[i]["est_Hours"].ToString(),
                        hours = dt.Rows[i]["hours"].ToString(),
                        Expense = dt.Rows[i]["Expnese"].ToString().Currency(),
                        Status = dt.Rows[i]["Status_id"].ToString(),
                        JobID = Convert.ToInt32(dt.Rows[i]["id"].ToString())
                    };
                    _jbs.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _mdl.error_text = "Error:" + exception.Message;
            }
            _mdl._jobs = _jbs;
            return _mdl;
        }
        public List<job_items> GetAllActiveOffersByIndex(ref int total, int notificationUSER, int startindex, int endindex, string search = "", string date = "", string Location = "", int cclient = 0, int cTechnician = 0, int cDispatcher = 0, int Status = 0, int userid = 0, int workspaceid = 0, int projectid = 0,int projectmanagerId=0,int clientManagerId =0,int screening=0,int[] _Skiils=null)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList _srt = new SortedList();
            Crypto _crypt = new Crypto();
            List<job_items> _jbs = new List<job_items>();
            try
            {
                _srt.Add("@workspaceid", workspaceid);
                if (!string.IsNullOrEmpty(date))
                {
                    var dates = date.Split('-');
                    _srt.Add("@startdate", dates[0]);
                    _srt.Add("@enddate", dates[1]);
                }
                if (!string.IsNullOrEmpty(search))
                {
                    _srt.Add("@search", search);
                }
                _srt.Add("@startindex", startindex);
                _srt.Add("@endindex", endindex);
                if (userid != 0)
                {
                    _srt.Add("@id", userid);
                }
                if (projectid != 0)
                {
                    _srt.Add("@projectid", projectid);
                }

                if (cclient != 0)
                {
                    _srt.Add("@client", cclient);
                }
                if (cTechnician != 0)
                {
                    _srt.Add("@Technician", cTechnician);
                }
                if (cDispatcher != 0)
                {
                    _srt.Add("@Dispatcher", cDispatcher);
                }
                if (clientManagerId != 0)
                {
                    _srt.Add("@clientManagerId", clientManagerId);
                }
                if (projectmanagerId != 0)
                {
                    _srt.Add("@projectmanagerId", projectmanagerId);
                }
                if(screening!=0)
                {
                    _srt.Add("@screening", screening);
                }
                if (_Skiils != null)
                {
                    var skill = string.Join<int>(",", _Skiils.Where(a => a != 0));
                    if (!string.IsNullOrEmpty(skill))
                    {
                        _srt.Add("@skills", skill);
                    }
                }
                string sp = "GetAllActiveOffersByIndexWithoutStatus";
                if (Status != 0)
                {
                    sp = "GetAllActiveOffersByIndexWithStatus";
                    _srt.Add("@Status", Status);
                }
                if (!string.IsNullOrEmpty(Location))
                {
                    _srt.Add("@Location", Location);
                }
                
                notificationServices _nser = new notificationServices();
                var dt = sqlHelper.fillDataTable(sp, string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    job_items _item = new job_items
                    {
                        Job_ID = dt.Rows[i]["JobId"].ToString().Count() > 8 ? dt.Rows[i]["JobId"].ToString().Substring(0, 8) : dt.Rows[i]["JobId"].ToString(),
                        cityname = dt.Rows[i]["city"].ToString(),
                        statename = dt.Rows[i]["state"].ToString(),
                        Title = dt.Rows[i]["Ttile"].ToString().Count() > 30 ? dt.Rows[i]["Ttile"].ToString().Substring(0, 30) : dt.Rows[i]["Ttile"].ToString(),
                        Client = dt.Rows[i]["Client"].ToString().ToTitleCase(),
                        Dispatcher = dt.Rows[i]["Dispatcher"].ToString().ToTitleCase(),
                        sdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()).ToString("yyyy-MM-dd"),
                        stime = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()).ToString("hh:mm tt"),
                        etime = Convert.ToDateTime(dt.Rows[i]["enddate"].ToString()).ToString("hh:mm tt"),
                        Client_rate = dt.Rows[i]["Client_rate"].ToString().Currency(),
                        Pay_rate = dt.Rows[i]["rate"].ToString().Currency(),
                        Status = dt.Rows[i]["Status"].ToString(),
                        JobID = Convert.ToInt32(dt.Rows[i]["id"].ToString())
                    };
                    _item.view = GetViewCount(_item.JobID);
                    _item.accepted = GetacceptedCount(_item.JobID);
                    _item.token = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString());
                    _item.status_id = Convert.ToInt32(dt.Rows[i]["Status_id"].ToString());
                    _item.commentCount = _nser.GetNotificationCountByType(_item.JobID, NotificationType.Offer, notificationUSER);
                    total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _jbs.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _jbs;
        }
        public int GetViewCount(int jobid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            int count = 0;
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@jobid", jobid }
                };
                var dt = sqlHelper.fillDataTable("tbl_offer_GetViewCountByJobId", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    count = Convert.ToInt32(dt.Rows[0][0].ToString());
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return count;
        }

        public int GetacceptedCount(int jobid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            int count = 0;
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@jobid", jobid }
                };
                var dt = sqlHelper.fillDataTable("tbl_offer_GetAcceptedCountByJobId", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    count = Convert.ToInt32(dt.Rows[0][0].ToString());
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return count;
        }

        public List<job_items> GetAllActiveOfferByIndexByUser(ref int total, int notificationuser, int startindex, int endindex, string search = "", string date = "", string Location = "", int cclient = 0, int cTechnician = 0, int cDispatcher = 0, int Status = 0, int userid = 0, string membertype = "T", int workspaceid = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList _srt = new SortedList();
            Crypto _crypt = new Crypto();
            List<job_items> _jbs = new List<job_items>();
            try
            {
                _srt.Add("@workspaceid", workspaceid);
                if (!string.IsNullOrEmpty(date))
                {
                    var dates = date.Split('-');
                    _srt.Add("@startdate", dates[0]);
                    _srt.Add("@enddate", dates[1]);
                }
                if (!string.IsNullOrEmpty(search))
                {
                    _srt.Add("@search", search);
                }
                _srt.Add("@startindex", startindex);
                _srt.Add("@endindex", endindex);
                if (userid != 0)
                {
                    _srt.Add("@id", userid);
                }
                if (!string.IsNullOrEmpty(Location))
                {
                    _srt.Add("@Location", Location);
                }
                if (cclient != 0)
                {
                    _srt.Add("@client", cclient);
                }
                if (cTechnician != 0)
                {
                    _srt.Add("@Technician", cTechnician);
                }
                if (cDispatcher != 0)
                {
                    _srt.Add("@Dispatcher", cDispatcher);
                }
                string sp = "GetAllActiveOfferByIndexByUserWithoutStatus";
                if (Status != 0)
                {
                    sp = "GetAllActiveOfferByIndexByUserWithStatus";
                    _srt.Add("@Status", Status);
                }
                notificationServices _nser = new notificationServices();
                var dt = sqlHelper.fillDataTable(sp, string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    job_items _item = new job_items
                    {
                        Job_ID = dt.Rows[i]["JobId"].ToString(),
                        cityname = dt.Rows[i]["city"].ToString(),
                        statename = dt.Rows[i]["state"].ToString(),
                        Title = dt.Rows[i]["Ttile"].ToString(),
                        Client = dt.Rows[i]["Client"].ToString().ToTitleCase(),
                        Technician = dt.Rows[i]["Technician"].ToString().ToTitleCase(),
                        Dispatcher = dt.Rows[i]["Dispatcher"].ToString().ToTitleCase(),
                        Project_manager = dt.Rows[i]["Project_manager"].ToString().ToTitleCase(),
                        sdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()).ToString("yyyy-MM-dd"),
                        stime = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()).ToString("hh:mm tt"),
                        etime = Convert.ToDateTime(dt.Rows[i]["enddate"].ToString()).ToString("hh:mm tt"),
                        Client_rate = membertype == "ST" ? "XXXXXX" : dt.Rows[i]["Client_rate"].ToString().Currency(),
                        _sdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()),
                        _endate = Convert.ToDateTime(dt.Rows[i]["enddate"].ToString()),
                        Pay_rate = membertype == "ST" ? "XXXXXX" : dt.Rows[i]["rate"].ToString().Currency(),
                        Est_hours = dt.Rows[i]["est_Hours"].ToString(),
                        hours = dt.Rows[i]["hours"].ToString(),
                        Expense = dt.Rows[i]["Expnese"].ToString().Currency(),
                        status_id = Convert.ToInt32(dt.Rows[i]["Status_id"].ToString()),
                        JobID = Convert.ToInt32(dt.Rows[i]["id"].ToString())
                    };
                    _item.Status = getofferstatusid(_item.JobID, cTechnician);
                    _item.token = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString());
                    total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _item.commentCount = _nser.GetNotificationCountByType(_item.JobID, NotificationType.Offer, notificationuser);
                    _jbs.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _jbs;
        }



        public string getofferstatusid(int jobid, int memberid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            int count = 0;
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@jobid", jobid },
                    { "@memberid", memberid }
                };
                count = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("GetoffernameByid", string.Empty, _srt).ToString()); sqlHelper.Dispose(); 
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            } 
            return getstatus(count);
        }
        private string getstatus(int id)
        {
            string status = string.Empty;
            switch (id)
            {
                case 1:
                    status = "Available";
                    break;
                case 2:
                    status = "Declined";
                    break;
                case 3:
                    status = "Accepted";
                    break;
                case 4:
                    status = "Standby";
                    break;
                case 5:
                    status = "Cancelled";
                    break;
            } 
            return status; 
        } 

        public jobmodal GetAllActiveOfferByUserIdForCalender(int id, string date, string Location = "", int cclient = 0, int cTechnician = 0, int cDispatcher = 0, int Status = 0, int workspaceid = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            jobmodal _mdl = new jobmodal();
            SortedList _srt = new SortedList(); 
            List<job_items> _jbs = new List<job_items>();
            try
            {
                if (!string.IsNullOrEmpty(date))
                {
                    var dates = date.Split('-');
                    _srt.Add("@startdate", dates[0]);
                    _srt.Add("@enddate", dates[1]);
                }
                if (!string.IsNullOrEmpty(Location))
                {
                    _srt.Add("@Location", Location);
                }
                if (cclient != 0)
                {
                    _srt.Add("@client", cclient);
                }
                if (cTechnician != 0)
                {
                    _srt.Add("@Technician", cTechnician);
                }
                if (cDispatcher != 0)
                {
                    _srt.Add("@Dispatcher", cDispatcher);
                }
                _srt.Add("@workspaceid", workspaceid);
                string sp = "GetAllActiveOfferByUserIdForCalenderWithoutStatus";
                if (Status != 0)
                {
                    sp = "GetAllActiveOfferByUserIdForCalenderWithStatus";
                    _srt.Add("@Status", Status);
                }
                if (id != 0)
                {
                    _srt.Add("@id", id);
                }
                var dt = sqlHelper.fillDataTable(sp, string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    job_items _item = new job_items
                    {
                        Job_ID = dt.Rows[i]["JobId"].ToString(),
                        cityname = dt.Rows[i]["city"].ToString(),
                        statename = dt.Rows[i]["state"].ToString(),
                        Title = dt.Rows[i]["Ttile"].ToString(),
                        Client = dt.Rows[i]["Client"].ToString().ToTitleCase(),
                        Technician = dt.Rows[i]["Technician"].ToString().ToTitleCase(),
                        Dispatcher = dt.Rows[i]["Dispatcher"].ToString().ToTitleCase(),
                        Project_manager = dt.Rows[i]["Project_manager"].ToString().ToTitleCase(),
                        sdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()).ToString("yyyy-MM-dd hh:mm tt"),
                        endate = Convert.ToDateTime(dt.Rows[i]["enddate"].ToString()).ToString("yyyy-MM-dd hh:mm tt"),
                        Client_rate = dt.Rows[i]["Client_rate"].ToString().Currency(),
                        Pay_rate = dt.Rows[i]["rate"].ToString().Currency(),
                        Est_hours = dt.Rows[i]["est_Hours"].ToString(),
                        hours = dt.Rows[i]["hours"].ToString(),
                        Expense = dt.Rows[i]["Expnese"].ToString().Currency(),
                        Status = dt.Rows[i]["Status_id"].ToString(),
                        JobID = Convert.ToInt32(dt.Rows[i]["id"].ToString()),
                        _sdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString()),
                        _endate = Convert.ToDateTime(dt.Rows[i]["enddate"].ToString())
                    };

                    _jbs.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                          _mdl.error_text = "Error:" + exception.Message;
            } 
            _mdl._jobs = _jbs;
            return _mdl;
        } 
        public job_items1 GetAllActiveOfferById(int id, int viewUserId, string subContractorType, ref int offerId, ref bool isOffered, int techId = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            job_items1 _item = new job_items1(); 
            Crypto _crypt = new Crypto(); 
            List<commentmodal> _commnts = new List<commentmodal>();
            List<FileModel> _files = new List<FileModel>();
            List<offermodal> _offers = new List<offermodal>();
            notificationServices _nser = new notificationServices();
            int acceptcount = 0;
            try
            {//offercount
                SortedList _srt = new SortedList
                {
                    { "@jobid", id },
                    { "@userid", viewUserId }
                };
                var ds = sqlHelper.fillDataSet("GetAllActiveOfferById", string.Empty, _srt); sqlHelper.Dispose();
                var dt = ds.Tables[0];
                var dt1 = ds.Tables[1];
                var dt2 = ds.Tables[2];
                var dt3 = ds.Tables[3];
                var dt4 = ds.Tables[4];
                ds.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _item.Job_ID = dt.Rows[0]["JobId"].ToString();
                    _item.Title = dt.Rows[0]["Ttile"].ToString();
                    _item.Client = dt.Rows[0]["Client"].ToString();
                    _item.MapValidateAddress = dt.Rows[0]["address"].ToString().GetAddressView(dt.Rows[0]["SuiteNumber"].ToString());
                    _item.token = _crypt.EncryptStringAES(dt.Rows[0]["id"].ToString());
                    _item.distoken = _crypt.EncryptStringAES(dt.Rows[0]["Dispatcherid"].ToString());
                    _item.Client_id = Convert.ToInt32(dt.Rows[0]["Client"].ToString());
                    _item.projectId = Convert.ToInt32(dt.Rows[0]["Projectid"].ToString());
                    _item.mgr_id = Convert.ToInt32(dt.Rows[0]["Project_manager"].ToString());
                    _item.mgr_id1 = Convert.ToInt32(dt.Rows[0]["Project_manager1"].ToString());
                    _item.Technician = dt.Rows[0]["Technician"].ToString();
                    _item.Dispatcher = dt.Rows[0]["Dispatcher"].ToString();
                    _item.Project_manager = dt.Rows[0]["Project_manager"].ToString();
                    _item.sdate = Convert.ToDateTime(dt.Rows[0]["startdate"].ToString()).ToString("yyyy-MM-dd");
                    _item.stime = Convert.ToDateTime(dt.Rows[0]["startdate"].ToString()).ToString("hh:mm tt");
                    _item.etime = Convert.ToDateTime(dt.Rows[0]["enddate"].ToString()).ToString("hh:mm tt");
                    _item.Client_rate = subContractorType == "ST" ? "XXXXXX" : dt.Rows[0]["Client_rate"].ToString().Currency();
                    _item.Expense = dt.Rows[0]["expense"].ToString().Currency();
                    _item.Est_hours = dt.Rows[0]["est_Hours"].ToString();
                    _item.Status = Convert.ToInt32(dt.Rows[0]["Status_id"].ToString());
                    _item.JobID = Convert.ToInt32(dt.Rows[0]["id"].ToString());
                    _item.description = dt.Rows[0]["description"].ToString();
                    _item.pay_rate = subContractorType == "ST" ? "XXXXXX" : dt.Rows[0]["pay_rate"].ToString().Currency();
                    _item.Technicianid = Convert.ToInt32(dt.Rows[0]["Technicianid"].ToString());
                    _item.Dispatcherid = Convert.ToInt32(dt.Rows[0]["Dispatcherid"].ToString());
                    _item.Contact = dt.Rows[0]["Contact"].ToString();
                    _item.Phone = dt.Rows[0]["Phone"].ToString();
                    _item.Instruction = dt.Rows[0]["Instruction"].ToString();
                   
                }
                dt.Dispose();
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    commentmodal _mdl = new commentmodal
                    {
                        Comment = dt1.Rows[i]["Comment"].ToString(),
                        createdby = dt1.Rows[i]["createdby"].ToString(),
                        CreatedDate = dt1.Rows[i]["CreatedDate"].ToString(),
                        Commentby = Convert.ToInt32(dt1.Rows[i]["commentby"].ToString()),
                        Commentto = Convert.ToInt32(dt1.Rows[i]["CommentTo"].ToString()),
                        isadminpost = Convert.ToBoolean(dt1.Rows[i]["IsAdminPost"].ToString()),
                        Commentid = Convert.ToInt32(dt1.Rows[i]["commentid"].ToString()),
                        Parentid = Convert.ToInt32(dt1.Rows[i]["parentid"].ToString()),
                        isread = dt1.Rows[i]["readusers"].ToString().Contains(viewUserId.ToString())
                    };
                    if (_mdl.Commentby == viewUserId)
                    {
                        _mdl.isread = true;
                    }
                    _commnts.Add(_mdl);
                }
                dt1.Dispose();
                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    FileModel _mdl = new FileModel
                    {
                        FileTitle = dt2.Rows[i]["Filetile"].ToString(),
                        FileId = Convert.ToInt32(dt2.Rows[i]["id"].ToString()),
                        Size = dt2.Rows[i]["Size"].ToString(),
                        Url = dt2.Rows[i]["url"].ToString(),
                        CreatedBy = dt2.Rows[i]["createdby"].ToString(),
                        CreatedDate = dt2.Rows[i]["Createddate"].ToString(),
                        FileToken = _crypt.EncryptStringAES(dt2.Rows[i]["id"].ToString())
                    };
                    _files.Add(_mdl);
                }
                dt2.Dispose();
                for (int i = 0; i < dt3.Rows.Count; i++)
                {
                    offermodal _mdl = new offermodal
                    {
                        id = Convert.ToInt32(dt3.Rows[i]["Id"].ToString()),
                        jobid = Convert.ToInt32(dt3.Rows[i]["JobId"].ToString()),
                        offertoken = _crypt.EncryptStringAES(dt3.Rows[i]["Id"].ToString()),
                        jobtoken = _crypt.EncryptStringAES(dt3.Rows[i]["JobId"].ToString()),
                        jobindexid = dt3.Rows[i]["jobIndexId"].ToString(),
                        rate = dt3.Rows[i]["pay_rate"].ToString().Currency(),
                        state = dt3.Rows[i]["state"].ToString(),
                        status = dt3.Rows[i]["statusName"].ToString(),
                        tech = dt3.Rows[i]["Technician"].ToString().ToTitleCase(),
                        city = dt3.Rows[i]["city"].ToString(),
                        createddate = Convert.ToDateTime(dt3.Rows[i]["createddate"]).ToString("yyyy-MM-dd hh:mm tt")
                    };
                    _offers.Add(_mdl);
                    if (techId == Convert.ToInt32(Convert.ToInt32(dt3.Rows[i]["techId"].ToString())))
                    {
                        offerId = _mdl.id;
                    }
                    if (_mdl.status.ToLower() == "accepted")
                    {
                        acceptcount = acceptcount + 1;
                    }
                }
                dt3.Dispose();
                isOffered = Convert.ToInt32(dt4.Rows[0]["offercount"].ToString()) > 0;
                dt4.Dispose();
                _item._Comments = _commnts;
                _item._files = _files;
                _item._offers = _offers;
                _item.acceptedCount = acceptcount;
                _item.msgcount = _nser.GetNotificationCountByType(_item.JobID, NotificationType.Offer, viewUserId); 
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _item.error_text = "Error :" + exception.Message;
            }
            List<int> Tools = new List<int>();   List<int> Skiils = new List<int>();
            GetToolsSkillsByOfferId(id, ref Tools,ref Skiils);
            _item.tools = Tools.ToArray();
            _item.skills = Skiils.ToArray();
            return _item;
        }
        public void GetToolsSkillsByOfferId(int OfferId, ref List<int> Tools, ref List<int> Skiils)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@OfferId", OfferId }
                };
                var ds = sqlHelper.fillDataSet("GetToolsSkillsByOfferId", string.Empty, _srt); sqlHelper.Dispose();
                var toolsdt = ds.Tables[0];
                var skillsdt1 = ds.Tables[1];
                ds.Dispose();
                if (toolsdt.Rows.Count > 0)
                {
                    for (int i = 0; i < toolsdt.Rows.Count; i++)
                    {
                        Tools.Add(Convert.ToInt32(toolsdt.Rows[i][0].ToString()));
                    }
                }
                toolsdt.Dispose();
                if (skillsdt1.Rows.Count > 0)
                {
                    for (int i = 0; i < skillsdt1.Rows.Count; i++)
                    {
                        Skiils.Add(Convert.ToInt32(skillsdt1.Rows[i][0].ToString()));
                    }
                }
                skillsdt1.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
        }
        public List<offermodal> get_all_active_offer_bypaging(ref int total, int startindex = 0, int endindex = 25, string search = "", int jobid = 0)
        {
            SqlHelper sqlHelper = new SqlHelper(); 
            List<offermodal> _model = new List<offermodal>();
            Crypto _crypt = new Crypto();
            SortedList _srtlist = new SortedList
            {
                { "@startindex", startindex },
                { "@endIndex", endindex },
                { "@jobid", jobid }
            }; 
            if (!string.IsNullOrEmpty(search))
            {
                _srtlist.Add("@search", search);
            }
            try
            {
                var dt3 = sqlHelper.fillDataTable("GetAdminofferbypaging", string.Empty, _srtlist); sqlHelper.Dispose();
                for (int i = 0; i < dt3.Rows.Count; i++)
                {
                    offermodal _mdl = new offermodal
                    {
                        id = Convert.ToInt32(dt3.Rows[i]["Id"].ToString()),
                        jobid = Convert.ToInt32(dt3.Rows[i]["JobId"].ToString()),
                        offertoken = _crypt.EncryptStringAES(dt3.Rows[i]["Id"].ToString()),
                        jobtoken = _crypt.EncryptStringAES(dt3.Rows[i]["JobId"].ToString()),
                        jobindexid = dt3.Rows[i]["jobIndexId"].ToString(),
                        rate = dt3.Rows[i]["pay_rate"].ToString().Currency(),
                        state = dt3.Rows[i]["state"].ToString(),
                        status = dt3.Rows[i]["statusName"].ToString(),
                        tech = dt3.Rows[i]["Technician"].ToString().ToTitleCase(),
                        city = dt3.Rows[i]["city"].ToString(),
                        createddate = Convert.ToDateTime(dt3.Rows[i]["createddate"]).ToString("yyyy-MM-dd hh:mm tt")
                    }; 
                    _model.Add(_mdl);
                }
                dt3.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
                } 
            return _model;
        } 
        public job GetAllActiveOfferDetailsById(int id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            job _item = new job();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@jobid", id }
                };
                Crypto _crypt = new Crypto();
                var dt = sqlHelper.fillDataTable("GetAllActiveOfferDetailsById", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _item.JobId = dt.Rows[0]["JobId"].ToString();
                    _item.title = dt.Rows[0]["Ttile"].ToString();
                    _item.client_id = Convert.ToInt32(dt.Rows[0]["Client"].ToString());
                    _item._projectid = Convert.ToInt32(dt.Rows[0]["Projectid"].ToString());
                    _item.mgr_id = Convert.ToInt32(dt.Rows[0]["Project_manager"].ToString());
                    _item.mgr_id1 = Convert.ToInt32(dt.Rows[0]["Project_manager1"].ToString());
                    _item.sdate = Convert.ToDateTime(dt.Rows[0]["startdate"].ToString()).ToString("yyyy-MM-dd");
                    _item.edate = Convert.ToDateTime(dt.Rows[0]["enddate"].ToString()).ToString("yyyy-MM-dd");
                    _item.stime = Convert.ToDateTime(dt.Rows[0]["startdate"].ToString()).ToString("hh:mm tt");
                    _item.etime = Convert.ToDateTime(dt.Rows[0]["enddate"].ToString()).ToString("hh:mm tt");
                    _item.Est_hours = Convert.ToDouble(dt.Rows[0]["est_Hours"].ToString());
                    _item.Status = Convert.ToInt32(dt.Rows[0]["Status_id"].ToString());
                    _item._jobid = id;
                    _item.description = dt.Rows[0]["Description"].ToString();
                    _item.Technician = Convert.ToInt32(dt.Rows[0]["Technician"].ToString());
                    _item.Dispatcher = Convert.ToInt32(dt.Rows[0]["Dispatcher"].ToString());
                    _item.Expense = dt.Rows[0]["Expnese"].ToString().Currency();
                    _item.suite = dt.Rows[0]["SuiteNumber"].ToString();
                    _item.Client_rate = dt.Rows[0]["Client_rate"].ToString().Currency();
                    _item.Pay_rate = dt.Rows[0]["payrate"].ToString().Currency();
                    _item.MapValidateAddress = dt.Rows[0]["address"].ToString();
                    _item.token = _crypt.EncryptStringAES(id.ToString());
                    _item.Contact = dt.Rows[0]["Contact"].ToString();
                    _item.Phone = dt.Rows[0]["Phone"].ToString();
                    _item.Instruction = dt.Rows[0]["Instruction"].ToString();
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _item.error_text = "Error:" + exception.Message; 
            } 
            return _item;
        }


        public job GetMailMergeJobDetailsById(int id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            job _item = new job();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@jobid", id }
                };
                var dt = sqlHelper.fillDataTable("tbl_job_offer_getfulljobsbyid", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _item.JobId = dt.Rows[0]["JobId"].ToString();
                    _item.title = dt.Rows[0]["Ttile"].ToString();
                    _item.sdate = Convert.ToDateTime(dt.Rows[0]["startdate"].ToString()).ToString("yyyy-MM-dd");
                    _item.datetime = Convert.ToDateTime(dt.Rows[0]["startdate"].ToString()).ToString("yyyy-MM-dd hh:mm tt");
                    _item.stime = Convert.ToDateTime(dt.Rows[0]["startdate"].ToString()).ToString("hh:mm tt");
                    _item.etime = Convert.ToDateTime(dt.Rows[0]["enddate"].ToString()).ToString("hh:mm tt");
                    _item.description = dt.Rows[0]["Description"].ToString();
                    _item.Pay_rate = dt.Rows[0]["payrate"].ToString().Currency();
                    _item.street = dt.Rows[0]["address"].ToString();
                    _item.city = dt.Rows[0]["cityname"].ToString();
                    _item.state = dt.Rows[0]["statename"].ToString();
                    _item.zip = dt.Rows[0]["zipcode"].ToString();
                    _item.address = _item.street.GetAddress(_item.city, _item.state, _item.zip, dt.Rows[0]["SuiteNumber"].ToString());
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _item.error_text = "Error:" + exception.Message;
            } 
            return _item;
        }


        public job GetMailMergeOnlyJobDetailsById(int id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            job _item = new job();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@jobid", id }
                };
                var dt = sqlHelper.fillDataTable("tbl_job_getfulljobsbyid_v1", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _item.JobId = dt.Rows[0]["JobId"].ToString();
                    _item.title = dt.Rows[0]["Ttile"].ToString();
                    _item.sdate = Convert.ToDateTime(dt.Rows[0]["startdate"].ToString()).ToString("yyyy-MM-dd");
                    _item.datetime = Convert.ToDateTime(dt.Rows[0]["startdate"].ToString()).ToString("yyyy-MM-dd hh:mm tt");
                    _item.stime = Convert.ToDateTime(dt.Rows[0]["startdate"].ToString()).ToString("hh:mm tt");
                    _item.etime = Convert.ToDateTime(dt.Rows[0]["enddate"].ToString()).ToString("hh:mm tt");
                    _item.description = dt.Rows[0]["Description"].ToString();
                    _item.Pay_rate = dt.Rows[0]["payrate"].ToString().Currency();
                    _item.street = dt.Rows[0]["address"].ToString();
                    _item.city = dt.Rows[0]["cityname"].ToString();
                    _item.state = dt.Rows[0]["statename"].ToString();
                    _item.zip = dt.Rows[0]["zipcode"].ToString();
                    _item.Contact = dt.Rows[0]["Contact"].ToString();
                    _item.Phone = dt.Rows[0]["Phone"].ToString();
                    _item.Instruction = dt.Rows[0]["Instruction"].ToString();
                    _item.address = _item.street.GetAddress(_item.city, _item.state, _item.zip, dt.Rows[0]["SuiteNumber"].ToString());
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _item.error_text = "Error:" + exception.Message;
            } 
            return _item;
        }
    }
}