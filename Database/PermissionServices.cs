﻿using hrm.Models;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class PermissionServices
    { 
        public List<PermissionRoleCountModal> GetPermissionRoleByCount(int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            PermissionRoleModal _mdl = new PermissionRoleModal();
            List<PermissionRoleCountModal> _model = new List<PermissionRoleCountModal>();
            try
            {
                SortedList _list = new SortedList
                {
                    { "@workspaceid", workspaceid }
                };
                var dt = sqlHelper.fillDataTable("GetPermissionRoleByCount", string.Empty, _list); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    PermissionRoleCountModal _item = new PermissionRoleCountModal
                    {
                        name = dt.Rows[i]["name"].ToString(),
                        y = Convert.ToInt32(dt.Rows[i]["count"].ToString()),
                        color = dt.Rows[i]["color"].ToString()
                    };
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _mdl.error_text = "Error:" + exception.Message;
            } 
            return _model;
        } 
        public PermissionRoleModal GetAllActivePermissionRoles(int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            PermissionRoleModal _mdl = new PermissionRoleModal();
            List<PermissionRoleItems> _model = new List<PermissionRoleItems>();
            SortedList _srt = new SortedList
            {
                { "@workspaceid", workspaceid }
            };
            try
            {
                var dt = sqlHelper.fillDataTable("GetAllActivePermissionRoles", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    PermissionRoleItems _item = new PermissionRoleItems
                    {
                        Name = dt.Rows[i]["name"].ToString(),
                        PermissionRoleId = Convert.ToInt32(dt.Rows[i]["id"].ToString())
                    };
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                 _mdl.error_text = "Error:" + exception.Message;
            } 
            _mdl._PermissionRoles = _model;
            return _mdl;
        }

        public Dictionary<int,string> GetAllActivePermissionRolesDropDown(int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            PermissionRoleModal _mdl = new PermissionRoleModal();
            Dictionary<int, string> _model = new Dictionary<int, string>();
            SortedList _srt = new SortedList
            {
                { "@workspaceid", workspaceid }
            };
            try
            {
                var dt = sqlHelper.fillDataTable("GetAllActivePermissionRoles", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                { 
                    _model.Add(Convert.ToInt32(dt.Rows[i]["id"].ToString()), dt.Rows[i]["name"].ToString());
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _mdl.error_text = "Error:" + exception.Message;
            }
           
            return _model;
        }
        public PermissionRoleModal GetAllActivePermissionRole(int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            PermissionRoleModal _mdl = new PermissionRoleModal();
            List<PermissionRoleItems> _model = new List<PermissionRoleItems>();
            SortedList _srt = new SortedList
            {
                { "@workspcaeid", workspaceid }
            };
            try
            {
                var dt = sqlHelper.fillDataTable("GetAllActivePermissionRole",string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    PermissionRoleItems _item = new PermissionRoleItems
                    {
                        Name = dt.Rows[i]["name"].ToString(),
                        PermissionRoleId = Convert.ToInt32(dt.Rows[i]["id"].ToString()),
                        StatusId = Convert.ToInt32(dt.Rows[i]["statusid"].ToString()),
                        Status = dt.Rows[i]["status"].ToString()
                    };
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                 _mdl.error_text = "Error:" + exception.Message;
            } 
            _mdl._PermissionRoles = _model;
            return _mdl;
        }
         
        public List<PermissionsItem> GetAllActivePermissionsWithType()
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<PermissionsItem> _mdl = new List<PermissionsItem>();
            try
            {
                var dt = sqlHelper.fillDataTable("GetAllActivePermissionsWithType", string.Empty, null); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    PermissionsItem _itm = new PermissionsItem
                    {
                        name = dt.Rows[i]["name"].ToString(),
                        type = dt.Rows[i]["type"].ToString(),
                        id = Convert.ToInt32(dt.Rows[i]["id"].ToString()),
                        Subtype = dt.Rows[i]["Subtype"].ToString()
                    };              // RoleItems _item = new RoleItems();
                    _mdl.Add(_itm);
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
                           } 
            return _mdl;
        }
        public string AddPermissionRole(PermissionRoleModal model, int userid, int workspaceid)
        {
            string message;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@PermissionRoleName", model.PermissionRole_name },
                    { "@statusid", model.statusid },
                    { "@workspaceid", workspaceid },
                    { "@createdby", userid }
                };
                message = sqlHelper.executeNonQueryWMessage("AddPermissionRole", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                  message = "Error:" + exception.Message;
            } 
            return message;
        }

        //AddSuperAdminRole
        public void add_newSuperAdminwith_role(int roleid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@roleid", roleid }
                };
                sqlHelper.executeNonQuery("AddSuperAdminRole", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
        }
        public void add_newAdminwith_role(int roleid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@roleid", roleid }
                };
                sqlHelper.executeNonQuery("AddAdminRole", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose(); 
            } 
        }
        public void add_newProjectmanagerwith_role(int roleid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@roleid", roleid }
                };
                sqlHelper.executeNonQuery("AddProjectManagerRole", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose(); 
            } 
        }


        public void add_newDispatchwith_role(int roleid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@roleid", roleid }
                };
                sqlHelper.executeNonQuery("AdddispatchRole", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose(); 
            } 
        }

        public void add_newTechwith_role(int roleid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@roleid", roleid }
                };
                sqlHelper.executeNonQuery("AddtechRole", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose(); 
            } 

        }

        public int add_new_role(string role_name, int workspaceid)
        {
            int roleid = 0;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@rolename", role_name },
                    { "@workspaceid", workspaceid }
                };
                roleid = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("tbl_role_addnew_v2", string.Empty, _srt).ToString());
                sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose(); 
            }
            return roleid;
        } 
        public string UpdatePermissionRole(PermissionRoleModal model)
        {
            string message ;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@PermissionRoleName", model.PermissionRole_name },
                    { "@statusid", model.statusid },
                    { "@PermissionRoleId", model.PermissionRole_id }
                };
                message = sqlHelper.executeNonQueryWMessage("UpdatePermissionRole", string.Empty, _srt).ToString();
                sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose(); 
                message = "Error:" + exception.Message;
            } 
            return message;
        } 
        public string DeletePermissionsRole(int PermissionRoleId)
        {
            string message;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@PermissionRoleId", PermissionRoleId }
                };
                message = sqlHelper.executeNonQueryWMessage("DeletePermissionsRole", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                 message = "Error:" + exception.Message;
            } 
            return message;
        }
        
        public List<int> GetAllActivePermissionsByPermissionRoleId(int PermissionRoleId, ref string name)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<int> _mdl = new List<int>();
            SortedList _srt = new SortedList();
            int id;
            try
            {
                _srt.Add("@PermissionRoleId", PermissionRoleId);
                var ds = sqlHelper.fillDataSet("GetAllActivePermissionsByPermissionRoleId", string.Empty, _srt); sqlHelper.Dispose();
                var dt = ds.Tables[0];
                var dt1 = ds.Tables[1];
                ds.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    id = Convert.ToInt32(dt.Rows[i]["PermissionId"].ToString());
                    _mdl.Add(id);
                }
                dt.Dispose();
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    name = dt1.Rows[i]["PermissionRoleName"].ToString();
                } 
                dt1.Dispose(); 
            }
            catch (Exception)
            {
                sqlHelper.Dispose(); 
            }
            return _mdl;
        }

        public List<int> GetAllMenuPermissionsByPermissionRoleId(int PermissionRoleId, int UserId, ref string Usertype)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<int> _mdl = new List<int>();
            SortedList _srt = new SortedList();
            int id;
            try
            {
                _srt.Add("@PermissionRoleId", PermissionRoleId);
                _srt.Add("@UserId", UserId);
                var ds = sqlHelper.fillDataSet("GetAllMenuPermissionsByPermissionRoleId", string.Empty, _srt); sqlHelper.Dispose();
                var dt = ds.Tables[0];
                var dt1 = ds.Tables[1];
                ds.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    id = Convert.ToInt32(dt.Rows[i]["PermissionId"].ToString());
                    _mdl.Add(id);
                }
                dt.Dispose();
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    Usertype = dt1.Rows[i]["type"].ToString();
                }
                dt1.Dispose(); 
            }
            catch (Exception)
            {
                sqlHelper.Dispose(); 
            }
            return _mdl;
        } 

        public List<int> GetAllMenuPermissionsByPermissionRoleId(int PermissionRoleId)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<int> _mdl = new List<int>();
            SortedList _srt = new SortedList();
            int id ;
            try
            {
                _srt.Add("@PermissionRoleId", PermissionRoleId); 
                var dt = sqlHelper.fillDataTable("GetAllMenuPermissionsByPermissionRoleId_v2", string.Empty, _srt); sqlHelper.Dispose(); 
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    id = Convert.ToInt32(dt.Rows[i]["PermissionId"].ToString());
                    _mdl.Add(id);
                }
                dt.Dispose();  
            }
            catch (Exception)
            {
                sqlHelper.Dispose(); 
            }
            return _mdl;
        }  
        public Dictionary<string, bool> NotificationPermissionByRoleId(int roleid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            Dictionary<string, bool> _permissions = new Dictionary<string, bool>();
            SortedList _srt = new SortedList();
            try
            {
                _srt.Add("@roleid", roleid);
                var dt = sqlHelper.fillDataTable("NotificationPermissionByRoleId", string.Empty, _srt);
                sqlHelper.Dispose();
                _permissions.Add("isadmin", Convert.ToBoolean(dt.Rows[0]["admin"].ToString()));
                _permissions.Add("billing", Convert.ToBoolean(dt.Rows[0]["billing"].ToString()));
                _permissions.Add("pricing", Convert.ToBoolean(dt.Rows[0]["pricing"].ToString()));
                _permissions.Add("resuces", Convert.ToBoolean(dt.Rows[0]["Resource"].ToString()));
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            } 
            return _permissions;
        }
        public string UpdatePermissionRule(int[] permissionids, int RuleId)
        {
            string message ;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@permissionids", string.Join<int>(",", permissionids) },
                    { "@RuleId", RuleId }
                };
                message = sqlHelper.executeNonQueryWMessage("UpdatePermissionRule", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            } 
            return message;
        }

        public string DeletePermissionRule(int RuleId)
        {
            string message ;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@RuleId", RuleId }
                };
                message = sqlHelper.executeNonQueryWMessage("DeletePermissionRule", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                 message = "Error:" + exception.Message;
            } 
            return message;
        }

    }
}