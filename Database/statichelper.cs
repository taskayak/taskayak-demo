﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public static class statichelper
    {
        public static string GetUserStatusById(this int value)
        {
            string Status = "Active";
            switch (value)
            {
                case 2:
                    Status = "Inactive";
                    break;
                case 1:
                case 1002:
                case 1003:
                case 1004:
                case 1005:
                case 1007:
                case 1009:
                    Status = "Active";
                    break;
            }
            return Status;
        } 
        public static string PhoneNumber(this string value)
        {
            string oldvalue = value;
            if (!string.IsNullOrEmpty(value))
            {
                value = value.Replace("-", "").Replace("+", "").Replace(".", "").Replace("(", "").Replace(")", ""); 
                value = new System.Text.RegularExpressions.Regex(@"\D").Replace(value, string.Empty); 
                if (string.IsNullOrEmpty(value))
                {
                    value = oldvalue;
                }
                else
                {
                    if (value.Length > 10) { 
                        value = value.TrimStart('1');
                     }
                    if (value.Length < 10)
                    {
                        value = "###-###-####";
                    }
                    else
                    { 
                        if (value.Length == 7)
                            return Convert.ToInt64(value).ToString("###-####");
                        if (value.Length == 10)
                            return Convert.ToInt64(value).ToString("###-###-####");
                        if (value.Length > 10)
                            return Convert.ToInt64(value)
                                .ToString("###-###-#### " + new String('#', (value.Length - 10)));
                    }
                }
            }
            else
            {
                value = "###-###-####";
            } 
            return value;
        }

        public static string Currency(this string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                value = value.Replace("$", "");
                try
                {
                    var new_rate = Convert.ToDouble(value);
                    value = String.Format(new CultureInfo("en-US"), "{0:C}", new_rate);
                }
                catch
                {

                }
            }
            else
            {
                value = "$0.00";
            }
            return value;
        }

        public static string ToTitleCase(this string str)
        {
            string result = str;
            if (!string.IsNullOrEmpty(str))
            {
                var words = str.Split(' ');
                for (int index = 0; index < words.Length; index++)
                {
                    var s = words[index];
                    if (s.Length > 0)
                    {
                        words[index] = s[0].ToString().ToUpper() + s.Substring(1);
                    }
                }
                result = string.Join(" ", words);
            }
            return result;
        }

        //12340 Boggy Creek Rd, Orlando, FL 32824
        public static void GetAddress(this string value, ref string street, ref string city, ref string state, ref string zipcode)
        {
            var address = value.Split(',');
            street = address[0];
            city = address[1].Trim();
            var adddress1 = address[2].TrimStart().Split(' ');
            state = adddress1[0];
            zipcode = adddress1[1];
        }
        public static string GetAddressView(this string _val, string suite = "")
        {
            string ViewAddress ; 
            string street = string.Empty;
            string city = string.Empty; 
            string state = string.Empty;
            string zipcode = string.Empty;
            if (!string.IsNullOrEmpty(_val))
            {
                var address = _val.Split(',');
                street = address[0];
                city = address[1].Trim();
                var adddress1 = address[2].TrimStart().Split(' ');
                state = adddress1[0];
                zipcode = adddress1[1];
            }
            ViewAddress = street;
            if (!string.IsNullOrEmpty(suite))
            {
                if (!string.IsNullOrEmpty(street))
                {
                    ViewAddress = street + " <br/>" + suite;
                }
                else
                {
                    ViewAddress = suite;
                }
               
            }
            if (!string.IsNullOrEmpty(city))
            {
                if (!string.IsNullOrEmpty(ViewAddress))
                {
                    ViewAddress = ViewAddress + " <br/>" + city;
                }
                else
                {
                    ViewAddress = city;
                } 
            }
            if (!string.IsNullOrEmpty(state))
            {
                if (!string.IsNullOrEmpty(ViewAddress))
                {
                    ViewAddress = ViewAddress + ", " + state.ToUpper();
                }
                else
                {
                    ViewAddress = state.ToUpper();
                }
               
            }
            if (!string.IsNullOrEmpty(zipcode))
            {
                if (!string.IsNullOrEmpty(ViewAddress))
                {
                    ViewAddress = ViewAddress + "  " + zipcode;
                }
                else
                {
                    ViewAddress = zipcode;
                }
               
            }
            return ViewAddress;
        }

        public static string GetAddress(this string street, string city, string state, string zip, string suite)
        {
            string address = string.Empty;
            if (!string.IsNullOrEmpty(street))
            {
                address = street;
            }

            if (!string.IsNullOrEmpty(suite))
            {
                if (!string.IsNullOrEmpty(address))
                {
                    address = address + " <br/>" + suite;
                }
                else
                {
                    address = suite;
                }

            }
            if (!string.IsNullOrEmpty(city))
            {
                if (!string.IsNullOrEmpty(address))
                {
                    address = address + " <br/>" + city;
                }
                else
                {
                    address = city;
                }

            }
            if (!string.IsNullOrEmpty(state))
            {
                if (!string.IsNullOrEmpty(address))
                {
                    address = address + ", " + state.ToUpper();
                }
                else
                {
                    address = state.ToUpper();
                }

            }
            if (!string.IsNullOrEmpty(zip))
            {
                if (!string.IsNullOrEmpty(address))
                {
                    address = address + "  " + zip;
                }
                else
                {
                    address = zip;
                }

            }
            return address;
        }
        public static string GetAddress(this string street, string city, string state, string zip)
        {
            string address = string.Empty;
            if (!string.IsNullOrEmpty(street))
            {
                address = street;
            } 
            if (!string.IsNullOrEmpty(city))
            {
                if (!string.IsNullOrEmpty(address))
                {
                    address = address + ", " + city;
                }
                else
                {
                    address = city;
                }

            }
            if (!string.IsNullOrEmpty(state))
            {
                if (!string.IsNullOrEmpty(address))
                {
                    address = address + ", " + state.ToUpper();
                }
                else
                {
                    address = state.ToUpper();
                }

            }
            if (!string.IsNullOrEmpty(zip))
            {
                if (!string.IsNullOrEmpty(address))
                {
                    address = address + "  " + zip;
                }
                else
                {
                    address = zip;
                }

            }
            return address;
        }
        public static string MailMergeBytext(this string text, string datetime = "", string date = "", string StartTime = "", string EndTime = "", string JobId = "", string JobTitle = "", string Price = "", string Description = "", string TechfirstName = "", string TechllastName = "", string TechfullName = "", string TechCity = "", string jobCity = "", string jobState = "", string TechState = "", string JobAddress = "", string AcceptLink = "", string techrate = "", string techPW = "", string techuser = "", string techemail = "", string techID = "", string declinelink = "")
        {
            text = text.Replace("{{DeclineLink}}", declinelink).Replace("{{AcceptLink}}", AcceptLink).Replace("{{JobAddress}}", JobAddress).Replace("{{TechState}}", TechState).Replace("{{jobState}}", jobState).Replace("{{jobCity}}", jobCity).Replace("{{TechCity}}", TechCity).Replace("{{TechfullName}}", TechfullName).Replace("{{TechllastName}}", TechllastName).Replace("{{TechfirstName}}", TechfirstName).Replace("{{Description}}", Description).Replace("{{Price}}", Price).Replace("{{techrate}}", techrate).Replace("{{techPW}}", techPW).Replace("{{techID}}", techID).Replace("{{techuser}}", techuser).Replace("{{techemail}}", techemail).Replace("{{JobTitle}}", JobTitle).Replace("{{JobId}}", JobId).Replace("{{date}}", date).Replace("{{StartTime}}", StartTime).Replace("{{EndTime}}", StartTime).Replace("{{datetime}}", datetime);
            return text;
        }
        public static string Validate(string EncodedResponse)
        {
            var client = new System.Net.WebClient(); 
            string PrivateKey = "6LfFi7sUAAAAAL3IgLuZRVjKdskSdd_PJk5Dk4_W";  
            var GoogleReply = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", PrivateKey, EncodedResponse));
             var captchaResponse = JsonConvert.DeserializeObject<ReCaptchaClass>(GoogleReply); 
            return captchaResponse.Success.ToLower();
        }

        public static int[] StringToArray(this string input, string separator)
        {
            string[] stringList = input.Split(separator.ToCharArray(),StringSplitOptions.RemoveEmptyEntries);
            int[] list = new int[stringList.Length];
            for (int i = 0; i < stringList.Length; i++)
            {
                list[i] = Convert.ToInt32(stringList[i]);
            }
            return list;
        }

        public static string ToDelimitedString<T>(this IEnumerable<T> lst, string separator = ", ")
        {
            return lst.ToDelimitedString(p => p, separator);
        }
        public static string ToDelimitedString<S, T>(this IEnumerable<S> lst, Func<S, T> selector, string separator = ", ")
        {
            return string.Join(separator, lst.Select(selector));
        }
    }
    public class ReCaptchaClass
    { 
        [JsonProperty("success")]
        public string Success
        {
            get { return m_Success; }
            set { m_Success = value; }
        } 
        private string m_Success;
        [JsonProperty("error-codes")]
        public List<string> ErrorCodes
        {
            get { return m_ErrorCodes; }
            set { m_ErrorCodes = value; }
        } 
        private List<string> m_ErrorCodes;
    }
}