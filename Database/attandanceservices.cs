﻿using hrm.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class attandanceservices
    {
        public string add_new_pay(attandanceadmin_items client, int userid, string url = "", int workspaceid = 0)
        {
            string message;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                var startdattime = Convert.ToDateTime(client.startdate + " " + client.starttime);
                var enddattime = Convert.ToDateTime(client.startdate + " " + client.endtime);
                TimeSpan span = enddattime - startdattime;
                double hour = Convert.ToDouble(String.Format("{0}.{1}", span.Hours, span.Minutes > 0 ? (int)Math.Round(Convert.ToDouble(span.Minutes) * 1.66) : span.Minutes));
                client._memberrate = client._memberrate ?? "$1";
                var amount = hour * Convert.ToDouble(client._memberrate.Replace("$", ""));
                amount += string.IsNullOrEmpty(client.expnse) ? 0 : Convert.ToDouble(client.expnse.Replace("$", ""));
                SortedList _srt = new SortedList
                {
                    { "@memberid", client.memberid },
                    { "@jobid", client.jobid },
                    { "@date", client.startdate },
                    { "@starttime", client.starttime },
                    { "@endtime", client.endtime },
                    { "@hour", hour },
                    { "@rate", client._memberrate },
                    { "@createdby", userid },
                    { "@Comment", client.comment },
                    { "@workspaceid", workspaceid },
                    { "@amount", "$" + amount.ToString() },
                    { "@expense", client.expnse }
                };
                if (!string.IsNullOrEmpty(url))
                {
                    _srt.Add("@docurl", url);
                }
                message = sqlHelper.executeNonQueryWMessage("tbl_attandance_add", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                message = "Error:" + exception.Message; sqlHelper.Dispose();
            }
            return message;
        }
        public string update_pay(attandanceadmin_items client, int userid, string url = "")
        {
            string message;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                var startdattime = Convert.ToDateTime(client.startdate + " " + client.starttime);
                var enddattime = Convert.ToDateTime(client.startdate + " " + client.endtime);
                TimeSpan span = enddattime - startdattime;
                double hour = Convert.ToDouble(String.Format("{0}.{1}", span.Hours, span.Minutes > 0 ? (int)Math.Round(Convert.ToDouble(span.Minutes) * 1.66) : span.Minutes));
                client._memberrate = client._memberrate ?? "$1";
                var amount = hour * Convert.ToDouble(client._memberrate.Replace("$", ""));
                amount += string.IsNullOrEmpty(client.expnse) ? 0 : Convert.ToDouble(client.expnse.Replace("$", ""));
                SortedList _srt = new SortedList
                {
                    { "@memberid", client.memberid },
                    { "@jobid", client.jobid },
                    { "@date", client.startdate },
                    { "@starttime", client.starttime },
                    { "@endtime", client.endtime },
                    { "@hour", hour },
                    { "@createdby", userid },
                    { "@statusid", client.statusid },
                    { "@payid", client.payid },
                    { "@amount", "$" + amount.ToString() },
                    { "@rate", client._memberrate },
                    { "@expnse", client.expnse }
                };
                message = sqlHelper.executeNonQueryWMessage("tbl_attandance_update", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }
        public string update_user_pay(attandanceadmin_items client, int userid, string url = "")
        {
            string message;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                var startdattime = Convert.ToDateTime(client.startdate + " " + client.starttime);
                var enddattime = Convert.ToDateTime(client.startdate + " " + client.endtime);
                TimeSpan span = enddattime - startdattime;
                double hour = Convert.ToDouble(String.Format("{0}.{1}", span.Hours, span.Minutes > 0 ? (int)Math.Round(Convert.ToDouble(span.Minutes) * 1.66) : span.Minutes));
                client._memberrate = client._memberrate ?? "$1";
                var amount = hour * Convert.ToDouble(client._memberrate.Replace("$", ""));
                amount += string.IsNullOrEmpty(client.expnse) ? 0 : Convert.ToDouble(client.expnse.Replace("$", ""));
                SortedList _srt = new SortedList
                {
                    { "@memberid", client.memberid },
                    { "@jobid", client.jobid },
                    { "@date", client.startdate },
                    { "@starttime", client.starttime },
                    { "@endtime", client.endtime },
                    { "@hour", hour },
                    { "@createdby", userid },
                    { "@payid", client.payid },
                    { "@amount", "$" + amount.ToString() },
                    { "@rate", client._memberrate },
                    { "@expense", client.expnse }
                };
                message = sqlHelper.executeNonQueryWMessage("tbl_attandance_update_user", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }
        public attandanceadmin get_all_active_attandance_dashboard(int? memberId, int? stateid, int[] jobstatus, int? paymentstatus, int[] cityId = null, bool isyear = false)
        {
            SqlHelper sqlHelper = new SqlHelper();
            attandanceadmin _mdl = new attandanceadmin();
            List<attandanceadmins> _model = new List<attandanceadmins>();
            try
            {
                SortedList _srt = new SortedList();
                if (memberId.HasValue && memberId.Value != 0)
                {
                    _srt.Add("@id", memberId.Value);
                }
                if (stateid.HasValue && stateid.Value != 0)
                {
                    _srt.Add("@stateid", stateid.Value);
                }
                if (jobstatus != null)
                {
                    _srt.Add("@jobstatus", string.Join<int>(",", jobstatus));
                }
                if (cityId != null)
                {
                    _srt.Add("@city", string.Join<int>(",", cityId));
                }
                if (paymentstatus.HasValue && paymentstatus.Value != 0)
                {
                    _srt.Add("@paymentstatus", paymentstatus.Value);
                }
                if (isyear)
                {
                    _srt.Add("@year", DateTime.Now.Year);
                }
                var dt = sqlHelper.fillDataTable("tbl_attandance_getattandance_map", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    attandanceadmins _item = new attandanceadmins
                    {
                        Client = dt.Rows[i]["Clientname"].ToString().ToTitleCase(),
                        date = Convert.ToDateTime(dt.Rows[i]["date"]).ToString("yyyy-MM-dd"),
                        Hoursworked = dt.Rows[i]["Hoursworked"].ToString(),
                        JobID = dt.Rows[i]["JobId"].ToString(),
                        Expense = dt.Rows[i]["expense"].ToString().Currency(),
                        Jobstatus = dt.Rows[i]["jobstatus"].ToString(),
                        MemberID = dt.Rows[i]["member_id"].ToString(),
                        Memberrate = dt.Rows[i]["rate"].ToString().Currency(),
                        Name = dt.Rows[i]["membername"].ToString().ToTitleCase(),
                        Pay = dt.Rows[i]["paymentamount"].ToString() == "default" ? (Convert.ToDouble(dt.Rows[i]["Hoursworked"].ToString()) * Convert.ToDouble(dt.Rows[i]["rate"].ToString().Replace("$", ""))).ToString().Currency() : dt.Rows[i]["paymentamount"].ToString().Currency(),
                        Paymentstatus = dt.Rows[i]["name"].ToString(),
                        attandanceid = Convert.ToInt32(dt.Rows[i]["id"].ToString()),
                        paymentstatusid = Convert.ToInt32(dt.Rows[i]["paymentstatusid"].ToString())
                    };
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _mdl.error_text = "Error:" + exception.Message;
            }
            _mdl._attandance = _model;
            return _mdl;
        }
        public attandanceadmin get_all_active_attandance(int? memberId, int? stateid, int[] jobstatus, int? paymentstatus, int[] cityId = null, bool isyear = false)
        {
            SqlHelper sqlHelper = new SqlHelper();
            attandanceadmin _mdl = new attandanceadmin();
            List<attandanceadmins> _model = new List<attandanceadmins>();
            SortedList _srt = new SortedList();
            try
            {
                if (memberId.HasValue && memberId.Value != 0)
                {
                    _srt.Add("@id", memberId.Value);
                }
                if (stateid.HasValue && stateid.Value != 0)
                {
                    _srt.Add("@stateid", stateid.Value);
                }
                if (jobstatus != null)
                {
                    _srt.Add("@jobstatus", string.Join<int>(",", jobstatus));
                }
                if (cityId != null)
                {
                    _srt.Add("@city", string.Join<int>(",", cityId));
                }
                if (paymentstatus.HasValue && paymentstatus.Value != 0)
                {
                    _srt.Add("@paymentstatus", paymentstatus.Value);
                }
                if (isyear)
                {
                    _srt.Add("@year", DateTime.Now.Year);
                }
                var dt = sqlHelper.fillDataTable("tbl_attandance_getattandance", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    attandanceadmins _item = new attandanceadmins
                    {
                        Client = dt.Rows[i]["Clientname"].ToString().ToTitleCase(),
                        date = Convert.ToDateTime(dt.Rows[i]["date"]).ToString("yyyy-MM-dd"),
                        Hoursworked = dt.Rows[i]["Hoursworked"].ToString(),
                        JobID = dt.Rows[i]["JobId"].ToString(),
                        Expense = dt.Rows[i]["expense"].ToString().Currency(),
                        Jobstatus = dt.Rows[i]["jobstatus"].ToString(),
                        MemberID = dt.Rows[i]["member_id"].ToString(),
                        Memberrate = dt.Rows[i]["rate"].ToString().Currency(),
                        Name = dt.Rows[i]["membername"].ToString().ToTitleCase(),
                        Pay = dt.Rows[i]["paymentamount"].ToString() == "default" ? (Convert.ToDouble(dt.Rows[i]["Hoursworked"].ToString()) * Convert.ToDouble(dt.Rows[i]["rate"].ToString().Replace("$", ""))).ToString().Currency() : dt.Rows[i]["paymentamount"].ToString().Currency(),
                        Paymentstatus = dt.Rows[i]["name"].ToString(),
                        attandanceid = Convert.ToInt32(dt.Rows[i]["id"].ToString()),
                        paymentstatusid = Convert.ToInt32(dt.Rows[i]["paymentstatusid"].ToString())
                    };
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _mdl.error_text = "Error:" + exception.Message;
            }
            _mdl._attandance = _model;
            return _mdl;
        }
        public List<attandanceadmins> GetAllActivePayByIndexClient(int endindex, int startindex, ref int total, int? memberId, string Location, int[] jobstatus, int? paymentstatus, string search = "", int userid = 0, int workspaceid = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            attandanceadmin _mdl = new attandanceadmin();
            List<attandanceadmins> _model = new List<attandanceadmins>();
            SortedList _srt = new SortedList();
            try
            {
                if (!string.IsNullOrEmpty(search))
                {
                    _srt.Add("@search", search);
                }
                if (!string.IsNullOrEmpty(Location))
                {
                    _srt.Add("@Location", Location);
                }
                if (userid != 0)
                {
                    _srt.Add("@id", userid);
                }
                _srt.Add("@startindex", startindex);
                _srt.Add("@workspaceid", workspaceid);
                _srt.Add("@endindex", endindex);
                if (memberId.HasValue && memberId.Value != 0)
                {
                    _srt.Add("@id", memberId.Value);
                }
                if (jobstatus != null)
                {
                    var jobs = string.Join<int>(",", jobstatus.Where(a => a != 0));
                    if (!string.IsNullOrEmpty(jobs))
                    {
                        _srt.Add("@jobstatus", jobs);
                    }
                }
                string sp = "GetAllAdminPayByIndexWithOutStatus";
                if (paymentstatus.HasValue && paymentstatus.Value != 0)
                {
                    sp = "GetAllAdminPayByIndexWithStatus";
                    _srt.Add("@paymentstatus", paymentstatus.Value);
                }
                var dt = sqlHelper.fillDataTable(sp, string.Empty, _srt); sqlHelper.Dispose();
                if (dt.Rows.Count > 0)
                {
                    Crypto _crypt = new Crypto();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        attandanceadmins _item = new attandanceadmins
                        {
                            Client = dt.Rows[i]["Clientname"].ToString().ToTitleCase(),
                            date = Convert.ToDateTime(dt.Rows[i]["date"]).ToString("yyyy-MM-dd"),
                            Hoursworked = dt.Rows[i]["Hoursworked"].ToString(),
                            JobID = dt.Rows[i]["JobId"].ToString(),
                            Expense = dt.Rows[i]["expense"].ToString().Currency(),
                            Jobstatus = dt.Rows[i]["jobstatus"].ToString(),
                            MemberID = dt.Rows[i]["member_id"].ToString(),
                            Memberrate = dt.Rows[i]["rate"].ToString().Currency(),
                            Name = dt.Rows[i]["membername"].ToString().ToTitleCase(),
                            Pay = dt.Rows[i]["paymentamount"].ToString() == "default" ? (Convert.ToDouble(dt.Rows[i]["Hoursworked"].ToString()) * Convert.ToDouble(dt.Rows[i]["rate"].ToString().Replace("$", ""))).ToString().Currency() : dt.Rows[i]["paymentamount"].ToString().Currency(),
                            Paymentstatus = dt.Rows[i]["name"].ToString(),
                            attandanceid = Convert.ToInt32(dt.Rows[i]["id"].ToString()),
                            paymentstatusid = Convert.ToInt32(dt.Rows[i]["paymentstatusid"].ToString()),
                            token = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString())
                        };
                        total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                        _model.Add(_item);
                    }
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _mdl.error_text = "Error:" + exception.Message;
            }
            return _model;
        }

        public List<attandanceadmins> GetAllActivePayByIndex(int endindex, int startindex, ref int total, int memberId, string Location, int[] jobstatus, int paymentstatus, string search = "", int workspaceid = 0,int Projectid=0,int ManagerId=0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            attandanceadmin _mdl = new attandanceadmin();
            List<attandanceadmins> _model = new List<attandanceadmins>();
            SortedList _srt = new SortedList();
            try
            {
                if (!string.IsNullOrEmpty(search))
                {
                    _srt.Add("@search", search);
                }
                if (!string.IsNullOrEmpty(Location))
                {
                    _srt.Add("@Location", Location);
                }
                _srt.Add("@startindex", startindex);
                _srt.Add("@workspaceid", workspaceid);
                _srt.Add("@endindex", endindex);
                if (memberId != 0)
                {
                    _srt.Add("@id", memberId);
                }
                if (Projectid != 0)
                {
                    _srt.Add("@Projectid", Projectid);
                }
                if (ManagerId != 0)
                {
                    _srt.Add("@ManagerId", ManagerId);
                } 
                if (jobstatus != null)
                {
                    var jobs = string.Join<int>(",", jobstatus.Where(a => a != 0));
                    if (!string.IsNullOrEmpty(jobs))
                    {
                        _srt.Add("@jobstatus", jobs);
                    }
                }
                string sp = "GetAllAdminPayByIndexWithOutStatus";
                if (paymentstatus != 0)
                {
                    sp = "GetAllAdminPayByIndexWithStatus";
                    _srt.Add("@paymentstatus", paymentstatus);
                }
                var dt = sqlHelper.fillDataTable(sp, string.Empty, _srt); sqlHelper.Dispose();
                if (dt.Rows.Count > 0)
                {
                    Crypto _crypt = new Crypto();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        attandanceadmins _item = new attandanceadmins
                        {
                            Client = dt.Rows[i]["Clientname"].ToString().ToTitleCase(),
                            date = Convert.ToDateTime(dt.Rows[i]["date"]).ToString("yyyy-MM-dd"),
                            Hoursworked = dt.Rows[i]["Hoursworked"].ToString(),
                            JobID = dt.Rows[i]["JobId"].ToString(),
                            Expense = dt.Rows[i]["expense"].ToString().Currency(),
                            Jobstatus = dt.Rows[i]["jobstatus"].ToString(),
                            MemberID = dt.Rows[i]["member_id"].ToString(),
                            Memberrate = dt.Rows[i]["rate"].ToString().Currency(),
                            Name = dt.Rows[i]["membername"].ToString().ToTitleCase(),
                            Pay = dt.Rows[i]["paymentamount"].ToString() == "default" ? (Convert.ToDouble(dt.Rows[i]["Hoursworked"].ToString()) * Convert.ToDouble(dt.Rows[i]["rate"].ToString().Replace("$", ""))).ToString().Currency() : dt.Rows[i]["paymentamount"].ToString().Currency(),
                            Paymentstatus = dt.Rows[i]["name"].ToString(),
                            attandanceid = Convert.ToInt32(dt.Rows[i]["id"].ToString()),
                            paymentstatusid = Convert.ToInt32(dt.Rows[i]["paymentstatusid"].ToString()),
                            token = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString())
                        };
                        total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                        _model.Add(_item);
                    }
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _mdl.error_text = "Error:" + exception.Message;
            }
            return _model;
        }
        public attandanceadmin get_all_active_attandance_byuserid(int id, int[] jobstatus, int? paymentstatus)
        {
            SqlHelper sqlHelper = new SqlHelper();
            attandanceadmin _mdl = new attandanceadmin();
            List<attandanceadmins> _model = new List<attandanceadmins>();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", id }
                };
                if (jobstatus != null)
                {
                    _srt.Add("@jobstatus", string.Join<int>(",", jobstatus));
                }
                if (paymentstatus.HasValue && paymentstatus.Value != 0)
                {
                    _srt.Add("@paymentstatus", paymentstatus.Value);
                }
                var dt = sqlHelper.fillDataTable("tbl_attandance_getattandance", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    attandanceadmins _item = new attandanceadmins
                    {
                        Client = dt.Rows[i]["Clientname"].ToString().ToTitleCase(),
                        date = Convert.ToDateTime(dt.Rows[i]["date"]).ToString("yyyy-MM-dd"),
                        Hoursworked = dt.Rows[i]["Hoursworked"].ToString(),
                        JobID = dt.Rows[i]["JobId"].ToString(),
                        Expense = dt.Rows[i]["expense"].ToString().Currency(),
                        Jobstatus = dt.Rows[i]["jobstatus"].ToString(),
                        MemberID = dt.Rows[i]["member_id"].ToString(),
                        Memberrate = dt.Rows[i]["rate"].ToString().Currency(),
                        Name = dt.Rows[i]["membername"].ToString().ToTitleCase(),
                        Pay = dt.Rows[i]["paymentamount"].ToString() == "default" ? (Convert.ToDouble(dt.Rows[i]["Hoursworked"].ToString()) * Convert.ToDouble(dt.Rows[i]["rate"].ToString().Replace("$", ""))).ToString().Currency() : dt.Rows[i]["paymentamount"].ToString().Currency(),
                        Paymentstatus = dt.Rows[i]["name"].ToString(),
                        attandanceid = Convert.ToInt32(dt.Rows[i]["id"].ToString()),
                        paymentstatusid = Convert.ToInt32(dt.Rows[i]["paymentstatusid"].ToString())
                    };
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _mdl.error_text = "Error:" + exception.Message;
            }
            _mdl._attandance = _model;
            return _mdl;
        }
        public attandanceadmin_items get_all_active_attandance_byid(int id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            attandanceadmin_items _item = new attandanceadmin_items();
            List<commentmodal> _commnts = new List<commentmodal>();
            List<FileModel> _files = new List<FileModel>();
            Crypto _crypt = new Crypto();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", id }
                };
                var ds = sqlHelper.fillDataSet("tbl_attandance_getpaybyid", string.Empty, _srt); sqlHelper.Dispose();
                var dt = ds.Tables[0];
                var dt1 = ds.Tables[1];
                var dt2 = ds.Tables[2];
                ds.Dispose();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _item.memberid = Convert.ToInt32(dt.Rows[i]["Memberid"].ToString());
                    _item.dispatcherid = Convert.ToInt32(dt.Rows[i]["dispatcherid"].ToString());
                    _item.dispatchername = dt.Rows[i]["dispatchername"].ToString();
                    _item.jobid = Convert.ToInt32(dt.Rows[i]["Jobid"].ToString());
                    _item.startdate = Convert.ToDateTime(dt.Rows[i]["date"].ToString()).ToString("yyyy-MM-dd");
                    _item.statusid = Convert.ToInt32(dt.Rows[i]["Paymentstatus"].ToString());
                    _item.starttime = dt.Rows[i]["StartTime"].ToString();
                    _item.endtime = dt.Rows[i]["Endtime"].ToString();
                    _item.membername = dt.Rows[i]["membername"].ToString().ToTitleCase();
                    _item.payid = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                    _item._token = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString());
                    _item._jobtoken = _crypt.EncryptStringAES(dt.Rows[i]["Jobid"].ToString());
                    _item._distoken = _crypt.EncryptStringAES(dt.Rows[i]["dispatcherid"].ToString());
                    _item._memberrate = dt.Rows[i]["rate"].ToString().Currency();
                    _item.createddate = Convert.ToDateTime(dt.Rows[i]["CreatedDate"].ToString()).ToString("yyyy-MM-dd");
                    _item.expnse = dt.Rows[i]["expnse"].ToString();
                    _item.jobtitle = dt.Rows[i]["Ttile"].ToString();
                    _item.jobstatus = dt.Rows[i]["Statusname"].ToString();
                    _item.hourworked = dt.Rows[i]["Hoursworked"].ToString();
                    _item.payamount = dt.Rows[i]["paymentamount"].ToString() == "default" ? (Convert.ToDouble(dt.Rows[i]["Hoursworked"].ToString()) * Convert.ToDouble(dt.Rows[i]["rate"].ToString().Replace("$", ""))).ToString().Currency() : dt.Rows[i]["paymentamount"].ToString().Currency();
                }
                dt.Dispose();
                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    commentmodal _mdl = new commentmodal
                    {
                        Comment = dt1.Rows[i]["Comment"].ToString(),
                        createdby = dt1.Rows[i]["createdby"].ToString().ToTitleCase(),
                        CreatedDate = dt1.Rows[i]["CreatedDate"].ToString()
                    };
                    _commnts.Add(_mdl);
                }
                dt1.Dispose();
                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    FileModel _mdl = new FileModel
                    {
                        FileTitle = dt2.Rows[i]["Doctitle"].ToString().ToTitleCase(),
                        Url = dt2.Rows[i]["docurl"].ToString(),
                        CreatedBy = dt2.Rows[i]["createdby"].ToString().ToTitleCase(),
                        CreatedDate = dt2.Rows[i]["Createddate"].ToString(),
                        FileId = Convert.ToInt32(dt2.Rows[i]["id"].ToString()),
                        FileToken = _crypt.EncryptStringAES(dt2.Rows[i]["id"].ToString())
                    };
                    _files.Add(_mdl);
                }
                dt2.Dispose();
                _item._Comments = _commnts;
                _item._files = _files;
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                _item.error_text = "Error:" + exception.Message;
            }
            return _item;
        }
        public string AddNewDocument(string FileTitle, int userid, int jobid, string Size, string url)
        {
            string message = "Document Added Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@createddate", DateTime.UtcNow.AddHours(-6) },
                    { "@FileTitle", FileTitle },
                    { "@Jobid", jobid },
                    { "@Size", Size },
                    { "@url", url },
                    { "@createdby", userid }
                };
                sqlHelper.executeNonQuery("tbl_attandance_files_add", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }
        public string remove_doc(int id)
        {
            string message = "Document Deleted Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", id }
                };
                sqlHelper.executeNonQuery("tbl_attandance_files_remove", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }
        public void update_status(int payid, int statusid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@payid", payid },
                    { "@statusid", statusid }
                };
                sqlHelper.executeNonQuery("tbl_attandance_updatestaus", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
        }
        public string remove_pay(int id)
        {
            string message = "Pay Deleted Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", id }
                };
                sqlHelper.executeNonQuery("tbl_attandance_remove", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }
        public string update_pay(int id, string amount, int userid)
        {
            string message = "Pay Amount Updated Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", id },
                    { "@createddate", DateTime.UtcNow.AddHours(-6) },
                    { "@amount", amount },
                    { "@createdby", userid }
                };
                sqlHelper.executeNonQuery("tbl_attandance_amount_update", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }
        public string add_new_comment(string comment, int userid, int jobid)
        {
            string message = "Comment Added Successfully";
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@createddate", DateTime.UtcNow.AddHours(-6) },
                    { "@JobId", jobid },
                    { "@comment", comment },
                    { "@createdby", userid }
                };
                sqlHelper.executeNonQuery("tbl_attandance_comment_add", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            }
            return message;
        }

    }
}