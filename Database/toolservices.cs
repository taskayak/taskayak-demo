﻿using hrm.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class toolservices
    {
        public List<tooltype_item> GetAllActiveToolType(int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList _srt = new SortedList();
            List<tooltype_item> _mdl = new List<tooltype_item>();
            try
            {
                _srt.Add("@workspaceid", workspaceid);
                var dt = sqlHelper.fillDataTable("tbl_tooltype_getalltype_v2", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    tooltype_item _item = new tooltype_item
                    {
                        name = dt.Rows[i]["name"].ToString(),
                        typeid = Convert.ToInt32(dt.Rows[i]["id"].ToString()),
                        mainorder = Convert.ToInt32(dt.Rows[i]["mainorder"].ToString())
                    };
                    _mdl.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
                        } 
            return _mdl;
        }
        public List<tools_item> GetAllToolsByIndex(ref int _total, int _startIndex = 0, int _endIndex = 25, string _search = "", int _workspaceId = 0)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<tools_item> _model = new List<tools_item>();
            SortedList _srtlist = new SortedList
            {
                { "@startindex", _startIndex },
                { "@endIndex", _endIndex },
                { "@workspaceid", _workspaceId }
            };
            if (!string.IsNullOrEmpty(_search))
            {
                _srtlist.Add("@search", _search);
            }
            try
            {
                var dt = sqlHelper.fillDataTable("GetAllToolsByIndex", string.Empty, _srtlist); sqlHelper.Dispose();
                if (dt.Rows.Count > 0)
                { 
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        tools_item _item = new tools_item
                        {
                            name = dt.Rows[i]["name"].ToString(),
                            toolid = Convert.ToInt32(dt.Rows[i]["id"].ToString()),
                            typeid = Convert.ToInt32(dt.Rows[i]["TypeId"].ToString()),
                            typename = dt.Rows[i]["Type"].ToString()
                        };
                        _model.Add(_item);
                        _total = Convert.ToInt32(dt.Rows[i]["total"].ToString()); 
                    }
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
            return _model;
        }

        public List<tools_item> GetAllTools(int workspaceid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<tools_item> _mdl = new List<tools_item>();
            SortedList _srt = new SortedList
            {
                { "@workspaceid", workspaceid }
            };
            try
            {
                var dt = sqlHelper.fillDataTable("tbl_tool_getalltools", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    tools_item _item = new tools_item
                    {
                        name = dt.Rows[i]["name"].ToString(),
                        toolid = Convert.ToInt32(dt.Rows[i]["id"].ToString()),
                        typeid = Convert.ToInt32(dt.Rows[i]["TypeId"].ToString()),
                        typename = dt.Rows[i]["Type"].ToString()
                    };
                    _mdl.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception )
            {
                sqlHelper.Dispose(); 
            } 
            return _mdl;
        }

        public string AddNewToolType(string name, int userid,int mainorder,int workspaceid)
        {
            string message ;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@deptName", name },
                    { "@workspaceid", workspaceid },
                    { "@mainorder", mainorder },
                    { "@createdby", userid }
                };
                message = sqlHelper.executeNonQueryWMessage("tbl_tbltooltype_addtype", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                 message = "Error:" + exception.Message;
            } 
            return message;
        }

        public string CreateTool(string name,int typeid, int userid,int workspaceid)
        {
            string message ;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@ToolName", name },
                    { "@workspaceid", workspaceid },
                    { "@typeid", typeid },
                    { "@createdby", userid }
                };
                message = sqlHelper.executeNonQueryWMessage("CreateTool", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            } 
            return message;
        }

        public string add_new_toolbytypename(string name, string typeid, int userid,int workspaceid)
        {
            string message;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@toolname", name },
                    { "@typename", typeid },
                    { "@createdby", userid },
                    {"@workspaceid",workspaceid }
                };
                message = sqlHelper.executeNonQueryWMessage("tbl_tbltool_addtoolbyuser", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                 message = "Error:" + exception.Message;
            } 
            return message;
        }

        public string UpdateTool(string name, int typeid,int toolid)
        {
            string message ;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@name", name },
                    { "@typeid", typeid },
                    { "@toolid", toolid }
                };
                message = sqlHelper.executeNonQueryWMessage("tbl_tbltool_updatetool", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            } 
            return message;
        }
        public string UpdateToolType(string deptname, int deptid, int userid,int mainorder)
        {
            string message ;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@deptName", deptname },
                    { "@createdby", userid },
                    { "@deptid", deptid },
                    { "@mainorder", mainorder }
                };
                message = sqlHelper.executeNonQueryWMessage("tbl_tooltype_updatetype", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                  message = "Error:" + exception.Message;
            } 
            return message;
        }
        public string DeleteToolsType(int deptid, int userid)
        {
            string message ;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@deptid", deptid },
                    { "@createdby", userid }
                };
                message = sqlHelper.executeNonQueryWMessage("tbl_tooltype_removetype", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                 message = "Error:" + exception.Message;
            } 
            return message;
        }

        public int getmaxorder(int workspaceid)
        {
            int order = 1;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@workspaceid", workspaceid }
                };
                order = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("tbl_toolytype_getmaxdept", string.Empty, _srt).ToString()); sqlHelper.Dispose();
            }
            catch (Exception )
            {
                sqlHelper.Dispose(); 
            }
            return order;
        }
        public string DeleteTool(int ToolId)
        {
            string message ;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@ToolId", ToolId }
                };
                message = sqlHelper.executeNonQueryWMessage("DeleteTool", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                 message = "Error:" + exception.Message; 
            }
            return message;
        }
    }
}