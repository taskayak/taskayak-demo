﻿using hrm.Models;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class prjmanagerservices
    { 
        public List<prjmngr_item>GetAllActiveManagerByClientIdWithPaging(ref int total, int id, int startIndex, int endIndex, string search = "")
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<prjmngr_item> _model = new List<prjmngr_item>();
            Crypto _crypt = new Crypto();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", id },
                    { "@startIndex", startIndex },
                    { "@endIndex", endIndex }
                };
                if (!string.IsNullOrEmpty(search))
                {
                    _srt.Add("@search", search);
                }
                var dt = sqlHelper.fillDataTable("tbl_projectmanager_getallprjs_by_index", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    prjmngr_item _item = new prjmngr_item
                    {
                        name = dt.Rows[i]["name"].ToString().ToTitleCase(),
                        phone = dt.Rows[i]["Phonenumber"].ToString().PhoneNumber(),
                        email = dt.Rows[i]["Email"].ToString(),
                        prjmanagerid = Convert.ToInt32(dt.Rows[i]["id"].ToString()),
                        clientid = id,
                        prj_token = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString()),
                        clt_token = _crypt.EncryptStringAES(id.ToString())
                    };
                    total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose(); 
            } 
            return _model;
        }
        public List<prjmngr_item> get_all_active_prj_bypartner_byindex(ref int total, int id, int startIndex, int endIndex, string search = "",int Workspaceid=0)
        {
            SqlHelper sqlHelper = new SqlHelper(); 
            List<prjmngr_item> _model = new List<prjmngr_item>();
            Crypto _crypt = new Crypto();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", id },
                    { "@startIndex", startIndex },
                    { "@endIndex", endIndex }
                };
                if (!string.IsNullOrEmpty(search))
                {
                    _srt.Add("@search", search);
                }
                var dt = sqlHelper.fillDataTable("tbl_projectmanager_getall_partner_by_index", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    prjmngr_item _item = new prjmngr_item
                    {
                        name = dt.Rows[i]["name"].ToString().ToTitleCase(),
                        phone = dt.Rows[i]["Phonenumber"].ToString().PhoneNumber(),
                        email = dt.Rows[i]["Email"].ToString(),
                        prjmanagerid = Convert.ToInt32(dt.Rows[i]["id"].ToString()),
                        clientid = id,
                        prj_token = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString()),
                        clt_token = _crypt.EncryptStringAES(id.ToString())
                    };
                    total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose(); 
            } 
            return _model;
        } 
        public string GetPosition(string Type)
        {
            string Position ;
            switch(Type)
            {
                case "ST":
                    Position = "Technician";
                    break;
                case "LT":
                    Position = "Lead Technician";
                    break;
                case "S":
                    Position = "Subcontractor";
                    break;
                default:
                    Position = "Technician";
                    break;
            } 
            return Position;
        }

        public List<ContractorProjectManager_Item> GetAllActiveProjectManagerByContractorByIndex(ref int total, int id, int startIndex, int endIndex, string search = "")
        {
            SqlHelper sqlHelper = new SqlHelper();
             List<ContractorProjectManager_Item> _model = new List<ContractorProjectManager_Item>();
            Crypto _crypt = new Crypto();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", id },
                    { "@startIndex", startIndex },
                    { "@endIndex", endIndex }
                };
                if (!string.IsNullOrEmpty(search))
                {
                    _srt.Add("@search", search);
                }
                var dt = sqlHelper.fillDataTable("GetAllActiveProjectManagerByContractorByIndex", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ContractorProjectManager_Item _item = new ContractorProjectManager_Item
                    {
                        name = dt.Rows[i]["name"].ToString().ToTitleCase(),
                        phone = dt.Rows[i]["Phonenumber"].ToString().PhoneNumber(),
                        email = dt.Rows[i]["Email"].ToString(),
                        position = GetPosition(dt.Rows[i]["position"].ToString()),
                        manager = dt.Rows[i]["manager"].ToString(),
                        prjmanagerid = Convert.ToInt32(dt.Rows[i]["id"].ToString()),
                        clientid = id,
                        prj_token = _crypt.EncryptStringAES(dt.Rows[i]["id"].ToString()),
                        clt_token = _crypt.EncryptStringAES(id.ToString())
                    };
                    total = Convert.ToInt32(dt.Rows[i]["total"].ToString());
                    _model.Add(_item);
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose(); 
            } 
            return _model;
        }

        public string CreateProjectManager(prjmngr_item _request, int userid,int workspaceid)
        {
            string message ;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@name", _request.name },
                    { "@clientid", _request.clientid },
                    { "@Email", _request.email },
                    { "@Phone", _request.phone },
                    { "@createdby", userid },
                    { "@workspaceid", workspaceid }
                };
                message = sqlHelper.executeNonQueryWMessage("CreateProjectManager", string.Empty, _srt).ToString(); 
                 sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            } 
            return message;
        }

        public string add_new_partner_manager(prjmngr_item prjectmgr, int userid)
        {
            string message ;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@name", prjectmgr.name },
                    { "@PartnerId", prjectmgr.partnerid },
                    { "@Email", prjectmgr.email },
                    { "@Phone", prjectmgr.phone },
                    { "@createdby", userid }
                };
                message = sqlHelper.executeNonQueryWMessage("tbl__partner_manager_add", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                 message = "Error:" + exception.Message;
            } 
            return message;
        }

        public string UpdateManagerProfile(prjmngr_item _request)
        {
            string message ;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@name", _request.name },
                    { "@Email", _request.email },
                    { "@Phone", _request.phone },
                    { "@managerid", _request.prjmanagerid }
                };
                message = sqlHelper.executeNonQueryWMessage("tbl_manager_update", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                 message = "Error:" + exception.Message;
            } 
            return message;
        }

        public string update__partner_manager(prjmngr_item prjectmgr)
        {
            string message ;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@name", prjectmgr.name },
                    { "@Email", prjectmgr.email },
                    { "@Phone", prjectmgr.phone },
                    { "@managerid", prjectmgr.prjmanagerid }
                };
                message = sqlHelper.executeNonQueryWMessage("tbl__partner_manager_update", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            } 
            return message;
        }

        public string DeleteManager(int managerid)
        {
            string message ;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@managerid", managerid }
                };
                message = sqlHelper.executeNonQueryWMessage("DeleteManager", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                 message = "Error:" + exception.Message;
            } 
            return message;
        }

        public string delete_partner_manager(int managerid)
        {
            string message ;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@managerid", managerid }
                };
                message = sqlHelper.executeNonQueryWMessage("tbl_partner_manager_remove", string.Empty, _srt).ToString(); sqlHelper.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
                message = "Error:" + exception.Message;
            } 
            return message;
        }
    }
}