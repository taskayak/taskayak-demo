﻿using hrm.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Database
{
    public class notificationServices
    {
        public int GetNotificationCountByType(int MainId, NotificationType type, int userId)
        {
            int count = 0;
            int _typeId = 1;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                switch (type)
                {
                    case NotificationType.Offer:
                        _typeId = 2;
                        break;
                    case NotificationType.Pay:
                        _typeId = 3;
                        break;
                }
                _srt.Add("@id", MainId);
                _srt.Add("@userId", userId);
                _srt.Add("@type", _typeId);
                count = Convert.ToInt32(sqlHelper.executeNonQueryWMessage("tbl_notification_get_count", string.Empty, _srt).ToString()); sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose(); 
            }
            return count;
        }
        public bool CheckreadStatusById(int messageId)
        {
            bool isread = false;
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", messageId }
                };
                isread = Convert.ToBoolean(sqlHelper.executeNonQueryWMessage("tbl_notification_get_count", string.Empty, _srt).ToString()); sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose(); 
            }
            return isread;
        }

        public void update_jobreadofferstatus(int id, int userid)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", id },
                    { "@offerid", userid }
                };
                sqlHelper.executeNonQuery("tbl_notification_updateJobNotification2", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose(); 
            } 
        }
        public void update_jobreadstatus(int id)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", id }
                };
                sqlHelper.executeNonQuery("tbl_notification_updateJobNotification", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose(); 
            } 
        }
        public void update_jobreadstatus2(int id,int receiver,string Proc= "tbl_notification_updateJobNotification2")
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", id },
                    { "@receiver", receiver }
                }; 
                sqlHelper.executeNonQuery(Proc, string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose(); 
            } 
        }

        public void UpdateJobConfirmStatus(int id, int receiver, int type)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", id },
                    { "@receiver", receiver },
                     { "@type", type }
                };
                sqlHelper.executeNonQuery("updateNotificationWithJobConfirm", string.Empty, _srt); sqlHelper.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose();
            }
        }


        public List<messageModal> get_all_active_notificationsbyjovbid(int id, int userId)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<messageModal> _model = new List<messageModal>();
            List<int> _MessageIds = new List<int>();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", id },
                    { "@userId", userId }
                };
                var dt = sqlHelper.fillDataTable("tbl_notification_get_notifications_jobs", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    messageModal _mdl = new messageModal();
                    _mdl.mainMessageid = Convert.ToInt32(dt.Rows[i]["mesgid"].ToString());
                    if (!_MessageIds.Contains(_mdl.mainMessageid))
                    {
                        _MessageIds.Add(_mdl.mainMessageid);
                        _mdl.body = dt.Rows[i]["body"].ToString();
                        _mdl.createdby = dt.Rows[i]["sender"].ToString();
                        _mdl.createdDate = dt.Rows[i]["Createddate"].ToString();
                        _mdl.isread = Convert.ToBoolean(dt.Rows[i]["isread"].ToString());
                        _mdl._createdby = Convert.ToInt32(dt.Rows[i]["createdby"].ToString()); 
                        _mdl.msgitem = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                        if (_mdl._createdby == userId)
                        {
                            _mdl.isread = true;
                        }
                        _model.Add(_mdl);
                    } 
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose(); 
            }
            return _model;
        }

        public List<messageModal> get_all_active_notificationsbyofferid(int id, int userId)
        {
            SqlHelper sqlHelper = new SqlHelper();
            List<messageModal> _model = new List<messageModal>(); List<int> _MessageIds = new List<int>();
            try
            {
                SortedList _srt = new SortedList
                {
                    { "@id", id },
                    { "@userId", userId }
                };
                var dt = sqlHelper.fillDataTable("tbl_notification_get_notifications_offer", string.Empty, _srt); sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    messageModal _mdl = new messageModal
                    {
                        mainMessageid = Convert.ToInt32(dt.Rows[i]["mesgid"].ToString())
                    };
                    if (!_MessageIds.Contains(_mdl.mainMessageid))
                    {
                        _mdl.body = dt.Rows[i]["body"].ToString();
                        _mdl.createdby = dt.Rows[i]["sender"].ToString();
                        _mdl.createdDate = dt.Rows[i]["Createddate"].ToString();
                        _mdl.isread = Convert.ToBoolean(dt.Rows[i]["isread"].ToString());
                        _mdl.msgitem = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                        _mdl.isread = _mdl._createdby == userId;
                        _model.Add(_mdl);
                    }
                }
                dt.Dispose();
            }
            catch (Exception)
            {
                sqlHelper.Dispose(); 
            }
            return _model;
        }
    }
}