﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace hrm.Models
{
    public class job : JobHelperModel
    {
        public string token { get; set; }
        public int _jobid { get; set; }
        public List<job_items> _jobs { get; set; }
        [Required(ErrorMessage = "Please enter job id", AllowEmptyStrings = false)]
        public int _projectid { get; set; }
        public string JobId { get; set; }
        [Required(ErrorMessage = "Please enter job title", AllowEmptyStrings = false)]

        public string title { get; set; }
        [Required(ErrorMessage = "Please enter client", AllowEmptyStrings = false)]
        public List<commentmodal> _Comments { get; set; }
        public int client { get; set; }
        public bool clr1 { get; set; }
        public bool clr2 { get; set; }
        public int client_id { get; set; }
        public int mgr_id { get; set; }
        public int mgr_id1 { get; set; }
        public string description { get; set; }
        public int Technician { get; set; }
        public int Dispatcher { get; set; }
        public int city_id { get; set; }
        public int state_id { get; set; }
        [Required(ErrorMessage = "Please enter project manager", AllowEmptyStrings = false)]

        public string Project_manager { get; set; }
        [Required(ErrorMessage = "Please enter job date", AllowEmptyStrings = false)]

        public string sdate { get; set; }
        public string edate { get; set; }

        [Required(ErrorMessage = "Please enter job start time", AllowEmptyStrings = false)]

        public string stime { get; set; }

        [Required(ErrorMessage = "Please enter job end time", AllowEmptyStrings = false)]

        public string etime { get; set; }
        [Required(ErrorMessage = "Please enter address", AllowEmptyStrings = false)] 
        public string MapValidateAddress { get; set; }
        [Required(ErrorMessage = "Please enter client rate", AllowEmptyStrings = false)]


        public string Client_rate { get; set; }
        public string Pay_rate { get; set; }

        public string street { get; set; }
        public string zip { get; set; }
        public double Est_hours { get; set; }
        public double Hours { get; set; }
        public string Expense { get; set; }
        public int Status { get; set; }
        ///mail merge items
        public string datetime { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string address { get; set; }
        public string suite { get; set; }
         
        public string Contact { get; set; }
        public string Phone { get; set; }
        public string Instruction { get; set; }
        public bool issubcontractor { get; set; }
        public int type { get; set; }
        public List<SkillItems> Skillset { get; set; }
        public int length { get; set; }
    }

    public class  JobHelperModel : ErrorModel
    {
        public string jdate { get; set; }
        public int jcclient { get; set; }
        public string jLocation { get; set; }
        public int jcTechnician { get; set; }
        public int jcDispatcher { get; set; }
        public int jcStatus { get; set; }
        public bool jisActiveFilter { get; set; }
        public int jproject { get; set; } 
        public List<SkillItems> _jskills { get; set; }
        public List<ToolItems> _jtools { get; set; }
        public Dictionary<int, string> jselect { get; set; }
        public bool jhiderate { get; set; }
        public int juserid { get; set; }
        public int jStateId { get; set; }
        public int jCityId { get; set; }
    }


    public class commentModel
    {
        public string createdBy { get; set; }
        public string text { get; set; }
        public string timedate { get; set; }
    }
    public class jobmodal : ErrorModel
    {
        public List<job_items> _jobs { get; set; }
    }

    public class _projectmodal : ErrorModel
    {
        public List<_projectitems> _jobs { get; set; }
    }
    public class _projectitems
    {
        public int IsDefaultProject { get; set; }
        public string project_ID { get; set; }
        public int  ID { get; set; }
        public string Title { get; set; }
         public int offerCount { get; set; }
        public int jobCount { get; set; }
        public string Client { get; set; }
        public string clientmanager { get; set; }
        public string Project_manager { get; set; }
        public string token { get; set; }
        public int commentCount { get; set; }
        public int status_id { get; set; }
        public string Status { get; set; }
        public int reminderCount { get; set; }  
    }

    public class job_items
    {
        public string Job_ID { get; set; }
        public int commentCount { get; set; }
        public string token { get; set; }
        public int JobID { get; set; }
        public int status_id { get; set; }
        public string Title { get; set; }
        public string Client { get; set; }
        public string cityname { get; set; }
        public string statename { get; set; }
        public string Technician { get; set; }
        public string Dispatcher { get; set; }
        public string Project_manager { get; set; }
        public string sdate { get; set; }
        public string stime { get; set; }
        public string etime { get; set; }
        public string endate { get; set; } 
        public DateTime _sdate { get; set; }
        public DateTime _endate { get; set; }
        public string Client_rate { get; set; }
        public string Pay_rate { get; set; }
        public string Est_hours { get; set; }
        public string hours { get; set; }
        public string Expense { get; set; }
        public string Status { get; set; }
        public int view { get; set; }
        public int accepted { get; set; }
        public int confirm { get; set; }
        public int JobConfirmation { get; set; }
        public string  ViewContact { get; set; }
        public string ViewPhone { get; set; }
        public string Viewzipcode { get; set; }
        public string Viewstreet { get; set; }
        public string ViewSuiteNumber { get; set; } 
    }

    public class job_items1 : ErrorModel
    {
        public string Job_ID { get; set; }
        public int acceptedCount { get; set; }
        public int msgcount { get; set; }
        public int unreadcount { get; set; }
        public string token { get; set; }
        public string distoken { get; set; }
        public string tectoken { get; set; }
        public string address { get; set; }
        public string filetitle { get; set; }
        public int JobID { get; set; }
        public string Title { get; set; }
        public string stime { get; set; }
        public string etime { get; set; }
        public string description { get; set; }
        public string Client { get; set; }
        public int mgr_id1 { get; set; }
        public int Client_id { get; set; }
        public int mgr_id { get; set; }
        public int projectId { get; set; }
        public string Technician { get; set; }
        public int Technicianid { get; set; }
        public string Dispatcher { get; set; }
        public int Dispatcherid { get; set; }
        public string Project_manager { get; set; }
        public string sdate { get; set; }
        public string endate { get; set; }
        public string Client_rate { get; set; }
        public string pay_rate { get; set; }
        public string Est_hours { get; set; }
        public int Status { get; set; }
        public string Expense { get; set; }
        public string Comment { get; set; }
        public List<commentmodal> _Comments { get; set; }
        public List<commentmodal> _childComments { get; set; }
        public List<FileModel> _files { get; set; }
        public List<offermodal> _offers { get; set; }
        public List<reminder> techreminder  { get; set; }
        public List<reminder> dispatcherreminder { get; set; }
        public List<SkillItems> _skills { get; set; }
        public List<ToolItems> _tools { get; set; }
        public int[]  skills { get; set; }
        public int[] tools { get; set; }
        public string projectToken   { get; set; }
        public int c { get; set; }
        public string DeclineAlert { get; set; }
        public string MapValidateAddress { get; set; }
        public string Contact { get; set; }
        public string Phone { get; set; }
        public string Instruction { get; set; }
        public OfferTechFilter _offerTechFilter { get; set; }
        public int viewtype { get; set; }
        public int type { get; set; }
        public int jobConfirmation { get; set; }
    }

    public class commentmodal
    {
        public string Comment { get; set; }
        public int Commentid { get; set; }
        public int Parentid { get; set; }
        public int createdbyId { get; set; }
        public bool isread { get; set; }
        public string createdby { get; set; }
        public string CreatedDate { get; set; }
        public int Commentby { get; set; }
        public int Commentto { get; set; }
        public bool isadminpost { get; set; }
    }

    public class FileModel
    {
        public string FileTypeName { get; set; }
        public int FileTypeId { get; set; }
        public int MainOrder { get; set; }
        public string FileTitle { get; set; }
        public string Url { get; set; }
        public string CreatedBy { get; set; }
        public string Size { get; set; }
        public string CreatedDate { get; set; }
        public int FileId { get; set; }
        public string FileToken { get; set; }
    }


    public class offermodal
    {
        public string offertoken { get; set; }
        public string jobtoken { get; set; }
        public string createddate { get; set; }
        public string jobindexid { get; set; }
        public int jobid { get; set; }
        public int id { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string tech { get; set; }
        public string rate { get; set; }
        public string status { get; set; }
    }


    public class OfferTechFilter
    {
        public string TecF_amount { get; set; }
        public string TecF_Screening { get; set; }
        public string TecF_skiils { get; set; }
        public string TecF_tools { get; set; }
        public int TecF_Distance { get; set; }
        public int TecF_Assign { get; set; }
        public string TecF_Distribution { get; set; }
        public bool IsProcessed { get; set; }
        public int Template { get; set; }
    }

    public class ReminderFetch
    {
        public string UserID { get; set; }
        public string CreatedDate { get; set; }
        public bool NeedConfirmation { get; set; }
        public string Status { get; set; }
        public string SendDate { get; set; }
        public string JobName { get; set; }
        public string Name { get; set; } 
    }

}