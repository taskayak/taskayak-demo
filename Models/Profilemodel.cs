﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace hrm.Models
{
    public class Profilemodel
    {
        public string Name { get; set; }
        public string Image { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
        public List<JobNotificationModel> OfferMessage { get; set; }
        public List<JobNotificationModel> JobMessage { get; set; }
        public List<PayNotificationModel> PayMessage { get; set; }
        public List<JobNotificationModel> Confirmation { get; set; } 
        public List<string> GeneralMessage { get; set; }
        public bool isnotificationpermission { get; set; }
        public bool billing { get; set; }
        public bool pricing { get; set; }
        public  bool resuces { get; set; } 
    }

    public class JobNotificationModel
    {
        public int NotificationId { get; set; }
        public string Url { get; set; }
        public int JobId { get; set; }
        public string Name { get; set; }
        public string JobIndexId { get; set; }
        public string Message { get; set; }
        public string Time { get; set; }
        public string ConfirmUrl { get; set; }
        public string DeclineUrl { get; set; }
    }

    public class PayNotificationModel
    {
        public int PayId { get; set; }
        public string Url { get; set; }
        public string JobIndexId { get; set; }
        public string Rate { get; set; }
        public string Name { get; set; }
        public string Hours { get; set; }
        public string Amount { get; set; }

    }


    public class ChangePassword : ErrorModel
    {
        [Required(ErrorMessage = "Please enter current password", AllowEmptyStrings = false)]
        public string CurrentPassword { get; set; }
        [StringLength(15, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 8)]
        [Required(ErrorMessage = "Please enter new password", AllowEmptyStrings = false)]
        public string Password { get; set; }
        [Compare("password", ErrorMessage = "The password and confirm password does not match")]
        [DataType(DataType.Password)]
        [StringLength(15, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 8)]
        public string ConfirmPassword { get; set; }

    }
}