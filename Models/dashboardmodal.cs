﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Models
{
    public class dashboardmodal:ErrorModel
    {
        public int member { get; set; }
        public int jobs { get; set; }
        public string monthuypay { get; set; }
        public string monthreveneue { get; set; }
        public int leads { get; set; }
        public List<dashboardjobs> _jobs { get; set; }
        public List<dashboardoffer> _offer { get; set; }
        public List<ratejobs> _pay { get; set; } 
        public string AccountName { get; set; }
        public string AccountId { get; set; }
    }

    public class dashboardmodaljson : ErrorModel
    { 
        public List<int> _jobs { get; set; }
        public List<int> _offer { get; set; }
    }

    public class dashboardjobs
    {
        public string Jobid { get; set; }
        public string clientname { get; set; }
        public string Technician { get; set; }
        public string startdate { get; set; }
        public string rate { get; set; }
    } 
    public class dashboardoffer
    {
        public string Jobid { get; set; }
        public string clientname { get; set; } 
        public string startdate { get; set; }
        public string Dispatcher { get; set; }
        public string rate { get; set; }
    } 
    public class ratejobs
    {
        public string rate { get; set; }
        public string Hoursworked { get; set; }
        public string member { get; set; }
        public string date { get; set; }
        public string name { get; set; }
    }

}