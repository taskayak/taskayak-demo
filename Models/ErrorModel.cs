﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Models
{
    public class ErrorModel
    {
        public string error_text { get; set; } 
        public string alertclass { get; set; }
        public List<int> PERMISSIONS { get; set; }
    }

    public enum alertclass
    {
        success,
        danger
    }
}