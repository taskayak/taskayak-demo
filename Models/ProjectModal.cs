﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace hrm.Models
{
    public class ProjectModal : ErrorModel
    {
        [Required(ErrorMessage = "Please enter project ID", AllowEmptyStrings = false)]
        public string ProjectId { get; set; }
        [Required(ErrorMessage = "Please enter project title", AllowEmptyStrings = false)]
        public string title { get; set; }
        public int Id { get; set; }
        public int clientId { get; set; }
        public int clientManagerId { get; set; }
        public int projectManagerId { get; set; }
        public int[] tools { get; set; }
        public int[] skills { get; set; }
        public int[] Screening { get; set; }
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string email { get; set; }
        public string description { get; set; }
        public string specialInstruction { get; set; }
        public int status { get; set; }
        public string token { get; set; }
        public List<SkillItems> _skills { get; set; }
        public List<ToolItems> _tools { get; set; }
        public List<SkillItems> _screening { get; set; }
        public string _client { get; set; }
        public string _clientManager { get; set; }
        public string _projectManager { get; set; }
        public List<Projectjobitems> jobOffer { get; set; }
        public List<commentmodal> _commnts { get; set; }
        public List<FileModel> _files { get; set; }
        public List<reminder> techreminder { get; set; }
        public List<reminder> dispatcherreminder { get; set; }
        public List<string> technicians { get; set; }
        public List<string> dispatchers { get; set; } 
        public string f_date { get; set; }
        public int f_cclient { get; set; }
        public int f_cmanager { get; set; }
        public int f_pmanager { get; set; }
        public int f_Status { get; set; }
        public bool f_isActiveFilter { get; set; } 
        public int f_reminderId { get; set; }
        public int f_userType { get; set; }
        public int f_tabid { get; set; }
        public int f_Screening { get; set; }
        public int f_length { get; set; }
    }

    public class _projectdetails
    {
        public int clientId { get; set; }
        public int projectManagerId { get; set; }
        public int ClientManagerId { get; set; }
    }

    public class Projectjobitems
    {
        public int id { get; set; }
        public string JobId { get; set; }
        public string title { get; set; }
        public string date { get; set; }
        public string Type { get; set; }
        public string Location { get; set; }
        public string tech { get; set; }
        public string token { get; set; }
        public string dispatcher { get; set; }
    }

    public class Projectjob
    {
        public List<Projectjobitems> jobOffer { get; set; }
    }
    public class remindersetting:ErrorModel
    {
        public List<reminder> techReminder { get; set; }
        public  List<reminder> dispatcherReminder { get; set; }
        public int Usertype { get; set; }
        public int Tabid { get; set; }
    }

    public class reminder
    {
        public int Projectid { get; set; }
        public int Confirmation { get; set; }
        public int _reminderId { get; set; }
        public string token { get; set; }
        public int repeattype { get; set; }
        public int repeatid { get; set; }
        public int reapetdays { get; set; }
        public int recurring { get; set; }
        public int Navlink { get; set; }
        public int Document { get; set; }
        public int infoId { get; set; }
        public int Title { get; set; }
        public int datetime { get; set; }
        public int address { get; set; }
        public int tools { get; set; }
        public int contact { get; set; }
        public int phone { get; set; }
        public int instruction { get; set; }
        public int emailtype { get; set; }
        public int Smstype { get; set; }
        public int Usertype { get; set; } 
        public int[] Attachment { get; set; }
        public int[] info { get; set; }
        public int[] type { get; set; } 
    }


}