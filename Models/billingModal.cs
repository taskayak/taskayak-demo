﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace hrm.Models
{
    public class billingModal:ErrorModel
    {
        public string AccountType { get; set; }
        public int EmailCount { get; set; }
        public int smsCount { get; set; }
        public string AccountNumber { get; set; }
        [StringLength(20, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 2)]
        [Required(ErrorMessage = "Please enter First name.", AllowEmptyStrings = false)]
        public string Name { get; set; }
        public string Address { get; set; }
        [Required(ErrorMessage = "Please enter email", AllowEmptyStrings = false)]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Please enter Telephone", AllowEmptyStrings = false)]
        public string Telephone { get; set; }
        [StringLength(20, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 2)]
        [Required(ErrorMessage = "Please enter Primary contact.", AllowEmptyStrings = false)]
        public string Primarycontact { get; set; }
        public List<subscriptions> packages { get; set; }

    }


    public class subscriptions
    {
        public int emailcount { get; set; }
        public int smsCount { get; set; }
        public string  subscriptionId { get; set; }
        public string Planname { get; set; }
    }


}