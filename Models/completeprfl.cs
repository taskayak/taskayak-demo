﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace hrm.Models
{
    public class pp
    {
        public bool isrtwcomple { get; set; }
        public bool isw9compl { get; set; }
        public bool isddcomple { get; set; }
        public bool ismailcomple { get; set; }
        public string imageurl { get; set; }
    }

    public class cmplt
    {
        public bool isrtwcomple { get; set; }
        public bool isw9compl { get; set; }
        public bool isddcomple { get; set; }
        public bool ismailcomple { get; set; }
        public string message { get; set; }
    }

    public class getmailing
    {
        public int cityid { get; set; }
        public int stateid { get; set; }
        public string zipcode { get; set; }
        public string travldistance { get; set; }
        public string hrlyrate { get; set; }
        public string address { get; set; }
        public bool isrtwcomple { get; set; }
        public bool isw9compl { get; set; }
        public bool isddcomple { get; set; }
    }


    public class rtw
    {
        public bool citizen { get; set; }
        public bool age { get; set; }
        public bool auth { get; set; }
        public bool fealony { get; set; }
        public bool iscomplete { get; set; }
        public string explainfelony { get; set; }
        public bool ismailing { get; set; }
        public bool isw9compl { get; set; }
        public bool isddcomple { get; set; }
    }

    public class dd
    {
        public string bankname { get; set; }
        public string Typeofaccount { get; set; }
        public string account_name { get; set; }
        public string account_number { get; set; }
        public string routingnumber { get; set; }
        public string Accountcitystate { get; set; }
         public bool bank_authorization { get; set; }
        public bool ismailing { get; set; }
        public bool isw9compl { get; set; }
        public bool isrtwcomple { get; set; }
    }

    public class w9
    {
        public string city { get; set; }
        public string state { get; set; }
        public int w9id { get; set; }
        public int entity { get; set; }
        public string EIN1 { get; set; }
        public string EIN2 { get; set; }
        public string EIN3 { get; set; }
        public string EIN4 { get; set; }
        public string SSN1 { get; set; }
        public string SSN2 { get; set; }
        public string SSN3 { get; set; }
        public string SSN4 { get; set; }
        public string SSN5 { get; set; }
        public string Business1 { get; set; }
        public string Business2 { get; set; }
        public string Business3 { get; set; }
        public string Business4 { get; set; }

        public int cityid { get; set; }
        public int stateid { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string zip { get; set; }
        public bool ismailing { get; set; }
        public bool isrtwcompl { get; set; }
        public bool isddcomple { get; set; }

    }

}