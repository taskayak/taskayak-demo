﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace hrm.Controllers
{
    [AuthorizeVerifiedloggedin]
    public class SkillsController : Controller
    {
        public List<int> _permission;
        public SkillsController()
        {
            string PList = System.Web.HttpContext.Current.Request.Cookies["_plist"] == null ? "0" : System.Web.HttpContext.Current.Request.Cookies["_plist"].Value.ToString();
            _permission = PList == "0" ? null : JsonConvert.DeserializeObject<List<int>>(PList);
        }

        public ActionResult Index()
        {
            if (!_permission.Contains(5))
            {
                return View("unauth");
            }
            var dept = new skillservices();
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            var _model = dept.get_all_active_skills(workspaceid);
            string message = "";
            if (TempData["error"] != null)
            {
                message = TempData["error"].ToString();
            }
            ViewBag.permissions = _permission;
            _model = new helper().GenerateError<skill>(_model, message);
            return View(_model);
        }

        public ActionResult update()
        {
            ViewBag.permissions = _permission;
            var dept = new skillservices();
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            var _model = dept.get_all_active_skills(workspaceid);
            _model = new helper().GenerateError<skill>(_model, _model.error_text);
            return View("index", _model);
        }

        [HttpPost]
        public ActionResult Index(skill _model)
        {
            var dept = new skillservices();
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            var error_text = dept.add_new_skill(_model.skill_name, userid, workspaceid);
            if (!error_text.ToLower().Contains("error"))
            {
                ModelState.Clear();
            }
            _model = dept.get_all_active_skills(workspaceid);
            _model = new helper().GenerateError<skill>(_model, error_text);
            return View(_model);
        }


        [HttpPost]
        public ActionResult update(string skill, int skillid)
        {
            ViewBag.permissions = _permission;
            var dept = new skillservices();
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            var error_text = dept.update_skill(skill, skillid, userid);
            skill _model = new skill();
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            _model = dept.get_all_active_skills(workspaceid);
            _model = new helper().GenerateError<skill>(_model, error_text);
            return View("index", _model);
        }


        public ActionResult delete(int skillid)
        {
            if (!_permission.Contains(28))
            {
                return View("unauth");
            }
            ViewBag.permissions = _permission;
            var dept = new skillservices();
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            var error_text = dept.delete_skill(skillid, userid);
            skill _model = new skill();
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            _model = dept.get_all_active_skills(workspaceid);
            _model = new helper().GenerateError<skill>(_model, error_text);
            return View("index", _model);
        }
    }
}