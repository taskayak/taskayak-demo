﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Hangfire;

namespace hrm.Controllers
{
    [AuthorizeVerifiedloggedin]
    public class jobsController : Controller
    {
        // GET: jobs
        public List<int> _permission;
        public int pageid = 4;
        public jobsController()
        {
            string PList = System.Web.HttpContext.Current.Request.Cookies["_plist"] == null ? "0" : System.Web.HttpContext.Current.Request.Cookies["_plist"].Value.ToString();
            _permission = PList == "0" ? null : JsonConvert.DeserializeObject<List<int>>(PList);
        }

        public JsonResult getjobofferdata(int? Id)
        {
            int workspaceid = !Id.HasValue ? Convert.ToInt32(Request.Cookies["domain"].Value.ToString()) : Id.Value;
            dashboardservices _ser = new dashboardservices();
            var model = _ser.get_all_active_dashboard_json(workspaceid);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetEvents(int id, string Skiil="",int PmanagerId=0,int cmanagerId=0,int screening=0, string start = null, string end = null, string date = "", int type = 1, string Location = "", int cclient = 0, int cTechnician = 0, int cDispatcher = 0, int Status = 0, int searchtype = 0, int project = 0)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            pageid = 98;
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            var _filtermodel = new jobfilter();
            switch (searchtype)
            {
                case 1:
                    _filtermodel.Location = Location;
                    _filtermodel.date = date;
                    _filtermodel.cclient = cclient;
                    _filtermodel.cTechnician = cTechnician;
                    _filtermodel.cDispatcher = cDispatcher;
                    _filtermodel.Status = Status;
                    _filtermodel.project = project;
                    _filtermodel.Skiil = Skiil;
                    _filtermodel.PmanagerId = PmanagerId;
                    _filtermodel.cmanagerId = cmanagerId;
                    _filtermodel.screening = screening; 
                    new filterservices().save_filter<jobfilter>(_filtermodel, userid, pageid);
                    break;
                case 2:
                    date = "";
                    Location = "";
                    cclient = 0;
                    cTechnician = 0;
                    cDispatcher = 0;
                    Status = 0;
                    project = 0;
                    Skiil = "0";
                    PmanagerId = 0;
                    cmanagerId = 0;
                    screening = 0; 
                    new filterservices().delete_filter(userid, pageid);
                    break; 
            }
            DateTime dateTime;
            Jobservices _ser = new Jobservices();
            DateTime dateTime1 = (!string.IsNullOrEmpty(start) ? Convert.ToDateTime(start) : DateTime.Now);
            List<Events> events = new List<Events>();
            if (string.IsNullOrEmpty(end))
            {
                DateTime currentTimeByTimeZone = DateTime.Now;
                dateTime = currentTimeByTimeZone.AddDays(30);
            }
            else
            {
                dateTime = Convert.ToDateTime(end);
            }
            var model = new jobmodal();
            bool cleardate = false;
            if (string.IsNullOrEmpty(date) && (!string.IsNullOrEmpty(end) && !string.IsNullOrEmpty(start)))
            {
                cleardate = true;
                date = start.Replace("-", "/") + "-" + end.Replace("-", "/");
            }
            if (_permission.Contains(51))
            {
                var UserType = new UserService().GetUserType(userid);
                model = _ser.GetAllActiveJobsByUserIdForCalender(id, date, Location, cclient, cTechnician, cDispatcher, Status, userid, UserType, workspaceid);
                //ViewBag.hiderate = true;
            }
            else
            {//
                ViewBag.hiderate = false;
                model = _ser.GetAllActiveJobsForCalender(date, Location, cclient, cTechnician, cDispatcher, Status, workspaceid, project: project);
            }
            var filterjobs = model._jobs.ToList();
            date = cleardate ? "" : date;
            if (!string.IsNullOrEmpty(date))
            {
                filterjobs = filterjobs.Where(a => a._sdate >= dateTime1 || a._endate <= dateTime).ToList();
            }
            Crypto _crypt = new Crypto();
            foreach (var item in filterjobs)
            {
                events.Add(new Events()
                {
                    id = item.JobID.ToString(),
                    start = item._sdate.ToString("s"),
                    end = item._endate.ToString("s"),
                    title = item.Title,
                    allDay = false,
                    token = _crypt.EncryptStringAES(item.JobID.ToString()),
                    ViewID = item.Job_ID,
                    ViewTitle = item.Title,
                    ViewDate = item.sdate,
                    ViewStime = item.stime,
                    ViewEtime = item.etime,
                    ViewDispatcher = item.Dispatcher,
                    ViewTechnician = item.Technician
                    // ViewAddress=item.
                    // ViewContact=item
                    //ViewPhone 
                });
            }
            Events[] array = events.ToArray();
            return base.Json(array, 0);
        }

        [HttpPost]
        public string getunassignjobsforpopup(int stateid = 0, string city = "", string jobid = "", int clientid = 0, bool needLabel = true)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string ddl = "";
            if (needLabel)
            {
                ddl = "<option  value='0' selected>Job ID</option>";
            }
            var deptser = new defaultservices();
            var list = deptser.get_all_job_unassign(stateid, city, jobid, clientid, workspaceid);
            foreach (var item in list)
            {
                ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }
        public List<SkillItems> get_all_skills(int workspaceid)
        {
            var dept = new skillservices();
            return dept.get_all_active_skills(workspaceid)._skill;
        } 
        public ActionResult Index(int? id, int type = 1, int srch = 1, string token = "", int c = 0, int Nid = 0)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            if (!_permission.Contains(7))
            {
                return View("unauth");
            }
            if (type == 1)
            {
                if (!_permission.Contains(1062))
                {
                    return View("unauth");
                }
            }
            else
            {
                pageid = 98;
                if (!_permission.Contains(1061))
                {
                    return View("unauth");
                }
            }
            defaultservices _default = new defaultservices();
            Jobservices _ser = new Jobservices();
            job _model = new job();
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            var membertype = new UserService().GetUserType(userid);
            ViewBag.issubcontractor = (membertype == "S" || membertype == "LT") ? true : false;
            var _filtermodel = new jobfilter();
            if (srch == 2)
            {
                new filterservices().delete_filter(userid, pageid); 
            } 
            _filtermodel = new filterservices().getfilter<jobfilter>(_filtermodel, userid, pageid);
            ViewBag.date = string.IsNullOrEmpty(_filtermodel.date) ? "" : _filtermodel.date;
            ViewBag.Location = string.IsNullOrEmpty(_filtermodel.Location) ? "" : _filtermodel.Location;
            ViewBag.cclient = _filtermodel.cclient ?? 0;
            ViewBag.cTechnician = _filtermodel.cTechnician ?? 0;
            ViewBag.cDispatcher = _filtermodel.cDispatcher ?? 0;
            ViewBag.Status = _filtermodel.Status ?? 0;
            ViewBag.isfilter = _filtermodel.isActiveFilter;
            ViewBag.project = _filtermodel.project ?? 0;
            ViewBag.length = _filtermodel.length ?? 25;
            List<int> _ctyiid = new List<int>(); 
            ViewBag.Skiil = string.IsNullOrEmpty(_filtermodel.Skiil) ? _ctyiid.ToArray() : Array.ConvertAll(_filtermodel.Skiil.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries), int.Parse);
            //ViewBag.Skiil = _ctyiid.ToArray();
            ViewBag.PmanagerId = _filtermodel.PmanagerId ?? 0;
            ViewBag.cmanagerId = _filtermodel.cmanagerId ?? 0;
            ViewBag.screening = _filtermodel.screening ?? 0;
            ViewBag.permissions = _permission;
            string message = "";
            _model.Skillset = get_all_skills(workspaceid);
            if (!string.IsNullOrEmpty(token))
            {
                Crypto _crypt = new Crypto();
                int jid = Convert.ToInt32(_crypt.DecryptStringAES(token));
                message = _ser.delete_job(jid);
            }
            if (TempData["error"] != null)
            {
                message = TempData["error"].ToString();
            }
            if (c > 0)
            {
                message = new ContractorServices().updatRemindernotification(Nid, c);
            }
            _model = new helper().GenerateError<job>(_model, message);
            string view = "admin";
            if (_permission.Contains(51))
            {
                ViewBag.hiderate = true;
                view = "member";
            }
            else
            {
                ViewBag.hiderate = (membertype == "S" || membertype == "LT") ? true : false;
            }
            ViewBag.select = _default.GetAllActiveState();

            if (type == 1)
            {
                return View(view, _model);
            }
            else
            {
                ViewBag.userid = userid;
                return View("calender", _model);
            }
        }
        public void update_status(int id, int statusId)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            new Jobservices().update_status(statusId, id, userid, workspaceid);
        }
        //should send usertype later when fixing code
        public JsonResult Fetchadminjobs(int start = 0, int length = 25, int draw = 1, string date = "", int type = 1, int cclient = 0, int cTechnician = 0, int cDispatcher = 0, int Status = 0, int searchtype = 0, int project = 0)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string search = Request.Params["search[value]"] == null ? "" : Request.Params["search[value]"];
            string Location = Request.Params["Location"] == null ? "" : Request.Params["Location"];
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            int PmanagerId = string.IsNullOrEmpty(Request.Params["PmanagerId"]) ? 0 : Convert.ToInt32(Request.Params["PmanagerId"]);
            int cmanagerId = string.IsNullOrEmpty(Request.Params["cmanagerId"]) ? 0 : Convert.ToInt32(Request.Params["cmanagerId"]);
            int screening = string.IsNullOrEmpty(Request.Params["screening"]) ? 0 : Convert.ToInt32(Request.Params["screening"]);
            string Skiil = Request.Params["Skiil"] ?? "0";
            var _filtermodel = new jobfilter();
          switch(searchtype)
            {
                case 1:
                    _filtermodel.Location = Location;
                    _filtermodel.date = date;
                    _filtermodel.cclient = cclient;
                    _filtermodel.cTechnician = cTechnician;
                    _filtermodel.cDispatcher = cDispatcher;
                    _filtermodel.Status = Status;
                    _filtermodel.project = project;
                    _filtermodel.Skiil = Skiil;
                    _filtermodel.PmanagerId = PmanagerId;
                    _filtermodel.cmanagerId = cmanagerId;
                    _filtermodel.screening = screening;
                    _filtermodel.length = length;
                    new filterservices().save_filter<jobfilter>(_filtermodel, userid, pageid);
                    break;
                case 2:
                    date = "";
                    Location = "";
                    cclient = 0;
                    cTechnician = 0;
                    cDispatcher = 0;
                    Status = 0;
                    project = 0;
                    Skiil = "0";
                    PmanagerId = 0;
                    cmanagerId = 0;
                    screening = 0;
                    length = 25;
                    new filterservices().delete_filter(userid, pageid);
                    break;

            } 
            int[] _Skiils = Array.ConvertAll(Skiil.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries), int.Parse);
            start = (start == 0 ? 1 : start + 1);
            length = (start == 1 ? length : (start - 1) + length);
            Jobservices _ser = new Jobservices();
            datatableAjax<job_items> tableDate = new datatableAjax<job_items>();
            int total = 0;
            var _model = _ser.GetAllActiveAdminJobsByIndex(ref total, start, length, search, date, Location, cclient, cTechnician, cDispatcher, Status, notificationUSER: userid, workspaceid: workspaceid, projectId: project,ClientManager: cmanagerId,ProjectManagerId:PmanagerId,Screening:screening, Skiils: _Skiils);
            tableDate.draw = draw;
            tableDate.data = _model;
            tableDate.recordsFiltered = (_model.Count() > 0 ? total : 0);
            tableDate.recordsTotal = total;
            return base.Json(tableDate, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Fetchmemberjobs(int start = 0, int length = 25, int draw = 1, string date = "", int type = 1, int state_filter_id = 0, int cclient = 0, int cTechnician = 0, int cDispatcher = 0, int Status = 0)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            string search = Request.Params["search[value]"] == null ? "" : Request.Params["search[value]"];
            string Location = Request.Params["Location"] == null ? "" : Request.Params["Location"];
            start = (start == 0 ? 1 : start + 1);
            length = (start == 1 ? length : (start - 1) + length);
            Jobservices _ser = new Jobservices();
            var UserType = new UserService().GetUserType(userid);
            datatableAjax<job_items> tableDate = new datatableAjax<job_items>();
            int total = 0;
            var _model = _ser.GetAllActiveJobsByIndex(ref total, start, length, search, date, Location, cclient, cTechnician, cDispatcher, Status, userid, notificationUSER: userid, UserType, workspaceid: workspaceid);
            tableDate.draw = draw;
            tableDate.data = _model;
            tableDate.recordsFiltered = (_model.Count() > 0 ? total : 0);
            tableDate.recordsTotal = total;
            return base.Json(tableDate, JsonRequestBehavior.AllowGet);
        }
         [HttpPost]
        public ActionResult editReminder(int usertype, reminder item, string jobtoken = "")
        {
            int reminderId = Convert.ToInt32(new Crypto().DecryptStringAES(item.token));
            TempData["errordata"] = new projectServices().UpdateReminder(usertype, item, 0, reminderId);
            ViewBag.tabId = reminderId;
            return RedirectToAction("view", new { token = jobtoken, type = 4, tabid = reminderId, userType = usertype });
        }

        [HttpPost]
        public ActionResult reminder(int usertype, reminder item, int type = 1, string jobtoken = "")
        {
            Crypto _crypt = new Crypto();
            int JobId = Convert.ToInt32(_crypt.DecryptStringAES(jobtoken));
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            int isprojectlevel = 1;
            int reminderId = 0;
            string message = new projectServices().AddNewReminderWithJobId(usertype, item, workspaceid, isprojectlevel, JobId, ref reminderId);
            TempData["errordata"] = message;
            return RedirectToAction("view", new { token = jobtoken, type = 4, tabid = reminderId, userType = usertype });
        }

        public ActionResult deleteReminder(string token = "", string jobtoken = "", int type = 1, int viewtype = 1)
        {
            int reminderId = Convert.ToInt32(new Crypto().DecryptStringAES(token));
            string message = new projectServices().DeactivateReminder(reminderId);
            TempData["errordata"] = message;
            ViewBag.tabId = reminderId;
            return RedirectToAction("view", new { token = jobtoken, type = 4, tabid = reminderId });
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult edit_skills(int[] skills, string token)
        {
            int JobId = Convert.ToInt32(new Crypto().DecryptStringAES(token));
            var message = new projectServices().UpdateSkillsByJob(skills, JobId);
            TempData["errordata"] = "Skills updated";
            return RedirectToAction("view", new { token = token, type = 6 });
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult edit_tools(int[] tools, string token)
        {
            int JobId = Convert.ToInt32(new Crypto().DecryptStringAES(token));
            var message = new projectServices().UpdateToolsByJob(tools, JobId);
            TempData["errordata"] = "Tools updated";
            return RedirectToAction("view", new { token = token, type = 5 });
        }
        public ActionResult view(string token, int type = 1, int viewtype = 1, string projectToken = "", int tabid = 0, int userType = 0, int c = 0, int Nid = 0,int read=0)
        {
            ViewBag.userType = userType;
            ViewBag.type = type;
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            if (!_permission.Contains(56))
            {
                return View("unauth");
            }
            Crypto _crypt = new Crypto();
            int id = Convert.ToInt32(_crypt.DecryptStringAES(token));
            string message = "";
            if (TempData["errordata"] != null)
            {
                message = TempData["errordata"].ToString();
            }
            if (c > 0)
            {
                message = new ContractorServices().updatRemindernotification(Nid, c); 
            }
            Jobservices _ser = new Jobservices();
            job_items1 _model  = _ser.GetAllActiveJobById(id, userid, workspaceid); 
            if (read!=0)
            {
                update_readstatus(read);
                message = "Job accepted by "+ _model.Technician;
                _model.jobConfirmation = 3;
            }
            ViewBag.permissions = _permission; 
            ViewBag.viewtype = viewtype;
            ViewBag.tabid = tabid;
            Session["type"] = viewtype; 
            _model = new helper().GenerateError<job_items1>(_model, message);
            _model.projectToken = projectToken;
            if (c == 1)
            {
                _model.DeclineAlert = "Please explain the reason for declining in the discussion box.";
            }
            _model.c = c;
            _model._tools = get_all_tools();
            _model._skills = get_all_skills();
            if (_permission.Contains(2062))
            {
                ViewBag.stype = new UserService().GetUserType(userid);
                return View("Tview", _model);
            }
            return View(_model);
        }
        public JsonResult getjob(int id)
        {
            Jobservices _ser = new Jobservices();
            var _model = _ser.GetAllActiveJobDetailsById(id);
            return Json(_model, JsonRequestBehavior.AllowGet);
        } 
        public ActionResult delete_file(string token, string jtoken)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            Crypto _crypt = new Crypto();
            int id = Convert.ToInt32(_crypt.DecryptStringAES(token));
            int jobid = Convert.ToInt32(_crypt.DecryptStringAES(jtoken));
            ViewBag.permissions = _permission;
            Jobservices _ser = new Jobservices();
            job_items1 _model = new job_items1();
            var message = _ser.delete_doc(id);
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            ViewBag.viewtype = Session["type"] == null ? 1 : Convert.ToInt32(Session["type"]);
            ViewBag.tabid = 0;
            _model = _ser.GetAllActiveJobById(jobid, userid, workspaceid);
            _model = new helper().GenerateError<job_items1>(_model, message);
            ViewBag.type = 3;
            _model._tools = get_all_tools();
            _model._skills = get_all_skills();
            if (_permission.Contains(2062))
            {
                return View("Tview", _model);
            }
            return View("view", _model);
        } 
        public ActionResult edit(string token, int type = 1)
        {
            if (!_permission.Contains(39))
            {
                return View("unauth");
            }
            Crypto _crypt = new Crypto();
            int id = Convert.ToInt32(_crypt.DecryptStringAES(token));
            ViewBag.permissions = _permission;
            Jobservices _ser = new Jobservices();
            job _model = new job();
            ViewBag.type = type;
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.GetAllActiveState();
            _model = _ser.GetAllActiveJobDetailsById(id);
            return View(_model);
        } 
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult update(int JobID, int type = 1, HttpPostedFileBase file = null, string Comment = "", string file_title = "")
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            ViewBag.permissions = _permission;
            ViewBag.type = type;
            Jobservices _ser = new Jobservices();
            job_items1 _model = new job_items1();
            var mes = "";
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            if (type == 2)
            {
                mes = _ser.add_new_comment(Comment, userid, JobID);
            }
            else if (type == 3)
            {
                string path = Server.MapPath("~/image/jobpicture-" + JobID);
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                string filenames = new helper().GetRandomAlphaNumeric(4);
                var extension = Path.GetExtension(file.FileName);
                var imagepath = path + "/" + filenames + extension;
                string tempfilename = "/image/jobpicture-" + JobID + "/" + filenames + extension;
                if (System.IO.File.Exists(imagepath))
                {
                    System.IO.File.Delete(imagepath);
                }
                file.SaveAs(imagepath);
                long b = file.ContentLength;
                long kb = b / 1024;
                mes = _ser.AddNewDocument(file_title, userid, JobID, kb.ToString() + " kb", tempfilename);
            }
            ModelState.Clear();
            _model = _ser.GetAllActiveJobById(JobID, userid, workspaceid);
            _model = new helper().GenerateError<job_items1>(_model, mes);
            ViewBag.viewtype = Session["type"] == null ? 1 : Convert.ToInt32(Session["type"]);
            ViewBag.tabid = 0;
            _model._tools = get_all_tools();
            _model._skills = get_all_skills();
            if (_permission.Contains(2062))
            {
                return View("Tview", _model);
            }
            return View("view", _model);
        }
 public bool validatejobId(string id)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            return new Jobservices().validateJobId(id, workspaceid);
        } 
        public bool validatejobIdEdit(int id, string jobid)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            return new Jobservices().validateJobId(id, jobid, workspaceid);
        }
        public int getcityidbystate(int stateid, string city)
        {
            if (string.IsNullOrEmpty(city))
            {
                return 0;
            }
            else
            {
                return new defaultservices().savegetCityId(stateid, city);
            }

        } 
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult add(job _model, int type = 1, string techrate = "", string clientrate = "", string MapValidateAddress = "", string zip = "", int projectId = 0)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            var _filtermodel = new jobfilter();
            _filtermodel = new filterservices().getfilter<jobfilter>(_filtermodel, userid, pageid);
            ViewBag.date = string.IsNullOrEmpty(_filtermodel.date) ? "" : _filtermodel.date;
            ViewBag.Location = string.IsNullOrEmpty(_filtermodel.Location) ? "" : _filtermodel.Location;
            ViewBag.cclient = _filtermodel.cclient ?? 0; 
            ViewBag.cTechnician = _filtermodel.cTechnician ?? 0;
            ViewBag.cDispatcher = _filtermodel.cDispatcher ?? 0;
            ViewBag.Status = _filtermodel.Status ?? 0;
            ViewBag.isfilter = _filtermodel.isActiveFilter;
            ViewBag.permissions = _permission;
            ViewBag.project = projectId == 0 ? _filtermodel.project ?? 0 : 0;
            ViewBag.length = _filtermodel.length ?? 25;
            ViewBag.PmanagerId = _filtermodel.PmanagerId ?? 0;
            ViewBag.cmanagerId = _filtermodel.cmanagerId ?? 0;
            ViewBag.screening = _filtermodel.screening ?? 0;
            List<int> _ctyiid = new List<int>();
            ViewBag.Skiil = string.IsNullOrEmpty(_filtermodel.Skiil) ? _ctyiid.ToArray() : Array.ConvertAll(_filtermodel.Skiil.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries), int.Parse);

            Jobservices _ser = new Jobservices();
            string street = ""; string city = ""; string state = ""; string zipcode = ""; int StateId = 0;
            MapValidateAddress.GetAddress(ref street, ref city, ref state, ref zipcode);
            _model.city_id = new defaultservices().CreateCity(ref StateId, state, city);
            _model.state_id = StateId;
            _model.zip = zipcode;
            _model.street = street;
            var mes = _ser.AddNewJob(_model, userid, techrate, clientrate, MapValidateAddress, zipcode, workspaceid, projectId);
            if (!mes.Contains("Error"))
            {
                var res = mes.Split('_');
                mes = res[0];
                _model._jobid = Convert.ToInt32(res[1]);
                if (_model.Technician != 0)
                {
                    //_ser.SendJobNotification(_model.Technician, _model._jobid, workspaceid, userid);
                    BackgroundJob.Enqueue(() => _ser.SendJobNotification(_model.Technician, _model._jobid, workspaceid, userid));
                }
            } 
            _model = new helper().GenerateError<job>(_model, mes);
            _model.Skillset = get_all_skills(workspaceid);
            string view = "admin";
            if (_permission.Contains(51))
            {
                // _model._jobs = _ser.get_all_active_jobs_bymemberid(userid, "")._jobs;
                ViewBag.hiderate = true;
                view = "member";
            }
            else
            {
                ViewBag.hiderate = false;
                // _model._jobs = _ser.get_all_active_jobs("")._jobs;
            }
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.GetAllActiveState();
            if (mes.Contains("Error"))
            {
                ViewBag.type = type;
                return View("edit", _model);
            }
            if (type == 1)
            {
                return View(view, _model);

            }
            else
            {
                ViewBag.userid = userid;
                return View("calender", _model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult update_job(job _model, int type = 1, int projectId = 0)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            var _filtermodel = new jobfilter();
            _filtermodel = new filterservices().getfilter<jobfilter>(_filtermodel, userid, pageid);
            ViewBag.date = string.IsNullOrEmpty(_filtermodel.date) ? "" : _filtermodel.date;
            ViewBag.Location = string.IsNullOrEmpty(_filtermodel.Location) ? "" : _filtermodel.Location;
            ViewBag.cclient = _filtermodel.cclient ?? 0;
            ViewBag.cTechnician = _filtermodel.cTechnician ?? 0;
            ViewBag.cDispatcher = _filtermodel.cDispatcher ?? 0;
            ViewBag.Status = _filtermodel.Status ?? 0;
            ViewBag.project = _filtermodel.project;
            ViewBag.isfilter = _filtermodel.isActiveFilter;
            ViewBag.length = _filtermodel.length ?? 25;
            ViewBag.type = type;
            ViewBag.permissions = _permission; 
            ViewBag.project = projectId==0?_filtermodel.project?? 0:0; 
            ViewBag.PmanagerId = _filtermodel.PmanagerId ?? 0;
            ViewBag.cmanagerId = _filtermodel.cmanagerId ?? 0;
            ViewBag.screening = _filtermodel.screening ?? 0;
            List<int> _ctyiid = new List<int>();
            ViewBag.Skiil = string.IsNullOrEmpty(_filtermodel.Skiil) ? _ctyiid.ToArray() : Array.ConvertAll(_filtermodel.Skiil.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries), int.Parse);

            Jobservices _ser = new Jobservices();
            string street = ""; string city = ""; string state = ""; string zipcode = ""; int StateId = 0;
            _model.MapValidateAddress.GetAddress(ref street, ref city, ref state, ref zipcode);
            _model.city_id = new defaultservices().CreateCity(ref StateId, state, city);
            _model.state_id = StateId;
            _model.zip = zipcode;
            _model.street = street;
            bool IsTechrest = false;
            var mes = _ser.UpdateJob(_model, userid, workspaceid, projectId,ref IsTechrest);
            if (IsTechrest&& _model.Technician!=0)
            {
                BackgroundJob.Enqueue(() => _ser.SendJobNotification(_model.Technician, _model._jobid, workspaceid, userid));
               // _ser.SendJobNotification(_model.Technician, _model._jobid, workspaceid, userid);
            }
            _model = new helper().GenerateError<job>(_model, mes); 
            _model.Skillset = get_all_skills(workspaceid);
            string view = "admin";
            if (_permission.Contains(51))
            {
                ViewBag.hiderate = true;
                view = "member";
            }
            else
            {
                ViewBag.hiderate = false;
            }
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.GetAllActiveState();
            if (mes.Contains("Error"))
            {
                ViewBag.type = type;
                return View("edit", _model);
            }
            if (type == 1)
            {
                return View(view, _model);
            }
            else
            {
                ViewBag.userid = userid;
                return View("calender", _model);
            }
        }
        
        [HttpPost]
        public string updateassign(int memberid, int jobid, string rate)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            var name = "";
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            Jobservices _ser = new Jobservices();
            name = _ser.assignjob(jobid, memberid, rate);
            //_ser.SendJobNotification(memberid, jobid, workspaceid, userid);
          BackgroundJob.Enqueue(() => _ser.SendJobNotification(memberid, jobid, workspaceid, userid));
            return name;
        }
        public List<SkillItems> get_all_skills()
        {
            var service = new skillservices();
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            return service.get_all_active_skills(workspaceid)._skill;
        }


        public List<ToolItems> get_all_tools()
        {
            var service = new skillservices();
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            return service.get_all_active_tools(workspaceid)._tool;
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult edit(job_items1 _model, int type = 1, int projectid = 0)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            ViewBag.permissions = _permission;
            Jobservices _ser = new Jobservices();
            ViewBag.type = type;
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            bool IsTechrest = false;
            var mes = _ser.update_job_byview(_model, userid, projectid,ref IsTechrest);
            if (IsTechrest&&_model.Technicianid!=0)
            {
                //_ser.SendJobNotification(_model.Technicianid, _model.JobID, workspaceid, userid);
                BackgroundJob.Enqueue(() => _ser.SendJobNotification(_model.Technicianid, _model.JobID, workspaceid, userid));
            }
            _model = _ser.GetAllActiveJobById(_model.JobID, userid, workspaceid);
            _model = new helper().GenerateError<job_items1>(_model, mes);
            ViewBag.type = Session["type"] == null ? 1 : Convert.ToInt32(Session["type"]);
            _model._tools = get_all_tools();
            _model._skills = get_all_skills();
            if (_permission.Contains(2062))
            {
                return View("Tview", _model);
            }
            return View("view", _model);
        } 
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Tedit(job_items1 _model, int type = 1)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            ViewBag.permissions = _permission;
            Jobservices _ser = new Jobservices();
            ViewBag.type = type;
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            var mes = _ser.update_job_byviewT(_model);
            _model = _ser.GetAllActiveJobById(_model.JobID, userid, workspaceid);
            _model = new helper().GenerateError<job_items1>(_model, mes);
            ViewBag.type = Session["type"] == null ? 1 : Convert.ToInt32(Session["type"]);
            _model._tools = get_all_tools();
            _model._skills = get_all_skills();
            if (_permission.Contains(2062))
            {
                return View("Tview", _model);
            }
            return View("view", _model);
        }
        public PartialViewResult getnotifications(int jobid)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            notificationModal _model = new notificationModal();
            _model.message = new notificationServices().get_all_active_notificationsbyjovbid(jobid, userid);
            _model.mainind = jobid;
            return PartialView("Reply", _model);
        }
        public void update_readstatus(int jobid)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            new notificationServices().update_jobreadstatus2(jobid, userid, "updatenotificationWithPopUp");
        }  

        public void updatejobstatus(string token,int type)
        {
            int id = Convert.ToInt32(new Crypto().DecryptStringAES(token));
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            new notificationServices().UpdateJobConfirmStatus(id, userid, type);
        }
        public void update_readstatus2(int jobid)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            new notificationServices().update_jobreadstatus2(jobid, userid);
        } 
        [ValidateInput(false)]
        public void Sendmessage(int JobID, string Comment)
        {
            Jobservices _ser = new Jobservices();
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            _ser.add_new_comment(Comment, userid, JobID);
        } 
        public class Events
        {
            public string ViewID { get; set; }
            public string ViewTitle { get; set; }
            public string ViewDate { get; set; }
            public string ViewStime { get; set; }
            public string ViewEtime { get; set; }
            public string ViewAddress { get; set; }
            public string ViewContact { get; set; }
            public string ViewPhone { get; set; }
            public string ViewDispatcher { get; set; }
            public string ViewTechnician { get; set; }
            public bool allDay { get; set; }
            public string token { get; set; }
            public string backgroundColor { get; set; }
            public string className { get; set; }
            public string date { get; set; }
            public string end { get; set; }
            public string id { get; set; }
            public string start { get; set; }
            public string title { get; set; }
            public string url { get; set; }
        } 

    }
}