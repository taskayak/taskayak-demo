﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Web.Mvc;

namespace hrm.Controllers
{
    [AuthorizeVerifiedloggedin]
    public class usersController : Controller
    {
        public List<int> _permission;
        public int pageid = 3;
        public usersController()
        {
            string PList = System.Web.HttpContext.Current.Request.Cookies["_plist"] == null ? "0" : System.Web.HttpContext.Current.Request.Cookies["_plist"].Value.ToString();
            _permission = PList == "0" ? null : JsonConvert.DeserializeObject<List<int>>(PList);
        }
        public ActionResult add()
        {
            UserModel _mdl = new UserModel();
            if (!_permission.Contains(34))
            {
                return View("unauth");
            }
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            _mdl.RoleList = get_all_roles(workspaceid);
            _mdl.SkillList = get_all_skills(workspaceid);
            _mdl.UserId = new UserService().GetNewUserId(workspaceid);
            return View(_mdl);
        }
        public ActionResult index(string token = "", int d = 0)
        {
            Users _UsersModel = new Users();
            helper _helper = new helper();
            if (!_permission.Contains(6))
            {
                return View("unauth");
            }
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            var type = Request.Cookies["_slist"] == null ? "T" : Request.Cookies["_slist"].Value.ToString();
            List<int> EmptyList = new List<int>();
            var _filtermodel = new memberfilter();
            _filtermodel = new filterservices().getfilter<memberfilter>(_filtermodel, userid, pageid);
            _UsersModel.F_Location = string.IsNullOrEmpty(_filtermodel.Location) ? string.Empty : _filtermodel.Location;
            _UsersModel.F_skills = string.IsNullOrEmpty(_filtermodel.skills) ? EmptyList.ToArray() : Array.ConvertAll(_filtermodel.skills.Split(','), int.Parse);
            _UsersModel.F_amount = string.IsNullOrEmpty(_filtermodel.amount) ? EmptyList.ToArray() : Array.ConvertAll(_filtermodel.amount.Split(','), int.Parse);
            //_UsersModel.F_drug = _filtermodel.drug;
            _UsersModel.F_membertype = string.IsNullOrEmpty(_filtermodel.membertype) ? "" : _filtermodel.membertype;
            //_UsersModel.F_background = _filtermodel.background;
            _UsersModel.F_status = _filtermodel.status ?? 0;
            _UsersModel.F_length = _filtermodel.length ?? 25;
            _UsersModel.F_isfilter = _filtermodel.isActiveFilter;
            _UsersModel.F_issubcontractor = type == "S" || type == "LT";
            _UsersModel.PERMISSIONS = _permission;
            _UsersModel.F_role = _filtermodel.Role ?? 0;
            _UsersModel.F_contractor = _filtermodel.Contractor  ?? 0;
            _UsersModel.F_screening= _filtermodel.Screening ?? 0;
            string message = "";
            if (!string.IsNullOrEmpty(token))
            {
                UserService _UserService = new UserService();
                Crypto _crypt = new Crypto();
                int _id = Convert.ToInt32(_crypt.DecryptStringAES(token));
                if (_id == userid)
                {
                    message = "Error : You can't delete your account";
                }
                else
                {
                    if (!_permission.Contains(32))
                    {
                        return View("unauth");
                    }
                    else
                    {
                        message = d == 1 ? _UserService.InactiveUser(_id) : _UserService.DeleteUser(_id);
                    }
                }
            }
            if (TempData["error"] != null)
            {
                message = TempData["error"].ToString();
            }
            _UsersModel = new helper().GenerateError<Users>(_UsersModel, message);
            _UsersModel.SkillList = get_all_skills(workspaceid);
            defaultservices _default = new defaultservices();
            _UsersModel.select = _default.GetAllActiveState();
            _UsersModel.WorkSpaceId = workspaceid;
            ViewBag.roles = new PermissionServices().GetAllActivePermissionRolesDropDown(workspaceid) ; 
            ViewBag.COntractor = new ContractorServices().GetAllActiveCOntractor(workspaceid); 
            return View(_UsersModel);

        }

        public ActionResult Account(string token = "", int d = 0)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            Users _UsersModel = new Users(); 
            if (workspaceid!=11)
            {
                return View("unauth");
            }
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            var _filtermodel = new memberfilter();
            _filtermodel = new filterservices().getfilter<memberfilter>(_filtermodel, userid, -20);
            _UsersModel.F_Location = string.IsNullOrEmpty(_filtermodel.Location) ? string.Empty : _filtermodel.Location;
            _UsersModel.F_status = _filtermodel.status ?? 0;
            _UsersModel.F_isfilter = _filtermodel.isActiveFilter;
            _UsersModel.F_workspace = _filtermodel.workspace ?? 0;
            _UsersModel.F_length = _filtermodel.length ?? 25;
            string message = "";
            if (!string.IsNullOrEmpty(token))
            {
                UserService _UserService = new UserService();
                Crypto _crypt = new Crypto();
                int _id = Convert.ToInt32(_crypt.DecryptStringAES(token));
                if (_id == userid)
                {
                    message = "Error : You can't delete your account";
                }
                else
                {
                    if (!_permission.Contains(32))
                    {
                        return View("unauth");
                    }
                    else
                    {
                        message = d == 1 ? _UserService.InactiveUser(_id) : _UserService.DeleteUser(_id);
                    }
                }
            }
            if (TempData["error"] != null)
            {
                message = TempData["error"].ToString();
            }
            _UsersModel = new helper().GenerateError<Users>(_UsersModel, message);
            return View(_UsersModel);
        }

        public ActionResult ViewAccount(int id)
        {
            dashboardservices _ser = new dashboardservices();
            var _model = _ser.GetDashBoardData(id);
            ViewBag.WorkspaceId = id;
            return View(_model);
        }

        public JsonResult FetchAccounts(int start = 0, int length = 25, int draw = 1, int searchtype = 0)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            string search = Request.Params["search[value]"] ?? string.Empty;
            string Location = Request.Params["Location"] ?? string.Empty;
            int workspaceid = Request.Params["workspaceid"] == null ? 0 : Convert.ToInt32(Request.Params["workspaceid"]);
            int status = Request.Params["status"] == null ? 0 : Convert.ToInt32(Request.Params["status"]);
            
            var _filtermodel = new memberfilter();
            switch (searchtype)
            { 
                case 1:
                    _filtermodel.status = status;
                    _filtermodel.Location = Location;
                    _filtermodel.workspace = workspaceid;
                    _filtermodel.length = length;
                    new filterservices().save_filter<memberfilter>(_filtermodel, userid, -20);
                    break;
                case 2:
                    status = 0;
                    Location = "";
                    workspaceid = 0;
                    length = 25;
                    new filterservices().delete_filter(userid, -20);
                    break;
            }
            start = start == 0 ? 1 : start + 1;
            length = start == 1 ? length : start - 1 + length;
            UserService _services = new UserService();
            datatableAjax<Accounts> tableDate = new datatableAjax<Accounts>();
            int total = 0;
            var _model = _services.GetAllActiveAccountsByIndex(ref total, start, length, search, Location, status, workspaceid);
            tableDate.draw = draw;
            tableDate.data = _model;
            tableDate.recordsFiltered = _model.Count() > 0 ? total : 0;
            tableDate.recordsTotal = total;
            return base.Json(tableDate, JsonRequestBehavior.AllowGet);
        }

        public void update_status(int id, int statusid)
        {
            new UserService().UpdateUserStatus(id, statusid);
        }
        public void UpdateAccountStatus(int id, int statusid)
        {
            new UserService().UpdateAccountStatus(id, statusid);
        }
        public JsonResult rolesbycount(int? Id)
        {
            int workspaceid = Id.HasValue ? Id.Value : Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            PermissionServices _ser = new PermissionServices();
            List<PermissionRoleCountModal> _mdl = _ser.GetPermissionRoleByCount(workspaceid);
            return Json(_mdl, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getjobbyid(int jobid)
        {
            var _model = new UserService().GetJobClientWithCityState(jobid);
            return Json(_model, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getofferbyid(int jobid)
        {
            var _model = new UserService().GetOfferClientWithCityState(jobid);
            return Json(_model, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Fetchmembers(int start = 0,int Role=0, int Contractor=0,int Screening=0,int length = 25, int draw = 1, string background = "", string drug = "", int status = 0, int searchtype = 0, string membertype = "M")
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string search = Request.Params["search[value]"] ?? string.Empty;
            string Location = Request.Params["Location"] ?? string.Empty;
            string _amount = Request.Params["amount"] ?? string.Empty;
            string _skills = Request.Params["skills"] ?? string.Empty; 
            bool phoneView = _permission.Contains(5064);
            bool EmailView = _permission.Contains(5065);
            bool rateView = _permission.Contains(5066);
            var _filtermodel = new memberfilter();
            switch (searchtype)
            { 
                case 1:
                    _filtermodel.status = status;
                    _filtermodel.background = background;
                    _filtermodel.drug = drug;
                    _filtermodel.Location = Location;
                    _filtermodel.amount = _amount;
                    _filtermodel.skills = _skills;
                    _filtermodel.membertype = membertype;
                    _filtermodel.Role = Role;
                    _filtermodel.Contractor = Contractor;
                    _filtermodel.Screening = Screening;
                     _filtermodel.length = length;
                    new filterservices().save_filter<memberfilter>(_filtermodel, userid, pageid);
                    break;
                case 2:
                    status = 0;
                    background = "all";
                    drug = "all";
                    Location = "";
                    _amount = "0";
                    _skills = "0";
                    membertype = "M";
                    Role = 0;
                    Contractor = 0;
                    Screening = 0;
                    length = 25;
                    new filterservices().delete_filter(userid, pageid);
                    break;  
            }
            if(Screening!=0)
            {
                switch(Screening)
                {
                    case 1:
                        background = "yes";
                        break;
                    case 2:
                        drug = "yes";
                        break;
                }
            }
            int[] amount = Array.ConvertAll(_amount.Split(','), int.Parse);
            int[] skills = Array.ConvertAll(_skills.Split(','), int.Parse);
            start = start == 0 ? 1 : start + 1;
            length = start == 1 ? length : start - 1 + length;
            UserService _services = new UserService();
            datatableAjax<UserItem> tableDate = new datatableAjax<UserItem>();
            int total = 0;
            var type = Request.Cookies["_slist"] == null ? "T" : Request.Cookies["_slist"].Value.ToString();
            var _model = _services.GetAllActiveUsersByIndex(ref total, start, length, search, ",", Location, amount, background, drug, status, skills, phoneView, EmailView, rateView, type, userid, membertype, workspaceid, Contractor, Role);
            tableDate.draw = draw;
            tableDate.data = _model;
            tableDate.recordsFiltered = _model.Count() > 0 ? total : 0;
            tableDate.recordsTotal = total;
            return base.Json(tableDate, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FetchAccountUsers(int start = 0, int length = 10, int draw = 1)
        {
            int workspaceid = Request.Params["domain"] == null ? 0 : Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            start = start == 0 ? 1 : start + 1;
            length = start == 1 ? length : start - 1 + length;
            UserService _services = new UserService();
            datatableAjax<UserItem> tableDate = new datatableAjax<UserItem>();
            int total = 0;
            var _model = _services.GetAllAccountUsersByIndex(ref total, start, length, string.Empty, ",", string.Empty, null, string.Empty, string.Empty, 0, null, true, true, true, "T", 0, "M", workspaceid);
            tableDate.draw = draw;
            tableDate.data = _model;
            tableDate.recordsFiltered = _model.Count() > 0 ? total : 0;
            tableDate.recordsTotal = total;
            return base.Json(tableDate, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FetchAccountJobs(int start = 0, int length = 10, int draw = 1)
        {
            int workspaceid = Request.Params["domain"] == null ? 0 : Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            start = start == 0 ? 1 : start + 1;
            length = start == 1 ? length : start - 1 + length;
            int total = 0;
            dashboardservices _services = new dashboardservices();
            datatableAjax<dashboardjobs> _AjaxResponse = new datatableAjax<dashboardjobs>();
            var _Model = _services.GetRecentJobs(ref total, start, length, workspaceid);
            _AjaxResponse.draw = draw;
            _AjaxResponse.data = _Model;
            _AjaxResponse.recordsFiltered = _Model.Count() > 0 ? total : 0;
            _AjaxResponse.recordsTotal = total;
            return base.Json(_AjaxResponse, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FetchAccountProjects(int start = 0, int length = 25, int draw = 1)
        {
            int workspaceid = Request.Params["domain"] == null ? 0 : Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            start = start == 0 ? 1 : start + 1;
            length = start == 1 ? length : start - 1 + length;
            projectServices _ser = new projectServices();
            datatableAjax<_projectitems> tableDate = new datatableAjax<_projectitems>();
            int total = 0;
            var _model = _ser.GetActiveProjectByIndex(ref total, start, length, string.Empty, string.Empty, 0, 0, 0, 0, workspaceid);
            tableDate.draw = draw;
            tableDate.data = _model;
            tableDate.recordsFiltered = _model.Count() > 0 ? total : 0;
            tableDate.recordsTotal = total;
            return Json(tableDate, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FetchSMSReminder(int start = 0, int length = 25, int draw = 1)
        {
            int workspaceid = Request.Params["domain"] == null ? 0 : Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            start = start == 0 ? 1 : start + 1;
            length = start == 1 ? length : start - 1 + length;
            dashboardservices _services = new dashboardservices();
            datatableAjax<ReminderFetch> tableDate = new datatableAjax<ReminderFetch>();
            int total = 0;
            var _model = _services.GetReminders(ref total, start, length, workspaceid,"SMS");
            tableDate.draw = draw;
            tableDate.data = _model;
            tableDate.recordsFiltered = _model.Count() > 0 ? total : 0;
            tableDate.recordsTotal = total;
            return Json(tableDate, JsonRequestBehavior.AllowGet);
        }
        public JsonResult FetchEmailReminder(int start = 0, int length = 25, int draw = 1)
        {
            int workspaceid = Request.Params["domain"] == null ? 0 : Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            start = start == 0 ? 1 : start + 1;
            length = start == 1 ? length : start - 1 + length;
            dashboardservices _services = new dashboardservices();
            datatableAjax<ReminderFetch> tableDate = new datatableAjax<ReminderFetch>();
            int total = 0;
            var _model = _services.GetReminders(ref total, start, length, workspaceid, "Email");
            tableDate.draw = draw;
            tableDate.data = _model;
            tableDate.recordsFiltered = _model.Count() > 0 ? total : 0;
            tableDate.recordsTotal = total;
            return Json(tableDate, JsonRequestBehavior.AllowGet);
        }
        public ActionResult edit(string token, int tab = 1)
        {
            if (!_permission.Contains(31))
            {
                return View("unauth");
            }
            UserService _m = new UserService();
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            Crypto _crypt = new Crypto();
            int id = Convert.ToInt32(_crypt.DecryptStringAES(token));
            UserModel _mdl = _m.GetUserProfileById(id);
            _mdl.Tab = tab;
            if (TempData["error"] != null)
            {
                _mdl = new helper().GenerateError<UserModel>(_mdl, TempData["error"].ToString());
            }
            var type = Request.Cookies["_slist"] == null ? "T" : Request.Cookies["_slist"].Value.ToString();
            _mdl.PERMISSIONS = _permission;
            _mdl.issubcontractor = type == "S" || type == "LT";
            //_mdl.allowTools = _mdl.AccountType != "Taskayak free";
            _mdl.RoleList = get_all_roles(workspaceid);
            _mdl.DocumentTypeList = get_all_doctypes();
            return View(_mdl);
        }
        public PartialViewResult GetClientList(int UserId, string token)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            UserClients model = new UserClients
            {
                Token = token
            };
            var tuple = new clientservices().GetUserClient(workspaceid, UserId);
            model.ClientList = tuple.Item1;
            model.Clients = tuple.Item2.ToArray();
            return PartialView("_GetClientList", model);
        }
        public PartialViewResult GetUserSkills(int UserId, string token)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            UserSkills model = new UserSkills
            {
                Token = token
            };
            var tuple = new skillservices().GetUserSkills(workspaceid, UserId);
            model.SkillList = tuple.Item1;
            model.Skills = tuple.Item2.ToArray();
            return PartialView("_GetUserSkills", model);
        }
        public PartialViewResult GetUserTools(int UserId, string token)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            UserTool model = new UserTool
            {
                Token = token
            };
            var tuple = new skillservices().GetUserTools(workspaceid, UserId);
            model.ToolList = tuple.Item1.GroupBy(a => a.TypeName).ToDictionary(a => a.Key, a => a.ToList());
            model.Tools = tuple.Item2.ToArray();
            return PartialView("_GetUserTools", model);
        }
        public PartialViewResult GetUserDocument(int UserId, string token)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            UserDoc model = new UserDoc
            {
                Token = token,
                DocumentList = new UserService().GetDocumentByUserid(UserId, workspaceid).OrderBy(a => a.MainOrder).GroupBy(a => a.FileTypeName).ToDictionary(a => a.Key, a => a.ToList()),
                DeletePermission = _permission.Contains(5069),
                DownloadPermission = _permission.Contains(5068)
            };
            return PartialView("_GetUserDocument", model);
        }
        public List<DocumentTypeItems> get_all_doctypes()
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            var dept = new documentservices();
            return dept.AllDocumentType(workspaceid);
        }
        [HttpPost]
        public ActionResult edit(UserModel _mdl)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            ViewBag.tab = 1;
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            UserService _m = new UserService();
            string street = ""; string city = ""; string state = ""; string zipcode = ""; int StateId = 0;
            _mdl.MapValidateAddress.GetAddress(ref street, ref city, ref state, ref zipcode);
            _mdl.CityId = new defaultservices().CreateCity(ref StateId, state, city);
            _mdl.StateId = StateId;
            _mdl.Zipcode = zipcode;
            _mdl.Address = street;
            var mes = _m.UpdateUserProfile(_mdl, userid, workspaceid);
            TempData["error"] = mes;
            string token = new Crypto().EncryptStringAES(_mdl.UserTableId.ToString());
            return RedirectToAction("edit", "users", new { token = token, tab = 1 });
        }


        public ActionResult delete_bank(string mtoken, string token)
        {
            Crypto _crypt = new Crypto();
            int id = Convert.ToInt32(_crypt.DecryptStringAES(token));
            ViewBag.tab = 1;
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            UserService _m = new UserService();
            var mes = _m.DeleteUserBank(id);
            TempData["error"] = mes;
            return RedirectToAction("edit", "users", new { token = mtoken, tab = 2 });
        }
        public ActionResult delete_doc(string mtoken, string token)
        {
            Crypto _crypt = new Crypto();
            int id = Convert.ToInt32(_crypt.DecryptStringAES(token));
            ViewBag.tab = 1;
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            UserService _m = new UserService();
            var mes = _m.DeleteUserDocumentById(id);
            TempData["error"] = mes;
            return RedirectToAction("edit", "users", new { token = mtoken, tab = 1 });
        }





        [HttpPost]
        public ActionResult edit_image(HttpPostedFileBase file, int userid)
        {
            Crypto _crypt = new Crypto();
            string token = _crypt.EncryptStringAES(userid.ToString());
            helper _helper = new helper();
            UserService _m = new UserService();
            string path = Server.MapPath("~/image/picture-" + userid);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            ViewBag.tab = 4;
            ViewBag.permissions = _permission;
            string filenames = _helper.GetRandomAlphaNumeric(3);
            var extension = Path.GetExtension(file.FileName);
            var imagepath = path + "/" + filenames + extension;
            string tempfilename = "~/image/picture-" + userid + "/" + filenames + extension;
            if (System.IO.File.Exists(imagepath))
            {
                System.IO.File.Delete(imagepath);
            }
            file.SaveAs(imagepath);
            var mes = _m.AddUserProfileImage(tempfilename, userid);
            TempData["error"] = mes;
            return RedirectToAction("edit", "users", new { token = token, tab = 4 });
        }


        [HttpPost]
        public ActionResult edit_doc(HttpPostedFileBase file, string token, string doc, int _dctype = 0)
        {
            Crypto _crypt = new Crypto();
            int userid = Convert.ToInt32(_crypt.DecryptStringAES(token.ToString()));
            ViewBag.permissions = _permission;
            UserService _m = new UserService();
            string path = Server.MapPath("~/image/doc-" + userid);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            ViewBag.tab = 4;
            string filenames = new helper().GetRandomAlphaNumeric(4);
            var extension = Path.GetExtension(file.FileName);
            var imagepath = path + "/" + filenames + extension;
            string tempfilename = "/image/doc-" + userid + "/" + filenames + extension;
            if (System.IO.File.Exists(imagepath))
            {
                System.IO.File.Delete(imagepath);
            }
            file.SaveAs(imagepath);
            var mes = _m.AddUserDocument(doc, userid, tempfilename, _dctype);
            TempData["error"] = mes;
            return RedirectToAction("edit", "users", new { token = token, tab = 1 });
        }
        [HttpPost]
        public ActionResult edit_skills(int[] skills, string token)
        {
            ViewBag.tab = 2;
            Crypto _crypt = new Crypto();
            int userid = Convert.ToInt32(_crypt.DecryptStringAES(token.ToString()));
            UserService _m = new UserService();
            var mes = _m.UpdateUserSkills(skills, userid);
            TempData["error"] = mes;
            return RedirectToAction("edit", "users", new { tab = 2, token = token });
        }

        [HttpPost]
        public ActionResult edit_clients(int[] clients, string token)
        {
            ViewBag.tab = 4;
            Crypto _crypt = new Crypto();
            int userid = Convert.ToInt32(_crypt.DecryptStringAES(token.ToString()));
            UserService _m = new UserService();
            var mes = _m.UpdateAllowedClientsList(clients, userid);
            TempData["error"] = mes;
            return RedirectToAction("edit", "users", new { tab = 4, token = token });
        }



        [HttpPost]
        public ActionResult add_tools(string tooltype1, string toolname1, string token)
        { 
            Crypto _crypt = new Crypto();
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            int userid = Convert.ToInt32(_crypt.DecryptStringAES(token.ToString()));
            TempData["error"] = new toolservices().add_new_toolbytypename(toolname1, tooltype1, userid, workspaceid); 
            return RedirectToAction("edit", "users", new { tab = 3, token = token });
        }

        [HttpPost]
        public ActionResult edit_tools(int[] tools, string token)
        {
            ViewBag.tab = 1;
            Crypto _crypt = new Crypto();
            int userid = Convert.ToInt32(_crypt.DecryptStringAES(token.ToString()));
            UserService _m = new UserService();
            var mes = _m.UpdateTools(tools, userid);
            TempData["error"] = mes;
            return RedirectToAction("edit", "users", new { tab = 3, token = token });
        }



        [HttpPost]
        public ActionResult add(UserModel _mdl)
        {
            UserService _m = new UserService();
            Users _model = new Users();
            defaultservices _default = new defaultservices();
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            List<int> _ctyiid = new List<int>();
            var _filtermodel = new memberfilter();
            _filtermodel = new filterservices().getfilter<memberfilter>(_filtermodel, userid, pageid);
            _model.F_skills = string.IsNullOrEmpty(_filtermodel.skills) ? _ctyiid.ToArray() : Array.ConvertAll(_filtermodel.skills.Split(','), int.Parse);
            _model.F_amount = string.IsNullOrEmpty(_filtermodel.amount) ? _ctyiid.ToArray() : Array.ConvertAll(_filtermodel.amount.Split(','), int.Parse); ;
            _model.F_Location = string.IsNullOrEmpty(_filtermodel.Location) ? "" : _filtermodel.Location;
            _model.F_drug = _filtermodel.drug;
            _model.F_background = _filtermodel.background;
            _model.F_status = _filtermodel.status ?? 0;
            _model.F_isfilter = _filtermodel.isActiveFilter;
            _model.PERMISSIONS = _permission;
            _model.F_role = _filtermodel.Role ?? 0;
            _model.F_contractor = _filtermodel.Contractor ?? 0;
            _model.F_screening = _filtermodel.Screening ?? 0;
            _model.F_length = _filtermodel.length ?? 25;
            string street = ""; string city = ""; string state = ""; string zipcode = ""; int StateId = 0;
            _mdl.MapValidateAddress.GetAddress(ref street, ref city, ref state, ref zipcode);
            _mdl.CityId = new defaultservices().CreateCity(ref StateId, state, city);
            _mdl.StateId = StateId;
            _mdl.Zipcode = zipcode;
            _mdl.Address = street;
            var mes = _m.AddNewUser(_mdl, userid, workspaceid);
            _model.SkillList = get_all_skills(workspaceid);
            _model = new helper().GenerateError<Users>(_model, mes);
            _model.select = _default.GetAllActiveState();
            var type = Request.Cookies["_slist"] == null ? "T" : Request.Cookies["_slist"].Value.ToString();
            _model.F_issubcontractor = type == "S" || type == "LT";
            _model.WorkSpaceId = workspaceid;
            ViewBag.roles = new PermissionServices().GetAllActivePermissionRolesDropDown(workspaceid);
            ViewBag.COntractor = new ContractorServices().GetAllActiveCOntractor(workspaceid);
            return View("index", _model);
        }
        public List<PermissionRoleItems> get_all_roles(int workspaceid)
        {
            var deptser = new PermissionServices();
            return deptser.GetAllActivePermissionRoles(workspaceid)._PermissionRoles;
        }

        public List<SkillItems> get_all_skills(int workspaceid)
        {
            var dept = new skillservices();
            return dept.get_all_active_skills(workspaceid)._skill;
        }
        public List<TechnicianClientsModel> get_all_tech_clients(int workspaceid)
        {
            var clservice = new clientservices();
            return clservice.GetActiveClient(workspaceid);
        }

        public string get_member_rate(int id)
        {
            var member = new UserService();
            return member.GetUserRateById(id);
        }

        [HttpPost]
        public bool validate(int type, string value)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            var _ac = new accountservices();
            bool isexist = false;
            switch (type)
            {
                case 1:
                    isexist = _ac.validate_email(value, workspaceid);
                    break;
                case 2:
                    isexist = _ac.validate_memberid(value, workspaceid);
                    break;
                case 3:
                    isexist = _ac.validate_username(value, workspaceid);
                    break;
            }
            return isexist;
        }


        public class TableDate<T>
        {
            public List<T> data
            {
                get;
                set;
            }

            public int draw
            {
                get;
                set;
            }

            public int recordsFiltered
            {
                get;
                set;
            }

            public int recordsTotal
            {
                get;
                set;
            }
        }

    }
}