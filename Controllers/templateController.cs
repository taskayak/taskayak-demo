﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace hrm.Controllers
{
    [AuthorizeVerifiedloggedin]
    public class templateController : Controller
    {
        public List<int> _permission;
        public templateController() { 
string PList = System.Web.HttpContext.Current.Request.Cookies["_plist"] == null ? "0" : System.Web.HttpContext.Current.Request.Cookies["_plist"].Value.ToString();
            _permission = PList == "0" ? null : JsonConvert.DeserializeObject<List<int>>(PList);
        }
        // GET: template
        public ActionResult Index()
        {
            if (!_permission.Contains(4064))
            {
                return View("unauth");
            }
            ViewBag.permissions = _permission;
            templateservices _ser = new templateservices();
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            templateModal _temp = _ser.get_all_active_template(null, workspaceid);
            return View(_temp);
        }

        public ActionResult add()
        {
            if (!_permission.Contains(5122))
            {
                return View("unauth");
            }
            ViewBag.permissions = _permission;
            templateservices _ser = new templateservices();
            templateitem _temp = new templateitem();
            return View(_temp);
        }

        public ActionResult edit(int id)
        {
            if (!_permission.Contains(5120))
            {
                return View("unauth");
            }
            ViewBag.permissions = _permission;
            templateservices _ser = new templateservices();
            templateitem _temp = _ser.get_template(id);
            return View(_temp);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult add(templateitem _temp)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            templateservices _ser = new templateservices();
            var message = _ser.add_new_template(_temp, userid, workspaceid);
            ModelState.Clear();
            templateModal _model = _ser.get_all_active_template(null, workspaceid);
            ViewBag.permissions = _permission;
            _model = new helper().GenerateError<templateModal>(_model, message);
            return View("Index", _model);
           
        }

        public ActionResult delete(int id)
        {
            if (!_permission.Contains(5121))
            {
                return View("unauth");
            }
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            ViewBag.permissions = _permission;
            var _ser = new templateservices();
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            var error_text = _ser.delete_template(id);
            templateModal _model = _ser.get_all_active_template(null, workspaceid);
            _model = new helper().GenerateError<templateModal>(_model, error_text);
            return View("Index", _model);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult edit(templateitem _temp)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
             templateservices _ser = new templateservices();
            var message = _ser.update_template(_temp);
            ModelState.Clear();
            templateModal _model = _ser.get_all_active_template(null, workspaceid);
            ViewBag.permissions = _permission;
            _model = new helper().GenerateError<templateModal>(_model, message);
            return View("Index", _model);

        }

        public ActionResult setdefault(int tempid,bool templateddl)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            templateservices _ser = new templateservices();
            var message = _ser.update_default(tempid, templateddl);
            ModelState.Clear();
            templateModal _model = _ser.get_all_active_template(null, workspaceid);
            ViewBag.permissions = _permission;
            _model = new helper().GenerateError<templateModal>(_model, message);
            return View("Index", _model);
        }
        public JsonResult Fetchtemplates(int start = 0, int length = 25, int draw = 1)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string search = Request.Params["search[value]"] == null ? "" : Request.Params["search[value]"];
            start = (start == 0 ? 1 : start + 1);
            length = (start == 1 ? length : (start - 1) + length);
            templateservices _services = new templateservices();
            datatableAjax<templateitem> tableDate = new datatableAjax<templateitem>();
            int total = 0;
            var _model = _services.get_all_active_template_bypaging(ref total, start, length, search, workspaceid);
            tableDate.draw = draw;
            tableDate.data = _model;
            tableDate.recordsFiltered = (_model.Count() > 0 ? total : 0);
            tableDate.recordsTotal = total;
            return base.Json(tableDate, JsonRequestBehavior.AllowGet);
        }
    }
}