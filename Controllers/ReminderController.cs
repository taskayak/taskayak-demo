﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace hrm.Controllers
{
    [AuthorizeVerifiedloggedin]
    public class ReminderController : Controller
    {
        public List<int> _permission;
        public ReminderController()
        {
            string PList = System.Web.HttpContext.Current.Request.Cookies["_plist"] == null ? "0" : System.Web.HttpContext.Current.Request.Cookies["_plist"].Value.ToString();
            _permission = PList == "0" ? null : JsonConvert.DeserializeObject<List<int>>(PList);
        }
        public ActionResult confirm(string jtoken, string rtoken)
        {
            return RedirectToAction("view", "jobs", new { token = jtoken });
        }

        // GET: Reminder
        public ActionResult Index(int reminderId = 0, int userType = 1)
        {
            if (!_permission.Contains(5124))
            {
                return View("unauth");
            }
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            remindersetting _setting = new projectServices().GetReminders(workspaceid);
            _setting.Tabid = reminderId;
            _setting.Usertype = userType;
            string message = TempData["errordata"] != null ? TempData["errordata"].ToString() : string.Empty;
            _setting = new helper().GenerateError<remindersetting>(_setting, message);
            return View(_setting);
        }

        [HttpPost]
        public ActionResult reminder(int usertype, reminder item)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString()); 
            var tuple = new projectServices().AddNewReminder(usertype, item, workspaceid, 0);
            TempData["errordata"] = tuple.Item1;
            return RedirectToAction("Index", new { reminderId = tuple.Item2, userType = usertype });
        }

        [HttpPost]
        public ActionResult editReminder(int usertype, reminder item, string button = "update")
        {
            int reminderId = Convert.ToInt32(new Crypto().DecryptStringAES(item.token));
            TempData["errordata"] = new projectServices().UpdateReminder(usertype, item, 0, reminderId); 
            return RedirectToAction("Index", new {  reminderId, userType = usertype });
        }

        public ActionResult deleteReminder(string token = "")
        {
            int reminderId = Convert.ToInt32(new Crypto().DecryptStringAES(token));
            TempData["errordata"] = new projectServices().DeleteReminder(reminderId); 
            return RedirectToAction("Index");
        }
    }
}