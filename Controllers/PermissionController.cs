﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace hrm.Controllers
{
    [AuthorizeVerifiedloggedin]
    public class PermissionController : Controller
    {
        public List<int> _permission;
        public PermissionController()
        {
            string PList = System.Web.HttpContext.Current.Request.Cookies["_plist"] == null ? "0" : System.Web.HttpContext.Current.Request.Cookies["_plist"].Value.ToString();
            _permission = PList == "0" ? null : JsonConvert.DeserializeObject<List<int>>(PList);
        }
        public ActionResult Index(int? id)
        {
            if (!_permission.Contains(52))
            {
                return View("unauth");
            }
            PermissionRoleModal _model = new PermissionRoleModal();
            var Permissions = new PermissionServices();
            string message = "";
            if (id.HasValue)
            {
                message = Permissions.DeletePermissionsRole(id.Value);
            }
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            _model = Permissions.GetAllActivePermissionRole(workspaceid);
            if (TempData["error"] != null)
            {
                message = TempData["error"].ToString();
            }
            ViewBag.permissions = _permission;
            _model = new helper().GenerateError<PermissionRoleModal>(_model, message);
            return View(_model);
        }


        public ActionResult set_permissions(int id)
        {
            //if (!_permission.Contains(60))
            //{
            //    return View("unauth");
            //}
            ViewBag.permissions = _permission;
            Permissions _model = new Permissions();
            var Service = new PermissionServices();
            string name = "";
            string message = "";
            if (TempData["error"] != null)
            {
                message = TempData["error"].ToString();
            }
            _model = new helper().GenerateError<Permissions>(_model, message);
            _model._Permissions = Service.GetAllActivePermissionsWithType();
            _model._Userpermissions = Service.GetAllActivePermissionsByPermissionRoleId(id, ref name);
            _model.Ruleid = id;
            _model.name = name;
            _model.tabId = string.Empty;
            return View(_model);
        }

        [HttpPost]
        public ActionResult set_permissions(int Ruleid, int[] perms = null,string tabid = "1")
        {
            ViewBag.permissions = _permission;
            Permissions _model = new Permissions();
            var Service = new PermissionServices();
            string message = "";
            if (perms != null)
            {
                message = Service.UpdatePermissionRule(perms, Ruleid);
            }
            else
            {
                message = Service.DeletePermissionRule(Ruleid);
            }
            _model = new helper().GenerateError<Permissions>(_model, message);
            string name = "";
            _model._Permissions = Service.GetAllActivePermissionsWithType();
            _model._Userpermissions = Service.GetAllActivePermissionsByPermissionRoleId(Ruleid, ref name);
            _model.Ruleid = Ruleid;
            _model.name = name;
            _model.tabId = tabid;
            return View(_model);
        }


        [HttpPost]
        public ActionResult add(PermissionRoleModal _model)
        {
            var Service = new PermissionServices();
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string message = Service.AddPermissionRole(_model, userid, workspaceid);
            if (!message.ToLower().Contains("error"))
            {
                ModelState.Clear();
            } 
            _model = Service.GetAllActivePermissionRole(workspaceid);
            _model = new helper().GenerateError<PermissionRoleModal>(_model, message);
            return View("index", _model);
        }


        public ActionResult add()
        {
            if (!_permission.Contains(35))
            {
                return View("unauth");
            }
            PermissionRoleModal _model = new PermissionRoleModal();
            var Service = new PermissionServices();
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            string message = "";
            if (!message.ToLower().Contains("error"))
            {
                ModelState.Clear();
            }
            _model = Service.GetAllActivePermissionRole(workspaceid);
            _model = new helper().GenerateError<PermissionRoleModal>(_model, message);
            return View("index", _model);
        }


        [HttpPost]
        public ActionResult edit(PermissionRoleModal _model)
        {
            var Service = new PermissionServices();
            ViewBag.permissions = _permission;
            string message = Service.UpdatePermissionRole(_model);
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            if (!message.ToLower().Contains("error"))
            {
                ModelState.Clear();
            }
            _model = Service.GetAllActivePermissionRole(workspaceid);
            _model = new helper().GenerateError<PermissionRoleModal>(_model, message);
            return View("index", _model);
        }


        public ActionResult edit()
        {
            if (!_permission.Contains(37))
            {
                return View("unauth");
            }
            PermissionRoleModal _model = new PermissionRoleModal();
            var Service = new PermissionServices(); 
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            ViewBag.permissions = _permission;
            string message = "";
            _model = Service.GetAllActivePermissionRole(workspaceid);
            _model = new helper().GenerateError<PermissionRoleModal>(_model, message);
            return View("index", _model);
        }

    }
}