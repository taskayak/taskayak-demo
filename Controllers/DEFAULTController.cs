﻿using hrm.Database;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrm.Controllers
{
    public class DefaultController : Controller
    {
        public void SendReminders()
        {
            SortedList _srt = new SortedList();
            SqlHelper sqlHelper = new SqlHelper();
            DateTime Startdate = DateTime.Now;
            int Id;
            int ReminderId;
            bool NeedConfirmation;
            int jobId;
            int UserId;
            string Type;
            string info;
            string Attachment;
            var dt = sqlHelper.fillDataTable("SendReminders", "", _srt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                jobId = Convert.ToInt32(dt.Rows[i]["jobId"].ToString());
                UserId = Convert.ToInt32(dt.Rows[i]["UserId"].ToString());
                ReminderId = Convert.ToInt32(dt.Rows[i]["ReminderId"].ToString());
                Id = Convert.ToInt32(dt.Rows[i]["Id"].ToString());
                NeedConfirmation = Convert.ToBoolean(dt.Rows[i]["NeedConfirmation"].ToString());
                Type = dt.Rows[i]["Type"].ToString();
                info = dt.Rows[i]["info"].ToString();
                Attachment = dt.Rows[i]["Attachment"].ToString();
                SendProcessedReminders(jobId, UserId, Id, NeedConfirmation, Type, info, ReminderId, Attachment);
            }
        }

        public void SendProcessedReminders(int _jobId, int _userId, int _Id, bool _NeedConfirmation, string _type, string _info, int _reminderId, string Attachment)
        {
            Crypto _crypt = new Crypto();
            string acceptlink = _NeedConfirmation == true ? "https://convo.taskayak.com/reminder/confirm?jtoken=" + _crypt.EncryptStringAES(_jobId.ToString()) + "&rtoken=" + _crypt.EncryptStringAES(_reminderId.ToString()) : "";
            string Attachments = "";
            var job = new jobofferservices().GetMailMergeOnlyJobDetailsById(_jobId);
            string GoogleMapLink = "https://www.google.com/maps/search/?api=1&query=" + Url.Encode(job.address.Replace("<br/>", " ").Replace(",", " "));
            var Tools = new Jobservices().GetToolsNameByJobId(_jobId).ToDelimitedString();
            var mdl = new jobofferservices().GetAllActiveMembersById(_userId);
            SqlHelper sqlHelper = new SqlHelper();
            string SmsBody = "Reminder for job ID " + job.JobId + " to be perform  " + job.sdate + " at  " + job.stime + " at this address " + job.address.Replace("<br/>", "");
            string Emailbody = "<p>Hi&nbsp;" + mdl.FirstName + ",</p><p>This is a reminder for job ID&nbsp;" + job.JobId + ".&nbsp;</p><table cellpadding='4' cellspacing='1' style='width:500px'><tbody>";
            if (_info.Contains("1"))
            {
                Emailbody = Emailbody + "<tr><td><b>ID</b></td><td>" + job.JobId + "</td></tr>";
            }
            if (_info.Contains("2"))
            {
                Emailbody = Emailbody + "<tr><td><b>Title</b></td><td> " + job.title + "</td></tr>";
            }
            if (_info.Contains("3"))
            {
                Emailbody = Emailbody + "<tr><td><b>Start Date</b></td><td> " + job.datetime + "</td></tr>";
            }
            if (_info.Contains("4"))
            {
                Emailbody = Emailbody + "<tr><td><b>Address</b></td><td> " + job.address.Replace("<br>", "").Replace("<br/>", "") + "</td></tr>";
            }
            if (_info.Contains("5"))
            {
                Emailbody = Emailbody + "<tr><td><b>Contact</b></td><td>" + job.Contact + "</td></tr>";
            }
            if (_info.Contains("6"))
            {
                Emailbody = Emailbody + "<tr><td><b>Phone</b></td><td>" + job.Phone + "</td></tr>";
            }
            if (_info.Contains("7"))
            {//tools
                Emailbody = Emailbody + "<tr><td><b>Tools</b></td><td>" + Tools + " </td></tr>";
            }
            if (_info.Contains("8"))
            {
                Emailbody = Emailbody + "<tr><td><b>Instructions</b></td><td>" + job.Instruction.Replace("<br>", "").Replace("<p>", "").Replace("</p>", "") + "</td></tr>";
            }
            Emailbody = Emailbody + "<tr>";
            if (_NeedConfirmation == true)
            {
                Emailbody = Emailbody + "<td> <a href=" + acceptlink + " > Confirm reminder</a></td>";
                SmsBody = SmsBody + Environment.NewLine + "Confirm reminder " + acceptlink;
            }
            else
            {
                Emailbody = Emailbody + "<td></td>";
            }
            if (Attachment.Contains("1"))
            {
                Emailbody = Emailbody + "<td> <a href=" + GoogleMapLink + " >View location </a></td> ";
                SmsBody = SmsBody + Environment.NewLine + "View location " + GoogleMapLink;
            }
            else
            {
                Emailbody = Emailbody + "<td></td>";
            }
            Emailbody = Emailbody + "</tr>";
            Emailbody = Emailbody + "</tbody></table>";
            if (Attachment.Contains("2"))
            {
                var AttchmentList = new Jobservices().GetFilesUrlbyJobId(_jobId);
                Attachments = AttchmentList.Count > 0 ? AttchmentList.ToDelimitedString() : "";
            }
            try
            {

                SortedList _srt = new SortedList();
                string smsbody = "";
                string body = "";
                smsbody = _type.ToLower() == "sms" ? SmsBody : "";
                body = _type.ToLower() == "email" ? Emailbody : "";
                _srt.Clear();
                _srt.Add("@memberid", _userId);
                _srt.Add("@pref", "Auto");
                _srt.Add("@offerId", 0);
                _srt.Add("@sms", _type.ToLower() == "sms" ? smsbody : "");
                _srt.Add("@email", _type.ToLower() == "email" ? body : "");
                _srt.Add("@subject", "This is reminder for job ID " + job.JobId);
                _srt.Add("@termspopup", false);
                if (!string.IsNullOrEmpty(Attachments))
                {
                    _srt.Add("@Attachments", Attachments);
                }
                sqlHelper.executeNonQuery("tbl_job_offer_add", "", _srt);
                _srt.Clear();
                _srt.Add("@Id", _Id);
                sqlHelper.executeNonQuery("UpdateReminderstatus", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();

            }
            finally
            {
                sqlHelper.Dispose();

            }
            // _mdl._jobs = _jbs;

        }


        /// <summary>
        /// -------------------------------------------------------------------------------
        /// </summary>
        public void ProcessRepeatReminder()
        {
            int workspaceid = 11;
            SortedList _srt = new SortedList();
            SqlHelper sqlHelper = new SqlHelper();
            _srt.Add("@workSpaceId", workspaceid);
            DateTime Startdate = DateTime.Now;
            int JobId = 0;
            int Technician = 0;
            int Dispatcher = 0;
            var dt = sqlHelper.fillDataTable("GetJobsWithWorkspace", "", _srt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                JobId = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                Technician = Convert.ToInt32(dt.Rows[i]["Technician"].ToString());
                Dispatcher = Convert.ToInt32(dt.Rows[i]["Dispatcher"].ToString());
                Startdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString());
                ProcessRepeatReminders(JobId, Startdate, Technician, Dispatcher);
            }
        }
        public void ProcessRepeatReminders(int _jobId, DateTime _startDate, int Technician, int Dispatcher)
        {
            List<int> _reminderIds = new List<int>();
            SqlHelper sqlHelper = new SqlHelper();
            List<int> ProcessReminderIds = new List<int>();
            int Id = 0;
            ReminderServices _rd = new ReminderServices();
            DateTime ReminderDate = DateTime.Now;
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@jobId", _jobId);
                int repeatid = 0;
                int reapetdays = 0;
                int Confirmation = 0;
                string type = "";
                int Usertype = 0;
                int UserId = 0;
                var dt = sqlHelper.fillDataTable("GetAllRemindersForrepeat", "", _srt);
                ///parentid
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Id = Convert.ToInt32(dt.Rows[i]["Id"].ToString());
                    repeatid = Convert.ToInt32(dt.Rows[i]["repeatid"].ToString());
                    reapetdays = Convert.ToInt32(dt.Rows[i]["reapetdays"].ToString());
                    type = dt.Rows[i]["type"].ToString();
                    Confirmation = Convert.ToInt32(dt.Rows[i]["Confirmation"].ToString());
                    Usertype = Convert.ToInt32(dt.Rows[i]["Usertype"].ToString());
                    UserId = Usertype == 1 ? Technician : Dispatcher;
                    if (UserId != 0)
                    {
                        if (repeatid == 1)
                        {//hour
                            ReminderDate = _startDate.AddHours(-reapetdays);
                            while (ReminderDate > DateTime.Now)
                            {

                                if (type.Contains("2"))
                                {
                                    _rd.AddReminderForSend(Id, Confirmation > 0 ? true : false, "This is test Body", "SMS", ReminderDate, Usertype == 1 ? Technician : Dispatcher, _jobId);

                                }
                                if (type.Contains("1"))
                                {
                                    _rd.AddReminderForSend(Id, Confirmation > 0 ? true : false, "This is test Body", "Email", ReminderDate, Usertype == 1 ? Technician : Dispatcher, _jobId);
                                }
                                ReminderDate = ReminderDate.AddHours(-reapetdays);
                            }
                        }
                        else if (repeatid == 2)
                        {//day

                            ReminderDate = _startDate.AddDays(-reapetdays);
                            while (ReminderDate > DateTime.Now)
                            {

                                if (type.Contains("2"))
                                {
                                    _rd.AddReminderForSend(Id, Confirmation > 0 ? true : false, "This is test Body", "SMS", ReminderDate, Usertype == 1 ? Technician : Dispatcher, _jobId);

                                }
                                if (type.Contains("1"))
                                {
                                    _rd.AddReminderForSend(Id, Confirmation > 0 ? true : false, "This is test Body", "Email", ReminderDate, Usertype == 1 ? Technician : Dispatcher, _jobId);
                                }
                                ReminderDate = ReminderDate.AddDays(-reapetdays);
                            }
                        }
                        else if (repeatid == 3)
                        {//week
                            ReminderDate = _startDate.AddDays(-(reapetdays * 7));
                            while (ReminderDate > DateTime.Now)
                            {

                                if (type.Contains("2"))
                                {
                                    _rd.AddReminderForSend(Id, Confirmation > 0 ? true : false, "This is test Body", "SMS", ReminderDate, Usertype == 1 ? Technician : Dispatcher, _jobId);

                                }
                                if (type.Contains("1"))
                                {
                                    _rd.AddReminderForSend(Id, Confirmation > 0 ? true : false, "This is test Body", "Email", ReminderDate, Usertype == 1 ? Technician : Dispatcher, _jobId);
                                }
                                ReminderDate = ReminderDate.AddDays(-(reapetdays * 7));
                            }
                        }
                        else if (repeatid == 4)
                        {//month
                            ReminderDate = _startDate.AddMonths(-reapetdays);
                            while (ReminderDate > DateTime.Now)
                            {

                                if (type.Contains("2"))
                                {
                                    _rd.AddReminderForSend(Id, Confirmation > 0 ? true : false, "This is test Body", "SMS", ReminderDate, Usertype == 1 ? Technician : Dispatcher, _jobId);

                                }
                                if (type.Contains("1"))
                                {
                                    _rd.AddReminderForSend(Id, Confirmation > 0 ? true : false, "This is test Body", "Email", ReminderDate, Usertype == 1 ? Technician : Dispatcher, _jobId);
                                }
                            }
                            ReminderDate = ReminderDate.AddMonths(-reapetdays);
                        }
                    }
                }
                dt.Dispose();

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();

            }
            finally
            {
                sqlHelper.Dispose();

            }
            // _mdl._jobs = _jbs;

        }
        /// <summary>
        /// -------------------------------------------------------------------------------------------------------
        /// </summary>
        public void ProcessDoesNotRepeatReminder()
        {
            int workspaceid = 11;
            SortedList _srt = new SortedList();
            SqlHelper sqlHelper = new SqlHelper();
            _srt.Add("@workSpaceId", workspaceid);
            DateTime Startdate = DateTime.Now;
            int JobId;
            int Technician;
            int Dispatcher;
            var dt = sqlHelper.fillDataTable("GetJobsWithWorkspace", "", _srt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                JobId = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                Technician = Convert.ToInt32(dt.Rows[i]["Technician"].ToString());
                Dispatcher = Convert.ToInt32(dt.Rows[i]["Dispatcher"].ToString());
                Startdate = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString());
                if (Startdate > DateTime.Now)
                {
                    ProcessDoesNotRepeatReminders(JobId, Startdate, Technician, Dispatcher);
                }
            }
        }
        public void ProcessDoesNotRepeatReminders(int _jobId, DateTime _startDate, int Technician, int Dispatcher)
        {
            List<int> _reminderIds = new List<int>();
            SqlHelper sqlHelper = new SqlHelper();
            List<int> ProcessReminderIds = new List<int>();
            int Id = 0;
            ReminderServices _rd = new ReminderServices();
            DateTime ReminderDate = DateTime.Now;
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@jobId", _jobId);
                int repeatid = 0;
                int reapetdays = 0;
                int Confirmation = 0;
                string type = "";
                int Usertype = 0;
                int UserId = 0;
                var dt = sqlHelper.fillDataTable("GetAllReminders", "", _srt);
                ///parentid
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Id = Convert.ToInt32(dt.Rows[i]["Id"].ToString());
                    repeatid = Convert.ToInt32(dt.Rows[i]["repeatid"].ToString());
                    reapetdays = Convert.ToInt32(dt.Rows[i]["reapetdays"].ToString());
                    type = dt.Rows[i]["type"].ToString();
                    Confirmation = Convert.ToInt32(dt.Rows[i]["Confirmation"].ToString());
                    Usertype = Convert.ToInt32(dt.Rows[i]["Usertype"].ToString());
                    UserId = Usertype == 1 ? Technician : Dispatcher;
                    if (repeatid == 1)
                    {//hour
                        ReminderDate = _startDate.AddHours(-reapetdays);
                    }
                    else if (repeatid == 2)
                    {//day
                        ReminderDate = _startDate.AddDays(-reapetdays);
                    }
                    else if (repeatid == 3)
                    {//week
                        ReminderDate = _startDate.AddDays(-(reapetdays * 7));
                    }
                    else if (repeatid == 4)
                    {//month
                        ReminderDate = _startDate.AddMonths(-reapetdays);
                    }
                    if (UserId != 0 && ReminderDate > DateTime.Now)
                    {
                        if (type.Contains("2"))
                        {
                            _rd.AddReminderForSend(Id, Confirmation > 0 ? true : false, "This is test Body", "SMS", ReminderDate, UserId, _jobId);

                        }
                        if (type.Contains("1"))
                        {
                            _rd.AddReminderForSend(Id, Confirmation > 0 ? true : false, "This is test Body", "Email", ReminderDate, UserId, _jobId);
                        }
                    }
                }
                dt.Dispose();

            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();

            }
            finally
            {
                sqlHelper.Dispose();

            }
            // _mdl._jobs = _jbs;

        }
        /// <summary>
        /// ----------------------------------------------------------------------------------------------------
        /// </summary> 
        public void ProcessAllReminderToAddInJob()
        {
            int workspaceid = 11;
            SortedList _srt = new SortedList();
            SqlHelper sqlHelper = new SqlHelper();
            _srt.Add("@workSpaceId", workspaceid);
            int JobId = 0;
            var dt = sqlHelper.fillDataTable("GetJobsWithWorkspace", "", _srt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                JobId = Convert.ToInt32(dt.Rows[i]["id"].ToString());
                ProcessReminders(JobId, workspaceid);
            }
        }
        public void ProcessReminders(int _jobId, int _workSpaceId)
        {
            List<int> _reminderIds = new List<int>();
            SqlHelper sqlHelper = new SqlHelper();
            List<int> ProcessReminderIds = new List<int>();
            int Id = 0;
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@workSpaceId", _workSpaceId);
                _srt.Add("@jobId", _jobId);
                var ds = sqlHelper.fillDataSet("ProcessRemindersWithService", "", _srt);
                var dt0 = ds.Tables[0];
                var dt = ds.Tables[1];
                ds.Dispose();

                for (int i = 0; i < dt0.Rows.Count; i++)
                {
                    Id = Convert.ToInt32(dt0.Rows[i]["parentid"].ToString());
                    if (!_reminderIds.Contains(Id)) { _reminderIds.Add(Id); }
                }
                ///parentid
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Id = Convert.ToInt32(dt.Rows[i]["Id"].ToString());
                    if (!_reminderIds.Contains(Id)) { ProcessReminderIds.Add(Id); }
                }
                dt.Dispose();
                if (ProcessReminderIds.Count > 0)
                {
                    _srt.Clear();
                    _srt.Add("@jobId", _jobId);
                    _srt.Add("@ProcessReminderIds", ProcessReminderIds.ToArray().ToDelimitedString());
                    sqlHelper.executeNonQuery("ProcessedReminders", "", _srt);
                }
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();

            }
            finally
            {
                sqlHelper.Dispose();

            }
            // _mdl._jobs = _jbs;

        }

        //public defaultController()
        //{
        //    Worspaceid = Convert.ToInt32(System.Web.HttpContext.Current.Request.Cookies["domain"].Value.ToString());
        //}

        // GET: DEFAULT
        public ActionResult Index()
        {
            return View();
        }
         
        [HttpPost]
        public string GetAllActiveToolType(int id = 0,bool NeedLabel=false)
        {
            int Worspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString()); 
            var deptser = new defaultservices();
            var list = deptser.GetAllActiveToolType(Worspaceid);
            string ddl = NeedLabel? "<option value='0'>Tool Type</option>":string.Empty;
            foreach (var item in list)
            { 
                    if (id == item.Key)
                    {
                        ddl += "<option selected value='" + item.Key + "'>" + item.Value + "</option>"; 
                    }
                    else
                    {
                        ddl += "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                    } 
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }


        [HttpPost]
        public string get_all_active_memberbycompanyid(int id = 0,int UserId=0)
        {
            int Worspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string ddl = "<option value='0'>Manager</option>";
            var deptser = new defaultservices();
            var list = deptser.get_all_active_memberbycompanyid(id, Worspaceid);
            foreach (var item in list)
            {
                if ( UserId!= item.Key)
                {
                    if (id == item.Key)
                    {
                        ddl = ddl + "<option selected value='" + item.Key + "'>" + item.Value + "</option>";

                    }
                    else
                    {
                        ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                    }
                }
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }

        [HttpPost]
        public string getallopenproject(int id = 0, string name = "Select")
        {
            int Worspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string ddl = "<option value='0'>" + name + "</option>";
            var deptser = new defaultservices();
            var list = deptser.get_all_active_project(id, Worspaceid);
            foreach (var item in list)
            {
                if (id == item.Key)
                {
                    ddl = ddl + "<option selected value='" + item.Key + "'>" + item.Value + "</option>";

                }
                else
                {
                    ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                }
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }



        [HttpPost]
        public string get_all_memberid(int id = 0, int cid = 0)
        {
            int Worspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string ddl = "<option value='0'>User Id</option>";
            var deptser = new defaultservices();
            var list = deptser.get_all_active_memberid(cid, Worspaceid);
            foreach (var item in list)
            {
                if (id == item.Key)
                {
                    ddl = ddl + "<option selected value='" + item.Key + "'>" + item.Value + "</option>";

                }
                else
                {
                    ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                }

            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }


        [HttpPost]
        public string get_all_mebertypeprojectmanager(int id = 0, string name = "Select")
        {
            int Worspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string ddl = "<option value='0'>" + name + "</option>";
            var deptser = new defaultservices();
            var list = deptser.get_all_membertype_projectmanager(Worspaceid);
            foreach (var item in list)
            {
                if (id == item.Key)
                {
                    ddl = ddl + "<option selected value='" + item.Key + "'>" + item.Value + "</option>";

                }
                else
                {
                    ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                }

            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }

        [HttpPost]
        public string get_all_manager_by_clientid(int id = 0, int clientid = 0, int ismulti = 0, string name = "Manager")
        {
            int Worspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string ddl = "<option value='0'>" + name + "</option>";
            if (clientid == 0)
            {
                ddl = "<option value='0' selected>" + name + "</option>";
            }
           if (ismulti == 1)
            {
                ddl = "";
            }
            var deptser = new defaultservices();
            var list = deptser.get_all_manager_byid(id);
            foreach (var item in list)
            {
                if (clientid == item.Key)
                {
                    ddl = ddl + "<option selected value='" + item.Key + "'>" + item.Value + "</option>";

                }
                else
                {
                    ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                }
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }


        [HttpPost]
        public string get_all_manager_by_clientiduserid(int Managerid = 0, int userid = 0, int ismulti = 0)
        {
            int Worspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string ddl = "<option value='0'>Manager</option>";
            if (ismulti == 1)
            {
                ddl = "";
            }
            var deptser = new defaultservices();
            var list = deptser.get_all_manager_byuserid(userid, Worspaceid);
            foreach (var item in list)
            {
                if (Managerid == item.Key)
                {
                    ddl = ddl + "<option selected value='" + item.Key + "'>" + item.Value + "</option>";

                }
                else
                {
                    ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                }
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }

        [HttpPost]
        public string get_all_jobs(int jobid = 0, int ismulti = 0)
        {
            int Worspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string ddl = "<option value='0'>Job</option>";
            if (ismulti == 1)
            {
                ddl = "";
            }
            var deptser = new defaultservices();
            var list = deptser.get_all_job(Worspaceid);
            foreach (var item in list)
            {
                if (jobid == item.Key)
                {
                    ddl = ddl + "<option selected value='" + item.Key + "'>" + item.Value + "</option>";

                }
                else
                {
                    ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                }
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }

        [HttpPost]
        public string get_assigned_jobs(int jobid = 0, int memberid = 0, int ismulti = 0, bool forpay = false)
        {
            int Worspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string ddl = "<option value='0'>Job</option>";
            if (ismulti == 1)
            {
                ddl = "";
            }
            var deptser = new defaultservices();
            var list = deptser.get_assigned_jobs(memberid, forpay, Worspaceid);
            foreach (var item in list)
            {
                if (jobid == item.Key)
                {
                    ddl = ddl + "<option selected value='" + item.Key + "'>" + item.Value + "</option>";

                }
                else
                {
                    ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                }
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }

        [HttpPost]
        public string get_all_clients(int clientid = 0, int ismulti = 0,bool needLabel=true)
        {
            int Worspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string ddl = "";
            if(needLabel)
            {
                ddl = "<option value='0'>Client</option>";
            }
            if (ismulti == 1)
            {
                ddl = "";
            }
            var deptser = new defaultservices();
            var list = deptser.get_all_client(Worspaceid);
            foreach (var item in list)
            {
                if (clientid == item.Key)
                {
                    ddl = ddl + "<option selected value='" + item.Key + "'>" + item.Value + "</option>";

                }
                else
                {
                    ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                }
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }


        [HttpPost]
        public string get_all_city_by_stateid(int id, int cityid = 0, int ismulti = 0, string city = "",bool needLabel=true)
        {
            int Worspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string ddl ="";
            if (needLabel)
            {
                ddl = "<option value='0'>City</option>";
            }
           
            if (ismulti == 1)
            {
                ddl = "";
            }
            if (id != 0)
            {
                var deptser = new defaultservices();
                var list = deptser.get_all_city_by_stateid(id);
                foreach (var item in list)
                {
                    if (!string.IsNullOrEmpty(city))
                    {
                        int[] nums = Array.ConvertAll(city.Split(','), int.Parse);
                        if (nums.Contains(item.Key))
                        {
                            ddl = ddl + "<option selected='selected' value='" + item.Key + "'>" + item.Value + "</option>";

                        }
                        else
                        {
                            ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                        }
                    }
                    else
                    {
                        if (cityid == item.Key)
                        {
                            ddl = ddl + "<option selected='selected' value='" + item.Key + "'>" + item.Value + "</option>";

                        }
                        else
                        {
                            ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                        }
                    }


                }
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }

        [HttpPost]
        public string get_all_state(int ismulti = 0, int stateid = 0)
        { 
            string ddl = "<option value='0'>Select</option>";
            if (ismulti == 1)
            {
                ddl = "";
            }
            var deptser = new defaultservices();
            var list = deptser.GetAllActiveState();
            foreach (var item in list)
            {
                if (stateid == item.Key)
                {
                    ddl = ddl + "<option selected value='" + item.Key + "'>" + item.Value + "</option>";

                }
                else
                {
                    ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                }
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }

        [HttpPost]
        public string get_all_member(int memberid = 0, int ismulti = 0, string optvalue = "Member", string type = "M", int currentid = 0, bool issubcontractor = false)
        {
            int Worspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
             string ddl = "<option value='0'>" + optvalue + "</option>";
            if (ismulti == 1)
            {
                ddl = "";
            }
            var deptser = new defaultservices();
            var list = deptser.get_all_active_member(type, issubcontractor, memberid, Worspaceid);
            foreach (var item in list)
            {
                if (memberid == item.Key)
                {
                    ddl = ddl + "<option selected value='" + item.Key + "'>" + item.Value + "</option>";

                }
                else
                {
                    ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                }
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }




        [HttpPost]
        public string get_selcted_member(int memberid = 0, string optvalue = "Member", string type = "T")
        {
            int Worspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string ddl = "";
            var deptser = new defaultservices();
            var list = deptser.get_all_active_member_byid(memberid, type, Worspaceid);
            foreach (var item in list)
            {
                if (memberid == item.Key)
                {
                    ddl = ddl + "<option selected value='" + item.Key + "'>" + item.Value + "</option>";
                }
                else
                {
                    ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                }
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }


        [HttpPost]
        public string get_selcted_managers(int userid = 0, string type = "T", int managerid = 0)
        {
            int Worspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string ddl = "<option  value='0'> Manager</option>";
            if(managerid==0)
            {
                ddl = "<option selected value='0'> Manager</option>";
            }
            var deptser = new defaultservices();
            var list = deptser.get_all_active_manager_byid(userid, type, Worspaceid);
            foreach (var item in list)
            {
                if (managerid == item.Key)
                {
                    ddl = ddl + "<option selected value='" + item.Key + "'>" + item.Value + "</option>";
                }
                else
                {
                    ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                }
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }

        [HttpPost]
        public string get_selcted_member_bycity(int cityid = 0, string optvalue = "Member")
        {
            int Worspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string ddl = "<option value='0'>" + optvalue + "</option>";
            var deptser = new defaultservices();
            var list = deptser.get_all_active_member_bycityid(cityid, Worspaceid);
            foreach (var item in list)
            {
                ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";

            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }


        [HttpPost]
        public string get_selcted_member_bystate(int stateid = 0, string optvalue = "Member")
        {
            int Worspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string ddl = "<option value='0'>" + optvalue + "</option>";
            var deptser = new defaultservices();
            var list = deptser.get_all_active_member_bystateid(stateid, Worspaceid);
            foreach (var item in list)
            {
                ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";

            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }

        [HttpPost]
        public string get_all_skills(int ismulti = 0)
        {
            int Worspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string ddl = "<option value='0'>Skills</option>";
            if (ismulti == 1)
            {
                ddl = "";
            }
            var deptser = new defaultservices();
            var list = deptser.get_all_skills(Worspaceid);
            foreach (var item in list)
            {
                ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }

        [HttpPost]
        public string get_selected_template(int templateid = 0, bool needLabel = true)
        {
            int Worspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string ddl = "";
            if (needLabel)
            {
                ddl = "<option  value='0'>Template</option>";
            } 
            var deptser = new defaultservices();
            int defaultid = 0;
            var list = deptser.get_all_template(ref defaultid, Worspaceid);
            foreach (var item in list)
            {
                if (defaultid == item.Key)
                {
                    ddl = ddl + "<option selected value='" + item.Key + "'>" + item.Value + "</option>";

                }
                else
                {
                    ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                }
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }


        [HttpPost]
        public string get_job_OfferByState(int stateId = 0)
        {
            int Workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string ddl = "<option  value='0'>Offer</option>";
            var deptser = new defaultservices();
            int defaultid = 0;
            var list = deptser.get_all_job_OffersByStateId(stateId, Workspaceid);
            foreach (var item in list)
            {
                if (defaultid == item.Key)
                {
                    ddl = ddl + "<option selected value='" + item.Key + "'>" + item.Value + "</option>";

                }
                else
                {
                    ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                }
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }
        [HttpPost]
        public string get_job_OfferBycity(string city = "", int Workspaceid = 0)
        {

            string ddl = "<option  value='0'>Offer</option>";
            var deptser = new defaultservices();
            var list = deptser.get_all_job_OffersBycity(city, Workspaceid);
            foreach (var item in list)
            {
                ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }
        [HttpPost]
        public string GetActiveWorkSpace(int Workspaceid = 0)
        {

            string ddl = "<option  value='0'>Domain</option>";
            var deptser = new defaultservices();
            var list = deptser.GetAllActiveWorkspace();
            foreach (var item in list)
            {
                if (Workspaceid == item.Key)
                {
                    ddl = ddl + "<option selected value='" + item.Key + "'>" + item.Value + "</option>"; 
                }
                else
                {
                    ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
                }
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }


    }
}