﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using Stripe;
using System.IO;
using Newtonsoft.Json;
using System.Collections;
using System.Data;

namespace hrm.Controllers
{
    public class testController : Controller
    {
        public string getmiles(string destination,string origins)
        {
            string url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins="+ origins + "&destinations="+ destination + "&mode=driving&language=en-EN&sensor=false&key=AIzaSyA1LyE18eEmk5UdDLezPmcOPF47Diugxls&units=imperial";
            return Get(url); 
        }

        public class rows
        {
            public _elements elements { get; set; }
        }

        public class _elements
        {
            public _distance distance { get; set; }
        }

        public class _distance
        {
            public string text { get; set; }
        }

        //{ "destination_addresses" : [ "Anchorage, AK 99501, USA" ], "origin_addresses" : [ "Anchorage, AK 99504, USA" ], "rows" : [ { "elements" : [ { "distance" : { "text" : "4.9 mi", "value" : 7821 }, "duration" : { "text" : "9 mins", "value" : 543 }, "status" : "OK" } ] } ], "status" : "OK" }

        public string Get(string uri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }
        public List<SkillItems> get_all_skills()
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            var dept = new skillservices();
            return dept.get_all_active_skills(workspaceid)._skill;
        }

        [HttpPost]
        public void testwebhook(RegisterModel _model, string stripeToken = "", int plantype = 1)
        {
            string subscriptionid = "";
            string customerid = "";
            Stripe_Payment _payment = new Stripe_Payment();
            _payment.StarterWithdaily(stripeToken, "Starter daily ", _model.Email, _model.FirstName, _model.PhoneNumber, ref customerid, ref subscriptionid);

        }
        public class mapuserModel
        {
            public string name { get; set; }
            public string address { get; set; }
            public string url { get; set; }
            public string icon { get; set; }
        }

        public class mapuserList
        {
            //  public List<mapuserModel> _list { get; set; }
            public List<string[]> _list { get; set; }
        }
        public ActionResult map()
        {
            return View();
        }


        public mapuserList employee(int userId)
        {
            mapuserList _mapuserList = new mapuserList();
            List<string[]> strArrays = new List<string[]>();
            SqlHelper sqlHelper = new SqlHelper();
            SortedList sortedLists = new SortedList();
            try
            {
                sortedLists.Add("@userid", userId);
                DataTable dataTable = sqlHelper.fillDataTable("GetEmployeeForMap", "", sortedLists);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    string[] str = new string[] { dataTable.Rows[i]["companyName"].ToString(), null, "", "http://maps.google.com/mapfiles/ms/icons/blue.png" };
                    str[1] = dataTable.Rows[i]["address"].ToString();
                    string[] strArrays1 = str[1].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                    str[1] = string.Join(",", strArrays1);
                    strArrays.Add(str);
                }
                _mapuserList._list = strArrays;
                dataTable.Dispose();
            }
            catch (Exception exception)
            {
                sqlHelper.Dispose();
            }
            return _mapuserList;
        }


        //[AuthorizeVerifiedloggedin]
        //public ActionResult edit(int tab = 1)
        //{
        //    int id = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
        //    ViewBag.tab = tab;
        //    ViewBag.isuncomplete = Convert.ToBoolean(Request.Cookies["_status"].Value.ToString());
        //    UserModel _mdl = new UserModel();
        //    UserService  _m = new UserService ();
        //    _mdl = _m.get_all_active_members_byid(id);
        //    string mes = "";
        //    if (TempData["error"] != null)
        //    {
        //        mes = TempData["error"].ToString();
        //        _mdl = new helper().GenerateError<UserModel>(_mdl, mes);
        //    }
        //    _mdl.DocumentList = _m.GetDocumentByUserid(id);
        //    _mdl.SkillList = get_all_skills();
        //    _mdl.ToolList = get_all_tools();
        //    _mdl.DocumentTypeList = get_all_doctypes();
        //    return View(_mdl);
        //}
        public List<ToolItems> get_all_tools()
        {
            var dept = new skillservices();
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            return dept.get_all_active_tools(workspaceid)._tool;
        }
        public List<DocumentTypeItems> get_all_doctypes()
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            var dept = new documentservices();
            return dept.AllDocumentType(workspaceid);
        }
        // GET: test
        public ActionResult city()
        {
          
            return View();
        }

        [HttpPost]
        public ActionResult city(string token)
        {
           // AddressOptions _addoptions = new AddressOptions();
           // _addoptions.Line1 = "89 E. Fulton Road";
           // _addoptions.City = "Grand Island";
           // _addoptions.State = "NE";
           // _addoptions.PostalCode = "68801";
          
           // CustomerCreateOptions _coptions = new CustomerCreateOptions();
           // _coptions.Source = token;
           // _coptions.Address = _addoptions;
           // _coptions.Description = "test subscription";
           // _coptions.Email = "3056umesh@gmai.com";
           // _coptions.InvoicePrefix = "INVOICE";
           // _coptions.Name = "Umesh Kumar";
           // _coptions.Phone = "9813151090";
           // _coptions.Plan = "plan_HJzedgq534jHM9";
           StripeClient _sclient = new StripeClient(apiKey: "sk_test_CFx1BIKKWhmL1OSPhY9YPbJJ", clientId: "acct_1BHKqYC0JOcJHmzl");
           
         CustomerService _cser = new CustomerService(_sclient);
           //_cser.Create(_coptions);
            CustomerListOptions _clist = new CustomerListOptions();
            _clist.Email = "3056umesh@gmai.com";
          var _list=  _cser.List(_clist);
            var id = "";
            foreach (var item in _list)
            {
                id = item.Id;
            }


            var Items = new List<SubscriptionItemOptions>
                {

                    new SubscriptionItemOptions
                    {
                        PriceData = new SubscriptionItemPriceDataOptions
                        {
                            Currency = "usd",
                            Product = "prod_HJzdyucunv4fet",
                            Recurring = new SubscriptionItemPriceDataRecurringOptions
                            {
                                Interval = "day",
                                IntervalCount = 30,
                            },
                            UnitAmountDecimal = 0.01234567890m, // Ensure decimals work
                        },
                        Quantity = 1,
                    },
                };

                 SubscriptionCreateOptions _scoptions = new SubscriptionCreateOptions();
            _scoptions.Customer= id;
            _scoptions.Items = Items;
            _scoptions.CollectionMethod = "charge_automatically";
            SubscriptionService _subservoce = new SubscriptionService(_sclient);
            _subservoce.Create(_scoptions);
            return View();
        }


        public ActionResult index()
        {
            return View();
        }
        public bool Sendmail(string to, string body)
        {
            bool str = true;
            try
            {
                string smtpEndpoint = "email-smtp.us-east-1.amazonaws.com";
                int port = 587;
                string senderName = "Taskayak";
                string senderAddress = "noreply@taskayak.com";
                string toAddress = to;
                string smtpUsername = "AKIAX27HMUUWXFPPOYVW";
                string smtpPassword = "BGQS+WegjbxpDbrQ6QkDwYHu606Jb1lXf5u61+M0G/Av"; string subject = "Taskayak";
                AlternateView htmlBody = AlternateView.
                            CreateAlternateViewFromString(body, null, "text/html");

                MailMessage message = new MailMessage();

                // Add sender and recipient email addresses to the message
                message.From = new MailAddress(senderAddress, senderName);
                message.To.Add(new MailAddress(toAddress));
                // Add the subject line, text body, and HTML body to the message
                message.Subject = subject;
                message.AlternateViews.Add(htmlBody);

                // Add optional headers for configuration set and message tags to the message
                //message.Headers.Add("X-SES-CONFIGURATION-SET", configurationSet);

                using (var client = new System.Net.Mail.SmtpClient(smtpEndpoint, port))
                {
                    // Create a Credentials object for connecting to the SMTP server
                    client.Credentials =
                        new NetworkCredential(smtpUsername, smtpPassword);

                    client.EnableSsl = true;

                    // Send the message
                    try
                    {

                        client.Send(message);

                    }
                    // Show an error message if the message can't be sent
                    catch (Exception)
                    {
                   //     new Comman().LogWrite(Enums.LogType.Error, ex.Message.ToString(), "Utilis", "Sendmail", Enums.Priority.Critical, ex.StackTrace);
                    }
                }

            }
            catch (Exception)
            {
            }
            return str;
        }


    }
}