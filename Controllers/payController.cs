﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace hrm.Controllers
{
    [AuthorizeVerifiedloggedin]
    public class payController : Controller
    {
        public int pageid = 1;
        public List<int> _permission;
        public payController()
        {
            string PList = System.Web.HttpContext.Current.Request.Cookies["_plist"] == null ? "0" : System.Web.HttpContext.Current.Request.Cookies["_plist"].Value.ToString();
            _permission = PList == "0" ? null : JsonConvert.DeserializeObject<List<int>>(PList);
        }
        public class paymodal
        {
            public string amount { get; set; }
            public string purchase_date { get; set; }
        }
        public JsonResult getpay()
        {
            List<paymodal> _mdl = new List<paymodal>();
            attandanceservices _ser = new attandanceservices();
            var model = _ser.get_all_active_attandance_dashboard(null, null, null, null, isyear: true);
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            //if (!_permission.Contains(1055))
            //{
            //    model = _ser.get_all_active_attandance_byuserid(userid, null, null);
            //}
            foreach (var item in model._attandance)
            {
                paymodal _item = new paymodal();
                _item.amount = item.Pay.Replace("$", "");
                _item.purchase_date = item.date;
                _mdl.Add(_item);
            }
            return Json(_mdl, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Index( string token = "")
        {
            if (!_permission.Contains(9))
            {
                return View("unauth");
            }
            Crypto _crypt = new Crypto();
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            List<int> _ctyiid = new List<int>();
            ViewBag.permissions = _permission;
            var _filtermodel = new Payfilter();
            _filtermodel = new filterservices().getfilter<Payfilter>(_filtermodel, userid, pageid);
            ViewBag.Location = string.IsNullOrEmpty(_filtermodel.Location) ? "" : _filtermodel.Location;
            ViewBag.jobstatus = string.IsNullOrEmpty(_filtermodel.JobStatus) ? _ctyiid.ToArray() : Array.ConvertAll(_filtermodel.JobStatus.Split(','), int.Parse);
            ViewBag.paymentstatus = _filtermodel.PaymentStatus;
            ViewBag.memberId = _filtermodel.MemberId;
            ViewBag.ProjectId = _filtermodel.ProjectId;
            ViewBag.ManagerId = _filtermodel.ManagerId; 
            ViewBag.isfilter = _filtermodel.isActiveFilter;
            ViewBag.length = _filtermodel.length??25;
            attandanceservices _ser = new attandanceservices();
            var mes = "";
            string view = "adminpay";
            if (!string.IsNullOrEmpty(token))
            {
                int id = Convert.ToInt32(_crypt.DecryptStringAES(token));
                mes = _ser.remove_pay(id);
            }
            var model = new attandanceadmin();// _ser.get_all_active_attandance(memberId, stateid, jobstatus, paymentstatus);

            ViewBag.isadmin = true;
            if (!_permission.Contains(1055))
            {
                view = "memberpay";
                // model = _ser.get_all_active_attandance_byuserid(userid, jobstatus, paymentstatus);
                ViewBag.isadmin = false;
            }
            model = new helper().GenerateError<attandanceadmin>(model, mes);
            //defaultservices _default = new defaultservices();
            //ViewBag.select = _default.GetAllActiveState();
            return View(view, model);
        }

        public JsonResult FetchAdminPayment(int start = 0, int length = 25, int draw = 1, int searchtype = 0)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            var _filtermodel = new Payfilter();
            string Location = Request.Params["Location"] ?? "";
            string jobstatus = Request.Params["jobstatus"] ?? "";
            string search = Request.Params["search[value]"] ?? "";
            int PaymentStatus= Request.Params["PayStatus"] ==null?0:Convert.ToInt32(Request.Params["PayStatus"]);
            int MemberId =string.IsNullOrEmpty( Request.Params["tech"]) ? 0 : Convert.ToInt32(Request.Params["tech"]);
            int ProjectId = string.IsNullOrEmpty(Request.Params["ProjectId"] ) ? 0 : Convert.ToInt32(Request.Params["ProjectId"]);
            int ManagerId = string.IsNullOrEmpty(Request.Params["ManagerId"])? 0 : Convert.ToInt32(Request.Params["ManagerId"]);
              if (searchtype == 1)
            {
                _filtermodel.Location = Location;
                _filtermodel.JobStatus = jobstatus;
                _filtermodel.PaymentStatus = PaymentStatus;
                _filtermodel.MemberId = MemberId;
                _filtermodel.ProjectId = ProjectId;
                _filtermodel.ManagerId = ManagerId;
                _filtermodel.length = length;
                new filterservices().save_filter<Payfilter>(_filtermodel, userid, pageid);
            }
            if (searchtype == 2)
            {
                Location = "";
                jobstatus = "0";
                PaymentStatus = 0;
                MemberId = 0;
                ProjectId = 0;
                ManagerId = 0;
                length = 25;
                new filterservices().delete_filter(userid, pageid);
                //clearsearch
            }
            int[] _jobstatus = Array.ConvertAll(jobstatus.Split(','), int.Parse);
            start = start == 0 ? 1 : start + 1;
            length = start == 1 ? length : start - 1 + length;
            attandanceservices _ser = new attandanceservices();
            datatableAjax<attandanceadmins> tableDate = new datatableAjax<attandanceadmins>();
            int total = 0;
            var _model = _ser.GetAllActivePayByIndex(length, start, ref total, MemberId, Location, _jobstatus, PaymentStatus, search, workspaceid: workspaceid, ProjectId,ManagerId);
            tableDate.draw = draw;
            tableDate.data = _model;
            tableDate.recordsFiltered = (_model.Count() > 0 ? total : 0);
            tableDate.recordsTotal = total;
            return base.Json(tableDate, JsonRequestBehavior.AllowGet);
        }
        public JsonResult FetchmemberPayment(int memberId=0, int stateid=0, int paymentstatus=0, int start = 0, int length = 25, int draw = 1, int searchtype = 0)
        {
            List<int> _ctyiid = new List<int>();
            string search = Request.Params["search[value]"] == null ? "" : Request.Params["search[value]"];
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            string Location = "";
            string _jobstatus = "";
            var _filtermodel = new Payfilter();
            Location = Request.Params["Location"] == null ? "" : Request.Params["Location"];
            _jobstatus = Request.Params["jobstatus"] == null ? "" : Request.Params["jobstatus"];
            if (searchtype == 0)
            {
                _filtermodel = new filterservices().getfilter<Payfilter>(_filtermodel, userid, pageid);
                memberId = _filtermodel.MemberId; 
                _jobstatus = string.IsNullOrEmpty(_filtermodel.JobStatus) ? "0" : _filtermodel.JobStatus;
                Location = string.IsNullOrEmpty(_filtermodel.Location) ? "" : _filtermodel.Location;
                paymentstatus = _filtermodel.PaymentStatus;
            }
            else if (searchtype == 1)
            {
                _filtermodel.MemberId = memberId;
                _filtermodel.Location = Location;
                _filtermodel.JobStatus = _jobstatus;
                _filtermodel.PaymentStatus = paymentstatus;
                new filterservices().save_filter<Payfilter>(_filtermodel, userid, pageid);
            }
            if (searchtype == 2)
            {
                memberId =0;
                _jobstatus = "0";
                Location = "";
                paymentstatus = 0;
                new filterservices().delete_filter(userid, pageid);
                //clearsearch
            }
            int[] jobstatus = Array.ConvertAll(_jobstatus.Split(','), int.Parse);
            start = (start == 0 ? 1 : start + 1);
            length = (start == 1 ? length : (start - 1) + length);
            attandanceservices _ser = new attandanceservices();
            datatableAjax<attandanceadmins> tableDate = new datatableAjax<attandanceadmins>();
            int total = 0;
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            var _model = _ser.GetAllActivePayByIndexClient(length, start, ref total, memberId, Location, jobstatus, paymentstatus, search, userid, workspaceid);
            tableDate.draw = draw;
            tableDate.data = _model;
            tableDate.recordsFiltered = (_model.Count() > 0 ? total : 0);
            tableDate.recordsTotal = total;
            return base.Json(tableDate, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult Index(int? memberId, int? stateid, int? paymentstatus, int[] cityId = null, int[] jobstatus = null, int print = 0)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            List<int> _ctyiid = new List<int>();
            ViewBag.permissions = _permission;
            var _filtermodel = new Payfilter();
            _filtermodel = new filterservices().getfilter<Payfilter>(_filtermodel, userid, pageid);
            ViewBag.memberId = _filtermodel.MemberId;
            ViewBag.jobstatus = string.IsNullOrEmpty(_filtermodel.JobStatus) ? _ctyiid.ToArray() : Array.ConvertAll(_filtermodel.JobStatus.Split(','), int.Parse);
            ViewBag.Location = string.IsNullOrEmpty(_filtermodel.Location) ? "" : _filtermodel.Location;
            ViewBag.paymentstatus = _filtermodel.PaymentStatus ;
            ViewBag.isfilter = _filtermodel.isActiveFilter;
            attandanceservices _ser = new attandanceservices();
            var mes = "";
            var model = _ser.get_all_active_attandance(memberId, stateid, jobstatus, paymentstatus, cityId);
            ViewBag.isadmin = true;
            if (!_permission.Contains(1055))
            {
                model = _ser.get_all_active_attandance_byuserid(userid, jobstatus, paymentstatus);
                ViewBag.isadmin = false;
            }
            if (print == 1)
            {
                string name = DateTime.Now.ToString("dd_MM_yyyy");
                helper _helper = new helper();
                DataTable datatable = _helper.ConvertToDataTable<attandanceadmins>(model._attandance);
                this.Getcsv(datatable, "Pay_Report_" + name);
            }
            model = new helper().GenerateError<attandanceadmin>(model, mes);
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.GetAllActiveState();
            return View(model);
        }


        private void Getcsv(DataTable dt, string FIleName)
        {
            DataColumnCollection columns = dt.Columns;
            if (columns.Contains("memder_p_id"))
            {
                columns.Remove("memder_p_id");
            }
            if (columns.Contains("MemberID"))
            {
                dt.Columns["MemberID"].ColumnName = "User Id";
            }
            if (columns.Contains("Memberrate"))
            {
                dt.Columns["Memberrate"].ColumnName = "User rate";
            }
            if (columns.Contains("token"))
            {
                columns.Remove("token");
            }
            if (columns.Contains("staus_color"))
            {
                columns.Remove("staus_color");
            }
            if (columns.Contains("attandanceid"))
            {
                columns.Remove("attandanceid");
            }
            if (columns.Contains("error_text"))
            {
                columns.Remove("error_text");
            }
            if (columns.Contains("alertclass"))
            {
                columns.Remove("alertclass");
            }
            if (columns.Contains("paymentstatusid"))
            {
                columns.Remove("paymentstatusid");
            }
            if (columns.Contains("PERMISSIONS"))
            {
                columns.Remove("PERMISSIONS");
            }
            string str = string.Concat("attachment; filename=", FIleName, ".csv");
            base.Response.ClearContent();
            base.Response.AddHeader("content-disposition", str);
            base.Response.ContentType = "application/vnd.ms-excel";
            string str1 = "";
            foreach (DataColumn column in dt.Columns)
            {
                base.Response.Write(string.Concat(str1, column.ColumnName));
                str1 = ",";
            }
            base.Response.Write("\n");
            foreach (DataRow row in dt.Rows)
            {
                str1 = "";
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    base.Response.Write(string.Concat(str1, row[i].ToString()));
                    str1 = ",";
                }
                base.Response.Write("\n");
            }
            base.Response.End();
        }
        [HttpPost]
        public ActionResult update_pay(int Payid, string Payname)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            List<int> _ctyiid = new List<int>();
            var _filtermodel = new Payfilter();
            _filtermodel = new filterservices().getfilter<Payfilter>(_filtermodel, userid, pageid);
             ViewBag.permissions = _permission;
            ViewBag.Location = string.IsNullOrEmpty(_filtermodel.Location) ? "" : _filtermodel.Location;
            ViewBag.jobstatus = string.IsNullOrEmpty(_filtermodel.JobStatus) ? _ctyiid.ToArray() : Array.ConvertAll(_filtermodel.JobStatus.Split(','), int.Parse);
            ViewBag.paymentstatus = _filtermodel.PaymentStatus;
            ViewBag.memberId = _filtermodel.MemberId;
            ViewBag.ProjectId = _filtermodel.ProjectId;
            ViewBag.ManagerId = _filtermodel.ManagerId;
            ViewBag.isfilter = _filtermodel.isActiveFilter;
            ViewBag.length = _filtermodel.length ?? 25;
            attandanceservices _ser = new attandanceservices();
            var mes = _ser.update_pay(Payid, Payname, userid);
            var model = new attandanceadmin();
            ViewBag.isadmin = true;
            string view = "adminpay";
            if (!_permission.Contains(1055))
            {
                //  model = _ser.get_all_active_attandance_byuserid(userid, null, null);
                view = "memberpay";
                ViewBag.isadmin = false;
            }
            model = new helper().GenerateError<attandanceadmin>(model, mes);
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.GetAllActiveState();
            return View(view, model);
        }

        public ActionResult add(int jobid = 0)
        {
            if (!_permission.Contains(47) && !_permission.Contains(1054))
            {
                return View("unauth");
            }
            ViewBag.permissions = _permission;
            attandanceadmin_items _model = new attandanceadmin_items();
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            ViewBag.jobid = jobid;
            if (!_permission.Contains(1054))
            {
                ViewBag.userid = userid;
                return View("add_user", _model);
            }
            else
            {
                return View("add", _model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult update(attandanceadmin_items _model, int? docid, int payid, int type = 1, HttpPostedFileBase file = null, string Comment = "", string file_title = "")
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            attandanceservices _ser = new attandanceservices();
            ViewBag.tab = type;
            var mes = "";
            if (type == 1)
            {
                mes = _ser.update_pay(_model, userid);
            }
            else if (type == 4)
            {
                mes = _ser.update_user_pay(_model, userid);
            }
            else if (type == 2)
            {
                mes = _ser.add_new_comment(Comment, userid, payid);
            }
            else if (type == 3)
            {
                if (docid.HasValue)
                {
                    mes = _ser.remove_doc(docid.Value);
                }
                else
                {
                    string path = Server.MapPath("~/image/jobdocs-" + payid);
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    string filenames = payid.ToString();
                    var extension = Path.GetExtension(file.FileName);
                    var imagepath = path + "/" + filenames + extension;
                    string tempfilename = "/image/jobdocs-" + payid + "/" + filenames + extension;
                    file.SaveAs(imagepath);
                    long b = file.ContentLength;
                    long kb = b / 1024;
                    mes = _ser.AddNewDocument(file_title, userid, payid, kb.ToString() + " kb", tempfilename);

                }

            }
            ModelState.Clear();
            //int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            _model = _ser.get_all_active_attandance_byid(payid);
            _model = new helper().GenerateError<attandanceadmin_items>(_model, mes);
            if (!_permission.Contains(1054))
            {
                ViewBag.userid = userid;
                return View("edit_user", _model);
            }
            else
            {
                return View("edit", _model);
            }
        }

        public ActionResult update()
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            ViewBag.permissions = _permission;
            attandanceservices _ser = new attandanceservices();
            attandanceadmin_items _model = new attandanceadmin_items();
            int payid = Session["payid"] == null ? 0 : Convert.ToInt32(Session["payid"]);
            //int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            _model = _ser.get_all_active_attandance_byid(payid);
            _model = new helper().GenerateError<attandanceadmin_items>(_model, "");
            if (!_permission.Contains(1054))
            {
                ViewBag.userid = userid;
                ViewBag.tab = 4;
                return View("edit_user", _model);
            }
            else
            {
                ViewBag.tab = 1;
                return View("edit", _model);
            }
        }

        public void update_pay_status(int payid, int statusid)
        {
            new attandanceservices().update_status(payid, statusid);
        }

        public ActionResult edit(string token, int tab = 1, string doctoken = "")
        {
            if (!_permission.Contains(48) && !_permission.Contains(3062))
            {
                return View("unauth");
            }
            Crypto _crypt = new Crypto();
            int id = Convert.ToInt32(_crypt.DecryptStringAES(token));
            ViewBag.permissions = _permission;
            ViewBag.tab = tab;
            Session["payid"] = id;
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            attandanceservices _ser = new attandanceservices();
            var mes = "";
            if (!string.IsNullOrEmpty(doctoken))
            {
                int docid = Convert.ToInt32(_crypt.DecryptStringAES(doctoken));
                mes = _ser.remove_doc(docid);
            }
            attandanceadmin_items _model = _ser.get_all_active_attandance_byid(id);
            _model = new helper().GenerateError<attandanceadmin_items>(_model, mes);
            if (!_permission.Contains(1054))
            {
                ViewBag.tab = tab == 1 ? 4 : tab;
                ViewBag.userid = userid;
                return View("edit_user", _model);
            }
            else
            {
                return View("edit", _model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult add(attandanceadmin_items _item, HttpPostedFileBase file)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            List<int> _ctyiid = new List<int>();
            var _filtermodel = new Payfilter();
            _filtermodel = new filterservices().getfilter<Payfilter>(_filtermodel, userid, pageid);
            ViewBag.permissions = _permission;
            ViewBag.Location = string.IsNullOrEmpty(_filtermodel.Location) ? "" : _filtermodel.Location;
            ViewBag.jobstatus = string.IsNullOrEmpty(_filtermodel.JobStatus) ? _ctyiid.ToArray() : Array.ConvertAll(_filtermodel.JobStatus.Split(','), int.Parse);
            ViewBag.paymentstatus = _filtermodel.PaymentStatus;
            ViewBag.memberId = _filtermodel.MemberId;
            ViewBag.ProjectId = _filtermodel.ProjectId;
            ViewBag.ManagerId = _filtermodel.ManagerId;
            ViewBag.isfilter = _filtermodel.isActiveFilter;
            ViewBag.length = _filtermodel.length ?? 25;
            clientmodal _model = new clientmodal();
            string view = "adminpay";
            attandanceservices _services = new attandanceservices();
            string tempfilename = "";
            if (file != null)
            {
                string path = Server.MapPath("~/image/jobdoc-" + userid);
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                string filenames = userid.ToString();
                var extension = Path.GetExtension(file.FileName);
                var imagepath = path + "/" + filenames + extension;
                tempfilename = "~/image/jobdoc-" + userid + "/" + filenames + extension;
                if (System.IO.File.Exists(imagepath))
                {
                    System.IO.File.Delete(imagepath);
                }
                file.SaveAs(imagepath);
            }
            var message = _services.add_new_pay(_item, userid, tempfilename, workspaceid);

            var model = new attandanceadmin();
            ViewBag.isadmin = true;
            if (!_permission.Contains(1055))
            {
                ViewBag.isadmin = false;
                view = "memberpay";
                //model = _services.get_all_active_attandance_byuserid(userid, null, null);
            }
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.GetAllActiveState();
            model = new helper().GenerateError<attandanceadmin>(model, message);
            return View(view, model);
        }

    }
}