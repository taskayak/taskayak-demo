﻿using hrm.Database;
using hrm.Models;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace hrm.Controllers
{
    public class BackgroundController : Controller
    {
        // GET: Background
        public void SendOfferToSimilarCityState(List<int> offers, int Template = 0, int Distribution = 1, bool sms = false, bool email = true, string _messagetypes = "1", string smsText = "", string emailText = "", int workspaceid = 11, string domain = "", int BatchId = 0)
        {
            Dictionary<int, List<int>> OfferWithUserId = GetOfferWithUsers(offers);
            foreach (var item in OfferWithUserId)
            {
                new jobofferservices().Sendoffers(item.Value, Distribution, item.Key, sms, email, smsText, emailText, "New JobOffer ", workspaceid, domain);
            }
        }
        public void SubmitOfferWithDistanceMatrix(int OfferId, int WorkspaceId)
        {
            string Origan = GetOfferAddress(OfferId);
            jobofferservices _ser = new jobofferservices();
            UserService _us = new UserService();
            OfferTechFilter _item = _ser.GetOfferTechFilter(OfferId);
            string smsText = "";
            string emailText = "";
            _item.TecF_Distribution = string.IsNullOrEmpty(_item.TecF_Distribution) ? "1" : _item.TecF_Distribution;
            bool sms = _item.TecF_Distribution.Contains("2") ? true : false;
            bool email = _item.TecF_Distribution.Contains("1") ? true : false;
            var template = new templateservices().get_template(_item.Template);
            smsText = sms ? template.sms : "";
            emailText = email ? template.email : "";
            string domain = new templateservices().getdomainname(WorkspaceId);
            double distance = 20000.00;
            switch (_item.TecF_Distance)
            {
                case 1:
                    distance = 50.00;
                    break;
                case 2:
                    distance = 75.00;
                    break;
                case 3:
                    distance = 100.00;
                    break;
                case 4:
                    distance = 150.00;
                    break;
                case 5:
                    distance = 200.00;
                    break;
                case 0:
                    distance = 20000.00;
                    break;
            }
            string Backgroung = _item.TecF_Screening.Contains("1") ? "Yes" : "";
            string Drug = _item.TecF_Screening.Contains("2") ? "Yes" : "";
            List<int> users = new List<int>();
            Dictionary<int, Tuple<string, double>> UserWithAddress = _us.GetUsersByTechDiscoveryFilter(string.IsNullOrEmpty(_item.TecF_amount) ? null : _item.TecF_amount.Split(',').Select(int.Parse).ToArray(), Backgroung, Drug, string.IsNullOrEmpty(_item.TecF_skiils) ? null : _item.TecF_skiils.Split(',').Select(int.Parse).ToArray(), string.IsNullOrEmpty(_item.TecF_tools) ? null : _item.TecF_tools.Split(',').Select(int.Parse).ToArray(), WorkspaceId, distance, OfferId);
            foreach (var item in UserWithAddress)
            {
                var distanceAllowed = item.Value.Item2;
                double Miles = GetMiles(Origan, item.Value.Item1);
                if (Miles <= distance && Miles <= distanceAllowed)
                {
                    users.Add(item.Key);
                }
            }
            if (users.Count > 0)
            {
                new jobofferservices().Sendoffers(users, _item.TecF_Assign, OfferId, sms, email, smsText, emailText, "New JobOffer ", WorkspaceId, domain);
            }
            UpdateIsProcessed(OfferId);
        }
        public double GetMiles(string origins, string destination)
        {
            double Miles;
            string url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + origins + "&destinations=" + destination + "&mode=driving&language=en-EN&sensor=false&key=AIzaSyA1LyE18eEmk5UdDLezPmcOPF47Diugxls&units=imperial";
            var _response = Get(url);
            JavaScriptSerializer js = new JavaScriptSerializer();
            var _distance = js.Deserialize<model>(_response);
            try
            {
                Miles = Convert.ToDouble(_distance.rows[0].elements[0].distance.text.Replace(" mi", string.Empty).Replace(",", string.Empty));
            }
            catch
            {
                Miles = 50.0;
            }
            return Miles;
        }
        public class model
        {
            public IList<Row> rows { get; set; }
        }
        public class Row
        {
            public IList<Element> elements { get; set; }
        }
        public class Element
        {
            public Distance distance { get; set; }
            public Duration duration { get; set; }
            public string status { get; set; }
        }
        public class Duration
        {
            public string text { get; set; }
            public int value { get; set; }
        }
        public class Distance
        {
            public string text { get; set; }
            public int value { get; set; }
        }
        public string Get(string uri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }
        private Dictionary<int, List<int>> GetOfferWithUsers(List<int> Offers)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList _srt = new SortedList();
            List<int> _users = new List<int>();
            Dictionary<int, List<int>> _dict = new Dictionary<int, List<int>>();
            foreach (var item in Offers)
            {
                _users = new List<int>();
                _srt = new SortedList();
                sqlHelper = new SqlHelper();
                _srt.Add("@OfferId", item);
                var dt = sqlHelper.fillDataTable("GetMemberWithOfferIdSimmilartCityState", "", _srt);
                sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _users.Add(Convert.ToInt32(dt.Rows[i][0].ToString()));
                }
                _dict.Add(item, _users);
            }
            return _dict;
        }
        private string GetOfferAddress(int OfferId)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList _srt = new SortedList();
            string _address = "";
            _srt.Add("@OfferId", OfferId);
            var dt = sqlHelper.fillDataTable("GetOfferAddressByOfferId", "", _srt);
            sqlHelper.Dispose();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                _address = dt.Rows[i]["mapvalidateaddress"].ToString();
            }
            return _address;
        }
        private void UpdateIsProcessed(int OfferId)
        {
            SqlHelper sqlHelper = new SqlHelper();
            SortedList _srt = new SortedList
            {
                { "@OfferId", OfferId }
            };
            sqlHelper.executeNonQuery("setIsprocessed", "", _srt);
            sqlHelper.Dispose();
        }





    }
}