﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Web.Mvc;

namespace hrm.Controllers
{
    public class documentController : Controller
    {
        public List<int> _permission;
        public documentController()
        {
            string PList = System.Web.HttpContext.Current.Request.Cookies["_plist"] == null ? "0" : System.Web.HttpContext.Current.Request.Cookies["_plist"].Value.ToString();
            _permission = PList == "0" ? null : JsonConvert.DeserializeObject<List<int>>(PList);
        }
        public ActionResult Index()
        {
            if (!_permission.Contains(5078))
            {
                return View("unauth");
            }
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            doctype _model = new doctype();
            var DocumentServices = new documentservices();
            _model.doc_type = DocumentServices.AllDocumentType(workspaceid);
            _model.mainorder = _model.doc_type.Count>0? _model.doc_type.Max(a=>a.ViewOrder)+1:1;
            string message = TempData["error"] != null ? TempData["error"].ToString() : string.Empty;
            _model = new helper().GenerateError<doctype>(_model, message);
            _model.PERMISSIONS = _permission;
            return View(_model);
        } 
        [HttpPost]
        public ActionResult Index(doctype _model)
        {
            var DocumentServices = new documentservices();
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            var error_text = DocumentServices.add_new_type(_model.name, userid, _model.mainorder, workspaceid);
            if (!error_text.ToLower().Contains("error"))
            {
                ModelState.Clear();
            }
            doctype _model1 = new doctype();
            _model1.doc_type = DocumentServices.AllDocumentType(workspaceid);
            _model1.mainorder = _model1.doc_type.Count > 0 ? _model1.doc_type.Max(a => a.ViewOrder) + 1 : 1;
            _model1 = new helper().GenerateError<doctype>(_model1, error_text);
            _model1.PERMISSIONS = _permission;
            return View(_model1);
        }

        [HttpPost]
        public ActionResult update(string name, int id, int mainorder)
        {
            var DocumentServices = new documentservices();
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            var error_text = DocumentServices.update_type(name, id, userid, mainorder); 
            doctype _model = new doctype(); ModelState.Clear();
            _model.doc_type = DocumentServices.AllDocumentType(workspaceid);
            _model.mainorder = _model.doc_type.Count > 0 ? _model.doc_type.Max(a => a.ViewOrder) + 1 : 1;
            _model = new helper().GenerateError<doctype>(_model, error_text);
            _model.PERMISSIONS = _permission;
            return View("index", _model);
        }
        public ActionResult delete(int id)
        {
            if (!_permission.Contains(5116))
            {
                return View("unauth");
            }
            var DocumentServices = new documentservices();
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            var error_text = DocumentServices.delete_type(id, userid);
            doctype _model = new doctype(); ModelState.Clear();
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            _model.doc_type = DocumentServices.AllDocumentType(workspaceid);
            _model.mainorder = _model.doc_type.Count > 0 ? _model.doc_type.Max(a => a.ViewOrder) + 1 : 1;
            _model = new helper().GenerateError<doctype>(_model, error_text);
            _model.PERMISSIONS = _permission;
            return View("index", _model);
        }

    }
}