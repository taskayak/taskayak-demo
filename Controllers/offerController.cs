﻿using Hangfire;
using hrm.Database;
using hrm.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using Newtonsoft.Json;

namespace hrm.Controllers
{

    [AuthorizeVerifiedloggedin]
    public class offerController : Controller
    {
        // GET: jobs
        public List<int> _permission;
        public int pageid = 5;
        public offerController()
        {
            string PList = System.Web.HttpContext.Current.Request.Cookies["_plist"] == null ? "0" : System.Web.HttpContext.Current.Request.Cookies["_plist"].Value.ToString();
            _permission = PList == "0" ? null : JsonConvert.DeserializeObject<List<int>>(PList);
        }

        public void sendoffer(List<int> data, int tid, int pref, int offerId, bool sms, bool email, string subject)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());  
            var template = new templateservices().get_template(tid);
            string smsText = sms ? template.sms : "";
            string emailText = email ? template.email : "";
            string domain = new templateservices().getdomainname(workspaceid);
            new jobofferservices().Sendoffers(data,pref, offerId, sms, email, smsText, emailText, subject, workspaceid, domain);
        }

        public void sendoffer2(List<int> data, int tid, bool termspopup, bool sms, bool email, string subject)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString()); 
            var template = new templateservices().get_template(tid);
            string smsText = sms ? template.sms : "";
            string emailText = email ? template.email : "";
            new jobofferservices().sendoffers2(data, sms, email, smsText, emailText, subject, termspopup, workspaceid);
        }

         public void sendoffer5(List<int> data, int tid, int jobid, bool sms, bool email, string subject)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString()); 
            var template = new templateservices().get_template(tid);
            string smsText = sms ? template.sms : "";
            string emailText = email ? template.email : "";
            new jobofferservices().sendoffers4(data, sms, email, smsText, emailText, subject, jobid, workspaceid);
        } 
        public class Events
        {
            public string ViewID { get; set; }
            public string ViewTitle { get; set; }
            public string ViewDate { get; set; }
            public string ViewStime { get; set; }
            public string ViewEtime { get; set; }
            public string ViewAddress { get; set; }
            public string ViewContact { get; set; }
            public string ViewPhone { get; set; }
            public string ViewDispatcher { get; set; }
            public string ViewTechnician { get; set; }  
            public bool allDay{ get;set;} 
            public string token{
                get;
                set;
            }

            public string backgroundColor
            {
                get;
                set;
            }

            public string className
            {
                get;
                set;
            }

            public string date
            {
                get;
                set;
            }

            public string end
            {
                get;
                set;
            }

            public string id
            {
                get;
                set;
            }

            public string start
            {
                get;
                set;
            }

            public string title
            {
                get;
                set;
            }

            public string url
            {
                get;
                set;
            }

        }


        public ActionResult GetEvents(int id, string start = null,int screening=0,int cmanagerId=0,int PmanagerId=0,string Skiil="", string end = null, string date = "", int type = 1, string Location = "", int cclient = 0, int cTechnician = 0, int cDispatcher = 0, int Status = 0, int searchtype = 0, int project = 0)
        {
            pageid = 45;
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            if (!_permission.Contains(3064))
            {
                return View("unauth");
            }
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            var _filtermodel = new jobfilter();
            switch(searchtype)
            {
                case 1:
                    _filtermodel.Location = Location;
                    _filtermodel.date = date;
                    _filtermodel.cclient = cclient;
                    _filtermodel.cTechnician = cTechnician;
                    _filtermodel.cDispatcher = cDispatcher;
                    _filtermodel.Status = Status; 
                    _filtermodel.project = project;
                    _filtermodel.Skiil = Skiil;
                    _filtermodel.PmanagerId = PmanagerId;
                    _filtermodel.cmanagerId = cmanagerId;
                    _filtermodel.screening = screening; 
                    new filterservices().save_filter<jobfilter>(_filtermodel, userid, pageid);
                    break;
                case 2:
                    Location = "";
                    date = "";
                    cclient = 0;
                    cTechnician = 0;
                    cDispatcher = 0;
                    Status = 0; project = 0;
                    Skiil = "0";
                    PmanagerId = 0;
                    cmanagerId = 0;
                    screening = 0; 
                    new filterservices().delete_filter(userid, pageid);
                    break;
            } 
            DateTime dateTime;
            jobofferservices _ser = new jobofferservices();
            DateTime dateTime1 = (!string.IsNullOrEmpty(start) ? Convert.ToDateTime(start) : DateTime.Now);
            List<Events> events = new List<Events>();
            if (string.IsNullOrEmpty(end))
            {
                DateTime currentTimeByTimeZone = DateTime.Now;
                dateTime = currentTimeByTimeZone.AddDays(30);
            }
            else
            {
                dateTime = Convert.ToDateTime(end);
            }
            bool cleardate = false;
            if (string.IsNullOrEmpty(date) && (!string.IsNullOrEmpty(end) && !string.IsNullOrEmpty(start)))
            {
                cleardate = true;
                date = start.Replace("-", "/") + "-" + end.Replace("-", "/");
            }
            var model = new jobmodal();
            if (_permission.Contains(3069))
            {
                model = _ser.GetAllActiveOfferByUserIdForCalender(0, date, Location, cclient, id, cDispatcher, Status, workspaceid: workspaceid);
                // ViewBag.hiderate = true;
            }
            else
            {// 
                int[] _Skiils = Array.ConvertAll(Skiil.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries), int.Parse);
                ViewBag.hiderate = false;
                model = _ser.GetAllActiveOffersForCalender(date, Location, cclient, cTechnician, cDispatcher, Status, workspaceid: workspaceid, project: project, projectmanagerId: PmanagerId, clientManagerId: cmanagerId, screening: screening, _Skiils: _Skiils);
            }
            date = cleardate ? "" : date;
            var filterjobs = model._jobs.ToList();
            if (!string.IsNullOrEmpty(date))
            {
                filterjobs = filterjobs.Where(a => a._sdate >= dateTime1 || a._endate <= dateTime).ToList();
            }
            Crypto _crypt = new Crypto();
            foreach (var item in filterjobs)
            {
                events.Add(new Events()
                {
                    id = item.JobID.ToString(),
                    start = item._sdate.ToString("s"),
                    end = item._endate.ToString("s"),
                    title = item.Title,
                    allDay = false,
                    token = _crypt.EncryptStringAES(item.JobID.ToString()) 
                }) ;
            }
            Events[] array = events.ToArray();
            return base.Json(array, 0);
        }

       //[OutputCache(Duration = 600, Location = OutputCacheLocation.Client)]
        public ActionResult Index( int type = 1, int srch = 1, string token = "")
        { 
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString()); 
            var membertype = Request.Cookies["_slist"] == null ? "T" : Request.Cookies["_slist"].Value.ToString();
            job _model = new job();
            if (!_permission.Contains(3064))
            {
                return View("unauth");
            }
            if (type == 2)
            {
                if (!_permission.Contains(3072))
                {
                    return View("unauth");
                }
                pageid = 45;
            }
            else
            {
                if (!_permission.Contains(3073))
                {
                    return View("unauth");
                }
            } 
            var _filtermodel = new jobfilter();
            if (srch == 2)
            {
                new filterservices().delete_filter(userid, pageid);
            }
            _filtermodel = new filterservices().getfilter<jobfilter>(_filtermodel, userid, pageid);
            _model.jdate = string.IsNullOrEmpty(_filtermodel.date) ? "" : _filtermodel.date;
            _model.jcclient = _filtermodel.cclient ?? 0;
            _model.jLocation = string.IsNullOrEmpty(_filtermodel.Location) ? "" : _filtermodel.Location;
            _model.jcTechnician = _filtermodel.cTechnician ?? 0;
            _model.jcDispatcher = _filtermodel.cDispatcher ?? 0;
            _model.jcStatus = _filtermodel.Status ?? 0;
            _model.jisActiveFilter = _filtermodel.isActiveFilter;
            _model.jproject = _filtermodel.project ?? 0;
            _model._jskills = get_all_skills(workspaceid);
            _model._jtools =get_all_tools(workspaceid);
            List<int> _ctyiid = new List<int>(); 
             ViewBag.Skiil = string.IsNullOrEmpty(_filtermodel.Skiil) ? _ctyiid.ToArray() : Array.ConvertAll(_filtermodel.Skiil.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries), int.Parse);
            ViewBag.PmanagerId = _filtermodel.PmanagerId ?? 0;
            ViewBag.cmanagerId = _filtermodel.cmanagerId ?? 0;
            ViewBag.screening = _filtermodel.screening ?? 0;
            ViewBag.length = _filtermodel.length ?? 25;
            List<commentmodal> _Comments = new List<commentmodal>();
            commentmodal _item = new commentmodal(); 
            _item.createdby = "Developer Ghjdshsj One";
            _item.CreatedDate = "02-13-2020 12:10:19 PM";
            _item.Comment = "<p>This is test notification</p>";
            _Comments.Add(_item);
            _model._Comments = _Comments;
            string message = "";
            if (!string.IsNullOrEmpty(token))
            {
                Crypto _crypt = new Crypto();
                int jid = Convert.ToInt32(_crypt.DecryptStringAES(token));
                message =new jobofferservices().delete_job(jid);
            } 
            message = TempData["error"] != null ?TempData["error"].ToString(): message; 
            _model = new helper().GenerateError<job>(_model, message);
            _model.PERMISSIONS = _permission;
            _model.issubcontractor = membertype == "S" || membertype == "LT";
            _model.type = type;
            _model.juserid = userid;
            string view = "admin";
            if (_permission.Contains(3069))
            {
                _model.jhiderate = true;
                _model.jStateId = _filtermodel.state_filter_id ?? 0;
                _model.jCityId = _filtermodel.city_filter_id ?? 0;
                view = "member";
            }
            else
            {
                _model.jhiderate = false;
            } 
            if (type <2)
            {
                return View(view, _model);
            }
            else
            { 
                return View("calender", _model);
            }
        } 
        public List<SkillItems> get_all_skills(int workspaceid)
        { 
            return new skillservices().get_all_active_skills(workspaceid)._skill;
        }
        public List<ToolItems> get_all_tools(int workspaceid)
        { 
            return new skillservices().get_all_active_tools(workspaceid)._tool;
        }
        [HttpPost]
        public string get_job_Offerforpopup(int stateid = 0, string city = "", string offerid = "", int clientid = 0)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string ddl = "<option  value='0'>Offer ID</option>";
            var deptser = new defaultservices();
            var list = deptser.get_all_offer_unassign(stateid, city, offerid, clientid, workspaceid);
            foreach (var item in list)
            {
                ddl = ddl + "<option  value='" + item.Key + "'>" + item.Value + "</option>";
            }
            HtmlString _str = new HtmlString(ddl);
            return _str.ToString();
        }
        public void update_status(int id, int statusId)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            new jobofferservices().update_status(statusId, id, userid);
        }

        public PartialViewResult getnotifications(int jobid)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            notificationModal _model = new notificationModal();
            _model.message = new notificationServices().get_all_active_notificationsbyofferid(jobid, userid);
            _model.mainind = jobid;
            return PartialView("Replyoffer", _model);
        }
        public PartialViewResult getnotificationsmember(int jobid)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            notificationModal _model = new notificationModal();
            _model.message = new notificationServices().get_all_active_notificationsbyofferid(jobid, userid);
            _model.mainind = jobid;
            return PartialView("Replyoffer2", _model);
        }


        [ValidateInput(false)]
        public void Sendmessage(int JobID, string Comment)
        {
            jobofferservices _ser = new jobofferservices();
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            _ser.add_new_comment(Comment, userid, JobID, true);
        }

        public JsonResult Fetchoffers(int start = 0, int length = 25, int draw = 1, int jobid = 0)
        {
              string search = Request.Params["search[value]"] == null ? "" : Request.Params["search[value]"];
            start = start == 0 ? 1 : start + 1;
            length = start == 1 ? length : start - 1 + length;
            jobofferservices _services = new jobofferservices();
            datatableAjax<offermodal> tableDate = new datatableAjax<offermodal>();
            int total = 0;
            var _model = _services.get_all_active_offer_bypaging(ref total, start, length, search, jobid);
            tableDate.draw = draw;
            tableDate.data = _model;
            tableDate.recordsFiltered = (_model.Count() > 0 ? total : 0);
            tableDate.recordsTotal = total;
            return base.Json(tableDate, JsonRequestBehavior.AllowGet);
        }


        public JsonResult Fetchadminjobs(int start = 0, int length = 25, int draw = 1, string date = "", int type = 1, int state_filter_id = 0, int cclient = 0, int cTechnician = 0, int cDispatcher = 0, int Status = 0, int searchtype = 0, int project = 0)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string search = Request.Params["search[value]"] ?? "";
            string Location = Request.Params["Location"] ?? "";
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            var _filtermodel = new jobfilter();
            int PmanagerId = string.IsNullOrEmpty(Request.Params["PmanagerId"]) ? 0 : Convert.ToInt32(Request.Params["PmanagerId"]);
            int cmanagerId = string.IsNullOrEmpty(Request.Params["cmanagerId"]) ? 0 : Convert.ToInt32(Request.Params["cmanagerId"]);
            int screening = string.IsNullOrEmpty(Request.Params["screening"]) ? 0 : Convert.ToInt32(Request.Params["screening"]);
            string Skiil = Request.Params["Skiil"] ?? "0";
            switch (searchtype)
            {
                
                case 1:
                    _filtermodel.Location = Location;
                    _filtermodel.date = date;
                    _filtermodel.cclient = cclient;
                    _filtermodel.cTechnician = cTechnician;
                    _filtermodel.cDispatcher = cDispatcher;
                    _filtermodel.Status = Status; _filtermodel.project = project;
                    _filtermodel.Skiil = Skiil;
                    _filtermodel.PmanagerId = PmanagerId;
                    _filtermodel.cmanagerId = cmanagerId;
                    _filtermodel.screening = screening;
                    _filtermodel.length = length;
                    new filterservices().save_filter<jobfilter>(_filtermodel, userid, pageid);
                    break;
                case 2:
                    Location = "";
                    date = "";
                    cclient = 0;
                    cTechnician = 0;
                    cDispatcher = 0;
                    Status = 0; project = 0;
                    Skiil = "0";
                    PmanagerId = 0;
                    cmanagerId = 0;
                    screening = 0;
                    length = 25;
                    new filterservices().delete_filter(userid, pageid);
                    break;
            }
          
            int[] _Skiils = Array.ConvertAll(Skiil.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries), int.Parse);
            start = start == 0 ? 1 : start + 1;
            length = start == 1 ? length : start - 1+ length;
            jobofferservices _ser = new jobofferservices();
            datatableAjax<job_items> tableDate = new datatableAjax<job_items>();
            int total = 0;
            var _model = _ser.GetAllActiveOffersByIndex(ref total, userid, start, length, search, date, Location, cclient, cTechnician, cDispatcher, Status, workspaceid: workspaceid, projectid: project,projectmanagerId: PmanagerId,clientManagerId: cmanagerId, screening: screening,_Skiils: _Skiils);
            tableDate.draw = draw;
            tableDate.data = _model;
            tableDate.recordsFiltered = _model.Count() > 0 ? total : 0;
            tableDate.recordsTotal = total;
            return  Json(tableDate, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Fetchuserjobs(int start = 0, int length = 25, int draw = 1  )
        {  
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string search = Request.Params["search[value]"] ?? "";
            string Location = Request.Params["Location"] ?? "";
            string date = Request.Params["date"] ?? "";
            int cclient = Request.Params["cclient"] == null ? 0 : Convert.ToInt32(Request.Params["cclient"]);
            int cDispatcher = Request.Params["cDispatcher"] == null ? 0 : Convert.ToInt32(Request.Params["cDispatcher"]);
            int Status = Request.Params["Status"] == null ? 0 : Convert.ToInt32(Request.Params["Status"]);
            int searchtype = Request.Params["searchtype"] == null ? 0 : Convert.ToInt32(Request.Params["searchtype"]);
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            var _filtermodel = new jobfilter();
            var membertype = Request.Cookies["_slist"] == null ? "T" : Request.Cookies["_slist"].Value.ToString();
            switch (searchtype)
            {
                case 0:
                    _filtermodel = new filterservices().getfilter<jobfilter>(_filtermodel, userid, pageid);
                    Location = string.IsNullOrEmpty(_filtermodel.Location) ? "" : _filtermodel.Location;
                    date = string.IsNullOrEmpty(_filtermodel.date) ? "" : _filtermodel.date; 
                    cclient = _filtermodel.cclient ?? 0; 
                    cDispatcher = _filtermodel.cDispatcher ?? 0;
                    Status = _filtermodel.Status ?? 0; 
                    break;
                case 1:
                    _filtermodel.Location = Location;
                    _filtermodel.date = date; 
                    _filtermodel.cclient = cclient; 
                    _filtermodel.cDispatcher = cDispatcher;
                    _filtermodel.Status = Status; 
                    new filterservices().save_filter<jobfilter>(_filtermodel, userid, pageid);
                    break;
                case 2: 
                    date = "";
                    Location = "";
                    cclient = 0; 
                    cDispatcher = 0;
                    Status = 0; 
                    new filterservices().delete_filter(userid, pageid);
                    //clearsearch
                    break;
            } 
           int cTechnician = userid; 
            start = start == 0 ? 1 : start + 1;
            length = start == 1 ? length : start - 1 + length;
            jobofferservices _ser = new jobofferservices();
            datatableAjax<job_items> tableDate = new datatableAjax<job_items>();
            int total = 0;
            var _model = _ser.GetAllActiveOfferByIndexByUser(ref total, userid, start, length, search, date, Location, cclient, cTechnician, cDispatcher, Status, membertype: membertype, workspaceid: workspaceid);
            tableDate.draw = draw;
            tableDate.data = _model;
            tableDate.recordsFiltered = _model.Count() > 0 ? total : 0;
            tableDate.recordsTotal = total;
            return Json(tableDate, JsonRequestBehavior.AllowGet);
        } 
        public void update_readstatus(int jobid)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            new notificationServices().update_jobreadofferstatus(jobid, userid);
        } 
        public ActionResult edit_skills(int[] skills, string token)
        {
            int JobId = Convert.ToInt32(new Crypto().DecryptStringAES(token));
            TempData["errordata"] = new projectServices().UpdateSkillsByOfferId(skills, JobId); 
            return RedirectToAction("view", new { token = token, type = 6 });
        } 
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult edit_tools(int[] tools, string token)
        {
            int JobId = Convert.ToInt32(new Crypto().DecryptStringAES(token));
            TempData["errordata"] = new projectServices().UpdateToolsByOffer(tools, JobId); 
            return RedirectToAction("view", new { token = token, type = 5 });
        }
        public List<SkillItems> get_all_skills()
        {
            var service = new skillservices();
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            return service.get_all_active_skills(workspaceid)._skill;
        } 
        public List<ToolItems> get_all_tools()
        {
            var service = new skillservices();
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            return service.get_all_active_tools(workspaceid)._tool;
        } 
        public OfferTechFilter GetFilterByOfferId(int offerId)
        {
            jobofferservices _ser = new jobofferservices(); 
            OfferTechFilter _model = _ser.GetOfferTechFilter(offerId);
            return _model;
        }  
        [HttpPost]       
        public string CheckProcess(int Id)
        {
            return  new jobofferservices().GetOfferTechFilterStatus(Id).ToString().ToUpper();  
        } 
        public ActionResult view(string token, int type = 1, int viewtype = 1, string projectToken = "")
        {
            if (!_permission.Contains(3071))
            {
                return View("unauth");
            }
             int id = Convert.ToInt32(new Crypto().DecryptStringAES(token)); 
            jobofferservices _ser = new jobofferservices();  
            int offerid = 0;
            Session["type"] = viewtype; 
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            var subcontractortype = Request.Cookies["_slist"] == null ? "T" : Request.Cookies["_slist"].Value.ToString(); 
            bool isoffered = false;
            job_items1 _model = _ser.GetAllActiveOfferById(id, userid, subcontractortype, ref offerid, ref isoffered, userid);
            _model.projectToken = projectToken;
            _model._tools =get_all_tools();
            _model._skills = get_all_skills();
            _model.viewtype = viewtype;
            _model.PERMISSIONS = _permission;
            _model.type = type;
            if (_permission.Contains(3074))
            {
                ViewBag.offerid = offerid;
                ViewBag.isoffered = isoffered;
                var comments = _model._Comments.Where(a => a.isadminpost == true || a.Commentby == userid || a.Commentto == userid).ToList();
                _model._Comments = comments;
                ViewBag.issubcontractor = (subcontractortype == "S" || subcontractortype == "LT") ? true : false;
                return View("Tview", _model);
            }
            else
            {
                var comments = _model._Comments.Where(a => a.Parentid == 0).ToList();
                var chldcomments = _model._Comments.Where(a => a.Parentid != 0).OrderByDescending(a => a.Parentid).ToList();
                _model._Comments = comments;
                _model._childComments = chldcomments;
            }
            _model._offerTechFilter = GetFilterByOfferId(id);
            return View(_model);
        }
        public JsonResult getjob(int id)
        {
            jobofferservices _ser = new jobofferservices();
            var _model = _ser.GetAllActiveOfferDetailsById(id);
            return Json(_model, JsonRequestBehavior.AllowGet);
        } 
        public ActionResult delete_file(string id, string jobid)
        {
            Crypto _crypt = new Crypto();
            int _id = Convert.ToInt32(_crypt.DecryptStringAES(id));
            int _jobid = Convert.ToInt32(_crypt.DecryptStringAES(jobid));
            ViewBag.permissions = _permission;
            jobofferservices _ser = new jobofferservices();
            job_items1 _model = new job_items1();
            var message = _ser.delete_doc(_id);
            int offerid = 0;
            ViewBag.viewtype = Session["type"] == null ? 1 : Convert.ToInt32(Session["type"]);
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            var subcontractortype = new UserService().GetUserType(userid);
            bool isoffered = false;
            ModelState.Clear();
            _model = _ser.GetAllActiveOfferById(_jobid, userid, subcontractortype, ref offerid, ref isoffered, userid);
            _model = new helper().GenerateError<job_items1>(_model, message); 
            ViewBag.type = 3; _model.type = 3;
            _model._tools = get_all_tools();
            _model._skills = get_all_skills();
            if (_permission.Contains(3074))
            {
                ViewBag.offerid = offerid;
                ViewBag.isoffered = isoffered;
                var comments = _model._Comments.Where(a => a.isadminpost == true || a.Commentby == userid || a.Commentto == userid).ToList();
                _model._Comments = comments;
                  ViewBag.issubcontractor = (subcontractortype == "S" || subcontractortype == "LT") ? true : false;
                 return View("Tview", _model);
            }
            else
            {
                var comments = _model._Comments.Where(a => a.Parentid == 0).ToList();
                var chldcomments = _model._Comments.Where(a => a.Parentid != 0).OrderByDescending(a => a.Parentid).ToList();
                _model._Comments = comments;
                _model._childComments = chldcomments;
            }
            _model._offerTechFilter = GetFilterByOfferId(_jobid);
            return View("view", _model);
        }

        public ActionResult edit(string token, int type = 1)
        {
            if (!_permission.Contains(3066))
            {
                return View("unauth");
            }
            Crypto _cryot = new Crypto();
            int id = Convert.ToInt32(_cryot.DecryptStringAES(token));
            ViewBag.permissions = _permission;
            jobofferservices _ser = new jobofferservices();
            job _model = new job();
            ViewBag.type = type;
            _model.type = 3;
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.GetAllActiveState();
            _model = _ser.GetAllActiveOfferDetailsById(id);
            return View(_model);
        }
        public void UpdateOfferFilter(int OfferId, string Rate, string Screening, string Tools, string Skills, int Distance, int AssignPreference, string Messagetype, int Template,int NotifiedUser)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@OfferId", OfferId);
                _srt.Add("@Rate", Rate);
                _srt.Add("@Tools", Tools);
                _srt.Add("@Skills", Skills);
                _srt.Add("@Distance", Distance);
                _srt.Add("@AssignPreference", AssignPreference);
                _srt.Add("@Screening", Screening);
                _srt.Add("@Messagetype", Messagetype);
                _srt.Add("@Template", Template);
                _srt.Add("@NotifiedUser", NotifiedUser); 
                sqlHelper.executeNonQuery("update_orderforProcessed", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();

            }
            finally
            {
                sqlHelper.Dispose();
            }

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult update(int JobID, int Template = 11, int Distance = 1, int Distribution = 1, int[] amount = null, int[] background = null, int[] Skills = null, int[] tools = null, int[] messagetype = null, int type = 1, HttpPostedFileBase file = null, string Comment = "", string file_title = "", int action = 0, bool isadminpost = false)
        {
            ViewBag.permissions = _permission;
            ViewBag.type = type; 
            jobofferservices _ser = new jobofferservices();
            job_items1 _model = new job_items1();
            var mes = "";
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            if (type == 4)
            {///0--assign 1-resubmiut--2-remove
                if (action == 2)
                {
                    mes = _ser.delete_submitedoffer(JobID);
                }
                else if (action == 1)
                {
                    mes = _ser.re_submitedoffer(JobID);
                }
                else if (action == 0)
                {
                    mes = _ser.assign_submitedoffer(JobID);
                }

            }
            if (type == 2)
            {
                mes = _ser.add_new_comment(Comment, userid, JobID, isadminpost);
            }
            else if (type == 3)
            {
                string path = Server.MapPath("~/image/jobofferpicture-" + JobID);
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                string filenames = new helper().GetRandomAlphaNumeric(4);
                var extension = Path.GetExtension(file.FileName);
                var imagepath = path + "/" + filenames + extension;
                string tempfilename = "/image/jobofferpicture-" + JobID + "/" + filenames + extension;
                if (System.IO.File.Exists(imagepath))
                {
                    System.IO.File.Delete(imagepath);
                }
                file.SaveAs(imagepath);
                long b = file.ContentLength;
                long kb = b / 1024;
                mes = _ser.AddNewDocument(file_title, userid, JobID, kb.ToString() + " kb", tempfilename);

            }
            else if (type == 4)
            {
                //int[] amount, int[] background ,int[] Skills ,int[] tools ,int[] messagetype,
                string _amount = amount == null ? "" : amount.ToDelimitedString();
                string _background = background == null ? "" : background.ToDelimitedString();
                string _Skills = Skills == null ? "" : Skills.ToDelimitedString();
                string _tools = tools == null ? "" : tools.ToDelimitedString();
                string _messagetype = messagetype == null ? "" : messagetype.ToDelimitedString();
                UpdateOfferFilter(JobID, _amount, _background, _tools, _Skills, Distance, Distribution, _messagetype, Template,userid); 
                BackgroundController _bc = new BackgroundController();
                BackgroundJob.Enqueue(() => _bc.SubmitOfferWithDistanceMatrix(JobID, workspaceid)); 
                mes = "Tech discovery is processing ";
            }
            bool isoffered = false;
            int offerId = 0;
            ModelState.Clear();
            var subcontractortype = new UserService().GetUserType(userid);
            _model = _ser.GetAllActiveOfferById(JobID, userid, subcontractortype, ref offerId, ref isoffered, userid);
            _model = new helper().GenerateError<job_items1>(_model, mes);
            ViewBag.viewtype = Session["type"] == null ? 1 : Convert.ToInt32(Session["type"]);
            _model._tools = get_all_tools();
            _model._skills = get_all_skills();
            _model.type = type;
            if (_permission.Contains(3074))
            {
                ViewBag.offerid = offerId;
                ViewBag.isoffered = isoffered;
                var comments = _model._Comments.Where(a => a.isadminpost == true || a.Commentby == userid || a.Commentto == userid).ToList();
                _model._Comments = comments;
                ViewBag.issubcontractor = (subcontractortype == "S" || subcontractortype == "LT") ? true : false;
                return View("Tview", _model);
            }
            else
            {
                var comments = _model._Comments.Where(a => a.Parentid == 0).ToList();
                var chldcomments = _model._Comments.Where(a => a.Parentid != 0).OrderByDescending(a => a.Parentid).ToList();
                _model._Comments = comments;
                _model._childComments = chldcomments;
            }
            _model._offerTechFilter = GetFilterByOfferId(JobID); 
            return View("view", _model);
        } 
        public ActionResult acceptoffer(string token)
        {
            Crypto _crypt = new Crypto(); 
            int offerid = Convert.ToInt32(_crypt.DecryptStringAES(token));
            TempData["error"] = new jobofferservices().re_acceptedoffer(offerid); 
            return RedirectToAction("index");
        }

        public ActionResult declineoffer(string token)
        {
            Crypto _crypt = new Crypto();
            jobofferservices _ser = new jobofferservices();
            int offerid = Convert.ToInt32(_crypt.DecryptStringAES(token));
            TempData["error"] = _ser.assign_declineoffer(offerid); 
            return RedirectToAction("index");
        }

        public ActionResult update_subjob(int JobID = 0, int offerid = 0, int btn = 0, int type = 1, HttpPostedFileBase file = null, string Comment = "", string file_title = "", int action = 0, string jtoken = "", string token = "")
        {
            Crypto _crypt = new Crypto();
            if (!string.IsNullOrEmpty(jtoken))
            {
                JobID = Convert.ToInt32(_crypt.DecryptStringAES(jtoken));
            }
            if (!string.IsNullOrEmpty(token))
            {
                offerid = Convert.ToInt32(_crypt.DecryptStringAES(token));
            }
            ViewBag.project = 0;
            ViewBag.permissions = _permission;
            ViewBag.type = type;
            jobofferservices _ser = new jobofferservices();
            job_items1 _model = new job_items1();
            var mes = "";
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            if (type == 4)
            {///0--assign 1-resubmiut--2-remove
                if (btn == 2)
                {
                    mes = _ser.delete_submitedoffer(offerid);
                }
                else if (btn == 1)
                {
                    mes = _ser.re_submitedoffer(offerid);
                }
                else if (btn == 4)
                {
                    ViewBag.type = 1;
                    type = 1;
                    mes = _ser.re_acceptedoffer(offerid);
                }
                else if (btn == 5)
                {
                    mes = _ser.assign_declineoffer(offerid);
                    TempData["error"] = mes;
                    return RedirectToAction("index");
                }
                else if (btn == 0)
                {
                    mes = _ser.assign_submitedoffer(offerid);
                    TempData["error"] = mes;
                    return RedirectToAction("index");
                }
            }
            if (type == 2)
            {

                mes = _ser.add_new_comment(Comment, userid, JobID);
            }
            else if (type == 3)
            {
                string path = Server.MapPath("~/image/jobofferpicture-" + JobID);
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                string filenames = new helper().GetRandomAlphaNumeric(4);
                var extension = Path.GetExtension(file.FileName);
                var imagepath = path + "/" + filenames + extension;
                string tempfilename = "/image/jobofferpicture-" + JobID + "/" + filenames + extension;
                if (System.IO.File.Exists(imagepath))
                {
                    System.IO.File.Delete(imagepath);
                }
                file.SaveAs(imagepath);
                long b = file.ContentLength;
                long kb = b / 1024;
                mes = _ser.AddNewDocument(file_title, userid, JobID, kb.ToString() + " kb", tempfilename);
            }
            bool isoffered = false;
            ModelState.Clear();
            int offerids = 0;
            var subcontractortype = new UserService().GetUserType(userid);
            _model = _ser.GetAllActiveOfferById(JobID, userid, subcontractortype, ref offerids, ref isoffered, userid);
            _model = new helper().GenerateError<job_items1>(_model, mes);
            ViewBag.viewtype = Session["type"] == null ? 1 : Convert.ToInt32(Session["type"]);
            _model._tools = get_all_tools();
            _model._skills = get_all_skills();
            _model.type = type;
            if (_permission.Contains(3074))
            {
                ViewBag.isoffered = isoffered;
                ViewBag.offerid = offerids;
                var comments = _model._Comments.Where(a => a.isadminpost == true || a.Commentby == userid || a.Commentto == userid).ToList();
                _model._Comments = comments;
                ViewBag.issubcontractor = (subcontractortype == "S" || subcontractortype == "LT") ? true : false; 
                return View("Tview", _model);
            }
            else
            {
                var comments = _model._Comments.Where(a => a.Parentid == 0).ToList();
                var chldcomments = _model._Comments.Where(a => a.Parentid != 0).OrderByDescending(a => a.Parentid).ToList();
                _model._Comments = comments;
                _model._childComments = chldcomments;
            }
            _model._offerTechFilter = GetFilterByOfferId(JobID); 
            return View("view", _model);
        } 
        public bool validatejobId(string id)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            return new jobofferservices().validateJobId(id, workspaceid);
        } 
        public string add_comment(string txt, int cmntid, int jobid)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            string createdBy = "";
            var cst = new jobofferservices().add_new_comment2(txt, userid, jobid, ref createdBy, false, cmntid);
            string res = "<tr><td nowrap style='width:24%;padding:0px;border:0px !important;color:#afaaaa;padding-left:10px;'>" + createdBy + "</td>";
            res = res + "<td style='width:50%;padding:0px;border:0px !important;color:#afaaaa;'>" + txt + "</td>";
            res = res + "<td style='width:20%;padding:0px;border:0px !important;color:#afaaaa;'>" + cst.ToString() + "</td>";
            res = res + "<td nowrap style='width: 20%;padding:0px;border:0px !important;color:#afaaaa;'></td></tr>";
            HtmlString htm = new HtmlString(res);
            return htm.ToString();
        } 
        public string getreply(int mainId)
        {
            string res = "";
            var lst = new jobofferservices().getreply(mainId);
            foreach (var item in lst)
            {
                res = res + "<tr><td nowrap style='width:24%;padding:0px;border:0px !important;color:#afaaaa;padding-left:10px;'>" + item.createdBy + "</td>";
                res = res + "<td style='width:50%;padding:0px;border:0px !important;color:#afaaaa;'>" + item.text + "</td>";
                res = res + "<td style='width:20%;padding:0px;border:0px !important;color:#afaaaa;'>" + item.timedate + "</td>";
                res = res + "<td nowrap style='width: 20%;padding:0px;border:0px !important;color:#afaaaa;'></td></tr>"; 
            }
            HtmlString htm = new HtmlString(res);
            return htm.ToString();
        }
        public string getreply1(int mainId)
        {
            string res = "";
            var lst = new jobofferservices().getreply(mainId);
            foreach (var item in lst)
            {
                res += "<tr><td nowrap style='width:25%;padding:0px;border:0px !important;color:#afaaaa;'>" + item.createdBy + "</td>";
                res += "<td style='width:51%;padding:0px;border:0px !important;color:#afaaaa;'>" + item.text + "</td>";
                res += "<td style='width:20%;padding:0px;border:0px !important;color:#afaaaa;'>" + item.timedate + "</td>";
                res += "<td nowrap style='width: 20%;padding:0px;border:0px !important;color:#afaaaa;'></td></tr>"; 
            }
            HtmlString htm = new HtmlString(res);
            return htm.ToString();
        } 
        public bool validatejobIdEdit(int id, string jobid)
        {
            return new jobofferservices().validateJobId(id, jobid);
        } 
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult add(job _model, int type = 1, string techrate = "", string clientrate = "", string MapValidateAddress = "", string zip = "", int projectId = 0)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            var _filtermodel = new jobfilter();
            _filtermodel = new filterservices().getfilter<jobfilter>(_filtermodel, userid, pageid);
            _model.jdate = string.IsNullOrEmpty(_filtermodel.date) ? "" : _filtermodel.date;
            _model.jcclient = _filtermodel.cclient ?? 0;
            _model.jLocation = string.IsNullOrEmpty(_filtermodel.Location) ? "" : _filtermodel.Location;
            _model.jcTechnician = _filtermodel.cTechnician ?? 0;
            _model.jcDispatcher = _filtermodel.cDispatcher ?? 0;
            _model.jcStatus = _filtermodel.Status ?? 0;
            _model.jisActiveFilter = _filtermodel.isActiveFilter;
            _model.jproject = _filtermodel.project ?? 0;
            _model._jskills = get_all_skills(workspaceid);
            _model._jtools = get_all_tools(workspaceid);
            _model.juserid = userid;
            _model.PERMISSIONS = _permission;
            List<int> _ctyiid = new List<int>();
            ViewBag.Skiil = string.IsNullOrEmpty(_filtermodel.Skiil) ? _ctyiid.ToArray() : Array.ConvertAll(_filtermodel.Skiil.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries), int.Parse);

            ViewBag.PmanagerId = _filtermodel.PmanagerId ?? 0;
            ViewBag.cmanagerId = _filtermodel.cmanagerId ?? 0;
            ViewBag.screening = _filtermodel.screening ?? 0;
            ViewBag.length = _filtermodel.length ?? 25;
            jobofferservices _ser = new jobofferservices();
            //int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            string street = ""; string city = ""; string state = ""; string zipcode = ""; int StateId = 0;
            MapValidateAddress.GetAddress(ref street, ref city, ref state, ref zipcode);
            _model.city_id = new defaultservices().CreateCity(ref StateId, state, city);
            _model.state_id = StateId;
            _model.zip = zipcode;
            _model.street = street;
            var mes = _ser.AddNewOffer(_model, userid, techrate, clientrate, MapValidateAddress, zipcode, workspaceid, projectId);
            _model = new helper().GenerateError<job>(_model, mes); 
            string view = "admin";
            if (_permission.Contains(3069))
            {
                // _model._jobs = _ser.get_all_active_jobs_bymemberid(userid, "")._jobs;
                _model.jStateId = _filtermodel.state_filter_id ?? 0;
                _model.jCityId = _filtermodel.city_filter_id ?? 0;
                ViewBag.hiderate = true;
                view = "member";
            }
            else
            {
                ViewBag.hiderate = false;
                // _model._jobs = _ser.get_all_active_jobs("")._jobs;
            }
            defaultservices _default = new defaultservices();
            ViewBag.select = _default.GetAllActiveState();
            if (mes.Contains("Error"))
            {
                ViewBag.type = type;
                _model.type = type;
                return View("edit", _model);
            }
            if (type <2)
            {
                return View(view, _model); 
            }
            else
            { 
                return View("calender", _model);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult update_job(job _model, int type = 1)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            var _filtermodel = new jobfilter();
            _filtermodel = new filterservices().getfilter<jobfilter>(_filtermodel, userid, pageid); 
            _model.jdate = string.IsNullOrEmpty(_filtermodel.date) ? "" : _filtermodel.date;
            _model.jcclient = _filtermodel.cclient ?? 0;
            _model.jLocation = string.IsNullOrEmpty(_filtermodel.Location) ? "" : _filtermodel.Location;
            _model.jcTechnician = _filtermodel.cTechnician ?? 0;
            _model.jcDispatcher = _filtermodel.cDispatcher ?? 0;
            _model.jcStatus = _filtermodel.Status ?? 0;
            _model.jisActiveFilter = _filtermodel.isActiveFilter;
            _model.jproject = _filtermodel.project ?? 0;
            _model._jskills = get_all_skills(workspaceid);
            _model._jtools = get_all_tools(workspaceid);
            _model.juserid = userid;
            _model.PERMISSIONS = _permission; List<int> _ctyiid = new List<int>();
            ViewBag.Skiil = string.IsNullOrEmpty(_filtermodel.Skiil) ? _ctyiid.ToArray() : Array.ConvertAll(_filtermodel.Skiil.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries), int.Parse);

            ViewBag.PmanagerId = _filtermodel.PmanagerId ?? 0;
            ViewBag.cmanagerId = _filtermodel.cmanagerId ?? 0;
            ViewBag.screening = _filtermodel.screening ?? 0;
            ViewBag.length = _filtermodel.length ?? 25;
            jobofferservices _ser = new jobofferservices();
            string street = ""; string city = ""; string state = ""; string zipcode = ""; int StateId = 0;
            _model.MapValidateAddress.GetAddress(ref street, ref city, ref state, ref zipcode);
            _model.city_id = new defaultservices().CreateCity(ref StateId, state, city);
            _model.state_id = StateId;
            _model.zip = zipcode;
            _model.street = street;
            var mes = _ser.UpdateOffer(_model, userid);
            _model = new helper().GenerateError<job>(_model, mes); 
            string view = "admin";
            if (_permission.Contains(3069))
            {
                _model.jStateId = _filtermodel.state_filter_id ?? 0;
                _model.jCityId = _filtermodel.city_filter_id ?? 0;
                // _model._jobs = _ser.get_all_active_jobs_bymemberid(userid, "")._jobs;
                //ViewBag.hiderate = true;
                view = "member";
            }
            else
            {
               
                //ViewBag.hiderate = false;
                //  _model._jobs = _ser.get_all_active_jobs("")._jobs;
            }
            //defaultservices _default = new defaultservices();
            //ViewBag.select = _default.GetAllActiveState();
            _model.type = type;
            if (mes.Contains("Error"))
            {
                ViewBag.type = type;
                return View("edit", _model);
            } 
            if (type <2)
            {
                return View(view, _model); 
            }
            else
            { 
                return View("calender", _model);
            }
        } 
        [HttpPost]
        public string updateassign(int memberid, int jobid, string rate)
        {
            var name = "";
            jobofferservices _ser = new jobofferservices();
            name = _ser.assignjob(jobid, memberid, rate);
            return name;
        }  
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult edit(job_items1 _model, int type = 1)
        {
            bool isoffered = false;
            ViewBag.permissions = _permission;
            jobofferservices _ser = new jobofferservices();
            ViewBag.type = type;
            int offerid = 0;
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            var mes = _ser.update_job_byview(_model, userid);
            var subcontractortype = new UserService().GetUserType(userid);
            _model = _ser.GetAllActiveOfferById(_model.JobID, userid, subcontractortype, ref offerid, ref isoffered, userid);
            _model = new helper().GenerateError<job_items1>(_model, mes);
            ViewBag.type = Session["type"] == null ? type : Convert.ToInt32(Session["type"]);
            _model._tools = get_all_tools();
            _model._skills = get_all_skills();
            _model.type = Session["type"] == null ? type : Convert.ToInt32(Session["type"]);
            _model.viewtype = _model.type;
            if (_permission.Contains(3074))
            {
                ViewBag.isoffered = isoffered;
                ViewBag.offerid = offerid;
                var comments = _model._Comments.Where(a => a.isadminpost == true || a.Commentby == userid || a.Commentto == userid).ToList();
                _model._Comments = comments;
                ViewBag.issubcontractor = (subcontractortype == "S" || subcontractortype == "LT") ? true : false; 
                return View("Tview", _model);
            }
            else
            {
                var comments = _model._Comments.Where(a => a.Parentid == 0).ToList();
                var chldcomments = _model._Comments.Where(a => a.Parentid != 0).OrderByDescending(a => a.Parentid).ToList();
                _model._Comments = comments;
                _model._childComments = chldcomments;
            }
            _model._offerTechFilter = GetFilterByOfferId(_model.JobID);

            return View("view", _model);
        } 
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Tedit(job_items1 _model, int type = 1)
        {
            bool isoffered = false;
            ViewBag.permissions = _permission;
            jobofferservices _ser = new jobofferservices();
            ViewBag.type = type;
            int offerid = 0;
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            var subcontractortype = new UserService().GetUserType(userid);
            var mes = _ser.update_job_byviewT(_model);
            _model = _ser.GetAllActiveOfferById(_model.JobID, userid, subcontractortype, ref offerid, ref isoffered, userid);
            _model = new helper().GenerateError<job_items1>(_model, mes);
            ViewBag.type = Session["type"] == null ? 1 : Convert.ToInt32(Session["type"]);
            _model.type = Session["type"] == null ? 1 : Convert.ToInt32(Session["type"]);
            _model._tools = get_all_tools();
            _model._skills = get_all_skills();
            if (_permission.Contains(3074))
            {
                ViewBag.isoffered = isoffered;
                ViewBag.offerid = offerid;
                var comments = _model._Comments.Where(a => a.isadminpost == true || a.Commentby == userid || a.Commentto == userid).ToList();
                _model._Comments = comments;
                ViewBag.issubcontractor = (subcontractortype == "S" || subcontractortype == "LT") ? true : false; 
                return View("Tview", _model);
            }
            else
            {
                var comments = _model._Comments.Where(a => a.Parentid == 0).ToList();
                var chldcomments = _model._Comments.Where(a => a.Parentid != 0).OrderByDescending(a => a.Parentid).ToList();
                _model._Comments = comments;
                _model._childComments = chldcomments;
            }
            _model._offerTechFilter = GetFilterByOfferId(_model.JobID);

            return View("view", _model);
        }
    }
}