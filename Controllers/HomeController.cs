﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.Web.UI;

namespace hrm.Controllers
{
    [AuthorizeVerifiedloggedin]
    public class HomeController : Controller
    {
        public List<int> _permission;
        public List<int> _Paypermission;
        public int roleid;
        public HomeController()
        {
            roleid = System.Web.HttpContext.Current.Request.Cookies["_pid"] == null ? 0 : Convert.ToInt32(System.Web.HttpContext.Current.Request.Cookies["_pid"].Value.ToString());
            string PList = System.Web.HttpContext.Current.Request.Cookies["_plist"] == null ? "0" : System.Web.HttpContext.Current.Request.Cookies["_plist"].Value.ToString();
            _permission = PList=="0"?null: JsonConvert.DeserializeObject<List<int>>(PList); 
        }

        public ActionResult reset_password()
        {
            ResetModel _model = new ResetModel();
            return View(_model);
        }

        [HttpPost]
        public ActionResult reset_password(ResetModel _model)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            var newpassword = new Crypto().EncryptStringAES(_model.Password);
            TempData["error"] = new accountservices().update_password(userid, newpassword);
            return RedirectToAction("Index", "home");
        }

        //[OutputCache(Duration = 60, VaryByParam = "id,v",Location = OutputCacheLocation.Client)]
        public ActionResult Index(int? id)
        {
            string message = TempData["error"] != null ? TempData["error"].ToString() : "";
            int webspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            if (_permission.Contains(54))
            {
                dashboardservices _ser = new dashboardservices();
                var _model = _ser.GetDashBoardData(webspaceid);
                _model = new helper().GenerateError<dashboardmodal>(_model, message);
                return View("index", _model);
            }
            else
            {
                noticeboardServices _sers = new noticeboardServices();
                var _model1 = _sers.get_all_active_notice(id, 1, webspaceid);
                _model1 = new helper().GenerateError<noticemodel>(_model1, message);
                return View("dashboard", _model1);
            }
        }

        public JsonResult Fetchjobs(int draw = 1)
        {
            int WorkspaceId = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            dashboardservices _services = new dashboardservices();
            datatableAjax<dashboardjobs> _AjaxResponse = new datatableAjax<dashboardjobs>();
             var _Model = _services.GetRecentJobs(WorkspaceId);
            _AjaxResponse.draw = draw;
            _AjaxResponse.data = _Model; 
            return base.Json(_AjaxResponse, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Fetchoffers(int draw = 1)
        {
            int WorkspaceId = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            dashboardservices _services = new dashboardservices();
            datatableAjax<dashboardoffer> _AjaxResponse = new datatableAjax<dashboardoffer>();
             var _Model = _services.GetRecentOffers(WorkspaceId);
            _AjaxResponse.draw = draw;
            _AjaxResponse.data = _Model; 
            return base.Json(_AjaxResponse, JsonRequestBehavior.AllowGet);
        }


        public void readgeneral()
        {
            int memberid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            new ContractorServices().updatnotification(memberid);
        }
        public PartialViewResult getprofile()
        {
            int memberid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            bool PermissionsCookieExist = Request.Cookies["_psd"] == null ? false : true; 
            UserService _ser = new UserService();
            Dictionary<string, bool> _permissions ;
            if (!PermissionsCookieExist)
            {
                helper _helper = new helper();
                _permissions = new PermissionServices().NotificationPermissionByRoleId(roleid); //make single call 
                string _Cookie = JsonConvert.SerializeObject(_permissions);
                _helper.CreateCookie<string>(_Cookie, "_psd", 50);
            }
            else
            {
                string _cokie = Request.Cookies["_psd"].Value.ToString();
                _permissions = JsonConvert.DeserializeObject<Dictionary<string, bool>>(_cokie);
            }
            var _model = _ser.GetUserProfile(memberid, _permissions["isadmin"]);
            _model.isnotificationpermission = _permission.Contains(5088);
            _model.billing = _permissions["billing"];// _rser.get_all_active_permissions_bypermissionId(5104, roleid);
            _model.pricing = _permissions["pricing"];// _rser.get_all_active_permissions_bypermissionId(5105, roleid);
            _model.resuces = _permissions["resuces"];// _rser.get_all_active_permissions_bypermissionId(5106, roleid);
            return PartialView("profile", _model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}