﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Web.Mvc;

namespace hrm.Controllers
{
    [AuthorizeVerifiedloggedin]
    public class toolsController : Controller
    {
        public List<int> _permission;
        public toolsController()
        {
            string PList = System.Web.HttpContext.Current.Request.Cookies["_plist"] == null ? "0" : System.Web.HttpContext.Current.Request.Cookies["_plist"].Value.ToString();
            _permission = PList == "0" ? null : JsonConvert.DeserializeObject<List<int>>(PList);
        }
        public ActionResult Index()
        {
            if (!_permission.Contains(5076))
            {
                return View("unauth");
            }
            tools _model = new tools();
            string message = TempData["error"] != null? TempData["error"].ToString():string.Empty;
            _model.PERMISSIONS = _permission; 
            _model = new helper().GenerateError<tools>(_model, message);
            return View(_model);
        }

        public JsonResult FetchTools(int start = 0, int length = 25, int draw = 1)
        {
            toolservices toolservices = new toolservices();
            datatableAjax<tools_item> _AjaxResponse = new datatableAjax<tools_item>();
            int _Total = 0;
            int _WorkSpaceId = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string _Search = Request.Params["search[value]"] ?? "";
            start = start == 0 ? 1 : start + 1;
            length = start == 1 ? length : start - 1 + length;
            var ToolModel = toolservices.GetAllToolsByIndex(ref _Total, start, length, _Search, _WorkSpaceId);
            _AjaxResponse.draw = draw;
            _AjaxResponse.data = ToolModel;
            _AjaxResponse.recordsFiltered = ToolModel.Count() > 0 ? _Total : 0;
            _AjaxResponse.recordsTotal = _Total;
            return Json(_AjaxResponse, JsonRequestBehavior.AllowGet);
        } 
        [HttpPost]
        public ActionResult Index(tools _model)
        {
            var toolservices = new toolservices();
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString()); 
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString()); 
            var error_text = toolservices.CreateTool(_model.name,_model.typeid ,userid, workspaceid);//Will be added as asyn in future
            if (!error_text.ToLower().Contains("error"))
            {
                ModelState.Clear();
            }
            tools _model1 = new tools(); 
            _model1 = new helper().GenerateError<tools>(_model1, error_text);
            _model1.PERMISSIONS = _permission;
            return View(_model1);
        } 
        [HttpPost]
        public JsonResult Update(tools _model)
        {    var error_text = new toolservices().UpdateTool(_model.name, _model.typeid, _model.toolid);
            return Json(error_text, JsonRequestBehavior.AllowGet); 
        }
        [HttpPost]
        public ActionResult Delete(int id) { 
            var error_text = new toolservices().DeleteTool(id);
            return Json(error_text, JsonRequestBehavior.AllowGet);
        }
    } 
}