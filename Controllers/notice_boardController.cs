﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace hrm.Controllers
{
    [AuthorizeVerifiedloggedin]
    public class notice_boardController : Controller
    {
        public List<int> _permission;
        public notice_boardController()
        {
            string PList = System.Web.HttpContext.Current.Request.Cookies["_plist"] == null ? "0" : System.Web.HttpContext.Current.Request.Cookies["_plist"].Value.ToString();
            _permission = PList == "0" ? null : JsonConvert.DeserializeObject<List<int>>(PList);
        }
        public ActionResult Index(int? id)
        {
            if (!_permission.Contains(1056))
            {
                return View("unauth");
            }
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            noticemodel _model = new noticemodel();
            noticeboardServices _ser = new noticeboardServices();
            string  message = "";
            if (id.HasValue)
            {
                if (!_permission.Contains(1059))
                {
                    return View("unauth");
                }
                else
                {
                    message = _ser.delete_notice(id.Value);
                }
            }
            _model = _ser.get_all_active_notice(null,null, workspaceid);
            ViewBag.permissions = _permission;
            if (TempData["error"] != null)
            {
                 message = TempData["error"].ToString();
            }
            _model = new helper().GenerateError<noticemodel>(_model, message);
            return View(_model);
        }

        public JsonResult get_des_byid(int id)
        {
          var message=  new noticeboardServices().get_notice(id);
            return Json(message, JsonRequestBehavior.AllowGet);
        }


        public void update_notice_status(int noticeid,int noticestatusid)
        {
            new noticeboardServices().update_notice(noticeid, noticestatusid);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult add(noticemodel _model)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            noticeboardServices _ser = new noticeboardServices();
            var message = _ser.add_new_notice(_model, userid, workspaceid);
            ModelState.Clear();
            _model = _ser.get_all_active_notice(null,null, workspaceid);
            ViewBag.permissions = _permission;
            _model = new helper().GenerateError<noticemodel>(_model, message);
            return View("Index", _model);
        }

          public ActionResult add()
        {
            if (!_permission.Contains(1057))
            {
                return View("unauth");
            }
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            noticemodel _model = new noticemodel();
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            noticeboardServices _ser = new noticeboardServices();
            var message = "";
            ModelState.Clear();
            _model = _ser.get_all_active_notice(null,null, workspaceid);
            ViewBag.permissions = _permission;
            _model = new helper().GenerateError<noticemodel>(_model, message);
            return View("Index", _model);
        }

        public ActionResult edit()
        {
            if (!_permission.Contains(1058))
            {
                return View("unauth");
            }
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            noticemodel _model = new noticemodel();
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            noticeboardServices _ser = new noticeboardServices();
            var message = "";
            ModelState.Clear();
            _model = _ser.get_all_active_notice(null,null, workspaceid);
            ViewBag.permissions = _permission;
            _model = new helper().GenerateError<noticemodel>(_model, message);
            return View("Index", _model);
        }



        [HttpPost]
        [ValidateInput(false)]
        public ActionResult edit(noticemodel _model)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            noticeboardServices _ser = new noticeboardServices();
            var message = _ser.update_notice(_model, userid);
            ModelState.Clear();
            _model = _ser.get_all_active_notice(null,null, workspaceid);
            ViewBag.permissions = _permission;
            _model = new helper().GenerateError<noticemodel>(_model, message);
            return View("Index", _model);
        }
    }
}