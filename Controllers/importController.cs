﻿using Hangfire;
using hrm.Database;
using hrm.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrm.Controllers
{
    [AuthorizeVerifiedloggedin]
    public class importController : Controller
    {
        helper _helper = new helper();
        public string conctionstring = ConfigurationManager.ConnectionStrings["con"].ToString();

        private String sendError(string msg)
        {
            String funcNum = System.Web.HttpContext.Current.Request["CKEditorFuncNum"];
            return "<scr" + "ipt type='text/javascript'> window.parent.CKEDITOR.tools.callFunction(" + funcNum + ", '', '" + msg + "')</scr" + "ipt>";

        }
        [HttpPost]
        public String Upload(string baseData)
        {
            //String basePath = "C:\\TestFiles\\";
            String basePath = Server.MapPath("~/CkEditorFiles"); //"D:\\TestFiles\\";

            String baseUrl = "https://www.taskayak.com/CkEditorFiles/";
            //String baseUrl = "/ckfinder/userfiles/";
            // Optional: instance name (might be used to adjust the server folders for example)
            String CKEditor = System.Web.HttpContext.Current.Request["CKEditor"];

            // Required: Function number as indicated by CKEditor.
            String funcNum = System.Web.HttpContext.Current.Request["CKEditorFuncNum"];

            // Optional: To provide localized messages
            String langCode = System.Web.HttpContext.Current.Request["langCode"];

            int total;
            try
            {
                total = System.Web.HttpContext.Current.Request.Files.Count;
            }
            catch (Exception)
            {
                return sendError("Error uploading the file");
            }
            if (total == 0)
                return sendError("No file has been sent");

            if (!System.IO.Directory.Exists(basePath))
                return sendError("basePath folder doesn't exists");

            //Grab the file name from its fully qualified path at client
            HttpPostedFile theFile = System.Web.HttpContext.Current.Request.Files[0];

            String strFileName = theFile.FileName;
            if (strFileName == "")
                return sendError("File name is empty");

            String sFileName = System.IO.Path.GetFileName(strFileName);

            String name = System.IO.Path.Combine(basePath, sFileName);
            theFile.SaveAs(name);

            String url = baseUrl + sFileName.Replace("'", "\'");

            // ------------------------
            // Write output
            // ------------------------

            //return "<scr" + "ipt type='text/javascript'> window.parent.CKEDITOR.tools.callFunction(" + funcNum + ", '" + url + "', '')</scr" + "ipt>";

            return "<scr" + "ipt type='text/javascript'> window.parent.CKEDITOR.tools.callFunction(" + funcNum + ", '" + url + "', '')</scr" + "ipt>";
        }
        public ActionResult uploadPartial(int CKEditorFuncNum = 1)
        {
            var appData = Server.MapPath("~/CkEditorFiles");
            var images = Directory.GetFiles(appData).Select(x => new imagesviewmodel
            {
                Url = "https://www.taskayak.com/CkEditorFiles/" + Path.GetFileName(x)
            });
            ViewBag.CKEditorFuncNum = CKEditorFuncNum;
            return View(images);
        }
        [HttpPost]
        public JsonResult uploadimage(HttpPostedFileBase myfile)
        {
            string newfilename = "i" + DateTime.Now.Ticks.ToString() + myfile.FileName;
            string filepath = Path.Combine(Server.MapPath("~/csvfiles/"), newfilename);
            myfile.SaveAs(filepath);
            var rpath = "https://www.taskayak.com/csvfiles/" + newfilename;
            return Json(rpath, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult client(HttpPostedFileBase file)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string ext = Path.GetExtension(file.FileName);
            if (ext.ToLower() == ".csv")
            {
                try
                {
                    Dictionary<int, string> _defaultvalues = new Dictionary<int, string>();
                    string newfilename = "cl_" + DateTime.Now.Ticks.ToString() + file.FileName;
                    string filepath = Path.Combine(Server.MapPath("~/csvfiles/"), newfilename);
                    file.SaveAs(filepath);
                    DataTable dataTable = _helper.ConvertToDataTable(Server.MapPath("~/csvfiles/" + newfilename), 10, true);
                    dataTable = GetClientForBulkInsert(dataTable, userid, workspaceid);
                    var objBulk = new BulkUploadToSql()
                    {
                        InternalStore_dt = dataTable,
                        TableName = "tbl_clients",
                        CommitBatchSize = 200,
                        ConnectionString = conctionstring
                    };
                    objBulk.Commit_datatable();
                    TempData["error"] = dataTable.Rows.Count + " Clients Imported succesfully";
                }
                catch (Exception ex)
                {
                    TempData["error"] = "Error : " + ex.Message;
                }
                return RedirectToAction("index", "client", new { v = 1234 });
            }
            else
            {
                TempData["error"] = "Error : Please upload a valid Document with the extenion .csv";
                return RedirectToAction("index", "client", new { v = 1234 });
            }
        }
        public DataTable GetClientForBulkInsert(DataTable gv, int createdby, int workspaceid)
        {
            defaultservices cityService = new defaultservices();
            int stateId = 0;
            int clientid = Convert.ToInt32(new clientservices().GetLatestClientId());
            DataTable dataTable = new DataTable("DummyTable");
            dataTable.Columns.Add("Id", typeof(int));
            dataTable.Columns.Add("Clientname", typeof(string));
            dataTable.Columns.Add("rate", typeof(string));
            dataTable.Columns.Add("Address", typeof(string));
            dataTable.Columns.Add("Cityid", typeof(int));
            dataTable.Columns.Add("Stateid", typeof(int));
            dataTable.Columns.Add("Pincode", typeof(string));
            dataTable.Columns.Add("CreatedDate", typeof(DateTime));
            dataTable.Columns.Add("createdby", typeof(int));
            dataTable.Columns.Add("Email", typeof(string));
            dataTable.Columns.Add("Phone", typeof(string));
            dataTable.Columns.Add("primarycontact", typeof(string));
            dataTable.Columns.Add("workspaceid", typeof(int));
            dataTable.Columns.Add("clientIndexId", typeof(string));
            dataTable.Columns.Add("SuiteNumber", typeof(string));
            dataTable.Columns.Add("MapValidateAddress", typeof(string));
            try
            {
                for (int i = 0; i <= gv.Rows.Count - 1; i++)
                {
                    int cityId = cityService.CreateCity(ref stateId, gv.Rows[i][8].ToString(), gv.Rows[i][7].ToString());
                    clientid += 1;
                    DataRow row = dataTable.NewRow();
                    row[0] = 0;
                    row[1] = gv.Rows[i][0].ToString();//name
                    row[2] = gv.Rows[i][3].ToString();//rate
                    row[3] = gv.Rows[i][6].ToString();//address
                    row[4] = cityId;//city
                    row[5] = stateId;//state
                    row[6] = gv.Rows[i][9].ToString();//zip
                    row[7] = DateTime.Now;
                    row[8] = createdby;
                    row[9] = gv.Rows[i][1].ToString();//email
                    row[10] = gv.Rows[i][2].ToString();//phone
                    row[11] = gv.Rows[i][4].ToString();//primary contact
                    row[12] = workspaceid;
                    row[13] = clientid;
                    row[14] = gv.Rows[i][5].ToString();//SuiteNumber
                    row[15] = gv.Rows[i][6].ToString().GetAddress(gv.Rows[i][7].ToString(), gv.Rows[i][8].ToString(), gv.Rows[i][9].ToString());//address+city+state+zip
                    dataTable.Rows.Add(row);
                }
            }
            catch (Exception)
            {
            }
            return dataTable;
        }
        [HttpPost]
        public ActionResult manager(HttpPostedFileBase file, int clientid)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            Crypto _crypt = new Crypto();
            string token = _crypt.EncryptStringAES(clientid.ToString());
            string ext = Path.GetExtension(file.FileName);
            if (ext.ToLower() == ".csv")
            {
                try
                {
                    Dictionary<int, string> _defaultvalues = new Dictionary<int, string>();
                    string newfilename = "mgr_" + DateTime.Now.Ticks.ToString() + file.FileName;
                    string filepath = Path.Combine(Server.MapPath("~/csvfiles/"), newfilename);
                    file.SaveAs(filepath);
                    DataTable dataTable = _helper.ConvertToDataTable(Server.MapPath("~/csvfiles/" + newfilename), 3, true);
                    dataTable = GetmanagerForBulkInsert(dataTable, userid, clientid, workspaceid);
                    var objBulk = new BulkUploadToSql()
                    {
                        InternalStore_dt = dataTable,
                        TableName = "tbl_projectmanager",
                        CommitBatchSize = 200,
                        ConnectionString = conctionstring
                    };
                    objBulk.Commit_datatable();
                    TempData["error"] = dataTable.Rows.Count + " Project Manager Imported succesfully";
                }
                catch (Exception ex)
                {
                    TempData["error"] = "Error : " + ex.Message;
                }
                return RedirectToAction("view", "client", new { token });
            }
            else
            {
                TempData["error"] = "Error : Please upload a valid Document with the extenion .csv";
                return RedirectToAction("view", "client", new { token });
            }
        }
        public DataTable GetmanagerForBulkInsert(DataTable gv, int createdby, int partnerid, int Workspaceid)
        {
            DataTable dataTable = new DataTable("DummyTable");
            dataTable.Columns.Add("id", typeof(int));
            dataTable.Columns.Add("Name", typeof(string));
            dataTable.Columns.Add("ClientId", typeof(int));
            dataTable.Columns.Add("Createddate", typeof(DateTime));
            dataTable.Columns.Add("CreatedBy", typeof(int));
            dataTable.Columns.Add("Phonenumber", typeof(string));
            dataTable.Columns.Add("Email", typeof(string));
            dataTable.Columns.Add("prj_workspaceid", typeof(int));
            try
            {
                for (int i = 0; i <= gv.Rows.Count - 1; i++)
                {
                    DataRow row = dataTable.NewRow();
                    row[0] = 0;
                    row[1] = gv.Rows[i][0].ToString();
                    row[2] = partnerid;
                    row[3] = DateTime.Now;
                    row[4] = createdby;
                    row[5] = gv.Rows[i][2].ToString();
                    row[6] = gv.Rows[i][1].ToString();
                    row[7] = Workspaceid;
                    dataTable.Rows.Add(row);
                }
            }
            catch (Exception)
            {
            }
            return dataTable;
        }
        [HttpPost]
        public ActionResult partner(HttpPostedFileBase file)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string ext = Path.GetExtension(file.FileName);
            if (ext.ToLower() == ".csv")
            {
                try
                {
                    Dictionary<int, string> _defaultvalues = new Dictionary<int, string>();
                    string newfilename = "pr_" + DateTime.Now.Ticks.ToString() + file.FileName;
                    string filepath = Path.Combine(Server.MapPath("~/csvfiles/"), newfilename);
                    file.SaveAs(filepath);
                    DataTable dataTable = _helper.ConvertToDataTable(Server.MapPath("~/csvfiles/" + newfilename), 10, true);
                    dataTable = GetpartnerForBulkInsert(dataTable, userid, workspaceid);
                    var objBulk = new BulkUploadToSql()
                    {
                        InternalStore_dt = dataTable,
                        TableName = "tbl_partners",
                        CommitBatchSize = 200,
                        ConnectionString = conctionstring
                    };
                    objBulk.Commit_datatable();
                    TempData["error"] = dataTable.Rows.Count + " Partners Imported succesfully";
                }
                catch (Exception ex)
                {
                    TempData["error"] = "Error :" + ex.Message;
                }
                return RedirectToAction("index", "partners", new { v = 1234 });
            }
            else
            {
                TempData["error"] = "Error : Please upload a valid Document with the extenion .csv";
                return RedirectToAction("index", "partners", new { v = 1234 });
            }
        }
        public DataTable GetpartnerForBulkInsert(DataTable gv, int createdby, int workspaceid)
        {
            defaultservices cityService = new defaultservices();
            int stateId = 0;
            DataTable dataTable = new DataTable("DummyTable");
            dataTable.Columns.Add("Id", typeof(int));
            dataTable.Columns.Add("Partnername", typeof(string));
            dataTable.Columns.Add("rate", typeof(string));
            dataTable.Columns.Add("Address", typeof(string));
            dataTable.Columns.Add("Cityid", typeof(int));
            dataTable.Columns.Add("Stateid", typeof(int));
            dataTable.Columns.Add("Pincode", typeof(string));
            dataTable.Columns.Add("CreatedDate", typeof(DateTime));
            dataTable.Columns.Add("createdby", typeof(int));
            dataTable.Columns.Add("Email", typeof(string));
            dataTable.Columns.Add("Phone", typeof(string));
            dataTable.Columns.Add("primarycontact", typeof(string));
            dataTable.Columns.Add("workspaceid", typeof(int));
            dataTable.Columns.Add("SuiteNumber", typeof(string));
            dataTable.Columns.Add("MapValidateAddress", typeof(string)); 
            try
            {
                for (int i = 0; i <= gv.Rows.Count - 1; i++)
                {
                    int cityId = cityService.CreateCity(ref stateId, gv.Rows[i][8].ToString(), gv.Rows[i][7].ToString()); 
                    DataRow row = dataTable.NewRow();
                    row[0] = 0;
                    row[1] = gv.Rows[i][0].ToString();//name
                    row[2] = gv.Rows[i][3].ToString();//rate
                    row[3] = gv.Rows[i][6].ToString();//address
                    row[4] = cityId;//city
                    row[5] = stateId;//state
                    row[6] = gv.Rows[i][9].ToString();//zip
                    row[7] = DateTime.Now;
                    row[8] = createdby;
                    row[9] = gv.Rows[i][1].ToString();//email
                    row[10] = gv.Rows[i][2].ToString();//phone
                    row[11] = gv.Rows[i][4].ToString();//primary contact
                    row[12] = workspaceid; 
                    row[13] = gv.Rows[i][5].ToString();//SuiteNumber
                    row[14] = gv.Rows[i][6].ToString().GetAddress(gv.Rows[i][7].ToString(), gv.Rows[i][8].ToString() , gv.Rows[i][9].ToString());//address+city+state+zip
                    dataTable.Rows.Add(row);
                }
            }
            catch (Exception)
            {
            }
            return dataTable;
        }

        [HttpPost]
        public ActionResult pmanager(HttpPostedFileBase file, int partnerid)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            Crypto _crypt = new Crypto();
            string token = _crypt.EncryptStringAES(partnerid.ToString());
            string ext = Path.GetExtension(file.FileName);
            if (ext.ToLower() == ".csv")
            {
                try
                {
                    Dictionary<int, string> _defaultvalues = new Dictionary<int, string>();
                    string newfilename = "pmgr_" + DateTime.Now.Ticks.ToString() + file.FileName;
                    string filepath = Path.Combine(Server.MapPath("~/csvfiles/"), newfilename);
                    file.SaveAs(filepath);
                    DataTable dataTable = _helper.ConvertToDataTable(Server.MapPath("~/csvfiles/" + newfilename), 3, true);
                    dataTable = GetpmanagerForBulkInsert(dataTable, userid, partnerid, workspaceid);
                    var objBulk = new BulkUploadToSql()
                    {
                        InternalStore_dt = dataTable,
                        TableName = "tbl_projectmanager_partner",
                        CommitBatchSize = 200,
                        ConnectionString = conctionstring
                    };
                    objBulk.Commit_datatable();
                    TempData["error"] = dataTable.Rows.Count + " Project Manager Imported succesfully";
                }
                catch (Exception ex)
                {
                    TempData["error"] = "Error : " + ex.Message;
                }
                return RedirectToAction("view", "partners", new { token = token });
            }
            else
            {
                TempData["error"] = "Error : Please upload a valid Document with the extenion .csv";
                return RedirectToAction("view", "partners", new { token = token });
            }
        }

        public DataTable GetpmanagerForBulkInsert(DataTable gv, int createdby, int partnerid, int Workspaceid)
        {
            DataTable dataTable = new DataTable("DummyTable");
            DataRow row = null;
            dataTable.Columns.Add("id", typeof(int));
            dataTable.Columns.Add("Name", typeof(string));
            dataTable.Columns.Add("PartnerId", typeof(int));
            dataTable.Columns.Add("Createddate", typeof(DateTime));
            dataTable.Columns.Add("CreatedBy", typeof(int));
            dataTable.Columns.Add("Phonenumber", typeof(string));
            dataTable.Columns.Add("Email", typeof(string));
            dataTable.Columns.Add("prj_workspaceid", typeof(int));
            try
            {
                for (int i = 0; i <= gv.Rows.Count - 1; i++)
                {
                    row = dataTable.NewRow();
                    row[0] = 0;
                    row[1] = gv.Rows[i][0].ToString();
                    row[2] = partnerid;
                    row[3] = DateTime.Now;
                    row[4] = createdby;
                    row[5] = gv.Rows[i][2].ToString();
                    row[6] = gv.Rows[i][1].ToString();
                    row[7] = Workspaceid;
                    dataTable.Rows.Add(row);
                }
            }
            catch (Exception)
            {
            }
            return dataTable;
        }


        [HttpPost]
        public ActionResult cmember(HttpPostedFileBase file, int clientid)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            var type = new UserService().GetUserType(userid);
            bool issubcontractor = type.ToLower() == "s" ? true : false;
            Crypto _crypt = new Crypto();
            string token = _crypt.EncryptStringAES(clientid.ToString());
            string ext = Path.GetExtension(file.FileName);
            List<string> members = new List<string>();
            if (ext.ToLower() == ".csv")
            {
                try
                {
                    Dictionary<int, string> _defaultvalues = new Dictionary<int, string>();
                    string newfilename = "cmem_" + DateTime.Now.Ticks.ToString() + file.FileName;
                    string filepath = Path.Combine(Server.MapPath("~/csvfiles/"), newfilename);
                    file.SaveAs(filepath);
                    DataTable dataTable = _helper.ConvertToDataTable(Server.MapPath("~/csvfiles/" + newfilename), 8, true);
                    dataTable = GetcmemberForBulkInsert(dataTable, userid, clientid, ref members, workspaceid);
                    var objBulk = new BulkUploadToSql()
                    {
                        InternalStore_dt = dataTable,
                        TableName = "tbl_member",
                        CommitBatchSize = 200,
                        ConnectionString = conctionstring
                    };
                    objBulk.Commit_datatable();
                    var template = new templateservices().get_template(3);
                    new jobofferservices().sendoffersimport4(members, true, true, template.sms, template.email, "Field Technician Account", true, workSpaceid: workspaceid);
                    TempData["error"] = dataTable.Rows.Count + " Users imported succesfully";
                }
                catch (Exception ex)
                {
                    TempData["error"] = "Error : " + ex.Message;
                }
                return RedirectToAction("view", "Company", new { token = token, issubcontractor = issubcontractor });
            }
            else
            {
                TempData["error"] = "Error : Please upload a valid Document with the extenion .csv";
                return RedirectToAction("view", "Company", new { token = token, issubcontractor = issubcontractor });
            }
        }


        [HttpPost]
        public ActionResult skill(HttpPostedFileBase file)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string ext = Path.GetExtension(file.FileName);
            if (ext.ToLower() == ".csv")
            {
                try
                {
                    Dictionary<int, string> _defaultvalues = new Dictionary<int, string>();
                    string newfilename = "skl_" + DateTime.Now.Ticks.ToString() + file.FileName;
                    string filepath = Path.Combine(Server.MapPath("~/csvfiles/"), newfilename);
                    file.SaveAs(filepath);
                    DataTable dataTable = _helper.ConvertToDataTable(Server.MapPath("~/csvfiles/" + newfilename), 1, true);
                    dataTable = GetSkillForBulkInsert(dataTable, userid, workspaceid);
                    var objBulk = new BulkUploadToSql()
                    {
                        InternalStore_dt = dataTable,
                        TableName = "tbl_skill_master_import",
                        CommitBatchSize = 200,
                        ConnectionString = conctionstring
                    };
                    objBulk.Commit_datatable();
                    TempData["error"] = dataTable.Rows.Count + " Skills Imported succesfully";
                }
                catch (Exception ex)
                {
                    TempData["error"] = "Error : " + ex.Message;
                }
                // return Redirect("http://localhost:50912/Skills/index/");
                return RedirectToAction("index", "Skills", new { v = 1234 });
            }
            else
            {
                TempData["error"] = "Error : Please upload a valid Document with the extenion .csv";
                // return Redirect("http://localhost:50912/Skills/index/");
                return RedirectToAction("index", "Skills", new { v = 1234 });
            }
        }
        public DataTable GetSkillForBulkInsert(DataTable gv, int createdby, int workspaceid)
        {
            DataTable dataTable = new DataTable("DummyTable");
            DataRow row = null;
            dataTable.Columns.Add("skill_Id", typeof(int));
            dataTable.Columns.Add("name", typeof(string));
            dataTable.Columns.Add("CReatedDate", typeof(DateTime));
            dataTable.Columns.Add("Createdby", typeof(int));
            dataTable.Columns.Add("UpdatedBy", typeof(int));
            dataTable.Columns.Add("UpdatedDate", typeof(DateTime));
            dataTable.Columns.Add("IsActive", typeof(bool));
            dataTable.Columns.Add("skill_workspaceid", typeof(int));

            try
            {
                for (int i = 0; i <= gv.Rows.Count - 1; i++)
                {
                    row = dataTable.NewRow();
                    row[0] = 0;
                    row[1] = gv.Rows[i][0].ToString();
                    row[2] = DateTime.Now;
                    row[3] = createdby;
                    row[4] = createdby;
                    row[5] = DateTime.Now;
                    row[6] = true;
                    row[7] = workspaceid;
                    dataTable.Rows.Add(row);
                }
            }
            catch (Exception)
            {
            }
            return dataTable;
        }
        public int getcityidbystate(int stateid, string city)
        {
            if (string.IsNullOrEmpty(city))
            {
                return 0;
            }
            else
            {
                return new defaultservices().savegetCityId(stateid, city);
            }

        }

        public int getstateidbystatename(string statename)
        {
            return new defaultservices().GetStateId(statename);
        }
        public int getmemberidbymembername(string name)
        {
            return new defaultservices().GetmemberId(name);
        }

        public int getrateidbyrate(string rate)
        {
            return new defaultservices().GetrateId(rate);
        }
        public void AddOfferFilter(int batchId, string Rate, string Screening, string Tools, string Skills, int Distance, int AssignPreference, string Messagetype, int Template,bool techdiscovery,int NotifiedUserId)
        {
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@batchId", batchId);
                _srt.Add("@Rate", Rate);
                _srt.Add("@Tools", Tools);
                _srt.Add("@Skills", Skills);
                _srt.Add("@Distance", Distance);
                _srt.Add("@AssignPreference", AssignPreference);
                _srt.Add("@Screening", Screening);
                _srt.Add("@Messagetype", Messagetype);
                _srt.Add("@Template", Template);
                _srt.Add("@techdiscovery", techdiscovery);
                _srt.Add("@NotifiedUserId", NotifiedUserId);  
                sqlHelper.executeNonQuery("Add_orderforProcessed", "", _srt);
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();

            }
            finally
            {
                sqlHelper.Dispose();
            }

        }

        public List<int> GetOfferByBatchid(int batchId)
        {
            List<int> _offers = new List<int>();
            SqlHelper sqlHelper = new SqlHelper();
            try
            {
                SortedList _srt = new SortedList();
                _srt.Add("@batchId", batchId);
                var dt = sqlHelper.fillDataTable("GetOfferWithBatchid", "", _srt);
                sqlHelper.Dispose();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    _offers.Add(Convert.ToInt32(dt.Rows[i][0].ToString()));
                }
            }
            catch (Exception exception)
            {
                //(new Logger(exception.Message, this.GetType().Namespace, this.GetType().Name, 0, exception.StackTrace, Enums.LogType.Error)).LogWrite();
            }
            finally
            {
            }
            return _offers;
        }

        //offers
        [HttpPost]
        public ActionResult offer(HttpPostedFileBase file, int[] amount, int[] background, int[] Skills, int[] tools, int[] messagetype, int type = 1, int Distance = 0, int Distribution = 0, int uploadProject_manager = 0, int uplodclient = 0, string upclientRate = "$0.00", int uploadProject_manager1 = 0, int upload_dispatcher = 0, int uplodproject = 0, int Template = 0, int techdiscovery = 1)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string ext = Path.GetExtension(file.FileName);
            if (ext.ToLower() == ".csv")
            {
                try
                {
                    Dictionary<int, string> _defaultvalues = new Dictionary<int, string>();
                    string newfilename = "joboffer_" + DateTime.Now.Ticks.ToString() + file.FileName;
                    string filepath = Path.Combine(Server.MapPath("~/csvfiles/"), newfilename);
                    file.SaveAs(filepath);
                    DataTable dataTable = _helper.ConvertToDataTable(Server.MapPath("~/csvfiles/" + newfilename), 15, true);
                    dataTable = GetjoboffersForBulkInsert(dataTable, userid, uplodclient, uploadProject_manager, upclientRate, uploadProject_manager1, workspaceid, upload_dispatcher, uplodproject);
                    var objBulk = new BulkUploadToSql()
                    {
                        InternalStore_dt = dataTable,
                        TableName = "tbl_job_offer_temp",
                        CommitBatchSize = 200,
                        ConnectionString = conctionstring
                    };
                    var Batchid = objBulk.Commit_datatableWithretrun("tbl_job_offer_insert_from_temp", workspaceid);
                    if (Batchid != 0)
                    {//int[] amount, int[] background ,int[] Skills ,int[] tools ,int[] messagetype,
                        string _amount = amount == null ? "" : amount.ToDelimitedString();
                        string _background = background == null ? "" : background.ToDelimitedString();
                        string _Skills = Skills == null ? "" : Skills.ToDelimitedString();
                        string _tools = tools == null ? "" : tools.ToDelimitedString();
                        string _messagetype = messagetype == null ? "" : messagetype.ToDelimitedString();
                        AddOfferFilter(Batchid, _amount, _background, _tools, _Skills, Distance, Distribution, _messagetype, Template, techdiscovery >1?true:false, userid);
                    }
                    if (techdiscovery > 1)
                    {
                        string _messagetypes = messagetype == null ? "1" : messagetype.ToDelimitedString();
                        helper _helper = new helper();
                        string smsText = "";
                        string emailText = "";
                        bool sms = _messagetypes.Contains("2") ? true : false;
                        bool email = _messagetypes.Contains("1") ? true : false;
                        var template = new templateservices().get_template(Template);
                        smsText = sms ? template.sms : "";
                        emailText = email ? template.email : "";
                        string domain = new templateservices().getdomainname(workspaceid); ;
                        List<int> _dict = GetOfferByBatchid(Batchid);
                        BackgroundController _bc = new BackgroundController();
                        BackgroundJob.Enqueue(() => _bc.SendOfferToSimilarCityState(_dict, Template, Distribution, sms, email, _messagetypes, smsText, emailText, workspaceid, domain, Batchid));
                        foreach (var item in _dict)
                        {
                            BackgroundJob.Enqueue(() => _bc.SubmitOfferWithDistanceMatrix(item, workspaceid));
                        }
                    }
                    TempData["error"] = dataTable.Rows.Count + " offers imported succesfully";
                }
                catch (Exception ex)
                {
                    TempData["error"] = "Error : " + ex.Message;
                }
                return RedirectToAction("index", "offer", new { type = type });
                // return Redirect("http://localhost:50912/offer/index?type="+ type);
            }
            else
            {
                TempData["error"] = "Error : Please upload a valid document with the extenion .csv";
                // return Redirect("http://localhost:50912/offer/index?type=" + type);
                return RedirectToAction("index", "offer", new { type = type });
            }
        }
        public DataTable GetjoboffersForBulkInsert(DataTable gv, int createdby, int client, int projectmanager, string clientRate, int uploadProject_manager1, int workspaceid, int upload_dispatcher = 0, int uplodproject = 0)
        {
            DataTable dataTable = new DataTable("DummyTable");
            DataRow row = null;
            dataTable.Columns.Add("Id", typeof(int));
            dataTable.Columns.Add("JobId", typeof(string));
            dataTable.Columns.Add("Ttile", typeof(string));
            dataTable.Columns.Add("Client", typeof(int));
            dataTable.Columns.Add("Technician", typeof(int));
            dataTable.Columns.Add("Dispatcher", typeof(int));
            dataTable.Columns.Add("Project_manager", typeof(int));
            dataTable.Columns.Add("startdate", typeof(DateTime));
            dataTable.Columns.Add("enddate", typeof(DateTime));
            dataTable.Columns.Add("Client_rate", typeof(string));
            dataTable.Columns.Add("Pay_rate", typeof(string));
            dataTable.Columns.Add("hours", typeof(double));
            dataTable.Columns.Add("est_Hours", typeof(double));
            dataTable.Columns.Add("Status_id", typeof(int));
            dataTable.Columns.Add("CreatedBy", typeof(int));
            dataTable.Columns.Add("CreatedDate", typeof(DateTime));
            dataTable.Columns.Add("City_id", typeof(int));
            dataTable.Columns.Add("State_id", typeof(int));
            dataTable.Columns.Add("Description", typeof(string));
            dataTable.Columns.Add("Expnese", typeof(string));
            dataTable.Columns.Add("street", typeof(string));
            dataTable.Columns.Add("zipcode", typeof(string));
            dataTable.Columns.Add("isview", typeof(bool));
            dataTable.Columns.Add("isaccepted", typeof(bool));
            dataTable.Columns.Add("projectmangerid2", typeof(int));
            dataTable.Columns.Add("workspaceid", typeof(int));
            dataTable.Columns.Add("projectid", typeof(int));
            dataTable.Columns.Add("contact", typeof(string));
            dataTable.Columns.Add("phone", typeof(string));
            dataTable.Columns.Add("instructions", typeof(string));
            dataTable.Columns.Add("MapValidateAddress", typeof(string));
            try
            {

                int city = 0;
                int state = 0;
                string MapValidateAddress = "";
                for (int i = 0; i <= gv.Rows.Count - 1; i++)
                {
                    MapValidateAddress = gv.Rows[i][6].ToString() + ", " + gv.Rows[i][7].ToString() + ", " + (!string.IsNullOrEmpty(gv.Rows[i][8].ToString()) ? gv.Rows[i][8].ToString().ToUpper() : gv.Rows[i][8].ToString()) + " " + gv.Rows[i][9].ToString();
                    state = getstateidbystatename(gv.Rows[i][8].ToString());
                    city = getcityidbystate(state, gv.Rows[i][7].ToString());
                    string _sdate = gv.Rows[i][2].ToString() + " " + gv.Rows[i][3].ToString();//date starttime
                    string _edate = gv.Rows[i][2].ToString() + " " + gv.Rows[i][4].ToString();//date endttime
                    TimeSpan span = (Convert.ToDateTime(_edate) - Convert.ToDateTime(_sdate));
                    var estimhour = String.Format("{0}.{1}", span.Hours, span.Minutes > 0 ? (int)Math.Round(Convert.ToDouble(span.Minutes) * 1.66) : span.Minutes);
                    row = dataTable.NewRow();
                    row[0] = 0;
                    row[1] = gv.Rows[i][0].ToString();//jobid
                    row[2] = gv.Rows[i][1].ToString();//title
                    row[3] = client;
                    row[4] = 0;
                    row[5] = upload_dispatcher == 0 ? getmemberidbymembername(gv.Rows[i][11].ToString()) : upload_dispatcher;
                    row[6] = projectmanager;
                    row[7] = _sdate;
                    row[8] = _edate;
                    row[9] = clientRate;
                    row[10] = gv.Rows[i][10].ToString().Currency();
                    row[11] = estimhour;
                    row[12] = estimhour;
                    row[13] = 1;
                    row[14] = createdby;
                    row[15] = DateTime.Now;
                    row[16] = city;
                    row[17] = state;
                    row[18] = gv.Rows[i][5].ToString();///description
                    row[19] = "";
                    row[20] = gv.Rows[i][6].ToString();///address
                    row[21] = gv.Rows[i][9].ToString();///zipcode
                    row[22] = false;
                    row[23] = false;
                    row[24] = uploadProject_manager1;
                    row[25] = workspaceid;
                    row[26] = uplodproject;
                    row[27] = gv.Rows[i][12].ToString();
                    row[28] = gv.Rows[i][13].ToString();
                    row[29] = gv.Rows[i][14].ToString();
                    row[30] = MapValidateAddress;
                    dataTable.Rows.Add(row);
                }
            }
            catch (Exception)
            {
            }
            return dataTable;
        }


        [HttpPost]
        public ActionResult job(HttpPostedFileBase file, int type = 1, int uploadProject_manager = 0, int uploadProject_manager1 = 0, int uplodclient = 0, string upclientRate = "$0.00", int uprojectId = 0, int uploadProject_dispatcher = 0)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string ext = Path.GetExtension(file.FileName);
            if (ext.ToLower() == ".csv")
            {
                try
                {
                    Dictionary<int, string> _defaultvalues = new Dictionary<int, string>();
                    string newfilename = "job_" + DateTime.Now.Ticks.ToString() + file.FileName;
                    string filepath = Path.Combine(Server.MapPath("~/csvfiles/"), newfilename);
                    file.SaveAs(filepath);
                    DataTable dataTable = _helper.ConvertToDataTable(Server.MapPath("~/csvfiles/" + newfilename), 15, true);
                    dataTable = GetjobsForBulkInsert(dataTable, userid, uplodclient, uploadProject_manager, upclientRate, uploadProject_manager1, workspaceid, uprojectId, uploadProject_dispatcher);
                    var objBulk = new BulkUploadToSql()
                    {
                        InternalStore_dt = dataTable,
                        TableName = "tbl_job_temp",
                        CommitBatchSize = 200,
                        ConnectionString = conctionstring
                    };
                    objBulk.Commit_datatable("tbl_job_insert_from_temp", workspaceid);
                    TempData["error"] = dataTable.Rows.Count + " jobs imported succesfully";
                }
                catch (Exception ex)
                {
                    TempData["error"] = "Error : " + ex.Message;
                }
                return RedirectToAction("index", "jobs", new { type = type });
                // return Redirect("http://localhost:50912/jobs/index?type=" + type);
            }
            else
            {
                TempData["error"] = "Error : Please upload a valid Document with the extenion .csv";
                return RedirectToAction("index", "jobs", new { type = type });
                //  return Redirect("http://localhost:50912/jobs/index?type=" + type);
            }
        }

        public DataTable GetjobsForBulkInsert(DataTable gv, int createdby, int client, int projectmanager, string clientRate, int uploadProject_manager1, int workspaceid, int uprojectId, int uploadProject_dispatcher)
        {
            DataTable dataTable = new DataTable("DummyTable");
            DataRow row = null;
            dataTable.Columns.Add("Id", typeof(int));
            dataTable.Columns.Add("JobId", typeof(string));
            dataTable.Columns.Add("Ttile", typeof(string));
            dataTable.Columns.Add("Client", typeof(int));
            dataTable.Columns.Add("Technician", typeof(int));
            dataTable.Columns.Add("Dispatcher", typeof(int));
            dataTable.Columns.Add("Project_manager", typeof(int));
            dataTable.Columns.Add("startdate", typeof(DateTime));
            dataTable.Columns.Add("enddate", typeof(DateTime));
            dataTable.Columns.Add("Client_rate", typeof(string));
            dataTable.Columns.Add("Pay_rate", typeof(string));
            dataTable.Columns.Add("hours", typeof(double));
            dataTable.Columns.Add("est_Hours", typeof(double));
            dataTable.Columns.Add("Status_id", typeof(int));
            dataTable.Columns.Add("CreatedBy", typeof(int));
            dataTable.Columns.Add("CreatedDate", typeof(DateTime));
            dataTable.Columns.Add("City_id", typeof(int));
            dataTable.Columns.Add("State_id", typeof(int));
            dataTable.Columns.Add("Description", typeof(string));
            dataTable.Columns.Add("Expnese", typeof(string));
            dataTable.Columns.Add("street", typeof(string));
            dataTable.Columns.Add("zipcode", typeof(string));
            dataTable.Columns.Add("projectmangerid2", typeof(int));
            dataTable.Columns.Add("workspaceid", typeof(int));
            dataTable.Columns.Add("contact", typeof(string));
            dataTable.Columns.Add("phone", typeof(string));
            dataTable.Columns.Add("instructions", typeof(string));
            dataTable.Columns.Add("ProjectId", typeof(int));
            dataTable.Columns.Add("MapValidateAddress", typeof(string));
            try
            {
                int city = 0;
                int state = 0;
                string MapValidateAddress = string.Empty;
                for (int i = 0; i <= gv.Rows.Count - 1; i++)
                {
                    MapValidateAddress = gv.Rows[i][6].ToString() + ", " + gv.Rows[i][7].ToString() + ", " + (!string.IsNullOrEmpty(gv.Rows[i][8].ToString()) ? gv.Rows[i][8].ToString().ToUpper() : gv.Rows[i][8].ToString()) + " " + gv.Rows[i][9].ToString();
                    state = getstateidbystatename(gv.Rows[i][8].ToString());
                    city = getcityidbystate(state, gv.Rows[i][7].ToString());
                    string _sdate = gv.Rows[i][2].ToString() + " " + gv.Rows[i][3].ToString();//date starttime
                    string _edate = gv.Rows[i][2].ToString() + " " + gv.Rows[i][4].ToString();//date endttime
                    TimeSpan span = (Convert.ToDateTime(_edate) - Convert.ToDateTime(_sdate));
                    var estimhour = String.Format("{0}.{1}", span.Hours, span.Minutes > 0 ? (int)Math.Round(Convert.ToDouble(span.Minutes) * 1.66) : span.Minutes);
                    row = dataTable.NewRow();
                    row[0] = 0;
                    row[1] = gv.Rows[i][0].ToString();//jobid
                    row[2] = gv.Rows[i][1].ToString();//title
                    row[3] = client;
                    row[4] = 0;
                    row[5] = uploadProject_dispatcher == 0 ? getmemberidbymembername(gv.Rows[i][11].ToString()) : uploadProject_dispatcher;
                    row[6] = projectmanager;
                    row[7] = _sdate;
                    row[8] = _edate;
                    row[9] = clientRate;
                    row[10] = gv.Rows[i][10].ToString().Currency();
                    row[11] = estimhour;
                    row[12] = estimhour;
                    row[13] = 1;
                    row[14] = createdby;
                    row[15] = DateTime.Now;
                    row[16] = city;
                    row[17] = state;
                    row[18] = gv.Rows[i][5].ToString();///description
                    row[19] = "";
                    row[20] = gv.Rows[i][6].ToString();///address
                    row[21] = gv.Rows[i][9].ToString();///zipcode
                    row[22] = uploadProject_manager1;
                    row[23] = workspaceid;
                    row[24] = gv.Rows[i][12].ToString();///,Contact name
                    row[25] = gv.Rows[i][13].ToString();///phone
                    row[26] = gv.Rows[i][14].ToString();///Instructions
                    row[27] = uprojectId;
                    row[28] = MapValidateAddress;
                    dataTable.Rows.Add(row);
                }
            }
            catch (Exception)
            {
            }
            return dataTable;
        }


        [HttpPost]
        public ActionResult users(HttpPostedFileBase file)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string ext = Path.GetExtension(file.FileName);
            if (ext.ToLower() == ".csv")
            {
                try
                {
                    Dictionary<int, string> _defaultvalues = new Dictionary<int, string>();
                    string newfilename = "mbr_" + DateTime.Now.Ticks.ToString() + file.FileName;
                    string filepath = Path.Combine(Server.MapPath("~/csvfiles/"), newfilename);
                    file.SaveAs(filepath);
                    DataTable dataTable = _helper.ConvertToDataTable(Server.MapPath("~/csvfiles/" + newfilename), 13, true);
                    dataTable = GetmemberForBulkInsert(dataTable, userid, workspaceid);
                    var objBulk = new BulkUploadToSql()
                    {
                        InternalStore_dt = dataTable,
                        TableName = "tbl_member_temp",
                        CommitBatchSize = 200,
                        ConnectionString = conctionstring
                    };
                    objBulk.Commit_datatable("tbl_member_insert_from_temp", workspaceid);
                    TempData["error"] = dataTable.Rows.Count + " users imported succesfully";
                }
                catch (Exception ex)
                {
                    TempData["error"] = "Error : " + ex.Message;
                }
                //return Redirect("http://localhost:50912/users/index");
                return RedirectToAction("index", "users", new { v = 1234 });
            }
            else
            {
                TempData["error"] = "Error : Please upload a valid Document with the extenion .csv";
                // return Redirect("http://localhost:50912/users/index");
                return RedirectToAction("index", "users", new { v = 1234 });
            }
        }
        public DataTable GetmemberForBulkInsert(DataTable gv, int createdby, int workspaceid)
        {

            int userid = Convert.ToInt32( new UserService().GetLatestUserId(workspaceid)); 
            int cityid = 0;
            int stateid = 0;
            Crypto _cr = new Crypto();
            DataTable dataTable = new DataTable("DummyTable");
            DataRow row = null;
            dataTable.Columns.Add("Id", typeof(int));
            dataTable.Columns.Add("member_id", typeof(string));
            dataTable.Columns.Add("f_name", typeof(string));
            dataTable.Columns.Add("l_name", typeof(string));
            dataTable.Columns.Add("address", typeof(string));
            dataTable.Columns.Add("city_id", typeof(int));
            dataTable.Columns.Add("state_id", typeof(int));
            dataTable.Columns.Add("Zip_code", typeof(string));
            dataTable.Columns.Add("Phone", typeof(string));
            dataTable.Columns.Add("EMail", typeof(string));
            dataTable.Columns.Add("Alternative_phone", typeof(string));
            dataTable.Columns.Add("RateId", typeof(int));
            dataTable.Columns.Add("Background", typeof(string));
            dataTable.Columns.Add("drug_tested", typeof(string));
            dataTable.Columns.Add("dob", typeof(DateTime));
            dataTable.Columns.Add("Gender", typeof(string));
            dataTable.Columns.Add("RoleId", typeof(int));
            dataTable.Columns.Add("Department_Id", typeof(int));
            dataTable.Columns.Add("Designation_id", typeof(int));
            dataTable.Columns.Add("ManagerId", typeof(int));
            dataTable.Columns.Add("Username", typeof(string));
            dataTable.Columns.Add("Password", typeof(string));
            dataTable.Columns.Add("Tax", typeof(string));
            dataTable.Columns.Add("member_status_id", typeof(int));
            dataTable.Columns.Add("CreatedDate", typeof(DateTime));
            dataTable.Columns.Add("CreatedBy", typeof(int));
            dataTable.Columns.Add("Converted_lead", typeof(bool));
            dataTable.Columns.Add("issuper_Admin", typeof(bool));
            dataTable.Columns.Add("Imageurl", typeof(string));
            dataTable.Columns.Add("Skills", typeof(string));
            dataTable.Columns.Add("resetneeded", typeof(bool));
            dataTable.Columns.Add("MemberType", typeof(string));
            dataTable.Columns.Add("workspaceId", typeof(int));
            dataTable.Columns.Add("isshow", typeof(string));
            dataTable.Columns.Add("SuiteNumber", typeof(string));
            dataTable.Columns.Add("MapValidateAddress", typeof(string)); 
            try
            {
                string MapValidateAddress = string.Empty;
                var password = string.Empty;
                for (int i = 0; i <= gv.Rows.Count - 1; i++) 
                {password = (!string.IsNullOrEmpty(gv.Rows[i][2].ToString()) ? gv.Rows[i][2].ToString().ToUpper() : new helper().Get9RandomDigit()) ;
                    MapValidateAddress = gv.Rows[i][9].ToString() + ", " + gv.Rows[i][10].ToString() + ", " + (!string.IsNullOrEmpty(gv.Rows[i][11].ToString()) ? gv.Rows[i][11].ToString().ToUpper() : gv.Rows[i][11].ToString()) + " " + gv.Rows[i][12].ToString();
                    userid += 1;
                   
                    stateid = getstateidbystatename(gv.Rows[i][11].ToString());
                    cityid = getcityidbystate(stateid, gv.Rows[i][10].ToString());
                    row = dataTable.NewRow();
                    row[0] = 0;
                    row[1] = userid;//memberid
                    row[2] = gv.Rows[i][0].ToString().ToTitleCase();//fname
                    row[3] = gv.Rows[i][1].ToString().ToTitleCase();//lname
                    row[4] = gv.Rows[i][9].ToString();//address
                    row[5] = cityid;//city
                    row[6] = stateid;//state
                    row[7] = gv.Rows[i][12].ToString();//zip
                    row[8] = gv.Rows[i][4].ToString().PhoneNumber();//phone
                    row[9] = gv.Rows[i][3].ToString();//email
                    row[10] = "";
                    row[11] = getrateidbyrate(gv.Rows[i][5].ToString().Currency());//rate
                    row[12] = "";
                    row[13] = "";
                    row[14] = DateTime.Now;
                    row[15] = "";
                    row[16] = new UserService().GetTechRoleIdByWorkSpaceId(workspaceid);
                    row[17] = 0;
                    row[18] = 0;
                    row[19] = 0;//gv.Rows[i][6].ToString()manager
                    row[20] = gv.Rows[i][3].ToString();//email
                    row[21] = _cr.EncryptStringAES(password);//password
                    row[22] = gv.Rows[i][5].ToString();
                    row[23] = 1;
                    row[24] = DateTime.Now;
                    row[25] = createdby;
                    row[26] = false;
                    row[27] = false;
                    row[28] = "default";
                    row[29] = "";
                    row[30] = false;
                    row[31] = "T";
                    row[32] = workspaceid;
                    row[33] = "Y";
                    row[34] = gv.Rows[i][8].ToString();
                    row[35] = MapValidateAddress;
                    dataTable.Rows.Add(row);
                }
            }
            catch (Exception)
            {
            }
            return dataTable;
        }

        public string GetUserType(string typename)
        {
            string membertype = "M";
            switch (typename.ToLower())
            {
                case "employee":
                    membertype = "E";
                    break;
                case "dispatcher":
                    membertype = "D";
                    break;
                case "project manager":
                    membertype = "PM";
                    break;
                case "technician":
                    membertype = "T";
                    break;
                case "subcontractor":
                    membertype = "S";
                    break;
                case "subtechnician":
                    membertype = "ST";
                    break;
                case "lead technician":
                    membertype = "LT";
                    break;
            }
            return membertype;
        }
        public int getmemberMainidBymemberid(string memberid)
        {
            int m = 0;


            return m;
        }
        public DataTable GetcmemberForBulkInsert(DataTable gv, int createdby, int companyid, ref List<string> lst, int workspaceid)
        {
            Crypto _cr = new Crypto();
            DataTable dataTable = new DataTable("DummyTable");
            DataRow row = null;
            dataTable.Columns.Add("Id", typeof(int));
            dataTable.Columns.Add("member_id", typeof(string));
            dataTable.Columns.Add("f_name", typeof(string));
            dataTable.Columns.Add("l_name", typeof(string));
            dataTable.Columns.Add("address", typeof(string));
            dataTable.Columns.Add("city_id", typeof(int));
            dataTable.Columns.Add("state_id", typeof(int));
            dataTable.Columns.Add("Zip_code", typeof(string));
            dataTable.Columns.Add("Phone", typeof(string));
            dataTable.Columns.Add("EMail", typeof(string));
            dataTable.Columns.Add("Alternative_phone", typeof(string));
            dataTable.Columns.Add("RateId", typeof(int));
            dataTable.Columns.Add("Background", typeof(string));
            dataTable.Columns.Add("drug_tested", typeof(string));
            dataTable.Columns.Add("dob", typeof(DateTime));
            dataTable.Columns.Add("Gender", typeof(string));
            dataTable.Columns.Add("RoleId", typeof(int));
            dataTable.Columns.Add("Department_Id", typeof(int));
            dataTable.Columns.Add("Designation_id", typeof(int));
            dataTable.Columns.Add("ManagerId", typeof(int));
            dataTable.Columns.Add("Username", typeof(string));
            dataTable.Columns.Add("Password", typeof(string));
            dataTable.Columns.Add("Tax", typeof(string));
            dataTable.Columns.Add("member_status_id", typeof(int));
            dataTable.Columns.Add("CreatedDate", typeof(DateTime));
            dataTable.Columns.Add("CreatedBy", typeof(int));
            dataTable.Columns.Add("Converted_lead", typeof(bool));
            dataTable.Columns.Add("issuper_Admin", typeof(bool));
            dataTable.Columns.Add("Imageurl", typeof(string));
            dataTable.Columns.Add("Skills", typeof(string));
            dataTable.Columns.Add("resetneeded", typeof(bool));
            dataTable.Columns.Add("Membertype", typeof(string));
            dataTable.Columns.Add("Traveldistance", typeof(string));
            dataTable.Columns.Add("profilecomplete", typeof(bool));
            dataTable.Columns.Add("profilestep", typeof(bool));
            dataTable.Columns.Add("citizencheck", typeof(bool));
            dataTable.Columns.Add("authrizework", typeof(bool));
            dataTable.Columns.Add("company", typeof(string));
            dataTable.Columns.Add("isprofilecompleted", typeof(bool));
            dataTable.Columns.Add("ismailcompleted", typeof(bool));
            dataTable.Columns.Add("citizen", typeof(bool));
            dataTable.Columns.Add("age", typeof(bool));
            dataTable.Columns.Add("auth", typeof(bool));
            dataTable.Columns.Add("fealony", typeof(bool));
            dataTable.Columns.Add("isrtwcomplete", typeof(bool));
            dataTable.Columns.Add("isw9complete", typeof(bool));
            dataTable.Columns.Add("isbankcomplete", typeof(bool));
            dataTable.Columns.Add("explainfelony", typeof(string));
            dataTable.Columns.Add("completeddate", typeof(DateTime));
            dataTable.Columns.Add("acceptterms", typeof(bool));
            dataTable.Columns.Add("accepttermsdate", typeof(DateTime));
            dataTable.Columns.Add("accepttermsrqud", typeof(bool));
            dataTable.Columns.Add("declineterms", typeof(bool));
            dataTable.Columns.Add("companyid", typeof(int));
            dataTable.Columns.Add("position", typeof(string));
            dataTable.Columns.Add("havecompany", typeof(bool));
            dataTable.Columns.Add("workspaceid", typeof(int));

            string memberid = "";
            string companyname = "";
            try
            {
                for (int i = 0; i <= gv.Rows.Count - 1; i++)
                {
                    memberid = new helper().Get5RandomDigit();
                    lst.Add(memberid);
                    row = dataTable.NewRow();
                    row[0] = 0;//id
                    row[1] = memberid;//member id
                    row[2] = gv.Rows[i][0].ToString().ToTitleCase();//first name
                    row[3] = gv.Rows[i][1].ToString().ToTitleCase();//last name
                    row[4] = gv.Rows[i][6].ToString();// address
                    row[5] = 0;//city
                    row[6] = 0;//state
                    row[7] = gv.Rows[i][7].ToString();//zip
                    row[8] = gv.Rows[i][4].ToString().PhoneNumber();//phone
                    row[9] = gv.Rows[i][3].ToString();//email
                    row[10] = new ContractorServices().getalterntivephone(companyid, ref companyname); //alternative
                    row[11] = 0;//rate
                    row[12] = "";//background
                    row[13] = "";//drug tested
                    row[14] = DateTime.Now;//dob
                    row[15] = "";//gender
                    row[16] = new UserService().GetTechRoleIdByWorkSpaceId(workspaceid);//roleid
                    row[17] = 0;//deptid
                    row[18] = 0;//desid
                    row[19] = new UserService().GetUserTableRowId(gv.Rows[i][5].ToString());//5/managerid
                    row[20] = gv.Rows[i][3].ToString();//username
                    row[21] = _cr.EncryptStringAES(gv.Rows[i][4].ToString());//password
                    row[22] = "";//tax
                    row[23] = 1;//member status id
                    row[24] = DateTime.Now;//created date
                    row[25] = createdby;//createdby
                    row[26] = false;//converted lead
                    row[27] = false;//issuper_Admin
                    row[28] = "default";//image url
                    row[29] = "";//skills
                    row[30] = true;//resetneeded
                    row[31] = GetUserType(gv.Rows[i][8].ToString());//mmebertype
                    row[32] = "";//travel distnace
                    row[33] = false;//profilecomplete
                    row[34] = false;//profilestep
                    row[35] = false;//citizencheck
                    row[36] = false;//authrizework
                    row[37] = companyname;//company
                    row[38] = false;//isprofilecompleted
                    row[39] = false;//ismailcompleted
                    row[40] = false;//citizen
                    row[41] = false;//age
                    row[42] = false;//auth
                    row[43] = false;//fealony
                    row[44] = false;//isrtwcomplete
                    row[45] = false;//isw9complete
                    row[46] = false;//isbankcomplete
                    row[47] = "";//explainfelony
                    row[48] = DateTime.Now;//completeddate
                    row[49] = false;//acceptterms
                    row[50] = DateTime.Now;//accepttermsdate
                    row[51] = true;//accepttermsrqud
                    row[52] = false;//declineterms
                    row[53] = companyid;//companyid
                    row[54] = gv.Rows[i][2].ToString();//position
                    row[55] = false;//havecompany
                    row[56] = workspaceid;//workspaceid
                    dataTable.Rows.Add(row);
                }
            }
            catch (Exception)
            {
            }
            return dataTable;
        }


        [HttpPost]
        public ActionResult city(HttpPostedFileBase file, int stateid)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            string ext = Path.GetExtension(file.FileName);
            if (ext.ToLower() == ".csv")
            {
                Dictionary<int, string> _defaultvalues = new Dictionary<int, string>();
                string newfilename = "cl_" + DateTime.Now.Ticks.ToString() + file.FileName;
                string filepath = Path.Combine(Server.MapPath("~/csvfiles/"), newfilename);
                file.SaveAs(filepath);
                DataTable dataTable = _helper.ConvertToDataTable(Server.MapPath("~/csvfiles/" + newfilename), 1, false);
                dataTable = GetcityForBulkInsert(dataTable, stateid);
                var objBulk = new BulkUploadToSql()
                {
                    InternalStore_dt = dataTable,
                    TableName = "tbl_temp_city",
                    CommitBatchSize = 200,
                    ConnectionString = conctionstring
                };
                objBulk.Commit_datatable();
                TempData["error"] = dataTable.Rows.Count + " Clients Imported succesfully";
                return RedirectToAction("city", "test", new { v = 1234 });
            }
            else
            {
                TempData["error"] = "Error : Please upload a valid Document with the extenion .csv";
                return RedirectToAction("city", "test", new { v = 1234 });
            }
        }
        public DataTable GetcityForBulkInsert(DataTable gv, int stateid)
        {
            DataTable dataTable = new DataTable("DummyTable");
            DataRow row = null;
            dataTable.Columns.Add("Id", typeof(int));
            dataTable.Columns.Add("state_Id", typeof(int));
            dataTable.Columns.Add("name", typeof(string));
            dataTable.Columns.Add("Isactive", typeof(bool));
            try
            {
                for (int i = 0; i <= gv.Rows.Count - 1; i++)
                {
                    row = dataTable.NewRow();
                    row[0] = 0;
                    row[1] = stateid;
                    row[2] = gv.Rows[i][0].ToString();
                    row[3] = true;
                    dataTable.Rows.Add(row);
                }
            }
            catch (Exception)
            {
            }
            return dataTable;
        }

    }

}