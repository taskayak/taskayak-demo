﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrm.Controllers
{
    public class selectController : Controller
    {
        [HttpGet]
        public ActionResult get_init(string searchTerm)
        {
            selectservices _ser = new selectservices();
            int item = 0;
            List<Select2Model> select2Models = new List<Select2Model>();
            Select2Model _item = new Select2Model();
            _item.id = 1;
            _item.name = "----Select " + searchTerm+"----";
            select2Models.Add(_item);
            Select2PagedResult select2Format = this.ConvertToSelect2Format(select2Models, item);
            return new JsonpResult()
            {
                Data = select2Format,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public ActionResult get_All_active_state(string searchTerm, int pageSize, int? pageNum, int? sid)
        {
            selectservices _ser = new selectservices();
            int startindex = 1;
            int endindex = pageSize;
            if (pageNum.HasValue)
            {
                startindex = Convert.ToInt32(pageNum);
                startindex--;
                startindex *= pageSize;
            }
            endindex = startindex + pageSize - 1;
            int item = 0;
            List<Select2Model> select2Models = _ser.get_all_satets(startindex, endindex, searchTerm, sid);
            if (select2Models.Count > 0)
            {
                item = select2Models[0].total;
            }
            Select2PagedResult select2Format = this.ConvertToSelect2Format(select2Models, item);
            return new JsonpResult()
            {
                Data = select2Format,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public ActionResult get_All_city_by_stateId(string searchTerm, int pageSize, int? pageNum, int? sid)
        {
            selectservices _ser = new selectservices();
            int startindex = 1;
            int endindex = pageSize;
            if (pageNum.HasValue)
            {
                startindex = Convert.ToInt32(pageNum);
                startindex--;
                startindex *= pageSize;
            }
            endindex = startindex + pageSize - 1;
            int item = 0;
            List<Select2Model> select2Models = _ser.get_all_city_by_stateid(startindex, endindex, searchTerm, sid);
            if (select2Models.Count > 0)
            {
                item = select2Models[0].total;
            }
            Select2PagedResult select2Format = this.ConvertToSelect2Format(select2Models, item);
            return new JsonpResult()
            {
                Data = select2Format,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }



        [HttpGet]
        public ActionResult get_All_city_by_cityId(string searchTerm, int pageSize, int? pageNum, int? sid)
        {
            selectservices _ser = new selectservices();
            int startindex = 1;
            int endindex = pageSize;
            if (pageNum.HasValue)
            {
                startindex = Convert.ToInt32(pageNum);
                startindex--;
                startindex *= pageSize;
            }
            endindex = startindex + pageSize - 1;
            int item = 0;
            List<Select2Model> select2Models = _ser.get_all_city_by_cityid(startindex, endindex, searchTerm, sid);
            if (select2Models.Count > 0)
            {
                item = select2Models[0].total;
            }
            Select2PagedResult select2Format = this.ConvertToSelect2Format(select2Models, item);
            return new JsonpResult()
            {
                Data = select2Format,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public ActionResult get_All_active_user(string searchTerm, int pageSize, int? pageNum, int? sid)
        {
            selectservices _ser = new selectservices();
            int startindex = 1;
            int endindex = pageSize;
            if (pageNum.HasValue)
            {
                startindex = Convert.ToInt32(pageNum);
                startindex--;
                startindex *= pageSize;
            }
            endindex = startindex + pageSize - 1;
            int item = 0;
            List<Select2Model> select2Models = _ser.get_all_users(startindex, endindex, searchTerm, sid);
            if (select2Models.Count > 0)
            {
                item = select2Models[0].total;
            }
            Select2PagedResult select2Format = this.ConvertToSelect2Format(select2Models, item);
            return new JsonpResult()
            {
                Data = select2Format,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public ActionResult get_All_active_jobs(string searchTerm, int pageSize, int? pageNum, int? sid)
        {
            selectservices _ser = new selectservices();
            int startindex = 1;
            int endindex = pageSize;
            if (pageNum.HasValue)
            {
                startindex = Convert.ToInt32(pageNum);
                startindex--;
                startindex *= pageSize;
            }
            endindex = startindex + pageSize - 1;
            int item = 0;
            List<Select2Model> select2Models = _ser.get_all_jobs(startindex, endindex, searchTerm, sid);
            if (select2Models.Count > 0)
            {
                item = select2Models[0].total;
            }
            Select2PagedResult select2Format = this.ConvertToSelect2Format(select2Models, item);
            return new JsonpResult()
            {
                Data = select2Format,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public ActionResult get_All_active_clients(string searchTerm, int pageSize, int? pageNum, int? sid)
        {
            selectservices _ser = new selectservices();
            int startindex = 1;
            int endindex = pageSize;
            if (pageNum.HasValue)
            {
                startindex = Convert.ToInt32(pageNum);
                startindex--;
                startindex *= pageSize;
            }
            endindex = startindex + pageSize - 1;
            int item = 0;
            List<Select2Model> select2Models = _ser.get_all_clients(startindex, endindex, searchTerm, sid);
            if (select2Models.Count > 0)
            {
                item = select2Models[0].total;
            }
            Select2PagedResult select2Format = this.ConvertToSelect2Format(select2Models, item);
            return new JsonpResult()
            {
                Data = select2Format,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public ActionResult get_All_active_prjs(string searchTerm, int pageSize, int? pageNum, int? sid)
        {
            selectservices _ser = new selectservices();
            int startindex = 1;
            int endindex = pageSize;
            if (pageNum.HasValue)
            {
                startindex = Convert.ToInt32(pageNum);
                startindex--;
                startindex *= pageSize;
            }
            endindex = startindex + pageSize - 1;
            int item = 0;
            List<Select2Model> select2Models = _ser.get_all_prjmgr(startindex, endindex, searchTerm, sid);
            if (select2Models.Count > 0)
            {
                item = select2Models[0].total;
            }
            Select2PagedResult select2Format = this.ConvertToSelect2Format(select2Models, item);
            return new JsonpResult()
            {
                Data = select2Format,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
















        //--------------------------Helper classes------------------------------------//
        private selectController.Select2PagedResult ConvertToSelect2Format(List<Select2Model> _list, int totalAttendees)
        {
            selectController.Select2PagedResult select2PagedResult = new Select2PagedResult()
            {
                Results = new List<selectController.Select2Result>()
            };
            foreach (Select2Model item in _list)
            {
                select2PagedResult.Results.Add(new selectController.Select2Result()
                {
                    id = item.id.ToString(),
                    text = item.name
                });
            }
            select2PagedResult.Total = totalAttendees;
            return select2PagedResult;
        }

        public class Select2PagedResult
        {
            public List<Select2Result> Results
            {
                get;
                set;
            }
            public int Total
            {
                get;
                set;
            }

        }

        public class Select2Result
        {
            public string id
            {
                get;
                set;
            }

            public string text
            {
                get;
                set;
            }
        }

    }
}