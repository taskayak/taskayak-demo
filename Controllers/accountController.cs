﻿using hrm.Database;
using hrm.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace hrm.Controllers
{
    public class accountController : Controller
    {
        public ActionResult login(string returnurl = "none")
        {
            LoginModel _model = new LoginModel();
            if (!Request.Url.Host.Contains("app.taskayak.com") && !Request.IsLocal)
            {
                return Redirect("https://app.taskayak.com/account/login?returnurl=" + returnurl);
            }
            CookieBuilder _builder = new CookieBuilder();
            Crypto _crypt = new Crypto();
            string _cookie = _builder.Get("_rCookie", Request.IsLocal);
            if (!string.IsNullOrEmpty(_cookie))
            {
                Dictionary<string, string> _rCookie = JsonConvert.DeserializeObject<Dictionary<string, string>>(_cookie);
                _model.Workspace = _crypt.DecryptStringAES(_rCookie["wsp"]);
                _model.UserName = _crypt.DecryptStringAES(_rCookie["usp"]);
                _model.Password = _crypt.DecryptStringAES(_rCookie["psp"]);
            }
            _model.ReturnUrl = returnurl;
            _model.RememberMe = false;
            if (TempData["error"] != null)
            {
                _model.error_text = TempData["error"].ToString();
            }
            return View(_model);
        }
        public ActionResult register(string returnurl = "none", int planid = 3)
        {
            if (!Request.Url.Host.Contains("app.taskayak.com") && !Request.IsLocal)
            {
                return Redirect("https://app.taskayak.com/account/register?returnurl=" + returnurl + "&PlanId=" + planid);
            }
            RegisterModel _model = new RegisterModel
            {
                PlanId = planid
            };
            if (planid == 3)
            {
                return Redirect("https://taskayak.com?RequestDemo=true"); 
                //return View("sendenquiry", _model);
            }
            else if (planid > 0)
            {
                return View("paid_register", _model);
            }
            return View(_model);
        }

        public ActionResult validate(string token = "")
        {
            helper _helpre = new helper();
            RegisterModel _model = new RegisterModel();
            string Message = string.Empty;
            if (string.IsNullOrEmpty(token))
            {
                Message = "Auth token can't validate, contact with admin@taskayak.com";
            }
            else
            {
                try
                {
                   
                    int _Id = Convert.ToInt32(new Crypto().DecryptStringAES(token));
                    var model = new UserService().GetEnquirymodel(_Id);
                    if (!model.IsValidate)
                    {
                        _helpre.SendEmailWithAWS("contact@convotechnology.com", model.EmailBody, model.ContactName.ToTitleCase() + " Account request", "Convo Technology");
                        var hsmodel = new HubSpotCOmpanyModel()
                        {
                            Name = model.CompanyName.ToTitleCase(),
                            Description = "Professional Account request from Taskayak",
                            City = "",
                            state = "",
                            Emailaddress = model.Email,
                            Phonenumber = model.PhoneNumber,
                            //message = model.Message,
                            //Primarycontact = model.ContactName.ToTitleCase(),
                            Website = model.website
                        };
                        new hubspot().CreateHubSpotCompany(hsmodel, model.ContactName);

                    }
                    Message = "Thanks for reaching out , Our Representatives will contact you soon";
                }
                catch (Exception)
                {
                    Message = "Auth token can't validate, contact with admin@taskayak.com";
                }
            }
            _model.PlanId = 3;
            _model = _helpre.GenerateError<RegisterModel>(_model, Message);
            return View("sendenquiry", _model);
        }



        [HttpPost]
        public ActionResult register_professional(RegisterModel model, int employees = 1, string message = "", string website = "")
        {
            string capctha = Request.Params["g-recaptcha-response"] != null ? Request.Params["g-recaptcha-response"].ToString() : string.Empty;
            helper _helpre = new helper();
            var error_text = "please validate recaptcha";
            if (!string.IsNullOrEmpty(capctha))
            {
                var res = statichelper.Validate(capctha) == "true";
                if (res)
                {
                    string body = "Compnay Name : " + model.CompanyName + " <br/> Email: " + model.Email + "<br/> Telephone :" + model.PhoneNumber + "<br/> Contact name :" + model.ContactName + "< br/> No of Employees :" + employees + "<br/> Message: " + message;
                    error_text = new UserService().AddEquiry(model.CompanyName, model.Email, model.PhoneNumber, model.ContactName, employees, message, body, website);
                    var m = error_text.Split('_');
                    error_text = m[0];
                    int SavedId = Convert.ToInt32(m[1]);
                    if (SavedId > 0)
                    {
                        string Token = new Crypto().EncryptStringAES(m[1]);
                        var _url = "https://app.taskayak.com/account/validate?token=" + Token;
                        body = "Dear " + model.ContactName + ",<br/><br/>Thank you for register with us.<br/><br/> Click below link to validate your account<br/><br/>" + _url;
                        _helpre.SendEmailWithAWS(model.Email, body, model.ContactName.ToTitleCase() + " Account request", model.ContactName, "Taskayak");
                    }

                }
            }
            model.PlanId = 3;
            model = _helpre.GenerateError<RegisterModel>(model, error_text);
            return View("sendenquiry", model);
        }

        [AuthorizeVerifiedloggedin]
        public ActionResult getProfessional()
        {
            RegisterModel _model = new RegisterModel();
            return View(_model);
        }
        [AuthorizeVerifiedloggedin]
        public ActionResult upgrade(int planid)
        {
            RegisterModel _model = new RegisterModel();
            _model.PlanId = planid;
            if (planid == 1)
            {
                return View("starter", _model);
            }
            return View("Advantage", _model);
        }
        [AuthorizeVerifiedloggedin]
        public ActionResult buy(int planid)
        {
            RegisterModel model = new RegisterModel();
            model.PlanId = planid;
            if (planid == 1)
            {
                ViewBag.total = 1000;
                ViewBag.price = "$25";
                return View("sms25", model);
            }
            else if (planid == 2)
            {
                ViewBag.total = 2000;
                ViewBag.price = "$35";
                return View("sms25", model);
            }
            else if (planid == 3)
            {
                ViewBag.total = 4000;
                ViewBag.price = "$50";
                return View("sms25", model);
            }
            else if (planid == 4)
            {
                ViewBag.total = 10000;
                ViewBag.price = "$100";
                return View("sms25", model);
            }
            else
            {
                ViewBag.total = 25000;
                ViewBag.price = "$200";
                return View("sms25", model);
            }
        }
        [HttpPost]
        public ActionResult buy(RegisterModel model, string stripeToken = "", int plantype = 1)
        {
            int UserId = Request.Cookies["_xid"] == null ? 0 : Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            int WorkspaceId = Request.Cookies["domain"] == null ? 0 : Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string CustomerId = "";
            string SubscriptionId = "";
            int SMSCount = 100;
            int EmailCount = 100;
            string AmountPaying = "$25/monthly";
            Stripe_Payment Stripe = new Stripe_Payment();
            UserService _UserService = new UserService();
            var member = _UserService.GetAllActiveMembersById(UserId);
            model.Email = member.EmailAddress;
            model.FirstName = member.FirstName;
            model.PhoneNumber = member.PhoneNumber;
            switch (model.PlanId)
            {
                case 1:
                    Stripe.SMS25(stripeToken, "SMS/Email Package", model.Email, model.FirstName, model.PhoneNumber, ref CustomerId, ref SubscriptionId);
                    SMSCount = 1000;
                    EmailCount = 1000;
                    AmountPaying = "$25/monthly";
                    break;
                case 2:
                    Stripe.SMS35(stripeToken, "SMS/Email Package", model.Email, model.FirstName, model.PhoneNumber, ref CustomerId, ref SubscriptionId);
                    SMSCount = 2000;
                    EmailCount = 2000;
                    AmountPaying = "$35/monthly";
                    break;
                case 3:
                    Stripe.SMS50(stripeToken, "SMS/Email Package", model.Email, model.FirstName, model.PhoneNumber, ref CustomerId, ref SubscriptionId);
                    SMSCount = 4000;
                    EmailCount = 4000;
                    AmountPaying = "$50/monthly";
                    break;
                case 4:
                    Stripe.SMS100(stripeToken, "SMS/Email Package", model.Email, model.FirstName, model.PhoneNumber, ref CustomerId, ref SubscriptionId);
                    SMSCount = 10000;
                    EmailCount = 10000;
                    AmountPaying = "$100/monthly";
                    break;
                case 5:
                    Stripe.SMS200(stripeToken, "SMS/Email Package", model.Email, model.FirstName, model.PhoneNumber, ref CustomerId, ref SubscriptionId);
                    SMSCount = 25000;
                    EmailCount = 25000;
                    AmountPaying = "$200/monthly";
                    break;
            }

            accountservices _accs = new accountservices();
            helper _helpre = new helper();
            _accs.Updatepacjkage(WorkspaceId, SMSCount, EmailCount, SubscriptionId, CustomerId, AmountPaying);
            _accs.Updatepacjkage(WorkspaceId, SMSCount, EmailCount, SubscriptionId, CustomerId, AmountPaying);
            TempData["error"] = "SMS/Email Package Added";
            return RedirectToAction("billing");
        }
        [HttpPost]
        public ActionResult upgrade(RegisterModel _model, string stripeToken = "", int plantype = 1)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            string customerid = "";
            string subscriptionid = "";
            Stripe_Payment _payment = new Stripe_Payment();
            UserService _m = new UserService();
            var member = _m.GetAllActiveMembersById(userid);
            _model.Email = member.EmailAddress;
            _model.FirstName = member.FirstName; _model.PhoneNumber = member.PhoneNumber;
            switch (_model.PlanId)
            {
                case 1:
                    if (plantype == 2)
                    {
                        _payment.StarterWithMonthly(stripeToken, "Starter $75/mo ", _model.Email, _model.FirstName, _model.PhoneNumber, ref customerid, ref subscriptionid);
                    }
                    else
                    {
                        _payment.StarterWithYearly(stripeToken, "Starter $65/mo ", _model.Email, _model.FirstName, _model.PhoneNumber, ref customerid, ref subscriptionid);

                    }
                    break;
                case 2:
                    if (plantype == 2)
                    {
                        _payment.AdvantageWithMonthly(stripeToken, "Advantage $195/mo ", _model.Email, _model.FirstName, _model.PhoneNumber, ref customerid, ref subscriptionid);
                    }
                    else
                    {
                        _payment.AdvantageWithYearly(stripeToken, "Advantage $175/mo ", _model.Email, _model.FirstName, _model.PhoneNumber, ref customerid, ref subscriptionid);

                    }
                    break;
            }
            int smscount = 100;
            int emailcount = 100;
            string service = "";
            switch (_model.PlanId)
            {
                case 1:
                    service = "Taskayak Starter";
                    smscount = 2500;
                    emailcount = 10000;
                    break;
                case 2:
                    service = "Taskayak Advantage";
                    smscount = 10000;
                    emailcount = 40000;
                    break;
                case 3:
                    service = "Taskayak Professional";
                    smscount = 50000;
                    emailcount = 50000;
                    break;
            }
            accountservices _accs = new accountservices();
            helper _helpre = new helper();
            _accs.UpdateBillingwithupgrade(service, workspaceid, emailcount, smscount, customerid, subscriptionid, _model.PlanId, userid);
            var message = "Account upgrade succesfully ,please logout and login again to apply new settings";
            _model = _helpre.GenerateError<RegisterModel>(_model, message);
            return View("pricing", _model);
        }
        [HttpPost]
        public ActionResult getProfessional(RegisterModel _model, int employees = 1, string message = "", string website = "")
        {
            helper _helpre = new helper();
            string body = "Compnay Name : " + _model.CompanyName + " <br/> Email: " + _model.Email + "<br/> Telephone :" + _model.PhoneNumber + "<br/> Contact name :" + _model.ContactName + "< br/> No of Employees :" + employees + "<br/> Message: " + message;
            _helpre.SendEmailWithAWS("contact@convotechnology.com", body, _model.FirstName.ToTitleCase() + " Professional Account request", "Convo Technology");
            var hsmodel = new HubSpotCOmpanyModel()
            {
                Name = _model.CompanyName.ToTitleCase(),
                Description = "Professional Account request from Taskayak",
                City = "",
                state = "",
                Emailaddress = _model.Email,
                Phonenumber = _model.PhoneNumber,
                //message = message,
                //Primarycontact = _model.ContactName.ToTitleCase(),
                Website = website
            };
            var HubSpotId = new hubspot().CreateHubSpotCompany(hsmodel, _model.ContactName.ToTitleCase());

            _model.PlanId = 3;
            var error_text = "Congratulations! Your query submitted succesfully, Our Representatives will contact you soon";
            _model = _helpre.GenerateError<RegisterModel>(_model, error_text);
            return View("getProfessional", _model);
        }
        public ActionResult forgot_password()
        {
            LoginModel _model = new LoginModel();
            return View(_model);
        }
        [HttpPost]
        public ActionResult forgot_password(LoginModel _model)
        {
            accountservices _accs = new accountservices();
            helper _helpre = new helper();
            var crypt = new Database.Crypto();
            var temppass = _helpre.GetRandomAlphaNumeric(8);
            string name = "";
            string email = "";
            var e_pass = crypt.EncryptStringAES(temppass);
            string message = _accs.reset_password(_model.UserName, e_pass, ref name, ref email);
            if (!message.Contains("Error"))
            {
                ModelState.Clear();
                string body = getforgetPasswordEmail(temppass, name);
                _helpre.SendEmailWithAWS(email, body, "Password Reset Request", name, "Noreply");
            }
            _model = _helpre.GenerateError<LoginModel>(_model, message);
            return View(_model);
        }
        private string getforgetPasswordEmail(string password, string user)
        {
            string empty = string.Empty;
            string str = string.Empty;
            try
            {
                string str1 = "~/App_Data/forgetpassword.txt";
                str = System.IO.File.ReadAllText(Server.MapPath(str1));
                str = str.Replace("%user%", user).Replace("%passowrd%", password).Replace("%websiteLogoUrl%", "https://app.taskayak.com/account/login").Replace("%CompanyName%", "Convo").Replace("%websiteLogoUrl1%", "https://www.textingpro.com/images/logo.png").Replace("%welcomeimagepath%", "#");
            }
            catch (Exception)
            {
            }
            return str;
        }
        public ActionResult logout(string returnurl = "none")
        {
            helper _helper = new helper();
            LoginModel _model = new LoginModel();
            CookieBuilder _builder = new CookieBuilder();
            Crypto _crypt = new Crypto();
            string _cookie = _builder.Get("_rCookie", Request.IsLocal);
            if (!string.IsNullOrEmpty(_cookie))
            {
                Dictionary<string, string> _rCookie = JsonConvert.DeserializeObject<Dictionary<string, string>>(_cookie);
                _model.Workspace = _crypt.DecryptStringAES(_rCookie["wsp"]);
                _model.UserName = _crypt.DecryptStringAES(_rCookie["usp"]);
                _model.Password = _crypt.DecryptStringAES(_rCookie["psp"]);
            }
            _model.ReturnUrl = returnurl;
            _model.RememberMe = false;
            _helper.CreateCookie<int>(0, "_xid", -5);
            _helper.CreateCookie<int>(0, "_pid", -5);
            _helper.CreateCookie<int>(0, "domain", -5);
            //_helper.CreateCookie<int>(0, "_status", -48);
            _helper.CreateCookie<string>("", "_psd", -50);
            if (!Request.Url.Host.Contains("app.taskayak.com") && !Request.IsLocal)
            {
                return Redirect("https://app.taskayak.com/account/logout?returnurl=" + returnurl);
            }
            return View("login", _model);
        }
        public void inactiveworkspace()
        {
            helper _helper = new helper();
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            _helper.CreateCookie<int>(0, "_xid", -5);
            _helper.CreateCookie<int>(0, "_pid", -5);
            //_helper.CreateCookie<int>(0, "_status", -48);
            _helper.CreateCookie<int>(0, "domain", -5);
            accountservices _acse = new accountservices();
            string workspacename = _acse.deactivateaccount(workspaceid);
            _helper.SendEmailWithAWS("admin@taskayak.com", workspacename + " is deactivated by user", "Account deactivated", "Taskayak Admin");
        }
        public string getptrn(string ptrn)
        {
            Database.Crypto _crpt = new Database.Crypto();
            return _crpt.DecryptStringAES(ptrn);
        }
        [AuthorizeVerifiedloggedin]
        public ActionResult password()
        {
            ChangePassword _mdl = new ChangePassword();
            return View("passowrd", _mdl);
        }
        public List<DocumentTypeItems> get_all_doctypes()
        {
            var dept = new documentservices();
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            return dept.AllDocumentType(workspaceid);
        }
        [HttpPost]
        public ActionResult password_update(string curpass, string newpass)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            accountservices _accs = new accountservices();
            var message = _accs.validatepassword(curpass, newpass, userid);
            TempData["error"] = message;
            return RedirectToAction("edit", "account", new { tab = 1 });
        }
        [HttpPost]
        public ActionResult addcompany(ContractorModal_Item _item, int cuserid)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            ContractorModal _model = new ContractorModal();
            ContractorServices _services = new ContractorServices();
            int cityId = new defaultservices().savegetCityId(_item.state_id, _item.city);
            _item.city_id = cityId;
            var message = _services.add_new_companywithmember(_item, cuserid);
            var template = new templateservices().get_template(11);
            List<int> _data = new List<int>();
            _data.Add(userid);
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            new jobofferservices().sendoffers2(_data, true, true, template.sms, template.email, "New company welcome", false, workspaceid);
            TempData["error"] = message;
            return RedirectToAction("edit", "account", new { tab = 1 });
        }
        [AuthorizeVerifiedloggedin]
        public ActionResult edit(int tab = 1, int test = 0)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            int id = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            ViewBag.tab = tab;
            //ViewBag.isuncomplete = test > 0 ? false : Convert.ToBoolean(Request.Cookies["_status"].Value.ToString());
            UserService _m = new UserService();
            UserModel _mdl = _m.GetAllActiveMembersById(id);
            string mes = TempData["error"] != null? TempData["error"].ToString():string.Empty; 
            ViewBag.allowTools = _mdl.AccountType == "Taskayak free" ? false : true;
            var type = new UserService().GetUserType(id);
            ViewBag.istech = (type == "T");
           _mdl.DocumentList = _m.GetDocumentByUserid(id, workspaceid);
            _mdl.SkillList = get_all_skills();
          _mdl.ToolLists = get_all_tools();
            _mdl.DocumentTypeList = get_all_doctypes();
            _mdl = new helper().GenerateError<UserModel>(_mdl, mes);
            return View(_mdl);
        }
        public List<ToolItems> get_all_tools()
        {
            var dept = new skillservices();
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            return dept.get_all_active_tools(workspaceid)._tool;
        }
        [HttpPost]
        public ActionResult edit_image(HttpPostedFileBase file)
        {
            Random rnd = new Random(0);
            var rnditem = rnd.Next(0, 10);
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            UserService _m = new UserService();
            string path = Server.MapPath("~/image/picture-" + userid);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            ViewBag.tab = 4;
            string filenames = userid.ToString();
            var extension = Path.GetExtension(file.FileName);
            var imagepath = path + "/" + filenames + extension;
            string tempfilename = "~/image/picture-" + userid + "/" + filenames + extension + "?v=" + rnditem;
            if (System.IO.File.Exists(imagepath))
            {
                System.IO.File.Delete(imagepath);
            }
            file.SaveAs(imagepath);
            var mes = _m.AddUserProfileImage(tempfilename, userid);
            TempData["error"] = mes;
            return RedirectToAction("edit", "account", new { tab = 1 });
        }
        [HttpPost]
        public ActionResult add_bck(HttpPostedFileBase file)
        {
            Random rnd = new Random(0);
            var rnditem = rnd.Next(0, 10);
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            UserService _m = new UserService();
            string path = Server.MapPath("/pdffiles/bckcheck-" + rnditem.ToString() + userid);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filenames = userid.ToString();
            var extension = Path.GetExtension(file.FileName);
            var imagepath = path + "/" + filenames + extension;
            string tempfilename = "/pdffiles/bckcheck-" + rnditem.ToString() + userid + "/" + filenames + extension + "?v=" + rnditem;
            if (System.IO.File.Exists(imagepath))
            {
                System.IO.File.Delete(imagepath);
            }
            file.SaveAs(imagepath);
            TempData["error"] = "Background check doc uploded successfully";
            _m.AddUserDocument("Background check doc", userid, tempfilename, 2);
            return RedirectToAction("edit", "account", new { tab = 1 });
        }
        [HttpPost]
        public ActionResult edit(UserModel _mdl)
        {
            string street = ""; string city = ""; string state = ""; string zipcode = ""; int StateId = 0;
            ViewBag.tab = 1;
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            UserService _m = new UserService();
            _mdl.MapValidateAddress.GetAddress(ref street, ref city, ref state, ref zipcode);
            _mdl.CityId = new defaultservices().CreateCity(ref StateId, state, city);
            _mdl.StateId = StateId;
            _mdl.StateId = StateId;
            _mdl.Zipcode = zipcode;
            _mdl.Address = street;
            var mes = _m.UpdateProfile(_mdl, userid);
            TempData["error"] = mes;
            return RedirectToAction("edit", "account", new { tab = 1 });
        }
        [HttpPost]
        public ActionResult edit_skills(int[] skills)
        {
            ViewBag.tab = 1;
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            UserService _m = new UserService();
            var mes = _m.UpdateUserSkills(skills, userid);
            TempData["error"] = mes;
            return RedirectToAction("edit", "account", new { tab = 2 });
        }
        [HttpPost]
        public ActionResult edit_tools(int[] tools)
        {
            ViewBag.tab = 1;
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            UserService _m = new UserService();
            var mes = _m.UpdateTools(tools, userid);
            TempData["error"] = mes;
            return RedirectToAction("edit", "account", new { tab = 3 });
        }
        public List<SkillItems> get_all_skills()
        {
            var dept = new skillservices();
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            return dept.get_all_active_skills(workspaceid)._skill;
        }
        [HttpPost]
        public ActionResult password(ChangePassword _mdl)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            accountservices _accs = new accountservices();
            var message = _accs.validatepassword(_mdl.CurrentPassword, _mdl.Password, userid);
            ModelState.Clear();
            _mdl = new helper().GenerateError<ChangePassword>(_mdl, message);
            return View("passowrd", _mdl);
        }
        [HttpPost]
        public ActionResult login(LoginModel _model, bool setcookie = false)
        {
            accountservices _accs = new accountservices();
            helper _helper = new helper();
            var tuple = _accs.validateLogin(_model);
            if (tuple.Item2 != 0)
            {
                List<int> permissions = new PermissionServices().GetAllMenuPermissionsByPermissionRoleId(tuple.Item3);
                var p = JsonConvert.SerializeObject(permissions);
                _helper.CreateCookie<string>(p, "_plist", 8760);
                _helper.CreateCookie<int>(tuple.Item2, "_xid", 5);
                _helper.CreateCookie<int>(tuple.Item3, "_pid", 5);
                _helper.CreateCookie<int>(tuple.Item4, "domain", 5);
                _helper.CreateCookie<string>(tuple.Item5, "_slist", 8760);
                if (setcookie)
                {
                    CookieBuilder _builder = new CookieBuilder();
                    Dictionary<string, string> _rCookie = new Dictionary<string, string>();
                    Crypto _crypt = new Crypto();
                    _rCookie.Add("wsp", _crypt.EncryptStringAES(_model.Workspace));
                    _rCookie.Add("usp", _crypt.EncryptStringAES(_model.UserName));
                    _rCookie.Add("psp", _crypt.EncryptStringAES(_model.Password));
                    string CookieString = JsonConvert.SerializeObject(_rCookie);
                    _builder.Set("_rCookie", CookieString, Request.IsLocal);
                }
                if (tuple.Item6)
                {
                    string url = "https://" + _model.Workspace + ".taskayak.com/home/reset_password?vid=hxujggbd1894nndnhhdbvd09478rnhsg74";
                    if (Request.IsLocal)
                    {
                        return RedirectToAction("reset_password", "home", new { v = "1234hfnhfn" });
                    }
                    return Redirect(url);
                }
                else
                {
                    if (_model.ReturnUrl == "none")
                    {
                        string url = "https://" + _model.Workspace + ".taskayak.com/home/index?vid=hxujggbd1894nndnhhdbvd09478rnhsg74";
                        if (Request.IsLocal)
                        {
                            return RedirectToAction("index", "home", new { v = "1234hfnhfn" });
                        }
                        return Redirect(url);
                    }
                    else
                    {
                        return Redirect(_model.ReturnUrl);
                    }
                }
            }
            else
            {
                _model = _helper.GenerateError<LoginModel>(_model, tuple.Item1);
            }
            return View(_model);
        }
        [AuthorizeVerifiedloggedin]
        public ActionResult billing(int tab = 1)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            accountservices _accs = new accountservices();
            billingModal _model = _accs.getBilling(workspaceid);
            ViewBag.tab = tab;
            if (TempData["error"] != null)
            {
                var message = TempData["error"].ToString();
                _model = new helper().GenerateError<billingModal>(_model, message);
            }
            return View(_model);
        }
        [HttpPost]
        public JsonResult getbilling()
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            accountservices _accs = new accountservices();
            billingModal _model = _accs.getBilling(workspaceid);
            return Json(_model, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult billing(billingModal _model)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            accountservices _accs = new accountservices();
            _accs.UpdateBilling(_model, workspaceid);
            _model = _accs.getBilling(workspaceid);
            string message = "Account updated successfully";
            _model = new helper().GenerateError<billingModal>(_model, message);
            ViewBag.tab = 2;
            return View(_model);
        }
        [HttpPost]
        public ActionResult register(RegisterModel _model, string stripeToken = "", int plantype = 1)
        {
            string Message = string.Empty;
            string capctha = Request.Params["g-recaptcha-response"] != null ? Request.Params["g-recaptcha-response"].ToString() : string.Empty;
            if (ModelState.ContainsKey("password") && _model.PlanId == 0)
                ModelState["password"].Errors.Clear();
            if (!ModelState.IsValid)
            {
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        Message = !string.IsNullOrEmpty(Message) ? Environment.NewLine + error.ErrorMessage : "Error: " + error.ErrorMessage;
                    }
                }
            }
            else if (!string.IsNullOrEmpty(capctha))
            {
                if (!string.IsNullOrEmpty(_model.Token) && _model.Token == "78b4cf23656dc395364f1b6c02907691f2cdffe1")
                {
                    var res = statichelper.Validate(capctha) == "true";
                    if (res)
                    {
                        Stripe_Payment _payment = new Stripe_Payment();
                        string subscriptionid = "";
                        string customerid = "";
                        if (_model.PlanId > 0)
                        {
                            switch (_model.PlanId)
                            {
                                case 1:
                                    if (plantype == 2)
                                    {
                                        _payment.StarterWithMonthly(stripeToken, "Starter $75/mo ", _model.Email, _model.FirstName, _model.PhoneNumber, ref customerid, ref subscriptionid);
                                    }
                                    else
                                    {
                                        _payment.StarterWithYearly(stripeToken, "Starter $65/mo ", _model.Email, _model.FirstName, _model.PhoneNumber, ref customerid, ref subscriptionid);

                                    }
                                    break;
                                case 2:
                                    if (plantype == 2)
                                    {
                                        _payment.AdvantageWithMonthly(stripeToken, "Advantage $195/mo ", _model.Email, _model.FirstName, _model.PhoneNumber, ref customerid, ref subscriptionid);
                                    }
                                    else
                                    {
                                        _payment.AdvantageWithYearly(stripeToken, "Advantage $175/mo ", _model.Email, _model.FirstName, _model.PhoneNumber, ref customerid, ref subscriptionid);

                                    }
                                    break;
                            }
                        }

                        helper _helper = new helper();
                        accountservices _accs = new accountservices();
                        string message = "";
                        int roleid;
                        bool isrest = false;
                        bool _status = true;
                        bool accepttermsrqud = true;
                        int wid = new domainservices().createnewworkspace(_model.Workspace);
                        if (wid == 0)
                        {
                            _model = new helper().GenerateError<RegisterModel>(_model, "Error: Domain is already in use");
                            return View(_model);
                        }
                        var dept = new PermissionServices();
                        string service = "Taskayak free";
                        int smscount = 100;
                        int emailcount = 100;
                        switch (_model.PlanId)
                        {
                            case 1:
                                service = "Taskayak Starter";
                                smscount = 2500;
                                emailcount = 10000;
                                break;
                            case 2:
                                service = "Taskayak Advantage";
                                smscount = 10000;
                                emailcount = 40000;
                                break;
                            case 3:
                                service = "Taskayak Professional";
                                smscount = 50000;
                                emailcount = 50000;
                                break;
                        }
                        int i;
                        _model.Password = new helper().Get9RandomDigit();
                        string memberid =  new UserService().GetNewUserId(wid); 
                        var mser = new UserService();
                        if (_model.PlanId == 0)
                        {
                            int techRoleid = dept.add_new_role("Technician", wid);
                            dept.add_newTechwith_role(techRoleid);///tech role with id
                            int DispatchRoleid = dept.add_new_role("Dispatcher", wid);
                            dept.add_newDispatchwith_role(DispatchRoleid);///Dispatch role with id
                            int projectmanagerRoleId = dept.add_new_role("Project Manager", wid);
                            dept.add_newProjectmanagerwith_role(projectmanagerRoleId);
                            roleid = dept.add_new_role("Admin", wid);
                            dept.add_newAdminwith_role(roleid);
                            int SuperAdminRole = dept.add_new_role("Super Admin", wid);
                            dept.add_newSuperAdminwith_role(SuperAdminRole);
                            UserModel _mdlsuperadmin = new UserModel();
                            _mdlsuperadmin.FirstName = "Super";
                            _mdlsuperadmin.LastName = "Admin";
                            _mdlsuperadmin.UserId = memberid;
                            _mdlsuperadmin.EmailAddress = "admin@taskayak.com";
                            _mdlsuperadmin.UserType = "E";
                            _mdlsuperadmin.CompanyName = _model.Workspace;
                            _mdlsuperadmin.Password = "w4B284g#TweF";
                            _mdlsuperadmin.HourlyRate = "$0.00";
                            _mdlsuperadmin.Tax = "$0.00";
                            _mdlsuperadmin.StatusId = 1007;
                            _mdlsuperadmin.RoleId = SuperAdminRole;
                            _mdlsuperadmin.Gender = "M";

                            _mdlsuperadmin.Background = "Pending";
                            _mdlsuperadmin.DrugTested = "Pending";
                            _mdlsuperadmin.PhoneNumber = "555-555-5555";
                            _mdlsuperadmin.AlertnativePhoneNumber = "555-555-5555";
                            _mdlsuperadmin.StateId = 0;
                            _mdlsuperadmin.CityId = 0;
                            _mdlsuperadmin.Zipcode = "";
                            _mdlsuperadmin.Address = "";
                            _mdlsuperadmin.DrivingDistance = 50;
                            mser.AddNewUser(_mdlsuperadmin, 0, wid, "N");
                        }
                        else
                        {
                            roleid = dept.add_new_role("Super Admin", wid);
                            dept.add_newSuperAdminwith_role(roleid);
                        }
                        UserModel _mdl = new UserModel();
                        _mdl.FirstName = _model.FirstName.ToTitleCase();
                        _mdl.LastName = _model.LastName.ToTitleCase();
                        _mdl.UserId = (Convert.ToInt32(memberid) + 1).ToString();
                        _mdl.EmailAddress = _model.Email;
                        _mdl.UserType = "E";
                        _mdl.CompanyName = _model.Workspace;
                        _mdl.Password = _model.Password;
                        _mdl.HourlyRate = "$0.00";
                        _mdl.Tax = "$0.00";
                        _mdl.StatusId = 1007;
                        _mdl.RoleId = roleid;
                        _mdl.Gender = "M";
                        _mdl.Background = "Pending";
                        _mdl.DrugTested = "Pending";
                        _mdl.PhoneNumber = _model.PhoneNumber;
                        _mdl.AlertnativePhoneNumber = "";
                        _mdl.StateId = 0;
                        _mdl.CityId = 0;
                        _mdl.Zipcode = "";
                        _mdl.Address = "";
                        _mdl.DrivingDistance = 50;
                        var msg = mser.AddNewUser(_mdl, 0, wid);
                        mser.CreateNewSubscriptionAccount(wid, service, smscount, emailcount, _model.Workspace, _model.Email, "", "", _model.FirstName + ' ' + _mdl.LastName, subscriptionid, customerid, _model.PlanId);
                        _model = new helper().GenerateError<RegisterModel>(_model, msg);
                        LoginModel _lmdl = new LoginModel();
                        _lmdl.UserName = _model.Email;
                        _lmdl.Password = _model.Password;
                        _lmdl.Workspace = _model.Workspace;
                        int workspaceid = 0;
                        var userId = _accs.validateLogin(_lmdl, ref message, ref roleid, ref isrest, ref _status, ref accepttermsrqud, ref workspaceid);
                        string body = "Name :" + _model.FirstName.ToTitleCase() + "</br>Last Name:" + _model.LastName.ToTitleCase() + "</br>Email:" + _model.Email + "</br>WorkSpace: " + _model.Workspace + "</br> Phone number: " + _model.PhoneNumber;
                        new helper().SendEmailWithAWS("contact@convotechnology.com", body, _model.Workspace + " New Account Register", "Taskayak Admin");
                        var template = new templateservices().get_template(3);
                        List<int> _data = new List<int>();
                        _data.Add(userId);
                        new jobofferservices().sendoffers2(_data, true, true, template.sms, template.email, "Welcome to taskayak.com", false, wid);
                        if (userId != 0)
                        {
                            if (_model.RememberMe)
                            {
                                _helper.CreateCookie<int>(userId, "_xid", 24);
                                _helper.CreateCookie<int>(roleid, "_pid", 24);
                            }
                            else
                            {
                                _helper.CreateCookie<int>(userId, "_xid", 24);
                                _helper.CreateCookie<int>(roleid, "_pid", 24);
                            }
                            _lmdl.error_text = "please check your registered email address to login";
                            _lmdl.Password = "";
                            _lmdl = new helper().GenerateError<LoginModel>(_lmdl, _lmdl.error_text);
                            return View("Login", _lmdl);
                        }
                    }
                    else
                    {
                        Message = "Error : Please validate recaptcha";
                    }
                }
                else
                {
                    Message = "Error : Please validate Token";
                }
            }
            else
            {
                Message = "Error : Please validate recaptcha";
            }
            _model = new helper().GenerateError<RegisterModel>(_model, Message);

            return View(_model);
        }
        [HttpPost]
        public string isfreeaccount()
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            accountservices _accs = new accountservices();
            string res = _accs.getaccounttype(workspaceid);
            return res;
        }
        public ActionResult pricing()
        {
            RegisterModel _model = new RegisterModel();
            return View(_model);
        }
        public ActionResult resources()
        {
            return View();
        }
        [HttpPost]
        public ActionResult edit_doc(HttpPostedFileBase file, int UserTableId, string doc, int _dctype = 0)
        {
            UserService _m = new UserService();
            string path = Server.MapPath("~/image/doc-" + UserTableId);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            ViewBag.tab = 1;
            string filenames = new helper().GetRandomAlphaNumeric(4);
            var extension = Path.GetExtension(file.FileName);
            var imagepath = path + "/" + filenames + extension;
            string tempfilename = "/image/doc-" + UserTableId + "/" + filenames + extension;
            if (System.IO.File.Exists(imagepath))
            {
                System.IO.File.Delete(imagepath);
            }
            file.SaveAs(imagepath);
            var mes = _m.AddUserDocument(doc, UserTableId, tempfilename, _dctype);
            TempData["error"] = mes;
            return RedirectToAction("edit", "account", new { tab = 1 });
        }
        [HttpPost]
        public ActionResult add_tools(string tooltype1, string toolname1)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString()); 
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            TempData["error"] = new toolservices().add_new_toolbytypename(toolname1, tooltype1, userid, workspaceid); 
            return RedirectToAction("edit", "account", new { tab = 3 });
        }
        public ActionResult delete_doc(int memberid, string id)
        {
            Crypto _crypt = new Crypto();
            int _id = Convert.ToInt32(_crypt.DecryptStringAES(id));
            ViewBag.tab = 1;
             UserService _m = new UserService();
            var mes = _m.DeleteUserDocumentById(_id);
            TempData["error"] = mes;
            return RedirectToAction("edit", "account", new { tab = 1 });
        }
        [HttpPost]
        [ValidateInput(false)]
        public void ConvoTechContact(string Adminemail = "contact@convotechnology.com", string Company = "", string Contact = "", string Email = "", string Phone = "", string Subject = "", string Message = "", string senderAddress = "no-reply@taskayak.com")
        {
            helper _helpre = new helper();
            string body = "Compnay Name : " + Company + " <br/> Email: " + Email + "<br/> Telephone :" + Phone + "<br/> Contact name :" + Contact + "<br/> Message: " + Message;
            _helpre.SendEmailWithAWS(Adminemail, body, "Convo Technology contact request " + Subject, "Convo Technology", "Convo Technology", senderAddress);
            var hsmodel = new HubSpotCOmpanyModel()
            {
                Name = Company.ToTitleCase(),
                Description = "Professional Account request from Taskayak",
                City = "",
                state = "",
                Emailaddress = Email,
                Phonenumber = Phone,
                //message = Message,
                //Primarycontact = Contact.ToTitleCase(),
                Website = ""
            };
            var HubSpotId = new hubspot().CreateHubSpotCompany(hsmodel, Contact.ToTitleCase());

        }

    }
}