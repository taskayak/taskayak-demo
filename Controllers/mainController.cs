﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace hrm.Controllers
{
    public class mainController : Controller
    {

        public ActionResult Index(bool RequestDemo = false)
        {
            var url = Request.Url.Host.Replace("https://", "").Replace("www.", ""); 
            if (url != "taskayak.com" && !Request.IsLocal)
            {
                if (url == "app.taskayak.com")
                {
                    return Redirect("https://app.taskayak.com/account/login");
                }
                else
                {
                    return Redirect("https://taskayak.com");
                }
            }
            ViewBag.PopUp = RequestDemo;
            return View();
        }
        public ActionResult Maintenance(string ReturnUrl = "")
        {
            return View();
        }


        public string getval(string ReturnUrl = "")
        {
            return Request.Url.Host;
        }

        public string Sendemail(string _to, string _text, string subject, string senderName = "Taskayak", string senderAddress = "no-reply@taskayak.com")
        {
            string str = "email sent";
            try
            {
                string smtpEndpoint = "email-smtp.us-east-1.amazonaws.com";
                int port = 587;
                string toAddress = _to;
                string smtpUsername = "AKIAX27HMUUWXFPPOYVW";
                string smtpPassword = "BGQS+WegjbxpDbrQ6QkDwYHu606Jb1lXf5u61+M0G/Av";
                AlternateView htmlBody = AlternateView.CreateAlternateViewFromString(_text, null, "text/html");
                MailMessage message = new MailMessage();
                message.From = new MailAddress(senderAddress, senderName);
                message.To.Add(new MailAddress(toAddress));
                message.Subject = subject;
                message.AlternateViews.Add(htmlBody);
                message.IsBodyHtml = true;
                using (var client = new System.Net.Mail.SmtpClient(smtpEndpoint, port))
                {
                    // Create a Credentials object for connecting to the SMTP server
                    client.Credentials =
                        new NetworkCredential(smtpUsername, smtpPassword);
                    client.EnableSsl = true;
                    try
                    { client.Send(message); 
                    }
                    // Show an error message if the message can't be sent
                    catch (Exception )
                    {
                        //     new Comman().LogWrite(Enums.LogType.Error, ex.Message.ToString(), "Utilis", "Sendmail", Enums.Priority.Critical, ex.StackTrace);
                    }
                }
            }
            catch (SmtpException smtpException)
            {
                str = string.Concat("Error : ", smtpException.Message);
            }
            catch (WebException webException1)
            {
                WebException webException = webException1;
                using (WebResponse response = webException.Response)
                {
                    HttpWebResponse httpWebResponse = (HttpWebResponse)response;
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        using (StreamReader streamReader = new StreamReader(responseStream))
                        {
                            str = string.Concat("Error : ", streamReader.ReadToEnd());
                        }
                    }
                }
            }
            return str;
        }

        [HttpPost]
        public void sendhubspot(string company, string email, string telephone, string message, string primary)
        {
            //var hsmodel = new HubSpotCOmpanyModel()
            //{
            //    Name = company.ToTitleCase(),
            //    Description = primary.ToTitleCase() + " send contact request",
            //    City = "",
            //    state = "",
            //    Emailaddress = email,
            //    Phonenumber = telephone,
            //    message = message,
            //    Primarycontact = primary.ToTitleCase()
            //};
            string body = "Company: " + company.ToTitleCase() + "<br/>  Email Address: " + email + Environment.NewLine + "<br/>  Telephone: " + telephone + Environment.NewLine + "<br/> Message: " + message + "<br/> Primary contact: " + primary.ToTitleCase();
           // var HubSpotId = new hubspot().CreateHubSpotCompany(hsmodel); 
            new helper().SendEmailWithAWS("contact@convotechnology.com", body, primary.ToTitleCase() + " send contact request", "Taskayak Admin", "Taskayak contact form");
        }

        [HttpPost]
        [ValidateInput(false)]
        public void senddemohubspot(string f_domain, string f_name, string l_name, string f_email, string f_phone, string f_company,string recaptcha="")
        { 
            if (!string.IsNullOrEmpty(recaptcha))
            { 
                var res = statichelper.Validate(recaptcha) == "true";
                if (res)
                {
                    var hsmodel = new HubSpotCOmpanyModel()
                    {
                        Name = f_company.ToTitleCase(),
                        Description = f_name.ToTitleCase() + " send demo request",
                        City = "",
                        state = "",
                        Emailaddress = f_email,
                        Phonenumber = f_phone,
                        //message = "Demo request",
                        //Primarycontact = f_name.ToTitleCase() + " " + l_name,
                        Domain = f_domain,
                        Website=f_domain 
                    };
                    string PrimaryName = f_name.ToTitleCase() + " " + l_name.ToTitleCase();
                    string body = "Company: " + f_company.ToTitleCase() + "<br/> Email Address: " + f_email +"<br/> Telephone: " + f_phone +  "<br/> Message: Demo request<br/> Primary contact: " + f_name.ToTitleCase() + " " + l_name;
                    var HubSpotId = new hubspot().CreateHubSpotCompany(hsmodel, PrimaryName); 
                     new helper().SendEmailWithAWS("contact@convotechnology.com", body, f_name.ToTitleCase() + " send demo request", "Taskayak Admin", "Taskayak Demo form");
                }
            }
            
        } 
    }
}