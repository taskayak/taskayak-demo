﻿using hrm.Database;
using hrm.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hrm.Controllers
{
    [AuthorizeVerifiedloggedin]
    public class clientController : Controller
    {
        public List<int> _permission;
        public clientController()
        {
            string PList = System.Web.HttpContext.Current.Request.Cookies["_plist"] == null ? "0" : System.Web.HttpContext.Current.Request.Cookies["_plist"].Value.ToString();
            _permission = PList == "0" ? null : JsonConvert.DeserializeObject<List<int>>(PList);
        }
        public string get_client_rate(int id)
        {
            return new clientservices().GetClientRate(id);
        } 
        public ActionResult Index()
        {
            int _WorkspaceId = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            clientmodal _Response = new clientmodal();
            if (!_permission.Contains(5074))
            {
                return View("unauth");
            }
            _Response.PERMISSIONS = _permission;
            string _Message;
            if (TempData["error"] != null)
            {
                _Message = TempData["error"].ToString();
                _Response = new helper().GenerateError<clientmodal>(_Response, _Message);
            }
            _Response.Workspaceid = _WorkspaceId;
            return View(_Response);
        }
        public JsonResult Fetchclients(int start = 0, int length = 25, int draw = 1)
        {
            clientservices _ClientService = new clientservices();
            datatableAjax<clientmodal_item> _AjaxResponse = new datatableAjax<clientmodal_item>();
            int _Total = 0;
            int _WorkSpaceId = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string _Search = Request.Params["search[value]"] ??"";
            start = start == 0 ? 1 : start + 1;
            length = start == 1 ? length : start - 1 + length;
            var _ClientModel = _ClientService.GetAllActiveClientWithPaging(ref _Total, start, length, _Search, _WorkSpaceId);
            _AjaxResponse.draw = draw;
            _AjaxResponse.data = _ClientModel;
            _AjaxResponse.recordsFiltered = _ClientModel.Count() > 0 ? _Total : 0;
            _AjaxResponse.recordsTotal = _Total;
            return base.Json(_AjaxResponse, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Fetchmanagers(int start = 0, int length = 25, int draw = 1, int clientid = 0)
        {
            prjmanagerservices _ManagerService = new prjmanagerservices();
            datatableAjax<prjmngr_item> _AjaxResponse = new datatableAjax<prjmngr_item>();
            int _Total = 0;
            string _Search = Request.Params["search[value]"] ?? "";
            start = start == 0 ? 1 : start + 1;
            length = start == 1 ? length : start - 1 + length;
            var _ManagerModel = _ManagerService.GetAllActiveManagerByClientIdWithPaging(ref _Total, clientid, start, length, _Search);
            _AjaxResponse.draw = draw;
            _AjaxResponse.data = _ManagerModel;
            _AjaxResponse.recordsFiltered = _ManagerModel.Count() > 0 ? _Total : 0;
            _AjaxResponse.recordsTotal = _Total;
            return base.Json(_AjaxResponse, JsonRequestBehavior.AllowGet);
        }
        public ActionResult add()
        {
            if (!_permission.Contains(11))
            {
                return View("unauth");
            }
            clientmodal_item _Response = new clientmodal_item(); 
            return View(_Response);
        } 
        [HttpPost]
        public ActionResult add(clientmodal_item _Request)
        { 
            int _UserId = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            int _WorkspaceId = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            clientmodal _Response = new clientmodal();
            clientservices _ClientService = new clientservices();
            string _Street = string.Empty; string _City = string.Empty; string _State = string.Empty; string _Zipcode = string.Empty; int _StateId = 0;
            _Request.MapValidateAddress.GetAddress(ref _Street, ref _City, ref _State, ref _Zipcode);
            _Request.city_id = new defaultservices().CreateCity(ref _StateId, _State, _City);
            _Request.state_id = _StateId;
            _Request.zipcode = _Zipcode;
            _Request.address = _Street;
            string _Message = _ClientService.CreateClient(_Request, _UserId, _WorkspaceId);
            _Response = new helper().GenerateError<clientmodal>(_Response, _Message);
            _Response.PERMISSIONS = _permission;
            _Response.Workspaceid = _WorkspaceId;
            return View("Index", _Response);
        }
        public ActionResult delete(string token)
        {
            int _WorkspaceId = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            if (!_permission.Contains(14))
            {
                return View("unauth");
            }
            int _ClientId = Convert.ToInt32(new Crypto().DecryptStringAES(token)); 
            clientmodal _Response = new clientmodal();
            clientservices _ClientService = new clientservices();
            var _Message = _ClientService.DeleteClient(_ClientId);
            _Response = new helper().GenerateError<clientmodal>(_Response, _Message);
            _Response.PERMISSIONS = _permission;
            _Response.Workspaceid = _WorkspaceId;
            return View("Index", _Response);
        }
        public ActionResult edit(string token)
        {
            if (!_permission.Contains(12))
            {
                return View("unauth");
            }
            int _ClientId = Convert.ToInt32(new Crypto().DecryptStringAES(token));
            clientservices _ClientService = new clientservices();
            clientmodal_item _Response = _ClientService.GetClientProfileByClientId(_ClientId);
            return View(_Response);
        }
        public ActionResult view(string token)
        {
            if (!_permission.Contains(13))
            {
                return View("unauth");
            } 
            int _ClientId = Convert.ToInt32(new Crypto().DecryptStringAES(token));
            clientviewmodal _Response = new clientviewmodal();
            clientservices _ClientService = new clientservices();
            if (TempData["error"] != null)
            { 
                _Response = new helper().GenerateError<clientviewmodal>(_Response, TempData["error"].ToString());
            }
            _Response.clientid = _ClientId;
            _Response.clients = _ClientService.GetClientProfileByClientId(_ClientId);
            _Response.PERMISSIONS = _permission;
            return View(_Response);
        }

        [HttpPost]
        public ActionResult add_manager(prjmngr_item _Request)
        {
            int _UserId = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            int _Workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            clientviewmodal _Response = new clientviewmodal();
            clientservices _ClientService = new clientservices();
            prjmanagerservices _ManagerService = new prjmanagerservices();
            _Response.clientid = _Request.clientid;
            Session["clinetid"] = _Request.clientid;
            var message = _ManagerService.CreateProjectManager(_Request, _UserId, _Workspaceid);
            _Response.clients = _ClientService.GetClientProfileByClientId(_Request.clientid);
            _Response = new helper().GenerateError<clientviewmodal>(_Response, message);
            _Response.PERMISSIONS = _permission;
            return View("view", _Response);
        }

        public ActionResult add_manager()
        {
            if (!_permission.Contains(18))
            {
                return View("unauth");
            }
            if (Session["clinetid"] == null)
            {
                return RedirectToAction("Index");
            }
            int _ClientId = Session["clinetid"] == null ? 0 : Convert.ToInt32(Session["clinetid"]);
            clientviewmodal _Response = new clientviewmodal();
            clientservices _ClientService = new clientservices();
            _Response.clientid = _ClientId; 
            _Response.clients = _ClientService.GetClientProfileByClientId(_ClientId);
            _Response.PERMISSIONS = _permission;
            return View("view", _Response);
        }

        [HttpPost]
        public ActionResult update_manager(prjmngr_item _Request)
        {
            clientviewmodal _Response = new clientviewmodal();
            clientservices _ClientService = new clientservices();
            prjmanagerservices _ManagerService = new prjmanagerservices();
            _Response.clientid = _Request.clientid;
            Session["clinetid"] = _Request.clientid;
            string _Message = _ManagerService.UpdateManagerProfile(_Request);
            _Response.clients = _ClientService.GetClientProfileByClientId(_Request.clientid);
            _Response = new helper().GenerateError<clientviewmodal>(_Response, _Message);
            _Response.PERMISSIONS = _permission;
            return View("view", _Response);
        }

        public ActionResult update_manager()
        {
            if (Session["clinetid"] == null)
            {
                return RedirectToAction("Index");
            }
            int _ClientId = Session["clinetid"] == null ? 0 : Convert.ToInt32(Session["clinetid"]);
            clientviewmodal _Response = new clientviewmodal();
            clientservices _ClientService = new clientservices(); 
            _Response.clients = _ClientService.GetClientProfileByClientId(_ClientId); 
            _Response.PERMISSIONS = _permission;
            return View("view", _Response);
        }
        public ActionResult delete_manager(string clienttoken, string token)
        {
            if (!_permission.Contains(17))
            {
                return View("unauth");
            }
            Crypto _Crypto = new Crypto();
            clientviewmodal _Response = new clientviewmodal();
            clientservices _ClientService = new clientservices();
            prjmanagerservices _ManagerService = new prjmanagerservices();
            int _ManagerId = Convert.ToInt32(_Crypto.DecryptStringAES(token));
            int _ClientId = Convert.ToInt32(_Crypto.DecryptStringAES(clienttoken));
            string _Message = _ManagerService.DeleteManager(_ManagerId);
            _Response.clientid = _ClientId;
            _Response.clients = _ClientService.GetClientProfileByClientId(_ClientId);
            _Response = new helper().GenerateError<clientviewmodal>(_Response, _Message);
            _Response.PERMISSIONS = _permission;
            return View("view", _Response);
        }
        [HttpPost]
        public ActionResult update(clientmodal_item _Request)
        {
            int _WorkspaceId = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            Session["clinetid"] = _Request.clientid;
            clientmodal _Response = new clientmodal();
            clientservices _ClientService = new clientservices();
            string _Street = ""; string _City = ""; string _State = ""; string _Zipcode = ""; int _StateId = 0;
            _Request.MapValidateAddress.GetAddress(ref _Street, ref _City, ref _State, ref _Zipcode);
            _Request.city_id = new defaultservices().CreateCity(ref _StateId, _State, _City);
            _Request.state_id = _StateId;
            _Request.zipcode = _Zipcode;
            _Request.address = _Street;
            var message = _ClientService.UpdateClientProfile(_Request);
            _Response = new helper().GenerateError<clientmodal>(_Response, message);
            _Response.PERMISSIONS = _permission;
            _Response.Workspaceid = _WorkspaceId;
            return View("Index", _Response);
        } 

        public PartialViewResult GetImportView()
        {
            return PartialView("_importClient");
        }
        
    }
}