﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace hrm.Controllers
{
    [AuthorizeVerifiedloggedin]
    public class tool_typeController : Controller
    {
        public List<int> _permission;
        public tool_typeController()
        {
            string PList = System.Web.HttpContext.Current.Request.Cookies["_plist"] == null ? "0" : System.Web.HttpContext.Current.Request.Cookies["_plist"].Value.ToString();
            _permission = PList == "0" ? null : JsonConvert.DeserializeObject<List<int>>(PList);
        }
        public ActionResult Index()
        {
            if (!_permission.Contains(5077))
            {
                return View("unauth");
            }
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            tooltype _model = new tooltype();
            var toolservices = new toolservices();
            _model.tool_type = toolservices.GetAllActiveToolType(workspaceid);
            _model.mainorder = _model.tool_type.Count>0? _model.tool_type.Max(a=>a.mainorder)+1:1;
            string message = TempData["error"] != null? TempData["error"].ToString():string.Empty; 
           _model.PERMISSIONS = _permission;
            _model = new helper().GenerateError<tooltype>(_model, message);
            return View(_model);
        }


        [HttpPost]
        public ActionResult Index(tooltype _model)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            var toolservices = new toolservices();
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString()); 
            var error_text = toolservices.AddNewToolType(_model.name, userid,_model.mainorder, workspaceid); 
            tooltype _model1 = new tooltype();
            if (!error_text.ToLower().Contains("error"))
            {
                ModelState.Clear();
            }
            _model1.tool_type = toolservices.GetAllActiveToolType(workspaceid);
            _model1.mainorder = _model1.tool_type.Count > 0 ? _model1.tool_type.Max(a => a.mainorder) + 1 : 1;
            _model1 = new helper().GenerateError<tooltype>(_model1, error_text);
            _model1.PERMISSIONS = _permission;
            return View(_model1);
        }

        [HttpPost]
        public ActionResult update(string name, int id,int mainorder)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            var toolservices = new toolservices();
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString()); 
            var error_text = toolservices.UpdateToolType(name, id, userid, mainorder);
            tooltype _model = new tooltype();
            ModelState.Clear();
            _model.tool_type = toolservices.GetAllActiveToolType(workspaceid);
            _model.mainorder = _model.tool_type.Count > 0 ? _model.tool_type.Max(a => a.mainorder) + 1 : 1; 
            _model = new helper().GenerateError<tooltype>(_model, error_text);
            _model.PERMISSIONS = _permission;
            return View("index", _model);
        }
        public ActionResult delete(int id)
        {
            if (!_permission.Contains(5112))
            {
                return View("unauth");
            }
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            var toolservices = new toolservices();
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString()); 
            var error_text = toolservices.DeleteToolsType(id, userid);
            tooltype _model = new tooltype(); ModelState.Clear();
            _model.tool_type = toolservices.GetAllActiveToolType(workspaceid);
            _model.mainorder = _model.tool_type.Count > 0 ? _model.tool_type.Max(a => a.mainorder) + 1 : 1;
            _model = new helper().GenerateError<tooltype>(_model, error_text);
            _model.PERMISSIONS = _permission;
            return View("index", _model);
        }


    }
}