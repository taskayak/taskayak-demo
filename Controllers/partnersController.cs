﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
namespace hrm.Controllers
{

    [AuthorizeVerifiedloggedin]
    public class partnersController : Controller
    {
        public List<int> _permission;
        public partnersController()
        {
            string PList = System.Web.HttpContext.Current.Request.Cookies["_plist"] == null ? "0" : System.Web.HttpContext.Current.Request.Cookies["_plist"].Value.ToString();
            _permission = PList == "0" ? null : JsonConvert.DeserializeObject<List<int>>(PList);
        }
        public string get_client_rate(int id)
        { 
            return new clientservices().GetClientRate(id);
        } 
        public ActionResult Index()
        {
            if (!_permission.Contains(5103))
            {
                return View("unauth");
            } 
            partnermodal _model = new partnermodal();
            _model.PERMISSIONS = _permission;
            if (TempData["error"] != null)
            { 
                _model = new helper().GenerateError<partnermodal>(_model, TempData["error"].ToString());
            }
            return View(_model);
        }
        public JsonResult Fetchpartners(int start = 0, int length = 25, int draw = 1)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string search = Request.Params["search[value]"] ?? "";
            start = start == 0 ? 1 : start + 1;
            length = start == 1 ? length : start - 1 + length;
            partnerService _services = new partnerService();
            datatableAjax<partnermodal_item> tableDate = new datatableAjax<partnermodal_item>();
            int total = 0;
            var _model = _services.get_all_active_partners_bypaging(ref total, start, length, search, workspaceid);
            tableDate.draw = draw;
            tableDate.data = _model;
            tableDate.recordsFiltered = _model.Count() > 0 ? total : 0;
            tableDate.recordsTotal = total;
            return base.Json(tableDate, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Fetchmanagers(int start = 0, int length = 25, int draw = 1, int partnerid = 0)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string search = Request.Params["search[value]"] ?? "";
            start = start == 0 ? 1 : start + 1;
            length = start == 1 ? length : start - 1 + length;
            prjmanagerservices _services = new prjmanagerservices();
            datatableAjax<prjmngr_item> tableDate = new datatableAjax<prjmngr_item>();
            int total = 0;
            var _model = _services.get_all_active_prj_bypartner_byindex(ref total, partnerid, start, length, search, workspaceid);
            tableDate.draw = draw;
            tableDate.data = _model;
            tableDate.recordsFiltered = _model.Count() > 0 ? total : 0;
            tableDate.recordsTotal = total;
            return base.Json(tableDate, JsonRequestBehavior.AllowGet);
        }
        public ActionResult add()
        {
            if (!_permission.Contains(5095))
            {
                return View("unauth");
            }
            partnermodal_item _item = new partnermodal_item();
            return View(_item);
        }
        [HttpPost]
        public ActionResult add(partnermodal_item _item)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
              int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            partnermodal _model = new partnermodal();
            partnerService _services = new partnerService();
            _model.PERMISSIONS = _permission;
            var message = _services.add_new_partner(_item, userid, workspaceid);
            _model = new helper().GenerateError<partnermodal>(_model, message);
            return View("Index", _model);
        }
        public ActionResult delete(string token)
        {
            if (!_permission.Contains(5098))
            {
                return View("unauth");
            }
            int id = Convert.ToInt32(new Crypto().DecryptStringAES(token));
               partnermodal _model = new partnermodal();
            partnerService _services = new partnerService();
            var message = _services.delete_partner(id);
            _model.PERMISSIONS = _permission;
            _model = new helper().GenerateError<partnermodal>(_model, message);
            return View("Index", _model);
        }
        public ActionResult edit(string token)
        {
            if (!_permission.Contains(5096))
            {
                return View("unauth");
            }
            int id = Convert.ToInt32(new Crypto().DecryptStringAES(token));
            partnerService _services = new partnerService(); 
            partnermodal_item _item = _services.get_all_active_partners_byid(id); 
            return View(_item);
        }
        public ActionResult view(string token)
        {
            if (!_permission.Contains(5097))
            {
                return View("unauth");
            }
            int id = Convert.ToInt32(new Crypto().DecryptStringAES(token));
               partnerviewmodal _model = new partnerviewmodal();
            partnerService _services = new partnerService();
            if (TempData["error"] != null)
            { 
                _model = new helper().GenerateError<partnerviewmodal>(_model, TempData["error"].ToString());
            }
            _model.PERMISSIONS = _permission;
            _model.partnerid = id;
            _model.partners = _services.get_all_active_partners_byid(id); 
            return View(_model);
        }
        [HttpPost]
        public ActionResult add_manager(prjmngr_item prjectmgr)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
               partnerviewmodal _model = new partnerviewmodal();
            partnerService _services = new partnerService();
            prjmanagerservices _ser = new prjmanagerservices(); 
            _model.partnerid = prjectmgr.partnerid;
            Session["partnerid"] = prjectmgr.partnerid;
            var message = _ser.add_new_partner_manager(prjectmgr, userid);
            _model.partners = _services.get_all_active_partners_byid(prjectmgr.partnerid); 
            _model = new helper().GenerateError<partnerviewmodal>(_model, message);
            _model.PERMISSIONS = _permission;
            return View("view", _model);
        }
        public ActionResult add_manager()
        {
            if (!_permission.Contains(5102))
            {
                return View("unauth");
            }
            prjmngr_item prjectmgr = new prjmngr_item();
                 partnerviewmodal _model = new partnerviewmodal();
            partnerService _services = new partnerService(); 
            prjectmgr.partnerid = Session["partnerid"] == null ? 0 : Convert.ToInt32(Session["partnerid"]);
            if (Session["clinetid"] == null)
            {
                return RedirectToAction("Index");
            }
            _model.partnerid = prjectmgr.partnerid; 
            _model.partners = _services.get_all_active_partners_byid(prjectmgr.clientid);
            _model.PERMISSIONS = _permission;
            return View("view", _model);
        }
        [HttpPost]
        public ActionResult update_manager(prjmngr_item prjectmgr)
        { 
              partnerviewmodal _model = new partnerviewmodal();
            partnerService _services = new partnerService();
            prjmanagerservices _ser = new prjmanagerservices();
            _model.partnerid = prjectmgr.partnerid;
            Session["partnerid"] = prjectmgr.partnerid;
            var message = _ser.update__partner_manager(prjectmgr);
            _model.partners = _services.get_all_active_partners_byid(prjectmgr.partnerid);
            _model = new helper().GenerateError<partnerviewmodal>(_model, message);
            _model.PERMISSIONS = _permission;
            return View("view", _model);
        }
        public ActionResult update_manager()
        {
            prjmngr_item prjectmgr = new prjmngr_item(); 
             partnerviewmodal _model = new partnerviewmodal();
            partnerService _services = new partnerService();
            prjmanagerservices _ser = new prjmanagerservices();
            _model.partnerid = Session["partnerid"] == null ? 0 : Convert.ToInt32(Session["partnerid"]);
            if (Session["clinetid"] == null)
            {
                return RedirectToAction("Index");
            }     _model.partners = _services.get_all_active_partners_byid(prjectmgr.partnerid);
            _model.PERMISSIONS = _permission;
            return View("view", _model);
        }
        public ActionResult delete_manager(string clienttoken, string token)
        {
            if (!_permission.Contains(5101))
            {
                return View("unauth");
            }
            Crypto _crypt = new Crypto();
            int id = Convert.ToInt32(_crypt.DecryptStringAES(token));
            int partnerid = Convert.ToInt32(_crypt.DecryptStringAES(clienttoken));
            partnerviewmodal _model = new partnerviewmodal();
            partnerService _services = new partnerService();
            prjmanagerservices _ser = new prjmanagerservices();
            _model.partnerid = partnerid;
             var message = _ser.delete_partner_manager(id);
            _model.partners = _services.get_all_active_partners_byid(partnerid);
            _model = new helper().GenerateError<partnerviewmodal>(_model, message);
            _model.PERMISSIONS = _permission;
            return View("view", _model);
        }
        [HttpPost]
        public ActionResult update(partnermodal_item _item)
        {
             partnermodal _model = new partnermodal();
            partnerService _services = new partnerService(); 
            var message = _services.update_partners(_item);
            _model = new helper().GenerateError<partnermodal>(_model, message);
            _model.PERMISSIONS = _permission;
            return View("Index", _model);
        }
        public ActionResult update()
        { 
            partnermodal _model = new partnermodal();
            _model.PERMISSIONS = _permission;
            return View("Index", _model);
        }

    }
}