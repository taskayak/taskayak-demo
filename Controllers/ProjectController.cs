﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace hrm.Controllers
{
    [AuthorizeVerifiedloggedin]
    public class ProjectController : Controller
    {
        public List<int> _permission;
        public int pageid = 25;
        public ProjectController()
        {
            string PList = System.Web.HttpContext.Current.Request.Cookies["_plist"] == null ? "0" : System.Web.HttpContext.Current.Request.Cookies["_plist"].Value.ToString();
            _permission = PList == "0" ? null : JsonConvert.DeserializeObject<List<int>>(PList);
        } 
     public void update_status(int id, int statusId)
        {
            new projectServices().update_status(id, statusId);
        }
        public ActionResult Index( int srch = 1, string token = "")
        {
            if (!_permission.Contains(5125))
            {
                return View("unauth");
            }
            ProjectModal _model = new ProjectModal();
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString()); 
            var _filtermodel = new projectfilter();
            if (srch == 2)
            {
                new filterservices().delete_filter(userid, pageid);
            }
            _filtermodel = new filterservices().getfilter<projectfilter>(_filtermodel, userid, pageid);
            _model.f_date = string.IsNullOrEmpty(_filtermodel.date) ? "" : _filtermodel.date;
            _model.f_cclient = _filtermodel.cclient ?? 0;
            _model.f_cmanager = _filtermodel.cmanager ?? 0;
            _model.f_pmanager = _filtermodel.pmanager ?? 0;
            _model.f_Status = _filtermodel.Status ?? 0;
            _model.f_length = _filtermodel.length ?? 25;
            _model.f_Screening = _filtermodel.Screening ?? 0;
            _model.f_isActiveFilter = _filtermodel.isActiveFilter;
           _model.PERMISSIONS = _permission;
            string message = TempData["errordata"] != null? TempData["errordata"].ToString():string.Empty; 
            if (!string.IsNullOrEmpty(token))
            {
                projectServices _ser = new projectServices();
                int projectid = Convert.ToInt32(new Crypto().DecryptStringAES(token));
                _ser.deleteproject(projectid);
                message = "Project deleted";
            }
            _model = new helper().GenerateError<ProjectModal>(_model, message);
            return View(_model);
        }

        public JsonResult Fetchprojects(int start = 0, int length = 25, int draw = 1, string date = "", int cclient = 0, int cmanager = 0, int pmanager = 0, int Status = 0, int searchtype = 0,int Screening=0)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            string search = Request.Params["search[value]"] ?? ""; 
            var _filtermodel = new projectfilter();
            switch(searchtype)
            { 
                case 1:
                    _filtermodel.cmanager = cmanager;
                    _filtermodel.date = date;
                    _filtermodel.pmanager = pmanager;
                    _filtermodel.cclient = cclient;
                    _filtermodel.Status = Status;
                    _filtermodel.Screening = Screening;
                    _filtermodel.length = length;
                    new filterservices().save_filter<projectfilter>(_filtermodel, userid, pageid);
                    break;
                case 2:
                    date = string.Empty;
                    cmanager = 0;
                    cclient = 0;
                    pmanager = 0;
                    Status = 0;
                    Screening = 0;
                    length = 25;
                    new filterservices().delete_filter(userid, pageid);
                    break;
            } 
            start = start == 0 ? 1 : start + 1;
            length = start == 1 ? length : start - 1 + length;
            projectServices _ser = new projectServices();
            datatableAjax<_projectitems> tableDate = new datatableAjax<_projectitems>();
            int total = 0;
            var _model = _ser.GetActiveProjectByIndex(ref total, start, length, search, date, cclient, cmanager, pmanager, Status, workspaceid, Screening);
            tableDate.draw = draw;
            tableDate.data = _model;
            tableDate.recordsFiltered = _model.Count() > 0 ? total : 0;
            tableDate.recordsTotal = total;
            return Json(tableDate, JsonRequestBehavior.AllowGet);
        }
        public ActionResult add()
        {
            if (!_permission.Contains(5129))
            {
                return View("unauth");
            }
            int WorkspaceId = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            ProjectModal _model = new ProjectModal
            {
                ProjectId = new projectServices().GetLatestProjectId(WorkspaceId),
                _tools = get_all_tools(),
                _skills = get_all_skills()
            };
            return View(_model);
        }

        public ActionResult edit(string token = "")
        {
            if (!_permission.Contains(5127))
            {
                return View("unauth");
            } 
            int id = Convert.ToInt32(new Crypto().DecryptStringAES(token));
            ProjectModal _model = new projectServices().GetProjectDetailById(id);
            _model._tools = get_all_tools();
            _model._skills = get_all_skills();
            return View(_model);
        }
        [HttpPost]
        public ActionResult reminder(int usertype, reminder item, string token)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            int isProjectLevel = 1;
            int projectId = Convert.ToInt32(new Crypto().DecryptStringAES(token));
            int reminderId = 0;
            int count = new projectServices().GetReminderJobCount(projectId);
            TempData["errordata"] = count < 5? new projectServices().addNewReminder(usertype, item, projectId, workspaceid, isProjectLevel, ref reminderId): "Project can have only 5 reminders";
             return RedirectToAction("view", new { token, tabid = 5, userType= usertype,  reminderId });
        }

        [HttpPost]
        public ActionResult editreminder( int usertype, reminder item, string projecttoken)
        {
            int projectId = Convert.ToInt32(new Crypto().DecryptStringAES(projecttoken));
            int reminderId = Convert.ToInt32(new Crypto().DecryptStringAES(item.token));
            TempData["errordata"] = new projectServices().UpdateReminder(usertype, item, projectId, reminderId); 
            return RedirectToAction("view", new { token = projecttoken, tabid = 5, userType = usertype,  reminderId });
        }

        public ActionResult deleteReminder(string token = "",string projectToken="")
        {
            if (!_permission.Contains(5128))
            {
                return View("unauth");
            }
            int reminderId = Convert.ToInt32(new Crypto().DecryptStringAES(token));
            TempData["errordata"] = new projectServices().DeleteReminder(reminderId);  
            return RedirectToAction("view", new { token = projectToken, tabid = 5 });
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult comment(string comment, string token)
        {
            List<int> jobs = new List<int>(); List<int> offers = new List<int>();
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            int ProjectId = Convert.ToInt32(new Crypto().DecryptStringAES(token));
            TempData["errordata"] = new projectServices().addprojectComment(comment, ProjectId, userid, ref jobs, ref offers);
            addjobcomment(jobs, comment);
            addoffercomment(offers, comment); 
            return RedirectToAction("view", new { token, tabid = 1 });
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult edit_skills(int[] skills, string token)
        {
            int ProjectId = Convert.ToInt32(new Crypto().DecryptStringAES(token));
            TempData["errordata"] = new projectServices().update_skills(skills, ProjectId); 
            return RedirectToAction("view", new { token, tabid = 4 });
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult edit_tools(int[] tools, string token)
        {
            int ProjectId = Convert.ToInt32(new Crypto().DecryptStringAES(token));
            TempData["errordata"] = new projectServices().update_tools(tools, ProjectId); 
            return RedirectToAction("view", new { token, tabid = 3 });
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult file(HttpPostedFileBase file = null, string file_title = "", string token = "")
        {
            List<int> jobs = new List<int>(); List<int> offers = new List<int>();
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            int ProjectId = Convert.ToInt32(new Crypto().DecryptStringAES(token));
            string path = Server.MapPath("~/image/projectpicture-" + ProjectId);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filenames = new helper().GetRandomAlphaNumeric(4);
            var extension = Path.GetExtension(file.FileName);
            var imagepath = path + "/" + filenames + extension;
            string tempfilename = "/image/projectpicture-" + ProjectId + "/" + filenames + extension;
            if (System.IO.File.Exists(imagepath))
            {
                System.IO.File.Delete(imagepath);
            }
            file.SaveAs(imagepath);
            long b = file.ContentLength;
            long kb = b / 1024;
            int parentId = 0;
            TempData["errordata"] = new projectServices().AddNewDocument(file_title, userid, ProjectId, kb.ToString() + " kb", tempfilename, ref jobs, ref offers, ref parentId);
            addjobfile(file_title, userid, jobs, kb.ToString() + " kb", tempfilename, parentId);
            addofferfile(file_title, userid, offers, kb.ToString() + " kb", tempfilename, parentId); 
            return RedirectToAction("view", new {  token, tabid = 2 });
        }

        public ActionResult delete_file(string token, string jtoken)
        {
            List<int> jobs = new List<int>(); List<int> offers = new List<int>();
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            Crypto _crypt = new Crypto();
            int id = Convert.ToInt32(_crypt.DecryptStringAES(token));
            int projectid = Convert.ToInt32(_crypt.DecryptStringAES(jtoken));
            new projectServices().delete_doc(id, projectid, ref jobs, ref offers);
            deletejobfile(id, jobs);
            deleteoffersfile(id, offers);
            TempData["errordata"] = "File deleted";
            return RedirectToAction("view", new { token = jtoken, tabid = 2 });
        }

        public void deletejobfile(int fileId, List<int> jobs)
        {
            Jobservices _js = new Jobservices();
            foreach (var item in jobs)
            {
                _js.delete_docByParent(fileId);
            }
        }


        public void deleteoffersfile(int fileId, List<int> offers)
        {
            jobofferservices _js = new jobofferservices();
            foreach (var item in offers)
            {
                _js.delete_docByParent(fileId);
            }
        }


        public void addjobfile(string file_title, int userid, List<int> jobs, string size, string url, int parentId)
        {
            Jobservices _js = new Jobservices();
            foreach (var item in jobs)
            {
                _js.AddNewDocument(file_title, userid, item, size, url, parentId);
            }
        }

        public void addofferfile(string file_title, int userid, List<int> jobs, string size, string url, int parentId)
        {//mes = _ser.AddNewDocument(file_title, userid, JobID, kb.ToString() + " kb", tempfilename); 
            jobofferservices _js = new jobofferservices();
            foreach (var item in jobs)
            {
                _js.AddNewDocument(file_title, userid, item, size, url, parentId);
            }
        }


        public void addjobcomment(List<int> jobs, string comment)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            Jobservices _js = new Jobservices();
            foreach (var item in jobs)
            {
                _js.add_new_comment(comment, userid, item);
            }
        }

        public void addoffercomment(List<int> offers, string comment)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            jobofferservices _js = new jobofferservices();
            foreach (var item in offers)
            {
                _js.add_new_comment(comment, userid, item);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult edit(ProjectModal _model)
        {
            int id = Convert.ToInt32(new Crypto().DecryptStringAES(_model.token));
            var message = new projectServices().UpdateProject(_model, id);
            _model = new projectServices().GetProjectDetailById(id);
            _model = new helper().GenerateError<ProjectModal>(_model, message);
            _model._tools = get_all_tools();
            _model._skills = get_all_skills();
            return View("edit", _model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult add(ProjectModal _model)
        { 
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());  
            TempData["errordata"] = new projectServices().CreateProject(_model, workspaceid);  
            return RedirectToAction("index"); 
        } 

        public ActionResult view(string token = "", int tabid = 0,int userType=1,int reminderId= 0)
        { 
            int id = Convert.ToInt32(new Crypto().DecryptStringAES(token));
            ProjectModal _model = new projectServices().GetProjectDetailById(id);
            _model._tools = get_all_tools();
            _model._skills = get_all_skills();
            _model.f_reminderId = reminderId;
            _model.f_userType = userType;
            _model.f_tabid = tabid;
            string message = TempData["errordata"] != null ? TempData["errordata"].ToString() : string.Empty ; 
            _model = new helper().GenerateError<ProjectModal>(_model, message);
            return View(_model);
        }

        [HttpPost]
        public ActionResult view(ProjectModal _model, int tabid = 1)
        {
            int id = Convert.ToInt32(new Crypto().DecryptStringAES(_model.token));
            var message = new projectServices().update_new_Project(_model, id);
            _model = new projectServices().GetProjectDetailById(id);
            _model = new helper().GenerateError<ProjectModal>(_model, message);
            _model._tools = get_all_tools();
            _model._skills = get_all_skills();
            _model.f_reminderId = 0;
            _model.f_userType = 1;
            _model.f_tabid = tabid;
            return View("View", _model);
        }

        public List<SkillItems> get_all_skills()
        {
            var service = new skillservices();
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            return service.get_all_active_skills(workspaceid)._skill;
        }


        public List<ToolItems> get_all_tools()
        {
            var service = new skillservices();
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            return service.get_all_active_tools(workspaceid)._tool;
        }


        public bool validateProjectId(string id)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            return new projectServices().validateProjectId(id, workspaceid);
        }

        [HttpPost]
        public JsonResult GetProjectDetails(int id)
        {
            _projectdetails _model = new projectServices().GetProjectdetails(id);
            return Json(_model, JsonRequestBehavior.AllowGet);
        }
    }
}