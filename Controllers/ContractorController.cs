﻿using hrm.Database;
using hrm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Web.Mvc;
namespace hrm.Controllers
{
    [AuthorizeVerifiedloggedin]
    public class ContractorController : Controller
    {
        public List<int> _permission;
        public ContractorController()
        {
            string PList = System.Web.HttpContext.Current.Request.Cookies["_plist"] == null ? "0" : System.Web.HttpContext.Current.Request.Cookies["_plist"].Value.ToString();
            _permission = PList == "0" ? null : JsonConvert.DeserializeObject<List<int>>(PList);
        }

        public ActionResult Index()
        {
            if (!_permission.Contains(5075))
            {
                return View("unauth");
            }
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            var type = Request.Cookies["_slist"] == null ? "E" : Convert.ToString(Request.Cookies["_slist"].Value.ToString());
            if (type.ToLower() == "s")
            {
                int id = new UserService().GetContractorIdByUserId(userid);
                var _token = new Crypto().EncryptStringAES(id.ToString());
                return RedirectToAction("view", new { token = _token, issubcontractor = true });
            }
            else
            {
                ContractorModal _model = new ContractorModal();
                _model.PERMISSIONS = _permission;
                _model = TempData["error"] != null ? new helper().GenerateError<ContractorModal>(_model, TempData["error"].ToString()) : _model;
                return View(_model);
            }

        }
        public JsonResult FetchContractor(int start = 0, int length = 25, int draw = 1)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string search = Request.Params["search[value]"] ?? "";
            start = start == 0 ? 1 : start + 1;
            length = start == 1 ? length : start - 1 + length;
            ContractorServices _services = new ContractorServices();
            datatableAjax<ContractorModal_Item> tableDate = new datatableAjax<ContractorModal_Item>();
            int total = 0;
            var _model = _services.GetAllActiveContractorByPaging(ref total, start, length, search, workspaceid);
            tableDate.draw = draw;
            tableDate.data = _model;
            tableDate.recordsFiltered = _model.Count() > 0 ? total : 0;
            tableDate.recordsTotal = total;
            return base.Json(tableDate, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Fetchmanagers(int start = 0, int length = 25, int draw = 1, int clientid = 0)
        {
            string search = Request.Params["search[value]"] ?? "";
            start = start == 0 ? 1 : start + 1;
            length = start == 1 ? length : start - 1 + length;
            prjmanagerservices _services = new prjmanagerservices();
            datatableAjax<ContractorProjectManager_Item> tableDate = new datatableAjax<ContractorProjectManager_Item>();
            int total = 0;
            var _model = _services.GetAllActiveProjectManagerByContractorByIndex(ref total, clientid, start, length, search);
            tableDate.draw = draw;
            tableDate.data = _model;
            tableDate.recordsFiltered = _model.Count() > 0 ? total : 0;
            tableDate.recordsTotal = total;
            return base.Json(tableDate, JsonRequestBehavior.AllowGet);
        }
        public ActionResult add()
        {
            if (!_permission.Contains(5079))
            {
                return View("unauth");
            }
            ContractorModal_Item _item = new ContractorModal_Item();
            return View(_item);
        }
        [HttpPost]
        public ActionResult add(ContractorModal_Item _item)
        {
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            ContractorModal _model = new ContractorModal();
            ContractorServices _services = new ContractorServices();
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            string street = ""; string city = ""; string state = ""; string zipcode = ""; int StateId = 0;
            _item.MapValidateAddress.GetAddress(ref street, ref city, ref state, ref zipcode);
            _item.city_id = new defaultservices().CreateCity(ref StateId, state, city);
            _item.state_id = StateId;
            _item.zipcode = zipcode;
            _item.address = street;
            var message = _services.AddNewContractor(_item, userid, workspaceid);
            _model = new helper().GenerateError<ContractorModal>(_model, message);
            _model.PERMISSIONS = _permission;
            return View("Index", _model);
        }

        [HttpPost]
        public ActionResult add_member(int member_id = 0, string membertype = "", string fname = "", int clientid = 0, string lname = "", string email = "", string phone = "", string MapValidateAddress = "", string suite = "", string Position = "", int manager_id = 0)
        {
            string street = ""; string city = ""; string state = ""; string zipcode = ""; int StateId = 0;
            MapValidateAddress.GetAddress(ref street, ref city, ref state, ref zipcode);
            int city_id = new defaultservices().CreateCity(ref StateId, state, city);
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            string _token = new Crypto().EncryptStringAES(clientid.ToString());
            UserService _services = new UserService();
            var template = new templateservices().get_template(3);
            int roleid = _services.GetRoleIdByPositionType(workspaceid, membertype);
            if (member_id != 0)
            {
                var message = _services.AssignTechnicianToCompany(member_id, membertype, fname, clientid, lname, email, phone, street, zipcode, Position, manager_id, userid, city_id, StateId, roleid, MapValidateAddress, suite);
                List<int> _data = new List<int>();
                _data.Add(member_id);
                // int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
                new jobofferservices().sendoffers2(_data, true, true, template.sms, template.email, "Field Technician Account", false, workspaceid);
                TempData["error"] = message;
            }
            else
            {
                Crypto _crypt = new Crypto();
                Session["end"] = "1";
                int i = 0;
                // int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
                string memberid = new UserService().GetNewUserId(workspaceid); 
                string companyname = "";
                UserModel _mdl = new UserModel();
                _mdl.FirstName = fname;
                _mdl.LastName = lname;
                _mdl.UserId = memberid;
                _mdl.EmailAddress = email;
                _mdl.UserType = membertype;
                _mdl.Position = Position;
                _mdl.Password = new helper().GetRandomAlphaNumeric(8);
                _mdl.HourlyRate = "$0.00";
                _mdl.Tax = "$0.00";
                _mdl.StatusId = 1;
                _mdl.RoleId = roleid;
                _mdl.Gender = "M";
                _mdl.Background = "Pending";
                _mdl.DrugTested = "Pending";
                _mdl.EmailAddress = email;
                _mdl.PhoneNumber = phone;
                _mdl.ManagerId = manager_id;
                _mdl.AlertnativePhoneNumber = new ContractorServices().getalterntivephone(clientid, ref companyname);
                _mdl.CompanyName = companyname;
                _mdl.StateId = StateId;
                _mdl.CompanyId = clientid;
                _mdl.CityId = city_id;
                _mdl.Zipcode = zipcode;
                _mdl.Address = street;
                _mdl.MapValidateAddress = MapValidateAddress;
                _mdl.SuiteNumber = suite;
                var mser = new UserService();
                int userId = Convert.ToInt32(mser.AddNewUserWithoutRegister(_mdl, 0, workspaceid));

                List<int> _data = new List<int>();
                _data.Add(userId);
                new jobofferservices().sendoffers2(_data,true, true, template.sms, template.email, "Field Technician Account", false, workspaceid);
                new jobofferservices().AddUserSignupNotification(userId, workspaceid);
            }
            var type = Request.Cookies["_slist"] == null ? "E" : Convert.ToString(Request.Cookies["_slist"].Value.ToString());
            bool issubcontractor = type.ToLower() == "s" ? true : false;
            return RedirectToAction("view", new { token = _token, issubcontractor = issubcontractor });
        }


        [HttpPost]
        public ActionResult update_member(int euserid = 0, string emembertype = "", string efname = "", int eclientid = 0, string elname = "", string eemail = "", string ephone = "", string eMapValidateAddress = "", string esuite = "", string ePosition = "", int emanager_id = 0, int emember_id = 0)
        {
            int workspaceid = Convert.ToInt32(Request.Cookies["domain"].Value.ToString());
            ViewBag.permissions = _permission;
            int userid = Convert.ToInt32(Request.Cookies["_xid"].Value.ToString());
            string _token = new Crypto().EncryptStringAES(eclientid.ToString());
            UserService _services = new UserService();
            string street = ""; string city = ""; string state = ""; string zipcode = ""; int StateId = 0;
            eMapValidateAddress.GetAddress(ref street, ref city, ref state, ref zipcode);
            int city_id = new defaultservices().CreateCity(ref StateId, state, city);
            int roleid = _services.GetRoleIdByPositionType(workspaceid, emembertype);
            var message = _services.AssignTechnicianToCompany(euserid, emembertype, efname, eclientid, elname, eemail, ephone, street, zipcode, ePosition, emanager_id, userid, city_id, StateId, roleid, eMapValidateAddress, esuite);
            TempData["error"] = message;
            var type = Request.Cookies["_slist"] == null ? "E" : Convert.ToString(Request.Cookies["_slist"].Value.ToString());
            bool issubcontractor = type.ToLower() == "s" ? true : false;
            return RedirectToAction("view", new { token = _token, issubcontractor = issubcontractor });
        }


        public JsonResult Getmeberbyid(int id)
        {
            var mdl = new UserService().get_members_detail_byid(id);
            return Json(mdl, JsonRequestBehavior.AllowGet);
        }

        public ActionResult delete(string token)
        {
            if (!_permission.Contains(5082))
            {
                return View("unauth");
            }
            int id = Convert.ToInt32(new Crypto().DecryptStringAES(token));
            ContractorModal _model = new ContractorModal();
            ContractorServices _services = new ContractorServices();
            var message = _services.DeleteContractor(id);
            _model = new helper().GenerateError<ContractorModal>(_model, message);
            _model.PERMISSIONS = _permission;
            return View("Index", _model);
        }
        public ActionResult edit(string token)
        {
            if (!_permission.Contains(5081))
            {
                return View("unauth");
            }
            int id = Convert.ToInt32(new Crypto().DecryptStringAES(token));
            ContractorServices _services = new ContractorServices();
            ContractorModal_Item _item = _services.GetAllActiveContractorById(id);
            return View(_item);
        }
        public ActionResult view(string token, bool issubcontractor = false)
        {
            if (!_permission.Contains(5080))
            {
                return View("unauth");
            }
            int id = Convert.ToInt32(new Crypto().DecryptStringAES(token));
            ContractorViewModal _model = new ContractorViewModal();
            ContractorServices _services = new ContractorServices();
            if (TempData["error"] != null)
            {
                _model = new helper().GenerateError<ContractorViewModal>(_model, TempData["error"].ToString());
            }
            _model.Contractorid = id;
            _model.Contractors = _services.GetAllActiveContractorById(id);
            _model.PERMISSIONS = _permission;
            _model.issubcontractor = issubcontractor;
            return View(_model);
        } 

        public ActionResult delete_member(string clienttoken, string token)
        {
            if (!_permission.Contains(5087))
            {
                return View("unauth");
            }
            Crypto _crypt = new Crypto();
            int id = Convert.ToInt32(_crypt.DecryptStringAES(token));
            ContractorServices _ser = new ContractorServices();
            TempData["error"] = _ser.delete_member(id);
            var type = Request.Cookies["_slist"] == null ? "E" : Convert.ToString(Request.Cookies["_slist"].Value.ToString());
            bool issubcontractor = type.ToLower() == "s" ? true : false;
            return RedirectToAction("view", new { token = clienttoken, issubcontractor = issubcontractor });
        } 

       
        [HttpPost]
        public ActionResult update(ContractorModal_Item _item)
        {
            ContractorModal _model = new ContractorModal();
            ContractorServices _services = new ContractorServices();
            string street = ""; string city = ""; string state = ""; string zipcode = ""; int StateId = 0;
            _item.MapValidateAddress.GetAddress(ref street, ref city, ref state, ref zipcode);
            _item.city_id = new defaultservices().CreateCity(ref StateId, state, city);
            _item.state_id = StateId;
            _item.zipcode = zipcode;
            _item.address = street;
            var message = _services.UpdateContractor(_item);
            _model = new helper().GenerateError<ContractorModal>(_model, message);
            _model.PERMISSIONS = _permission;
            return View("Index", _model);
        }

        [HttpPost]
        public ActionResult updatecompany(ContractorModal_Item _item)
        {
            ContractorServices _services = new ContractorServices();
            string street = ""; string city = ""; string state = ""; string zipcode = ""; int StateId = 0;
            _item.MapValidateAddress.GetAddress(ref street, ref city, ref state, ref zipcode);
            _item.city_id = new defaultservices().CreateCity(ref StateId, state, city);
            _item.state_id = StateId;
            _item.zipcode = zipcode;
            _item.address = street;
            TempData["error"] = _services.UpdateContractor(_item); 
            var _token = new Crypto().EncryptStringAES(_item.Contractorid.ToString());
            return RedirectToAction("view", new { token = _token, issubcontractor = true });
        }
        public ActionResult update()
        {
            ContractorModal _model = new ContractorModal();
            _model.PERMISSIONS = _permission;
            return View("Index", _model);
        }

    }
}