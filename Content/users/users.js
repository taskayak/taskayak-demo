﻿function getjobsbypopup() {
    var stateid = $("#slide3_state").val();
    var city = $("#slide3_city").val();
    var jobid = $("#jobidpopup").val();
    var clientid = $("#slide3_client").val();
    var dataString = 'stateid=' + stateid + '&city=' + city + '&jobid=' + jobid + '&clientid=' + clientid;//, string  = "", string  = "", int  = 0;
    $.ajax
        ({
            type: "POST",
            url: '/jobs/getunassignjobsforpopup',
            data: dataString,
            cache: false,
            success: function (data) {
                $("#popupjobs").html('').prop('disabled', false).removeClass("selectpicker").append($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                $("#popupjobs").selectpicker('refresh');
                var total = document.getElementById("popupjobs").length - 1;
                $("#jobcount").html("Total Jobs: " + (total > 0 ? total : 0));
            }
        });

}
function getofferbypopup() {
    var clientid = $("#slide1_client").val();
    var stateid = $("#slide_state").val();
    var city = $("#slide_city").val();
    var jobid = $("#offers").val();
    var dataString = 'stateid=' + stateid + '&city=' + city + '&offerid=' + jobid + '&clientid=' + clientid;//, string  = "", string  = "", int  = 0;
    $.ajax
        ({
            type: "POST",
            url: '/offer/get_job_Offerforpopup',
            data: dataString,
            cache: false,
            success: function (data) {
                $("#offers").html('').prop('disabled', false).removeClass("selectpicker").append($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');;
                $("#offers").selectpicker('refresh');
                var total = document.getElementById("offers").length - 1;
                $("#offercount").html("Total Offers: " + (total > 0 ? total : 0));
            }
        });

}
$(document).ready(function () {
    $(".div_delete").css("display", "none");
    $(".slide-toggle1").on("click", function (e) {
        $("#slide_city").html('<option value="0">City</option>').selectpicker('refresh');
        $("#subject1").val('');
        $('#slide1_client').val(0).selectpicker('refresh');
        $("#offers").val(0).selectpicker('refresh');
        $('#slide_state').val(0).selectpicker('refresh');
        $("#prefrence").val(0).selectpicker('refresh');
        $(".sms").removeClass("bck"),
            $(".email").removeClass("bck"),
            $(".box").animate({
                width: "hide"
            });
        $("#backdrop").css("display", "block");
        $("#box1").animate({
            width: "show"
        });
        getofferbypopup();
    });
    function get_citybyid2(city_id, state_id) {
        var dataString = 'id=' + state_id + "&ismulti=0&city=" + city_id;
        $.ajax
            ({
                type: "POST",
                url: '/default/get_all_city_by_stateid',
                data: dataString,
                cache: false,
                success: function (data) {
                    $("#slide_city").prop('disabled', false).removeClass("selectpicker").html($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');;
                    $("#slide_city").selectpicker('refresh');
                }
            });
    }
    $("#offers").on("change", function (e) {
        var url = "/users/getofferbyid";
        var jobid = $("#offers").val();
        $.get(url, { 'jobid': jobid }, function (data) {
            $('#slide_state').val(data.StateId).selectpicker('refresh');
            $('#slide1_client').val(data.ClientId).selectpicker('refresh');
            get_citybyid2(data.CityId, data.StateId);
        });
    });
    $(".slide-toggle2").on("click", function (e) {
        $("#subject2").val('');
        $(".termspopup1").removeClass("bck"),
            $(".sms2").removeClass("bck"),
            $(".email2").removeClass("bck")
        $(".box").animate({
            width: "hide"
        });
        $("#backdrop").css("display", "block");
        $("#box2").animate({
            width: "show"
        });
    });
    function get_citybyid(city_id, state_id) {
        var dataString = 'id=' + state_id + "&ismulti=0&needLabel=false&city=" + city_id;
        $.ajax
            ({
                type: "POST",
                url: '/default/get_all_city_by_stateid',
                data: dataString,
                cache: false,
                success: function (data) {
                    $("#slide3_city").prop('disabled', false).removeClass("selectpicker").html($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                    $("#slide3_city").selectpicker('refresh');
                }
            });

    }
    $("#popupjobs").on("change", function (e) {
        var url = "/users/getjobbyid";
        var jobid = $("#popupjobs").val();
        $.get(url, { 'jobid': jobid }, function (data) {
            get_citybyid(data.CityId, data.StateId);
            $('#slide3_state').val(data.StateId).selectpicker('refresh');
            $('#slide3_client').val(data.ClientId).selectpicker('refresh');
            $(".selectpicker").selectpicker('refresh');
        });
    });
    $(".slide-toggle3").on("click", function (e) {
        $("#slide3_city").val(0).prop('disabled', true).selectpicker('refresh');
        $("#subject3").val('');
        $("#popupjobs").val(0);
        $("#popupjobs").selectpicker('refresh');
        $('#slide3_state').val(0).selectpicker('refresh');
        $('#slide3_client').val(0).selectpicker('refresh');
        getjobsbypopup();
        var total = document.getElementById("popupjobs").length - 1;
        $("#jobcount").html("Total Jobs: " + (total > 0 ? total : 0));
        $(".sms3").removeClass("bck"),
            $(".email3").removeClass("bck"),
            $(".box").animate({
                width: "hide"
            });
        $("#backdrop").css("display", "block");
        $("#box3").animate({
            width: "show"
        });
    });
    $(".closebox").on("click", function (e) {
        $(".box").animate({
            width: "hide"
        });
        $("#backdrop").css("display", "none");
    });
    $(".cuschk").on("click", function () {
        var id = $(this).data("id");
        if ($(this).hasClass("bck")) {
            $("#" + id).attr("checked", false);
            $(this).removeClass("bck");
        } else {
            $("#" + id).attr("checked", true);
            $(this).addClass("bck");
        }
    });
    $("#table").on("click", ".chk_header", function (e) {
        if (!$(this).hasClass("bck")) {
            $(this).addClass("bck");
            $(':checkbox').each(function () {
                if ($(this).attr("class") == "chk") {
                    this.checked = true;
                }
            });
            $('table .cuschk').each(function () {
                $(this).addClass("bck");
            });
            $(".div_delete").show();
        }
        else {
            $(this).removeClass("bck");
            $(':checkbox').each(function () {
                if ($(this).attr("class") == "chk") {
                    this.checked = false;
                }
            });
            $('table .cuschk').each(function () {
                $(this).removeClass("bck");
            });
            $(".div_delete").hide();
            $(".box").animate({
                width: "hide"
            });
            $("#backdrop").css("display", "none");
        }
    });
    $("#table").on("click", ".cuschk", function (e) {
        var id = $(this).data("id");
        if ($(this).hasClass("bck")) {
            $("#" + id).attr("checked", false);
            $(this).removeClass("bck");
        } else {
            $("#" + id).attr("checked", true);
            $(this).addClass("bck");
        }
        $('#chk_header').prop('checked', false);
        $('.chk_header').removeClass("bck");
        var len = $('tbody .bck').length;
        if (len > 0) {
            $(".div_delete").show();
        }
        else {
            $(".div_delete").hide();
            $(".box").animate({
                width: "hide"
            });
            $("#backdrop").css("display", "none");
        }
    });
    $("#table").on("click", ".stupdater", function (e) {
        e.preventDefault();
        var _selectedValue = $(this).html();
        var statusid = $(this).data("statusid");
        var memberId = $(this).data("memberid");
        $("#mid").val(memberId);
        //$("#mstatusid").val(statusid);
        $('#mstatusid option').each(function () {
            var $this = $(this);
            if ($this.text() == _selectedValue) {
                $("#mstatusid").val($this.val());
                return false;
            }
        });
        $('#mstatusid').selectpicker('refresh');
        $('#mdlstatus').modal('show');
    });
    $("#table").on("click", ".cdelete", function (e) {
        e.preventDefault();
        var id = this.id;
        bootbox.confirm("Are you sure?", function (result) {
            if (result) {
                var _url = "/users/index";
                window.location.href = _url + "?token=" + id;
            }
        });
    });
    $("#table").on("click", ".cdeactivate", function (e) {
        e.preventDefault();
        var id = this.id;
        bootbox.confirm("Are you sure?", function (result) {
            if (result) {
                var _url = "/users/index";
                window.location.href = _url + "?token=" + id + "&d=1";
            }
        });
    });
    $("#skills").multiselect({
        buttonContainer: '<div class="btn-group bootstrap-select fit-width" />', refresh: true,
        enableCaseInsensitiveFiltering: true, enableFiltering: true, buttonText: function (options, select) {
            return 'Skill';
        },
    });
    $("#amount").multiselect({
        buttonContainer: '<div class="bootstrap-select fit-width btn-group" />', refresh: true,
        enableCaseInsensitiveFiltering: false, enableFiltering: false, refresh: true, buttonText: function (options, select) {
            return 'Rate';
        },
    });
    get_template();
    function get_template() {
        var dataString = 'templateid=0&needLabel=false';
        $.ajax
            ({
                type: "POST",
                url: '/default/get_selected_template',
                data: dataString,
                cache: false,
                success: function (data) {
                    $("#templates2").html('').removeClass("selectpicker").append($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                    $("#templates").html('').removeClass("selectpicker").append($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                    $("#templates3").html('').removeClass("selectpicker").append($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                    $(".selectpicker").selectpicker('refresh');
                }
            });

    }
    get_client();
    function get_client() {
        var dataString = 'clientid=0';
        $.ajax
            ({
                type: "POST",
                url: '/default/get_all_clients',
                data: dataString,
                cache: false,
                success: function (data) {
                    $("#slide3_client").html('').removeClass("selectpicker").append($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                    $("#slide1_client").html('').removeClass("selectpicker").append($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                    $(".selectpicker").selectpicker('refresh');
                }
            });

    }
    function get_offers(statid, cntrl) {
        var dataString = 'stateId=' + statid;
        $.ajax
            ({
                type: "POST",
                url: '/default/get_job_OfferByState',
                data: dataString,
                cache: false,
                success: function (data) {
                    $("#" + cntrl).html('').removeClass("selectpicker").append($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                    var total = document.getElementById(cntrl).length - 1;
                    $("#offercount").html("Total Offers: " + (total > 0 ? total : 0));
                    $("#" + cntrl).selectpicker('refresh');
                }
            });

    }
    function get_offers_by_city(city, cntrl) {
        var dataString = 'city=' + city;
        $.ajax
            ({
                type: "POST",
                url: '/default/get_job_OfferBycity',
                data: dataString,
                cache: false,
                success: function (data) {
                    $("#" + cntrl).html('').removeClass("selectpicker").append($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                    var total = document.getElementById(cntrl).length - 1;
                    $("#offercount").html("Total Offers: " + (total > 0 ? total : 0));
                    $("#" + cntrl).selectpicker('refresh');
                }
            });

    }
    $("#state_id").change(function () {
        var id = $(this).val();
        if (id != 0) {
            var dataString = 'id=' + id + "&ismulti=1";
            $.ajax
                ({
                    type: "POST",
                    url: '/default/get_all_city_by_stateid',
                    data: dataString,
                    cache: false,
                    success: function (data) {
                        $('#city_id').multiselect('destroy');
                        $("#city_id").html('');
                        $("#city_id").html(data).removeAttr('disabled');
                        $("#city_id").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
                            buttonContainer: '<div class="btn-group citydiv" />', refresh: true, enableCaseInsensitiveFiltering: true, enableFiltering: true, buttonText: function (options, select) {
                                return 'City';
                            },
                        });
                    }
                });
        } else {
            $('#city_id').multiselect('destroy');
            $("#city_id").html('');
            $("#city_id").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
                buttonContainer: '<div class="btn-group citydiv" />', refresh: true, enableCaseInsensitiveFiltering: true, enableFiltering: true, buttonText: function (options, select) {
                    return 'City';
                },
            });
        }
    });
    $("#slide3_state").change(function () {
        var id = $(this).val();
        if (id != 0) {
            var dataString = 'id=' + id + "&ismulti=0";
            $.ajax
                ({
                    type: "POST",
                    url: '/default/get_all_city_by_stateid',
                    data: dataString,
                    cache: false,
                    success: function (data) {
                        $("#slide3_city").html('').prop('disabled', false).removeClass("selectpicker").append($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                        $(".selectpicker").selectpicker('refresh');
                    }
                });

        } else {
            $("#slide3_city").html('<option value="0">City</option>').addClass("selectpicker").selectpicker('refresh').prop('disabled', true);

        }
        getjobsbypopup()
    });
    $("#slide_state").change(function () {
        var id = $(this).val();
        if (id != 0) {
            var dataString = 'id=' + id + "&ismulti=0";
            $.ajax
                ({
                    type: "POST",
                    url: '/default/get_all_city_by_stateid',
                    data: dataString,
                    cache: false,
                    success: function (data) {
                        $("#slide_city").removeClass("selectpicker").html($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                        $("#slide_city").selectpicker('refresh');
                    }
                });

        }
        getofferbypopup();
    });
    $("#slide_city").on('change', function (e) {
        getofferbypopup();
    });
    getofferbypopup();
    getjobsbypopup();
    $("#slide3_city").on('change', function (e) {
        getjobsbypopup();
    });
    $("#jobidpopup").on('change', function (e) {
        getjobsbypopup();
    });
    $("#slide3_client").on('change', function (e) {
        getjobsbypopup();
    });
    $("#slide1_client").on('change', function (e) {
        getofferbypopup();
    });
});
$(document).ready(function () {
    var _amountselected = $("#amount").find("option:selected");
    _amountselected.each(function () {
        amountSelected = amountSelected + $(this).val() + ",";
    });
    var _skillsselected = $("#skills").find("option:selected");
    _skillsselected.each(function () {
        skillsSelected = skillsSelected + $(this).val() + ",";
    });
    otable = $('.data-table').DataTable({
        "processing": true,
        "serverSide": true,
        "autoWidth": false,
        'language': {
            'loadingRecords': '&nbsp;',
            'processing': "<img src='/Content/Images/loader.gif'> Loading..."
        }, "drawCallback": function (settings) { 
            settings._iDisplayLength = pageSize;
            $('.round:checkbox').each(function () {
                this.checked = false;
            });
        },
        "oLanguage": { "sSearch": "", 'sLengthMenu': "Show _MENU_" },
        "iDisplayLength": pageSize,
        "order": [],
        "bSort": false,
        "bFilter": true,
        "ajax": {
            "url": "/users/Fetchmembers",
            "data": function (d) {
                d.Location = $("#Filter_Location").val();
                //d.background = $("#background").val();
                //d.drug = $("#drug").val();
                d.status = $("#status").val();
                d.amount = amountSelected + "0";
                d.skills = skillsSelected + "0";
                d.searchtype = searchtype;
                d.membertype = $("#membertype").val();
                d.Role = $("#Roles").val();
                d.Contractor = $("#contractor").val();
                d.Screening = $("#background").val();
            }
        },
        "aoColumns": [
            {
                bSortable: false,
                mRender: function (data, type, full) { return '<div class="round pd0"> <input type="checkbox" id="chk_' + full.UserTableId + '" value="' + full.UserTableId + '" class="chk" />  <label for="checkbox" data-val="' + full.UserTableId + '" class="cuschk" data-id="chk_' + full.UserTableId + '"></label></div>'; }
            },
            {
                bSortable: false,
                mRender: function (data, type, full) {
                    return '<label style="font-weight: 400;">' + full.UserId + '</label>';
                }
            },
            {
                bSortable: false,
                mRender: function (data, type, full) {
                    if (editpermission == "TRUE") {
                        return '<a href="/users/edit?token=' + full.Token + '">' + full.FirstName + ' ' + full.LastName + '</a>';
                    }
                    else {
                        return '<a href="#" class="disabledbtn" data-msg="you are not authorized to view selected user">' + full.FirstName + ' ' + full.LastName + '</a>';
                    }
                }
            },
            { mData: "CityName" },
            { mData: "StateName" },
            { mData: "PhoneNumber" },
            { mData: "EmailAddress" },
            { mData: "HourlyRate" },
            {
                bSortable: false,
                mRender: function (data, type, full) {
                    return "<span id='spn_" + full.UserTableId + "' data-statusid='" + full.StatusId + "' data-memberid='" + full.UserTableId + "' class='stupdater memberstatus-" + full.StatusName.toLowerCase() + "'>" + full.StatusName + "</span>";
                }
            },
            {
                bSortable: false,
                mRender: function (data, type, full) {
                    var tbl = '<div class="btn-group pos-static"><a class="customedropdown" data-toggle="customedropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v fn21"></i> </a><div class="dropdown-menu customdropdown-menu" x-placement="bottom-start" style="top: 0px; transform: translate3d(-38px, 10px, 0px);will-change: transform;position: absolute;left: 0px; display: none;">';
                    if (editpermission == "TRUE") {
                        tbl = tbl + '<a class="btn btn-success btn-xs" href="/users/edit?token=' + full.Token + '"><i class="fa fa-edit"></i> Edit</a>';

                    }
                    else {
                        tbl = tbl + '<a href="#" class="disabledbtn btn btn-success btn-xs" data-msg="you are not authorized to edit selected user"><i class="fa fa-edit"></i> Edit</a>';
                    }
                    if (deletepermission == "TRUE") {
                        if (subcntr == "TRUE") {
                            tbl = tbl + '<a class="btn btn-danger btn-xs cdeactivate" id="' + full.Token + '"><i class="fa fa-trash"></i> Deactivate</a>';

                        } else {
                            tbl = tbl + '<a class="btn btn-danger btn-xs cdelete" id="' + full.Token + '"><i class="fa fa-trash"></i> Delete</a>';
                        }
                    }
                    else {
                        tbl = tbl + '<a href="#" class="disabledbtn btn btn-danger btn-xs" data-msg="you are not authorized to delete selected user"><i class="fa fa-trash"></i> Delete</a>';
                    }
                    tbl = tbl + '<a href="#" class="btn btn-info btn-xs" ><i class="fa fa-window-close"></i> Close</a>';

                    tbl = tbl + '</div></div>';
                    return tbl;
                }
            }

        ]
    });
    otable.on('length.dt', function (e, settings, len) {
        pageSize = len;  
    });
});
function ClearFilter() {
    pageSize = 25;
    amountSelected = "";
    searchtype = 2;
    skillsSelected = "";
    $("#background").val("0");
    $("#status").val("0");
    $("#membertype").val("M");
    $("#Roles").val("0");
    $("#contractor").val("0");
    $("#background").val("0");
    $(".chk_header").removeClass("bck");
    $('.selectpicker').selectpicker('refresh');
    $('input[type=search]').val('');
    $("#Filter_Location").val('');
    $('#amount').multiselect('deselectAll', false);
    $('#skills').multiselect('deselectAll', false);
    $("#filterpopup").css("display", "none"); 
    //$('.data-table').attr('data-page-length', 25);
    otable.page.len(pageSize).draw(true);
    ClearMultiDropDown('amount', 'amount', false);
    ClearMultiDropDown('skills', 'skills', false);
    ClearSingleDropDown('screening', 'background', false);
    ClearSingleDropDown('role', 'Roles', false);
    ClearSingleDropDown('contractor', 'contractor', false);
}
function FilterTable() {
    $("#filterpopup").css("display", "block");
    searchtype = 1;
    amountSelected = "";
    skillsSelected = "";
    var _amountselected = $("#amount").find("option:selected");
    _amountselected.each(function () {
        amountSelected = amountSelected + $(this).val() + ",";
    });
    var _skillsselected = $("#skills").find("option:selected");
    _skillsselected.each(function () {
        skillsSelected = skillsSelected + $(this).val() + ",";
    });
    otable.page.len(pageSize).draw(true);
}
function TriggerFilter() { 
    var IsFilterActive = true;
    searchtype = 0;
    amountSelected = "";
    skillsSelected = "";
    var Background = $("#background").val();//0
    var Status = $("#status").val();//0 
    var MemberType = $("#membertype").val();//M
    var Roles = $("#Roles").val();//0 
    var Contractor = $("#contractor").val();//0 
    var Search = $("input[type=search]").val();//'' 
    var Location = $("#Filter_Location").val();//'' 
    var _amountselected = $("#amount").find("option:selected");
    _amountselected.each(function () {
        amountSelected = amountSelected + $(this).val() + ",";
    });
    var _skillsselected = $("#skills").find("option:selected");
    _skillsselected.each(function () {
        skillsSelected = skillsSelected + $(this).val() + ",";
    });
    if (MemberType == "M" && amountSelected == "" && amountSelected == "" && Location == "" && Search == "" && Contractor == "0" && Roles == "0" && Status == "0" && Background == "0") {
        IsFilterActive = false;
    }
    if (IsFilterActive) {
        $("#filterpopup").css("display", "block");
    } else {
        $("#filterpopup").css("display", "none");
    }
    otable.page.len(pageSize).draw(true);
}
function Uploadfile() {
    $(".box").animate({
        width: "hide"
    });
    $("#box4").animate({
        width: "show"
    });
    $("#backdrop").css("display", "block");
}
function updatestatus() {
    var nid = $("#mid").val();
    var nsid = $("#mstatusid").val();
    var _url = "/users/update_status";
    $.get(_url, { 'id': nid, 'statusid': nsid }, function () {
        var _txt = $("#mstatusid").find("option:selected").text();
        var cls = $("#spn_" + nid).html();
        $("#spn_" + nid).html(_txt);
        $("#spn_" + nid).removeClass("memberstatus-" + cls.toLowerCase()).addClass("memberstatus-" + _txt.toLowerCase());
        $('#mdlstatus').modal('hide');
        alertify.log('User status updated succesfully', 'success', 5000);
    });
}
function sendoffer() {
    var offerId = $("#offers").val();
    if (offerId == 0) {
        alertify.log('You must select a offer to submit', 'error', 5000);
        return false;
    }
    var pref = $("#prefrence").val();
    if (pref == "0") {
        alertify.log('You must select a preference to submit', 'error', 5000);
        return false;
    }

    var templates = $("#templates").val();
    if (templates == 0 || templates == '') {
        alertify.log('You must select template', 'error', 5000);
        return false;
    }
    var subj = $("#subject1").val();
    if (subj == "") {
        alertify.log('You must add a email subject to submit', 'error', 5000);
        return false;
    }
    var len = $('.sendoffer .bck').length;
    if (len == 0) {
        alertify.log('You must select alert type SMS/Email submit', 'error', 5000);
        return false;
    }
    var array = [];
    $('tbody .bck').each(function () {
        array.push($(this).data("val"));
    });
    var _url = "/offer/sendOffer";
    $.get(_url, $.param({
        data: array,
        'tid': templates,
        'pref': pref,
        'offerId': offerId,
        'sms': $(".sms").hasClass("bck"),
        'email': $(".email").hasClass("bck"),
        'subject': subj
    }, true), function () {
        $("#box1").animate({
            width: "hide"
        });
        $("#backdrop").css("display", "none");
        alertify.log('Offer sent to selected Users succesfully', 'success', 5000);
    });
}
function sendoffer3() {
    var chklen = $('tbody .bck').length;
    if (chklen > 1) {
        alertify.log('You can select only one user to assign job', 'error', 5000);
        return false;
    }
    var jobid = $("#popupjobs").val();
    if (jobid == "0") {
        alertify.log('You must select a Job ID to submit', 'error', 5000);
        return false;
    }
    var subj = $("#subject3").val();
    if (subj == "") {
        alertify.log('You must add a email subject to submit', 'error', 5000);
        return false;
    }
    var len = $('.sendoffer3 .bck').length;
    if (len == 0) {
        alertify.log('You must select alert type SMS/Email submit', 'error', 5000);
        return false;
    }
    var templates = $("#templates3").val();
    if (templates == 0 || templates == '') {
        alertify.log('You must select template', 'error', 5000);
        return false;
    }
    var array = [];
    $('tbody .bck').each(function () {
        array.push($(this).data("val"));
    });
    var _url = "/offer/sendOffer5";
    $.get(_url, $.param({
        data: array,
        'tid': templates,
        'jobid': jobid,
        'sms': $(".sms3").hasClass("bck"),
        'email': $(".email3").hasClass("bck"),
        'subject': subj
    }, true), function () {
        $("#box3").animate({
            width: "hide"
        });
        $("#backdrop").css("display", "none");
        alertify.log('Job assigned to selected user succesfully', 'success', 5000);
    });
}
function sendoffer2() {
    var subj = $("#subject2").val();
    if (subj == "") {
        alertify.log('You must add a email subject to submit', 'error', 5000);
        return false;
    }
    var len = $('.sendoffer2 .bck').length;
    if (len == 0) {
        alertify.log('You must select alert type SMS/Email submit', 'error', 5000);
        return false;
    }
    var templates = $("#templates2").val();
    if (templates == 0 || templates == '') {
        alertify.log('You must select template', 'error', 5000);
        return false;
    }
    var array = [];
    $('tbody .bck').each(function () {
        array.push($(this).data("val"));
    });
    var _url = "/offer/sendOffer2";
    $.get(_url, $.param({
        data: array, 'subject': subj,
        'tid': templates,
        'termspopup': $(".termspopup1").hasClass("bck"),
        'sms': $(".sms2").hasClass("bck"),
        'email': $(".email2").hasClass("bck")
    }, true), function () {
        $("#box2").animate({
            width: "hide"
        });
        $("#backdrop").css("display", "none");
        alertify.log('Email/SMS sent to selected users succesfully', 'success', 5000);
    });
}
function get_city(city_id, state_id) {
    if (city_id == '') {
        $("#city_id").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
            buttonContainer: '<div class="btn-group citydiv" />', refresh: true, enableCaseInsensitiveFiltering: true, enableFiltering: true, buttonText: function (options, select) {
                return 'City';
            },
        });
    } else {
        var dataString = 'id=' + state_id + "&ismulti=1&city=" + city_id;
        $.ajax
            ({
                type: "POST",
                url: '/default/get_all_city_by_stateid',
                data: dataString,
                cache: false,
                success: function (data) {
                    $('#city_id').multiselect('destroy');
                    $("#city_id").html('');
                    $("#city_id").html(data).removeAttr('disabled');
                    $("#city_id").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
                        buttonContainer: '<div class="btn-group citydiv" />', refresh: true, enableCaseInsensitiveFiltering: true, enableFiltering: true, buttonText: function (options, select) {
                            return 'City';
                        },
                    });

                }
            });
    }
}
$(window).load(function () {
    setTimeout(function () {
        $("#filter_div").css("display", "block");
    }, 1500);
});
 