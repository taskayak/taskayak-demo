﻿var email_exist = "False";
var memberid_exist = "False";
var username_exist = "False";  
$("#email").on("change", function () {
    var _val = $("#email").val();
    if (_val != '') {
        var _url = "/users/validate";
        $.post(_url, { 'type': 1, 'value': _val }, function (data) {
            email_exist = data;
        }); 
    }
});
$("#member_id").on("change", function () {
    var _val = $("#member_id").val();
    if (_val != '') {
        var _url = "/users/validate";
        $.post(_url, { 'type': 2, 'value': _val }, function (data) {
            memberid_exist = data;
        });
    }
});

$('form').submit(function (e) { 
   
    if ($("form").validate()) {
        if (email_exist == "True") {
            alertify.log('Email address already assigned', 'error', 5000);
            $("#email").focus();
            email_exist = false;
            e.preventDefault();
            return false;
        }
        else if (memberid_exist == "True") {
            alertify.log('Userid already assigned', 'error', 5000);
            $("#member_id").focus();
            memberid_exist = false;
            e.preventDefault();
            return false;
        } else {
            return true;
        }
    }
});
$(document).ready(function () { 
    get_Manager_selectbyid("E");
    $("#skills").multiselect({
        enableCaseInsensitiveFiltering: true, buttonContainer: '<div class="btn-group skills" />', enableFiltering: true, buttonText: function (options, select) {
            return 'Skills';
        },
    });
    function get_Manager_selectbyid(usertype) {
        var dataString = 'type=' + usertype;
        $.ajax
            ({
                type: "POST",
                url: '/default/get_selcted_managers',
                data: dataString,
                cache: false,
                success: function (data) {
                    $("#Managerid").html('').prop('disabled', false).removeClass("selectpicker").append($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                }
            });

    }
    $("#membertype").on("change", function () {
        var _type = $("#membertype").val();
        get_Manager_selectbyid(_type);
        $("#Managerid").val(0).selectpicker('refresh');
    });
     
});