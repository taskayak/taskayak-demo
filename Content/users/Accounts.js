﻿
$(document).ready(function () {
    otable = $('.data-table').DataTable({
        "processing": true,
        "serverSide": true,
        "autoWidth": false,
        'language': {
            'loadingRecords': '&nbsp;',
            'processing': "<img src='/Content/Images/loader.gif'> Loading..."
        },
        "oLanguage": { "sSearch": "", 'sLengthMenu': "Show _MENU_" },
        "drawCallback": function (settings) {
            settings._iDisplayLength = pageSize; 
        },
        "iDisplayLength": pageSize,
        "order": [],
        "bSort": false,
        "bFilter": true, 
        "ajax": {
            "url": "/users/FetchAccounts",
            "data": function (d) {
                d.Location = $("#Filter_Location").val();
                d.status = $("#status").val();
                d.searchtype = searchtype;
                d.workspaceid = $("#workspace").val();
            }
        },
        "aoColumns": [
            {
                bSortable: false,
                mRender: function (data, type, full) {
                    return '<label style="font-weight: 400;">' + full.AccountNumber + '</label>';
                }
            },
            {
                bSortable: false,
                mRender: function (data, type, full) {
                    return '<a href="/users/ViewAccount?id=' + full.WorkspaceId + '">' + full.CompanyName + '</a>';
                }
            },
            { mData: "City" },
            { mData: "State" },
            { mData: "PhoneNumber" },
            { mData: "Email" },
            { mData: "PrimaryContact" },
            { mData: "TotalUser" },
            {
                bSortable: false,
                mRender: function (data, type, full) {
                    return "<span id='spn_" + full.AccountId + "' data-statusid='" + full.StatusId + "' data-memberid='" + full.AccountId + "' class='stupdater memberstatus-" + full.Status.toLowerCase() + "'>" + full.Status + "</span>";
                }
            },
            {
                bSortable: false,
                mRender: function (data, type, full) {
                    var tbl = '<div class="btn-group pos-static"><a class="customedropdown" data-toggle="customedropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v fn21"></i> </a><div class="dropdown-menu customdropdown-menu" x-placement="bottom-start" style="top: 0px; transform: translate3d(-38px, 10px, 0px);will-change: transform;position: absolute;left: 0px; display: none;">';
                    tbl = tbl + '<a class="btn btn-complete btn-xs" href="/users/ViewAccount?id=' + full.AccountId + '"><i class="fa fa-eye"></i> View</a>';
                    //tbl = tbl + '<a class="btn btn-success btn-xs" href="/users/view?token=' + full.AccountId + '"><i class="fa fa-edit"></i> Edit</a>';
                    //tbl = tbl + '<a class="btn btn-danger btn-xs cdelete" id="' + full.AccountId + '"><i class="fa fa-trash"></i> Delete</a>';
                    tbl = tbl + '<a href="#" class="btn btn-info btn-xs" ><i class="fa fa-window-close"></i> Close</a>';
                    tbl = tbl + '</div></div>';
                    return tbl;
                }
            }

        ]
    }); 
    otable.on('length.dt', function (e, settings, len) {
        pageSize = len; 
    });
});
$(document).ready(function () {
    $(".div_delete").css("display", "none");

});
function ClearFilter() { 
    pageSize = 25;
    searchtype = 2;
    $("#workspace").val("0");
    $("#status").val("0"); 
    $('.selectpicker').selectpicker('refresh');
    $('input[type=search]').val('');
    $("#Filter_Location").val('');
    $("#filterpopup").css("display", "none");
    otable.page.len(pageSize).draw(true);
}
function FilterTable() {
    $("#filterpopup").css("display", "block");
    searchtype = 1;
    otable.page.len(pageSize).draw(true);
}
function TriggerFilter() {
    var IsFilterActive = true;
    searchtype = 0; 
    var workspace = $("#workspace").val();//0
    var status = $("#status").val();//0  
    var Location = $("#Filter_Location").val(); 
    if (Location == "" && status == "0" && workspace == "0") {
        IsFilterActive = false;
    }
    if (IsFilterActive) {
        $("#filterpopup").css("display", "block");
    } else {
        $("#filterpopup").css("display", "none");
    }
    otable.page.len(pageSize).draw(true);
}

function updatestatus() {
    var nid = $("#mid").val();
    var nsid = $("#mstatusid").val();
    var _url = "/users/UpdateAccountStatus";
    $.get(_url, { 'id': nid, 'statusid': nsid }, function () {
        var _txt = $("#mstatusid").find("option:selected").text();
        var cls = $("#spn_" + nid).html();
        $("#spn_" + nid).html(_txt);
        $("#spn_" + nid).removeClass("memberstatus-" + cls.toLowerCase()).addClass("memberstatus-" + _txt.toLowerCase());
        $('#mdlstatus').modal('hide');
        alertify.log('Account status updated succesfully', 'success', 5000);
    });
}

$(document).ready(function () { 

    $("#table").on("click", ".stupdater", function (e) {
        e.preventDefault();
        var _selectedValue = $(this).html();
        var statusid = $(this).data("statusid");
        var memberId = $(this).data("memberid");
        $("#mid").val(memberId);
        //$("#mstatusid").val(statusid);
        $('#mstatusid option').each(function () {
            var $this = $(this);
            if ($this.text() == _selectedValue) {
                $("#mstatusid").val($this.val());
                return false;
            }
        });
        $('#mstatusid').selectpicker('refresh');
        $('#mdlstatus').modal('show');
    });
    $("#table").on("click", ".cdelete", function (e) {
        e.preventDefault();
        var id = this.id;
        bootbox.confirm("Are you sure?", function (result) {
            if (result) {
                var _url = "/users/index";
                window.location.href = _url + "?token=" + id;
            }
        });
    });
    $("#table").on("click", ".cdeactivate", function (e) {
        e.preventDefault();
        var id = this.id;
        bootbox.confirm("Are you sure?", function (result) {
            if (result) {
                var _url = "/users/index";
                window.location.href = _url + "?token=" + id + "&d=1";
            }
        });
    });

    GetSubDomain(F_workspace);
    function GetSubDomain(Workspaceid) {
        var dataString = 'Workspaceid=' + Workspaceid;
        $.ajax
            ({
                type: "POST",
                url: '/default/GetActiveWorkSpace',
                data: dataString,
                cache: false,
                success: function (data) {
                    $("#workspace").html('').removeClass("selectpicker").append($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                    $(".selectpicker").selectpicker('refresh');
                }
            });

    }
});
$(window).load(function () {
    setTimeout(function () { $("#filter_div").css("display", "block"); }, 1500);
});