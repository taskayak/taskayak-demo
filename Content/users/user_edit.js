﻿function adddoc() {
    $("#docupdate").modal("show");
}
function gettoolwindow(e) {
    $("#toolupdate").modal("show");
    $("#tooltype1").val(e);
    $("#_tooltype1").val(e);
}
$(".cuschk").on("click", function () {
    var e = $(this).data("id");
    $(this).hasClass("bck") ? ($("#" + e).attr("checked", !1), $(this).removeClass("bck")) : ($("#" + e).attr("checked", !0), $(this).addClass("bck"));
}),
    $(".elink").on("click", function () {
        var e = $(this).data("bankid");
        $("#bankid").val(e);
        var t = $(this).data("pan");
        $("#pan_number").val(t);
        var a = $(this).data("ifsc");
        $("#ifsc_code").val(a);
        var i = $(this).data("acnumber");
        $("#account_number").val(i);
        var n = $(this).data("acc_name");
        $("#account_name").val(n);
        var s = $(this).data("brnchname");
        $("#branch_name").val(s);
        var l = $(this).data("bname");
        $("#bank_name").val(l), $("#mdlupdate").modal("show");
    });
var email_exist = "False",
    memberid_exist = "False",
    username_exist = "False";
function get_state(e) {
    var t = "ismulti=0&stateid=" + e;
    $.ajax({
        type: "POST",
        url: "/default/get_all_state",
        data: t,
        cache: !1,
        success: function (e) {
            $("#state_id").removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
        },
    });
}
function get_Manager_selectbyid(e, t, a) {
    var i = "managerid=" + e + "&userid=" + t + "&type=" + a;
    $.ajax({
        type: "POST",
        url: "/default/get_selcted_managers",
        data: i,
        cache: !1,
        success: function (e) {
            $("#Managerid").removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
        },
    });
}
$("#email").on("change", function () {
    var e = $("#email").val();
    if ("" != e) {
        $.post("/users/validate", { type: 1, value: e }, function (e) {
            email_exist = e;
        });
    }
}),
    $("#member_id").on("change", function () {
        var e = $("#member_id").val();
        if ("" != e) {
            $.post("/users/validate", { type: 2, value: e }, function (e) {
                memberid_exist = e;
            });
        }
    }),
    $(document).ready(function () {
       
        $("#table").on("click", ".tbankDelete").click(function (e) {
            e.preventDefault();
            var t = this.id,
                a = $(this).data("memberid");
            bootbox.confirm("Are you sure?", function (e) {
                if (e) {
                    window.location.href = "/users/delete_bank?mtoken=" + a + "&token=" + t;
                }
            });
        }) 
            $("#skills").multiselect({
                enableCaseInsensitiveFiltering: !0,
                enableFiltering: !0,
                buttonText: function (e, t) {
                    return "Skills";
                },
            }),
                $(".data-table").DataTable({ bSort: !1, pageLength: 25, oLanguage: { "sSearch": "", 'sLengthMenu': "Show _MENU_" } }); 
        $(".dataTables_length").parent().addClass("pull-right");
        $(".dataTables_filter").parent().addClass("pull-left");
        $(".dataTables_length>label").addClass("pull-right");
        $(".dataTables_filter>label").addClass("pull-left");
        $(".dataTables_filter input").attr("placeholder", "Search");
        $(".dataTables_length select[name='table_length']").removeClass("form-control").addClass("Selectpicker");
        $(".dataTables_length select[name='table_length']").removeClass("form-control").addClass("Selectpicker"); 

       
    });
function GetUserDocuments(_UserId, _UserToken) {
    var i = "UserId=" + _UserId + "&token=" + _UserToken;
    $.ajax({
        type: "GET",
        url: "/users/GetUserDocument",
        data: i,
        cache: !1,
        success: function (e) {
            $("#DocPanel").html(e);
            $("#table").DataTable({ bSort: !1, pageLength: 25, oLanguage: { "sSearch": "", 'sLengthMenu': "Show _MENU_" } })
            $(".dataTables_length").parent().addClass("pull-right");
            $(".dataTables_filter").parent().addClass("pull-left");
            $(".dataTables_length>label").addClass("pull-right");
            $(".dataTables_filter>label").addClass("pull-left");
            $(".dataTables_filter input").attr("placeholder", "Search");
            $(".dataTables_length select[name='table_length']").removeClass("form-control").addClass("Selectpicker");
            $(".dataTables_length select[name='table_length']").removeClass("form-control").addClass("Selectpicker"); 

        },
    });
}
function GetUserTools(_UserId, _UserToken) {
    var i = "UserId=" + _UserId + "&token=" + _UserToken;
    $.ajax({
        type: "GET",
        url: "/users/GetUserTools",
        data: i,
        cache: !1,
        success: function (e) {
            $("#Tool").html(e); 
        },
    });
}
function GetUserSkills(_UserId, _UserToken) {
    var i = "UserId=" + _UserId + "&token=" + _UserToken;
    $.ajax({
        type: "GET",
        url: "/users/GetUserSkills",
        data: i,
        cache: !1,
        success: function (e) {
            $("#Skill").html(e);
        },
    });
} 
function GetClientTech(_UserId, _UserToken) {
    var i = "UserId=" + _UserId + "&token=" + _UserToken;
    $.ajax({
        type: "GET",
        url: "/users/GetClientList",
        data: i,
        cache: !1,
        success: function (e) {
            $("#client_tech").html(e);
        },
    });
}

$(window).load(function () { 
    setTimeout(function () { GetUserDocuments(_UserId, _UserToken) }, 800);
    setTimeout(function () { GetUserTools(_UserId, _UserToken) }, 500); 
    setTimeout(function () { GetUserSkills(_UserId, _UserToken) }, 1000); 
    setTimeout(function () { GetClientTech(_UserId, _UserToken) }, 1200); 
});
 