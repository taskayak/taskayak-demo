﻿function deleteReminder(formId, token) {
    bootbox.confirm("Are you sure?", function (result) {
        if (result) {
            var _url = "/reminder/deleteReminder";
            window.location.href = _url + "?token=" + token;
        }
    });
} 
function addReminder(_value) {
    $("#backdrop").css("display", "block");
    $(".box").animate({
        width: "hide"
    });
    $("#AddReminderSlide").animate({
        width: "show"
    });
    if (_value == "1") {
        $("#spanremindertype").html("Technician");
    } else {
        $("#spanremindertype").html("Dispatcher");
    }
    $("#usertype").val(_value);
}

$(".closebox").on("click", function (e) {
    $("#AddReminderSlide").animate({
        width: "hide"
    });
    $("#backdrop").css("display", "none");
    $("#usertype").val('1');
});

$("#reminderForm").on("submit", function () {
    var repeatId = $("#repeatid").val();
    var Attachment = $("#Attachment").val();
    var type = $("#type").val();
    var info = $("#info").val();
    if (repeatId == 0) {
        alertify.log('Please select calender setting (Hour,Day,Week,Month)', 'error', 5000);
        return false;
    } 
    if (info == null) {
        alertify.log('Please select information', 'error', 5000);
        return false;
    }
    if (type == null) {
        alertify.log('Please select type', 'error', 5000);
        return false;
    }
    return true;
});

$(document).ready(function () {
    $(".cuschk").on("click", function () {
        var id = $(this).data("id");
        if ($(this).hasClass("bck")) {
            $("#" + id).attr("checked", false);
            $(this).removeClass("bck");
        } else {
            $("#" + id).attr("checked", true);
            $(this).addClass("bck");
        }
        if ($("#" + id).hasClass("Confirmation")) {
            if ($(this).hasClass("bck")) {
                $("#" + id).val('1');
            } else {
                $("#" + id).val('0');
            }
        }
    });
});


$(".Attachment").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
    buttonContainer: '<div class="btn-group attchment" />', enableFiltering: false, buttonText: function (options, select) {
        return 'Attachment';
    },
});

$(".info").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
    buttonContainer: '<div class="btn-group information" />', enableFiltering: false, buttonText: function (options, select) {
        return 'Information';
    },
});
$(".type").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
    buttonContainer: '<div class="btn-group type" />', enableFiltering: false, buttonText: function (options, select) {
        return 'Type';
    },
});

$(".dinfo").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
    buttonContainer: '<div class="btn-group dinformation" />', enableFiltering: false, buttonText: function (options, select) {
        return 'Information';
    },
});
$(window).load(function () {
    $(".Autorefresh").each(function () {
        var _id = $(this).data('id');
        $(this).val(_id);
        $(".selectpicker").selectpicker("refresh");
    });
});