﻿$(document).ready(function () {
    
    var chart2;
    var chart1;
    chart2 = function getexpanse() {
        var get_expense;
        var _url = "/jobs/getjobofferdata";
        $.get(_url, function (get_expense) {
            var ch2 = new Highcharts.chart({
                chart: {
                    type: 'column',
                    renderTo: 'expenseid',
                    style: {
                        fontFamily: 'Poppins'
                    }
                },
                title: {
                    text: 'Monthly Jobs and Offers',
                    align: 'left'
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: [
                        'Jan',
                        'Feb',
                        'Mar',
                        'Apr',
                        'May',
                        'Jun',
                        'Jul',
                        'Aug',
                        'Sep',
                        'Oct',
                        'Nov',
                        'Dec'
                    ],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    allowDecimals: false,
                    title: {
                        text: ''

                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y} </b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Jobs',
                    data: get_expense._jobs,
                    color: 'var(--green)'
                }, {
                    name: 'Offers',
                    data: get_expense._offer,
                    color: 'var(--blue)'
                }]
            }, function (chart1) {
                var arr = chart1.options.exporting.buttons.contextButton.menuItems;
                var index = arr.indexOf("viewData");
                if (index !== -1) arr.splice(index, 1);
            });
        });

    } 
    chart1 = function getusers() {
        var userdata;
        var _url1 ="/users/rolesbycount";
        $.get(_url1, function (userdata) {
            var ch1 = new Highcharts.chart({
                chart: {
                    renderTo: 'supportTickets',
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    style: {
                        fontFamily: 'Poppins'
                    }
                },
                title: {
                    text: 'Users',
                    align: 'left'
                },
                legend: {
                    enabled: true,
                    labelFormatter: function () {
                        var total = 0, percentage; $.each(this.series.data, function () { total += this.y; });
                        return this.name + ' (<span>' + this.y + ') ';
                    }

                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y}</b>'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },

                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true

                    }
                },
                series: [{
                    name: 'Total',
                    colorByPoint: true,
                    data: userdata
                }]
            }, function (chart) {
                var arr = chart.options.exporting.buttons.contextButton.menuItems;
                var index = arr.indexOf("viewData");
                if (index !== -1) arr.splice(index, 1);
            });
        });

    } 
    chart1();
    chart2();
    chart2();

 $('#table1').DataTable({
        "processing": true,
        "serverSide": true,
        'language': {
            'loadingRecords': '&nbsp;',
            'processing': "<img src='/Content/Images/loader.gif'> Loading..."
        },
        "iDisplayLength": 10,
        "order": [],
        "bSort": false,
     "bFilter": false,
     "bPaginate": false,
     "bLengthChange": false, 
     "bInfo": false, 
        "autoWidth": false,
        "ajax": {
            "url": "/home/Fetchjobs"
        },
        "aoColumns": [
            { mData: "Jobid" },
            { mData: "clientname" },
            { mData: "Technician" },
            { mData: "startdate" },
            { mData: "rate" }  
        ]
 });

    $('#table2').DataTable({
        "processing": true,
        "serverSide": true,
        'language': {
            'loadingRecords': '&nbsp;',
            'processing': "<img src='/Content/Images/loader.gif'> Loading..."
        },
        "iDisplayLength": 10,
        "order": [],
        "bSort": false,
        "bFilter": false,
        "bPaginate": false,
        "bLengthChange": false,
        "bInfo": false, 
        "autoWidth": false,
        "ajax": {
            "url": "/home/Fetchoffers"
        },
        "aoColumns": [
            { mData: "Jobid" },
            { mData: "clientname" },
            { mData: "Dispatcher" },
            { mData: "startdate" },
            { mData: "rate" }
        ]
    });

});