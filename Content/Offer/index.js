﻿$(document).ready(function () {
    var _skillsselected = $("#Skiil").find("option:selected");
    _skillsselected.each(function () {
        skillsSelected = skillsSelected + $(this).val() + ",";
    });
    
    otable = $('.data-table').DataTable({
        "processing": true,
        "serverSide": true,
        'language': {
            'loadingRecords': '&nbsp;',
            'processing': "<img src='/Content/Images/loader.gif'> Loading..."
        }, "oLanguage": { "sSearch": "", 'sLengthMenu': "Show _MENU_" }
        , "drawCallback": function (settings) {
            debugger;
            settings._iDisplayLength = pageSize;

        },
        "iDisplayLength": pageSize,
        "order": [],
        "bSort": false,
        "bFilter": true,
        "autoWidth": false,
        "ajax": {
            "url": "/offer/Fetchadminjobs",
            "data": function (d) {
                d.date = date;
                d.type = 1;
                d.Location = Location;
                d.cclient = cclient;
                d.cDispatcher = cDispatcher;
                d.Status = Status;
                d.searchtype = searchtype;
                d.project = ProjectId;
                d.PmanagerId = PmanagerId;
                d.cmanagerId = cmanagerId;
                d.screening = $("#Screening").val();
                d.Skiil = skillsSelected;  
            }
        },
        "aoColumns": [
            {
                bSortable: false,
                mRender: function (data, type, full) {
                    return '<span>' + full.Job_ID + '</span>';
                }
            },
            {
                bSortable: false,
                mRender: function (data, type, full) {
                    if (viewpermission == "TRUE") {
                        return '<a  href="/offer/view?token=' + full.token + '">' + full.Title + '</a>';
                    }
                    else {
                        return '<a href="#" class="disabledbtn" data-msg="you are not authorized to view selected Offer">' + full.Title + '</a>';
                    }
                }
            },
            {
                bSortable: false,
                mRender: function (data, type, full) {
                    return '<p id="member_' + full.JobID + '">' + full.Dispatcher + '</p>';
                }
            },
            { mData: "cityname" },
            { mData: "statename" },
            { mData: "Client" },
            { mData: "sdate" },
            { mData: "stime" },
            { mData: "etime" },
            { mData: "Client_rate" },
            {
                bSortable: false,
                mRender: function (data, type, full) {
                    return "<span id='rate_" + full.JobID + "'>" + full.Pay_rate + "</span>";
                }
            },
            {
                bSortable: false,
                mRender: function (data, type, full) {
                    return '<p style="font-size:11px;"><span>' + full.view + ' Viewed</span><br/><span>' + full.accepted + ' Accepted</span></p>';
                }
            },
            {
                bSortable: false,
                mRender: function (data, type, full) {
                    var _chatbody = "<span id='commentbadge_" + full.JobID + "' class='notification' data-id='" + full.JobID + "'>" + full.commentCount + "</span>";
                    if (full.commentCount == "0") {
                        _chatbody = "<span id='commentbadge_" + full.JobID + "' class='notification' data-id='" + full.JobID + "'>--</span>";
                    }
                    return _chatbody;
                },
                className: "centertext",
            },
            {
                bSortable: false,
                mRender: function (data, type, full) {
                    return "<span id='spn_" + full.JobID + "' data-statusid='" + full.status_id + "' data-memberid='" + full.JobID + "' class='stupdater " + full.Status.replace(" ", "").toLowerCase() + "'>" + full.Status + "</span>";
                }
            },
            {
                bSortable: false,
                mRender: function (data, type, full) {
                    var tbl = '<div class="btn-group pos-static"><a class="customedropdown" data-toggle="customedropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v fn21"></i> </a><div class="dropdown-menu customdropdown-menu" x-placement="bottom-start" style="top: 0px; transform: translate3d(-110px, 10px, 0px);will-change: transform;position: absolute;left: 0px; display: none;">';
                    if (viewpermission == "TRUE") {
                        tbl = tbl + '<a class="btn btn-complete btn-xs" href="/offer/view?token=' + full.token + '"><i class="fa fa-eye"></i> View</a>';

                    }
                    else {
                        tbl = tbl + '<a href="#" class="disabledbtn btn btn-complete btn-xs" data-msg="you are not authorized to edit selected Offer"><i class="fa fa-eye"></i> View</a>';
                    }
                    if (editpermission == "TRUE") {
                        tbl = tbl + '<a class="btn btn-success btn-xs" href="/offer/edit?token=' + full.token + '"><i class="fa fa-edit"></i> Edit</a>';

                    }
                    else {
                        tbl = tbl + '<a href="#" class="disabledbtn btn btn-success btn-xs" data-msg="you are not authorized to edit selected Offer"><i class="fa fa-edit"></i> Edit</a>';
                    }
                    if (deletepermission == "TRUE") {
                        tbl = tbl + '<a class="btn btn-danger btn-xs cdelete" id="' + full.token + '"><i class="fa fa-trash"></i> Delete</a>';

                    }
                    else {
                        tbl = tbl + '<a href="#" class="disabledbtn btn btn-danger btn-xs" data-msg="you are not authorized to delete selected Offer"><i class="fa fa-trash"></i> Delete</a>';
                    }
                    tbl = tbl + ' <a class="btn btn-info btn-xs"><i class="fa fa-window-close"></i> Close</a>';
                    tbl = tbl + '</div></div>';
                    return tbl;
                }
            }

        ]
    });


    otable.on('length.dt', function (e, settings, len) { 
        pageSize = len; 
    });
}); 

function TriggerFilter() {
    debugger;
    var IsFilterActive = true;
    searchtype = 0;
    skillsSelected = "";
    var _skillsselected = $("#Skiil").find("option:selected");
    _skillsselected.each(function () {
        skillsSelected = skillsSelected + $(this).val() + ",";
    });
    cclient = $("#cclient").val();
    cDispatcher = $("#cDispatcher").val();
    ProjectId = $("#fprojectId").val(); 
    date = $("#date").val();
    Location = $("#Filter_Location").val();
    Status = $("#Status").val();
    PmanagerId = $("#cProjectManager").val();
    cmanagerId = $("#cClientManager").val(); 
    var screening = $("#Screening").val(); 
    if (Status == "0" &&cclient == "0" && cDispatcher == "0" && ProjectId == "0" && date == "" && skillsSelected == "" && Location == "" && screening == "0" && cmanagerId == "0" && PmanagerId == "0") {
        IsFilterActive = false;
    }
    if (IsFilterActive) {
        $("#filterpopup").css("display", "block");
    } else {
        $("#filterpopup").css("display", "none");
    }
    otable.page.len(pageSize).draw(true);
}

function GetClientManager(id, eleid, clientid) {
    if (id != 0) {
        var dataString = 'id=' + id + '&clientid=' + clientid;
        $.ajax
            ({
                type: "POST",
                url: '/default/get_all_manager_by_clientid',
                data: dataString,
                cache: false,
                success: function (e) {
                    $("#" + eleid).removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                    $(".selectpicker").selectpicker('refresh'); 
                }
            });
    }
    getClientrate('uplodclient', 'upclientRate');
}
$("#frmadd").on("submit", function () {
    if ($("#frmadd").valid()) {
        var end_time = $('#etime').val();
        var start_time = $('#stime').val();
        var stt = new Date("May 26, 2016 " + start_time);
        stt = stt.getTime();
        var endt = new Date("May 26, 2016 " + end_time);
        endt = endt.getTime();
        if (stt > endt) {
            alertify.log('End time can not be less then start time', 'error', 5000);
            return false;
        }
        return true;
    }
});
function sendMessage() {
    var id = $("#mainjid").val();
    var _body = $("#messagebox").val();
    if (_body == '') {
        alertify.log("Please write message", "error", 2000);
        return false;
    }
    var _url = "/offer/Sendmessage";
    $.post(_url, { 'Comment': _body, 'JobID': id }, function (data) {
        $("#commentarea").html('<textarea class="form-control textarea-wysihtml5" rows="6" id="messagebox" placeholder="Please write text here...."></textarea>');
        $('.textarea-wysihtml5').wysihtml5({
            toolbar: {
                'fa': true
            }
        });
        alertify.log("Message Sent", "success", 2000);
    });
}
function ClearFilter() {
    ClearText('date', 'date', false);
    ClearSingleDropDown('dispatcher','cDispatcher',  false);
    ClearSingleDropDown('pmanager','cProjectManager',  false);
    ClearSingleDropDown('cmanager','cClientManager',  false);
    ClearMultiDropDown('skills', 'Skiil', false);
    ClearSingleDropDown('screening','Screening', false); 
    searchtype = 2;
    $("#date").val('');
    $("#cclient").val('0');
    $("#fprojectId").val('0');
    $("#cDispatcher").val('0');
    $("#Status").val('0');
    $("#cProjectManager").val('0');
    $("#cClientManager").val('0');
    $("#Screening").val('0');
    $('.selectpicker').selectpicker('refresh');
    $("#filterpopup").css("display", "none");
    $('input[type=search]').val('');
    $("#Filter_Location").val('');
    $('#Skiil').multiselect('deselectAll', false);
    skillsSelected = "";
    cclient = $("#cclient").val();
    cDispatcher = $("#cDispatcher").val();
    ProjectId = $("#fprojectId").val();
    date = $("#date").val();
    Location = $("#Filter_Location").val();
    Status = $("#Status").val();
    PmanagerId = $("#cProjectManager").val();
    cmanagerId = $("#cClientManager").val(); 
    pageSize = 25; debugger;
    otable.page.len(pageSize).draw(true);
}
function FilterTable() {
    $("#filterpopup").css("display", "block");
    var _skillsselected = $("#Skiil").find("option:selected");
    _skillsselected.each(function () {
        skillsSelected = skillsSelected + $(this).val() + ",";
    });
    cclient = $("#cclient").val();
    cDispatcher = $("#cDispatcher").val();
    ProjectId = $("#fprojectId").val();
    date = $("#date").val();
    Location = $("#Filter_Location").val();
    Status = $("#Status").val();
    PmanagerId = $("#cProjectManager").val();
    cmanagerId = $("#cClientManager").val(); 
    searchtype = 1; debugger;
    otable.page.len(pageSize).draw(true);
} 
 
function get_template(cnrtl) {
    var dataString = 'templateid=0&needLabel=False';
    $.ajax
        ({
            type: "POST",
            url: '/default/get_selected_template',
            data: dataString,
            cache: false,
            success: function (data) {
                $("#" + cnrtl).html('').removeClass("selectpicker").append($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                $(".selectpicker").selectpicker('refresh'); 
            }
        });

}
function ProjectManagerDropDown(id) {
    var dataString = 'ismulti=0&id=0';
    $.ajax
        ({
            type: "POST",
            url: '/default/get_all_mebertypeprojectmanager',
            data: dataString,
            cache: false,
            success: function (e) {
                $("#uploadProject_manager1").removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                $("#Project_manager1").removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                $(".selectpicker").selectpicker('refresh'); 
            }
        });

}
function get_projectmgr_byclient(id) {
    var dataString = 'ismulti=0&id=0';
    $.ajax
        ({
            type: "POST",
            url: '/default/get_all_mebertypeprojectmanager',
            data: dataString,
            cache: false,
            success: function (e) {
                $("#" + id).removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                $(".selectpicker").selectpicker('refresh'); 
            }
        });

}
$("#projectId").on("change", function () {
    var prjid = $("#projectId").val();
    if (prjid != 0) {
        var dataString = 'id=' + prjid;
        $.ajax
            ({
                type: "POST",
                url: '/project/GetProjectDetails',
                data: dataString,
                cache: false,
                success: function (data) {
                    $("#client").val(data.clientId).selectpicker('refresh');
                    //$("#Project_manager").val(data.ClientManagerId).selectpicker('refresh');
                    $("#Project_manager1").val(data.projectManagerId).selectpicker('refresh');
                    GetClientManager(data.clientId, 'Project_manager', data.ClientManagerId);
                    $(".selectpicker").selectpicker('refresh');  
                }
            });

    }
}); 
$("#uplodproject").on("change", function () {
    var prjid = $("#uplodproject").val();
    if (prjid != 0) {
        var dataString = 'id=' + prjid;
        $.ajax
            ({
                type: "POST",
                url: '/project/GetProjectDetails',
                data: dataString,
                cache: false,
                success: function (data) {

                    $("#uplodclient").val(data.clientId).selectpicker('refresh');
                    //$("#uploadProject_manager").val(data.ClientManagerId).selectpicker('refresh');
                    $("#uploadProject_manager1").val(data.projectManagerId).selectpicker('refresh');
                    GetClientManager(data.clientId, 'uploadProject_manager', data.ClientManagerId);
                    $(".selectpicker").selectpicker('refresh'); 
                }
            });

    }
});
$("#JobId").on("change", function (e) {
    var _url = "/offer/validatejobId";
    var jobid = $("#JobId").val();
    $.get(_url, { 'id': jobid }, function (data) {
        if (data == 'True') {
            return true;
        } else {
            alertify.log('OfferId ' + jobid + ' already in use', 'error', 5000);
            $("#JobId").val('');
            return false;
        }
    });
});  
function getClientrate(cntrl, rateCntrl) {
    var client = $("#" + cntrl).val();
    if (client != 0) {
        var _url = "/client/get_client_rate" + "?id=" + client;
        $.get(_url, function (data) {
            $("#" + rateCntrl).val(data);
        });
    } else {
        $("#" + rateCntrl).val("$1");
    }
}
function getrateTechnician() {
    var memberid = $("#Technician").val();
    if (memberid != 0) {
        var _url = "/member/get_member_rate" + "?id=" + memberid;
        $.get(_url, function (data) {
            $("#techrate").val(data);
        });
    } else {
        $("#techrate").val("$0.00");
    }
} 
function updatestatus() {
    var nid = $("#mid").val();
    var nsid = $("#mstatusid").val();
    if (nsid == 0) {
        alertify.log('please select Offer status', 'error', 5000);
        return false;
    }
    var _url = "/offer/update_status";
    $.get(_url, { 'id': nid, 'statusid': nsid }, function () {
        var _txt = $("#mstatusid").find("option:selected").text();
        var cls = $("#spn_" + nid).html();
        $("#spn_" + nid).html(_txt);
        $("#spn_" + nid).removeClass(cls.replace(" ", "").toLowerCase()).addClass(_txt.toLowerCase().replace(" ", "").toLowerCase());
        $('#mdlstatus').modal('hide');
        alertify.log('Offer status updated succesfully', 'success', 5000);
    });
}
function SendReply(textid, commentid, jobid, divid) {
    var _val = $("#" + textid).val();
    if (_val == '') {
        alertify.log('Reply can not be empty', 'error', 5000);
        return false;
    }
    var _url = "/offer/add_comment";
    $.get(_url, { 'txt': _val, 'cmntid': commentid, 'jobid': jobid }, function (data) {
        alertify.log('Reply Sent', 'success', 5000);
        $("#" + textid).val('');
        $("#" + divid + " table tbody").append(data);
    });
} 
function OpenRow(id, tr1) {
    $(".hdntextare").css("display", "none");
    $("#" + id).css("display", "table-row");
    $("#" + tr1).css("display", "table-row");
}
$(document).ready(function () { 
    //$(".cuschk").on("click", function () {
    //    var id = $(this).data("id");
    //    if ($(this).hasClass("bck")) {
    //        $("#" + id).attr("checked", false);
    //        $(this).removeClass("bck");
    //    } else {
    //        $("#" + id).attr("checked", true);
    //        $(this).addClass("bck");
    //    }
    //}); 
    $("#table").on("click", ".cdelete", function (e) {
        e.preventDefault();
        var id = this.id;
        bootbox.confirm("Are you sure?", function (result) {
            if (result) {
                var _url = "/offer/index";
                window.location.href = _url + "?token=" + id;
            }
        });
    });
    $("#table").on("click", ".notification", function (e) {
        e.preventDefault();
        var id = $(this).data("id");
        var _url = "/offer/getnotifications";
        $.get(_url, { 'jobid': id }, function (data) {
            $("#notificationmodal").html(data);
            $("#mainjid").val(id);
            $('#messagemodal').modal('show');
        });



    });
    $("#table").on("click", ".stupdater", function (e) {
        e.preventDefault();
        var statusid = $(this).data("statusid");
        var memberId = $(this).data("memberid");
        var _selectedValue = $(this).html();
        $("#mid").val(memberId);
        $('#mstatusid option').each(function () {
            var $this = $(this);
            if ($this.text() == _selectedValue) {
                $("#mstatusid").val($this.val());
                return false;
            }
        });
        $('#mstatusid').selectpicker('refresh');  
        $('#mdlstatus').modal('show');
    }); 
    $("#table").on("click", ".assign", function (e) {
        e.preventDefault();
        var jobid = $(this).data("jobid");
        $('#assignjobid').val(jobid);
        $("#assignmemberrate").val('');
        $("#assigncity_id").html('<option value="0">City</option>').attr('disabled', true).selectpicker('refresh');
        $("#assignTechnician").html('<option value="0">Technician</option>').attr('disabled', true).selectpicker('refresh');
        $('#assigntech').modal('show');  
    });  
   
    $("#client").change(function () {
        var id = $(this).val();
        if (id != 0) {
            var dataString = 'id=' + id;
            $.ajax
                ({
                    type: "POST",
                    url: '/default/get_all_manager_by_clientid',
                    data: dataString,
                    cache: false,
                    success: function (e) {
                        $("#Project_manager").removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                        $(".selectpicker").selectpicker('refresh');  
                    }
                });
        } else {
            $("#Project_manager").html('<option value="0">Manager</option>').attr('disabled', true).selectpicker('refresh');
        }
        getClientrate('client', 'c_techrate');
    }); 
    $("#uplodclient").change(function () {
        var id = $(this).val();
        if (id != 0) {
            var dataString = 'id=' + id;
            $.ajax
                ({
                    type: "POST",
                    url: '/default/get_all_manager_by_clientid',
                    data: dataString,
                    cache: false,
                    success: function (e) {
                        $("#uploadProject_manager").removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                        $(".selectpicker").selectpicker('refresh');  
                    }
                });
        } else {
            $("#uploadProject_manager").html('<option value="0">Manager</option>').attr('disabled', true).selectpicker('refresh');
        }
        getClientrate('uplodclient', 'upclientRate');
    }); 
   
    $("#state_filter_id").change(function () {
        var id = $(this).val();
        if (id != 0) {
            // $("#city_id").html('');
            var dataString = 'id=' + id + "&ismulti=1";
            $.ajax
                ({
                    type: "POST",
                    url: '/default/get_all_city_by_stateid',
                    data: dataString,
                    cache: false,
                    success: function (data) {
                        $('#city_filter_id').multiselect('destroy');
                        $("#city_filter_id").html('');
                        $("#city_filter_id").html(data).removeAttr('disabled');
                        $("#city_filter_id").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
                            enableCaseInsensitiveFiltering: true, enableFiltering: true, buttonText: function (options, select) {
                                return 'City';
                            },
                        });
                    }
                });
        } else {
            $('#city_filter_id').multiselect('destroy');
            $("#city_filter_id").html('');
            $("#city_filter_id").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
                enableCaseInsensitiveFiltering: true, enableFiltering: true, buttonText: function (options, select) {
                    return 'City';
                },
            });
            //  $("#city_id").html('<option value="0">City</option>').attr('disabled',true);
        }
    });
    $("#state_id").change(function () {
        var id = $(this).val();
        if (id != 0) {
            var dataString = 'id=' + id;
            $.ajax
                ({
                    type: "POST",
                    url: '/default/get_all_city_by_stateid',
                    data: dataString,
                    cache: false,
                    success: function (e) {
                        $("#city_id").removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                        $(".selectpicker").selectpicker('refresh');  
                    }
                });
        } else {
            $("#city_id").html('<option value="0">City</option>').attr('disabled', true).selectpicker('refresh');
        }
    }); 
    $("#assignstate_id").change(function () {
        var id = $(this).val();
        if (id != 0) {
            var dataString = 'id=' + id;
            $.ajax
                ({
                    type: "POST",
                    url: '/default/get_all_city_by_stateid',
                    data: dataString,
                    cache: false,
                    success: function (e) {
                        $("#assigncity_id").removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                        $(".selectpicker").selectpicker('refresh');  
                    }
                });
        } else {
            $("#assigncity_id").html('<option value="0">City</option>').attr('disabled', true).selectpicker('refresh');
        }
    });
    $("#assigncity_id").on("change", function () {
        var id = $(this).val();
        if (id != 0) {
            var dataString = 'cityid=' + id + "&optvalue=Technician";
            $.ajax
                ({
                    type: "POST",
                    url: '/default/get_selcted_member_bycity',
                    data: dataString,
                    cache: false,
                    success: function (e) {
                        $("#assignTechnician").removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                        $(".selectpicker").selectpicker('refresh');  
                    }
                });
        } else {
            $("#assignTechnician").html('<option value="0">Technician</option>').attr('disabled', true).selectpicker('refresh');
        }
    }); 
});

function get_Manager_selectbyid(_val, cntrlId, memberid, type) {
    var dataString = 'optvalue=' + _val + "&memberid=" + memberid + "&type=" + type;
    $.ajax
        ({
            type: "POST",
            url: '/default/get_all_member',
            data: dataString,
            cache: false,
            success: function (e) {
                $("#" + cntrlId).removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                $(".selectpicker").selectpicker('refresh');  
            }
        });

}
function CreateManagerDropDown(_val, cntrlId, memberid, type) {
    var dataString = 'optvalue=' + _val + "&memberid=" + memberid + "&type=" + type;
    $.ajax
        ({
            type: "POST",
            url: '/default/get_all_member',
            data: dataString,
            cache: false,
            success: function (e) {
                $("#upload_dispatcher").removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                $("#Dispatcher").removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                $(".selectpicker").selectpicker('refresh');  
            }
        });
}
function get_city(city_id, state_id) {

    if (city_id == '') {

        $("#city_filter_id").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
            enableCaseInsensitiveFiltering: true, enableFiltering: true, buttonText: function (options, select) {
                return 'City';
            },
        });
    } else {


        var dataString = 'id=' + state_id + "&ismulti=1&city=" + city_id;
        $.ajax
            ({
                type: "POST",
                url: '/default/get_all_city_by_stateid',
                data: dataString,
                cache: false,
                success: function (data) {
                    $('#city_filter_id').multiselect('destroy');
                    $("#city_filter_id").html('');
                    $("#city_filter_id").html(data).removeAttr('disabled');
                    $("#city_filter_id").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
                        enableCaseInsensitiveFiltering: true, enableFiltering: true, buttonText: function (options, select) {
                            return 'City';
                        },
                    });

                }
            });
    }

}
function get_client(client, clientid) {
    var dataString = 'ismulti=0&clientid=' + clientid;
    $.ajax
        ({
            type: "POST",
            url: '/default/get_all_clients',
            data: dataString,
            cache: false,
            success: function (e) {
                $("#" + client).removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                $(".selectpicker").selectpicker('refresh');  
            }
        });

}
function CreateClientDropDown(client, clientid) {
    var dataString = 'ismulti=0&clientid=' + clientid;
    $.ajax
        ({
            type: "POST",
            url: '/default/get_all_clients',
            data: dataString,
            cache: false,
            success: function (e) {
                $("#uplodclient").removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                $("#client").removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                $(".selectpicker").selectpicker('refresh'); 
            }
        });
} 
function getrate() {
    var memberid = $("#assignTechnician").val();
    if (memberid != 0) {
        var _url = "/member/get_member_rate?id=" + memberid;
        $.get(_url, function (data) {
            $("#assignmemberrate").val(data);
        });
    } else {
        $("#assignmemberrate").val("$0");
    }
}
function Uploadfile() {
    $("#backdrop").css("display", "block");
    $(".box").animate({
        width: "hide"
    });
    $("#uploadslide").animate({
        width: "show"
    });
}
$(".closebox").on("click", function (e) {
    $("#uploadslide").animate({
        width: "hide"
    });
    $("#backdrop").css("display", "none");
});

$(window).load(function () {
    setTimeout(function () { getOpenProjetList('fprojectId', ProjectId, 'Project'); CreateClientDropDown('client', 0); get_Manager_selectbyid('Dispatcher', 'cDispatcher', cDispatcher, 'D');   }, 500);
    setTimeout(function () { ProjectManagerDropDown('Project_manager1'); }, 1200);
    setTimeout(function () { get_template("Template");  }, 1500);
    setTimeout(function () { get_client('cclient', cclient);  }, 1800);
    setTimeout(function () { CallLoadFunction();  ;}, 2500);  
    setTimeout(function () {
        $("#filter_div").css("display", "block");  ;}, 3000);
    
});
function CallLoadFunction() {  
    CreateManagerDropDown('Dispatcher', 'Dispatcher', 0, 'D'); 
    CreateProjectDropDown(10, 'Select');
}

function getOpenProjetList(id, prjid, name) {
    var dataString = 'id=' + prjid + '&name=' + name;
    $.ajax
        ({
            type: "POST",
            url: '/default/getallopenproject',
            data: dataString,
            cache: false,
            success: function (e) {
                $("#" + id).removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                $(".selectpicker").selectpicker('refresh');  
            }
        });
}
function CreateProjectDropDown(prjid, name) {
    var dataString = 'id=' + prjid + '&name=' + name;
    $.ajax
        ({
            type: "POST",
            url: '/default/getallopenproject',
            data: dataString,
            cache: false,
            success: function (e) {
                $("#projectId").removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                $("#uplodproject").removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                $("#uprojectId").removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                $(".selectpicker").selectpicker('refresh');  
            }
        });
} 

$("#amount").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
    buttonContainer: '<div class="btn-group amount" />', enableFiltering: false, buttonText: function (options, select) {
        return 'Amount';
    },
});

$(".screening").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
    buttonContainer: '<div class="btn-group screening" />', enableFiltering: false, buttonText: function (options, select) {
        return 'Select';
    },
});

$(".Skills").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
    buttonContainer: '<div class="btn-group Skills" />', enableFiltering: true, enableCaseInsensitiveFiltering: true, buttonText: function (options, select) {
        return 'Select';
    },
});
$(".Tool").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
    buttonContainer: '<div class="btn-group Tool" />', enableFiltering: true, enableCaseInsensitiveFiltering: true, buttonText: function (options, select) {
        return 'Select';
    },
});