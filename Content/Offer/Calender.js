﻿get_clientmanagermain(cclient, cmanagerId);
$("#cclient").on("change", function () {
    var id = $(this).val();
    get_clientmanagermain(id, 0);
});
function get_clientmanagermain(clientid, managerid) {
    var dataString = 'name=Client Manager&id=' + clientid + '&clientid=' + managerid;
    $.ajax
        ({
            type: "POST",
            url: '/default/get_all_manager_by_clientid',
            data: dataString,
            cache: false,
            success: function (e) {
                $("#cClientManager").removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                $(".selectpicker").selectpicker('refresh');
            }
        });
}
function getOpenProjetListTwo(prjid, name) {
    var dataString = 'id=' + prjid + '&name=' + name;
    $.ajax
        ({
            type: "POST",
            url: '/default/getallopenproject',
            data: dataString,
            cache: false,
            success: function (e) {
                $("#uplodproject").removeClass("selectpicker").append($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                $("#projectId").removeClass("selectpicker").append($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                $(".selectpicker").selectpicker('refresh');
            }
        });
}
function getOpenProjetList(id, prjid, name) {
    var dataString = 'id=' + prjid + '&name=' + name;
    $.ajax
        ({
            type: "POST",
            url: '/default/getallopenproject',
            data: dataString,
            cache: false,
            success: function (e) {
                $("#" + id).removeClass("selectpicker").append($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                $(".selectpicker").selectpicker('refresh');
            }
        });
}
getOpenProjetList('fprojectId', ProjectId, 'Project');
getOpenProjetListTwo(10, 'Project');
get_projectmgr_byclient('uploadProject_manager1');
$(document).ready(function () {
    $(".cuschk").on("click", function () {
        var id = $(this).data("id");
        if ($(this).hasClass("bck")) {
            $("#" + id).attr("checked", false);
            $(this).removeClass("bck");
        } else {
            $("#" + id).attr("checked", true);
            $(this).addClass("bck");
        }
    });
});
function get_projectmgr_byclient(id) {
    var dataString = 'ismulti=0&id=0';
    $.ajax
        ({
            type: "POST",
            url: '/default/get_all_mebertypeprojectmanager',
            data: dataString,
            cache: false,
            success: function (e) {
                $("#uploadProject_manager1").removeClass("selectpicker").append($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                $("#Project_manager1").removeClass("selectpicker").append($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                $(".selectpicker").selectpicker('refresh');
            }
        });

}
$("#JobId").on("change", function (e) {
    var _url = "/offer/validatejobId";
    var jobid = $("#JobId").val();
    $.get(_url, { 'id': jobid }, function (data) {
        if (data == 'True') {
            return true;
        } else {
            alertify.log('OfferId ' + jobid + ' already in use', 'error', 5000);
            $("#JobId").val('');
            return false;
        }
    });
});
function edit() {
    var id = $("#eventid").val();
    var _url = "/offer/edit?type=2&token=" + id;
    location.href = _url;
}
function deletejob() {
    var id = $("#eventid").val();
    bootbox.confirm("Are you sure?", function (result) {
        if (result) {
            var _url = "/offer/index?type=2&token=" + id;
            window.location.href = _url;
        }
    });
}
function view() {
    var id = $("#eventid").val();
    var _url = "/offer/view?viewtype=2&token=" + id;
    location.href = _url;
}
function GetClientManager(id, eleid, clientid) {
    if (id != 0) {
        var dataString = 'id=' + id + '&clientid=' + clientid;
        $.ajax
            ({
                type: "POST",
                url: '/default/get_all_manager_by_clientid',
                data: dataString,
                cache: false,
                success: function (e) {
                    $("#" + eleid).removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                    $(".selectpicker").selectpicker('refresh');
                }
            });
    }
    getClientrate('uplodclient', 'upclientRate');
}
function getClientrate(cntrl, rateCntrl) {
    var client = $("#" + cntrl).val();
    if (client != 0) {
        var _url = "/client/get_client_rate?id=" + client;
        $.get(_url, function (data) {
            $("#" + rateCntrl).val(data);
        });
    } else {
        $("#" + rateCntrl).val("$0.00");
    }
}
function getrateTechnician() {
    var memberid = $("#Technician").val();
    if (memberid != 0) {
        var _url = "/member/get_member_rate?id=" + memberid;
        $.get(_url, function (data) {
            $("#techrate").val(data);
        });
    } else {
        $("#techrate").val("$0.00");
    }
}
var searchtype = 0;
getjobCalender(_userId, 0);
function getjobCalender(userid, _searchtype) {
    InitalizeCalender(userid, 0);
}
function TriggerFilter() {
    InitalizeCalender(_userId, 0);
}
function getjobCalenderFilter(userid) { 
    InitalizeCalender(userid, 1);
    $("#filterpopup").css("display", "block");
}
function InitalizeCalender(UserId, SerachType) {
    $('#jobcalendar').fullCalendar('destroy');
    var skillsSelected = "";
    var _skillsselected = $("#Skiil").find("option:selected");
    _skillsselected.each(function () {
        skillsSelected = skillsSelected + $(this).val() + ",";
    });
    var maxDate1 = moment(new Date()).format("YYYY-MM-DD");
    $("#jobcalendar").fullCalendar({
        theme: true,
        header: {
            left: 'prev,next',
            center: 'title',
            right: 'month,agendaDay,agendaWeek'
        },
        contentHeight: 600,
        fixedWeekCount: false,
        firstDay: 1,
        scrollTime: "07:00:00",
        slotEventOverlap: false,
        allDaySlot: false,
        defaultDate: maxDate1,
        navLinks: true, // can click day/week names to navigate views
        eventLimit: true, // allow "more" link when too many events
        //defaultView: 'agendaDay',
        editable: false,
        eventClick: function (event) {
            var id = event.token;
            $("#eventid").val(id);
            $('#viewmodal').modal('show');
            $("#action").html(event.title);
        },
        events: {
            url: '/offer/GetEvents/',
            cache: true,
            type: 'GET',
            data: {
                id: UserId,
                date: $("#date").val(),
                type: 2,
                Location: $("#Filter_Location").val(),
                cclient: $("#cclient").val(),
                cTechnician: $("#cTechnician").val(),
                cDispatcher: $("#cDispatcher").val(),
                Status: $("#Status").val(),
                project: $("#fprojectId").val(),
                searchtype: SerachType,
                PmanagerId: $("#cProjectManager").val(),
                cmanagerId: $("#cClientManager").val(),
                screening: $("#Screening").val(),
                Skiil: skillsSelected
            },
            error: function () {
                alert('there was an error while fetching events!');
            },
        }, eventRender: function (event, element) {


        }, navLinkDayClick: function (date, jsEvent) {
            return false;
        },
        dayRender: function (date, cell) {
        },
    });
    $(".fc-next-button").html("Next").addClass("btn").addClass("btn-default").addClass("custom-cal-button");
    $(".fc-prev-button").html("Prev").addClass("btn").addClass("btn-default").addClass("mrl").addClass("custom-cal-button");
    $(".fc-month-button").html("Month").addClass("btn").addClass("btn-default").addClass("mrl").addClass("custom-cal-button");
    $(".fc-agendaDay-button ").html("Day").addClass("btn").addClass("btn-default").addClass("mrl").addClass("custom-cal-button");
    $(".fc-agendaWeek-button").html("Week").addClass("btn").addClass("btn-default").addClass("custom-cal-button");

    $(".fc-month-button").on("click", function () {
        $(".fc-next-button").html("Next").addClass("btn").addClass("btn-default").addClass("custom-cal-button");
        $(".fc-prev-button").html("Prev").addClass("btn").addClass("btn-default").addClass("mrl").addClass("custom-cal-button");

    });
    $(".fc-agendaDay-button").on("click", function () {
        $(".fc-next-button").html("Next").addClass("btn").addClass("btn-default").addClass("custom-cal-button");
        $(".fc-prev-button").html("Prev").addClass("btn").addClass("btn-default").addClass("mrl").addClass("custom-cal-button");

    });
    $(".fc-agendaWeek-button").on("click", function () {
        $(".fc-next-button").html("Next").addClass("btn").addClass("btn-default").addClass("custom-cal-button");
        $(".fc-prev-button").html("Prev").addClass("btn").addClass("btn-default").addClass("mrl").addClass("custom-cal-button");

    });
    var _d = $("#date").val();
    if ($.trim(_d) != "") {
        var d = _d.split("-");
        $("#jobcalendar").fullCalendar('gotoDate', d[0]);
    }
}
$("#Skiil").multiselect({
    buttonContainer: '<div class="btn-group bootstrap-select fit-width" />', enableCaseInsensitiveFiltering: false, enableFiltering: false, buttonText: function (options, select) {
        return 'Skill';
    },
});
$("#projectId").on("change", function () {
    var prjid = $("#projectId").val();
    if (prjid != 0) {
        var dataString = 'id=' + prjid;
        $.ajax
            ({
                type: "POST",
                url: '/project/GetProjectDetails',
                data: dataString,
                cache: false,
                success: function (data) {
                    $("#client").val(data.clientId).selectpicker('refresh');
                    $("#Project_manager1").val(data.projectManagerId).selectpicker('refresh');
                    GetClientManager(data.clientId, 'Project_manager', data.ClientManagerId);
                    $(".selectpicker").selectpicker('refresh');

                }
            });
    }
});
$(document).ready(function () {
    $("#clear").on('click', function () {
        $('#jobcalendar').fullCalendar('destroy');
        $("#date").val('');
        $("#Filter_Location").val('');
        $("#cclient").val('0');
        $("#cTechnician").val('0');
        $("#cDispatcher").val('0');
        $("#Status").val('0');
        $('.selectpicker').selectpicker('refresh');

        getjobCalender(_userId, 2);
    });

    $("#submit").on('click', function () {
        searchtype = 1;
        getjobCalenderFilter(_userId);
    });
    var start = moment();
    var end = moment().add(30, 'days');

    function cb(start, end) {
        $('.dateRangePicker').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
    }
    $('.dateRangePicker').daterangepicker({
        opens: 'left',
        autoUpdateInput: false
    }, cb);
    //cb(start, end);
    $('.dateRangePicker').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
        $("#date").val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
        TriggerFilter();
    });
     
    get_client('cclient', cclient);
    get_client('client',0);
    get_client('uplodclient', 0);
    function get_client(Elementid, ClientId) {
        var dataString = 'ismulti=0&clientid=' + ClientId;
        $.ajax
            ({
                type: "POST",
                url: '/default/get_all_clients',
                data: dataString,
                cache: false,
                success: function (data) {
                    $("#" + Elementid).html('').removeClass("selectpicker").append($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                    $(".selectpicker").selectpicker('refresh');

                }
            });

    }
    $("#client").change(function () {
        var id = $(this).val();
        if (id != 0) {
            var dataString = 'id=' + id;
            $.ajax
                ({
                    type: "POST",
                    url: '/default/get_all_manager_by_clientid',
                    data: dataString,
                    cache: false,
                    success: function (data) {
                        $("#Project_manager").html('').removeClass("selectpicker").append($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                        $(".selectpicker").selectpicker('refresh');
                    }
                });
            getClientrate('client', 'c_techrate');
        } else {
            $("#Project_manager").html('<option value="0">Manager</option>').attr('disabled', true).selectpicker('refresh');
        }
    });
    $("#uplodclient").change(function () {
        var id = $(this).val();
        if (id != 0) {
            var dataString = 'id=' + id;
            $.ajax
                ({
                    type: "POST",
                    url: '/default/get_all_manager_by_clientid',
                    data: dataString,
                    cache: false,
                    success: function (data) {

                        $("#uploadProject_manager").html('').removeClass("selectpicker").append($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                        $(".selectpicker").selectpicker('refresh');
                    }
                });
        } else {
            $("#uploadProject_manager").html('<option value="0">Manager</option>').attr('disabled', true).selectpicker('refresh');
        }
        getClientrate('uplodclient', 'upclientRate');
    });
    function get_city(city_id, state_id) {
        if (city_id != '') {


            var dataString = 'id=' + state_id + "&ismulti=1&city=" + city_id;
            $.ajax
                ({
                    type: "POST",
                    url: '/default/get_all_city_by_stateid',
                    data: dataString,
                    cache: false,
                    success: function (data) {
                        $('#city_filter_id').multiselect('destroy');
                        $("#city_filter_id").html('');
                        $("#city_filter_id").html(data).removeAttr('disabled');
                        $("#city_filter_id").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
                            enableCaseInsensitiveFiltering: true, enableFiltering: true, buttonText: function (options, select) {
                                return 'City';
                            },
                        });

                    }
                });
        } else {
            $("#city_filter_id").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
                enableCaseInsensitiveFiltering: true, enableFiltering: true, buttonText: function (options, select) {
                    return 'City';
                },
            });
        }

    }
    $("#state_filter_id").change(function () {
        var id = $(this).val();
        if (id != 0) {
            // $("#city_id").html('');
            var dataString = 'id=' + id + "&ismulti=1";
            $.ajax
                ({
                    type: "POST",
                    url: '/default/get_all_city_by_stateid',
                    data: dataString,
                    cache: false,
                    success: function (data) {
                        $('#city_filter_id').multiselect('destroy');
                        $("#city_filter_id").html('');
                        $("#city_filter_id").html(data).removeAttr('disabled');
                        $("#city_filter_id").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
                            enableCaseInsensitiveFiltering: true, enableFiltering: true, buttonText: function (options, select) {
                                return 'City';
                            },
                        });
                    }
                });
        } else {
            $('#city_filter_id').multiselect('destroy');
            $("#city_filter_id").html('');
            $("#city_filter_id").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
                enableCaseInsensitiveFiltering: true, enableFiltering: true, buttonText: function (options, select) {
                    return 'City';
                },
            });
            //  $("#city_id").html('<option value="0">City</option>').attr('disabled',true);
        }
    });
    $("#state_id").change(function () {
        var id = $(this).val();
        if (id != 0) {
            var dataString = 'id=' + id;
            $.ajax
                ({
                    type: "POST",
                    url: '/default/get_all_city_by_stateid',
                    data: dataString,
                    cache: false,
                    success: function (data) { 
                        $("#city_id").html('').removeClass("selectpicker").append($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                        $(".selectpicker").selectpicker('refresh');
                    }
                });
        } else {
            $("#city_id").html('<option value="0">City</option>').attr('disabled', true).selectpicker('refresh');
        }
    });
});
function Uploadfile() {
    $("#backdrop").css("display", "block");
    $(".box").animate({
        width: "hide"
    });
    $("#uploadslide").animate({
        width: "show"
    });
}
$(".closebox").on("click", function (e) {
    $("#uploadslide").animate({
        width: "hide"
    });
    $("#viewslide").animate({
        width: "hide"
    });

    $("#backdrop").css("display", "none");
});
function get_Manager_selectbyid(_val, cntrlId, memberid, type, issubcontractor) {
    var dataString = 'optvalue=' + _val + "&memberid=" + memberid + "&type=" + type + '&issubcontractor=' + issubcontractor;
    $.ajax
        ({
            type: "POST",
            url: '/default/get_all_member',
            data: dataString,
            cache: false,
            success: function (data) {
                $("#" + cntrlId).html('').removeClass("selectpicker").append($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                $(".selectpicker").selectpicker('refresh');
            }
        });

}
$(window).load(function () {

    setTimeout(function () {
        $("#filter_div").css("display", "block");
        get_Manager_selectbyid('Select', 'upload_dispatcher', 0, 'D', 'false');
    }, 1500);

    setTimeout(function () {
        get_Manager_selectbyid('Dispatcher', 'Dispatcher', 0, 'D', 'false');
    }, 800);
    setTimeout(function () {
        get_Manager_selectbyid('Dispatcher', 'cDispatcher', cDispatcher, 'D', 'false');

    }, 1000);

});
get_Manager_selectbyid('Manager', 'cProjectManager', 'PmanagerId', 'PM');
$("#amount").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
    buttonContainer: '<div class="btn-group amount" />', enableFiltering: false, buttonText: function (options, select) {
        return 'Amount';
    },
});
$(".screening").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
    buttonContainer: '<div class="btn-group screening" />', enableFiltering: false, buttonText: function (options, select) {
        return 'Select';
    },
});
$(".Skills").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
    buttonContainer: '<div class="btn-group Skills" />', enableFiltering: true, enableCaseInsensitiveFiltering: true, buttonText: function (options, select) {
        return 'Select';
    },
});
$("#Skiil").multiselect({
    buttonContainer: '<div class="btn-group bootstrap-select fit-width" />', enableCaseInsensitiveFiltering: false, enableFiltering: false, buttonText: function (options, select) {
        return 'Skill';
    },
});
$(".Tool").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
    buttonContainer: '<div class="btn-group Tool" />', enableFiltering: true, enableCaseInsensitiveFiltering: true, buttonText: function (options, select) {
        return 'Select';
    },
}); 