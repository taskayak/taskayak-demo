﻿$(document).ready(function () {
    otable = $('#table').DataTable({
        "processing": true,
        "serverSide": true,
        'language': {
            'loadingRecords': '&nbsp;',
            'processing': "<img src='/Content/Images/loader.gif'> Loading..."
        },
        "oLanguage": { "sSearch": "", 'sLengthMenu': "Show _MENU_" },
        "iDisplayLength": 25,
        "order": [],
        "bSort": false,
        "bFilter": true,
        "autoWidth": false,
        "ajax": {
            "url": "/Tools/FetchTools"
        },
        "aoColumns": [
            {
                bSortable: false,
                mRender: function (data, type, full) {
                    if (editpermission == "TRUE") {
                        return '<a  href="#" data-toolid="' + full.toolid + '" data-name="' + full.name + '" data-typeid="' + full.typeid + '" onclick="update(this)">' + full.name + '</a>';
                    }
                    else {
                        return '<a href="#" class="disabledbtn" data-msg="You are not authorized to edit selected tool">' + full.name + '</a>';
                    }
                }
            }, { mData: "typename" },
            {
                bSortable: false,
                mRender: function (data, type, full) {
                    var tbl = '<div class="btn-group pos-static"><a class="customedropdown" data-toggle="customedropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v fn21"></i> </a><div class="dropdown-menu customdropdown-menu" x-placement="bottom-start" style="top: 0px; transform: translate3d(-110px, 10px, 0px);will-change: transform;position: absolute;left: 0px; display: none;">';

                    if (editpermission == "TRUE") {
                        tbl += '<a class="btn btn-complete btn-xs" href="#" data-toolid="' + full.toolid + '" data-name="' + full.name + '" data-typeid="' + full.typeid + '" onclick="update(this)"><i class="fa fa-eye"></i> View </a>';
                    }
                    else {
                        tbl += '<a href="#" class="disabledbtn" data-msg="You are not authorized to edit selected tool"><i class="fa fa-eye"></i> View </a>';
                    }
                    if (deletepermission == "TRUE") {
                        tbl += '<a class="btn btn-danger btn-xs cdelete" id="' + full.toolid + '"><i class="fa fa-trash"></i> Delete</a>';
                    }
                    else {
                        tbl += '<a href="#" class="disabledbtn btn btn-danger btn-xs" data-msg="You are not authorized to delete selected tool"><i class="fa fa-trash"></i> Delete</a>';
                    }
                    tbl = tbl + ' <a class="btn btn-info btn-xs"><i class="fa fa-window-close"></i> Close</a>';
                    tbl = tbl + '</div></div>';
                    return tbl;
                }
            }

        ]
    });
});
$(document).ready(function () {
    $("#table").on("click", ".cdelete", function (e) {
        e.preventDefault();
        var id = this.id;
        bootbox.confirm("Are you sure?", function (result) {
            if (result) {
                var _url = "/tools/delete";
                $.post(_url, { 'id': id }, function (data) {
                    if (data.indexOf('Error') == -1) {
                        alertify.log(data, 'success', 5000);
                        otable.ajax.reload(null, false);
                    } else {
                        alertify.log(data, 'error', 5000);
                    }
                });
            }
        });
    });
});
function update(ele) {
    var toolid = $(ele).data('toolid');
    var toolname = $(ele).data('name');
    var typeid = $(ele).data('typeid');
    $('#mdlupdate').modal('show');
    $("#toolname").val(toolname);
    $("#toolid").val(toolid);
    $('#UpdateTypeId').val(typeid);
    $('.selectpicker').selectpicker('refresh');
}
function UpdateTool() {
    var _url = '/tools/Update';
    var name = $("#toolname").val();
    var typeid = $("#UpdateTypeId").val();
    var toolid = $("#toolid").val();
    $.post(_url, { 'name': name, 'typeid': typeid, 'toolid': toolid }, function (data) {
        if (data.indexOf('Error') == -1) {
            alertify.log(data, 'success', 5000);
            $('#mdlupdate').modal('hide');
            otable.ajax.reload(null, false);
        } else {
            alertify.log(data, 'error', 5000);
        }
    });
}
function GetActiveToolType() {
    $.ajax
        ({
            type: "POST",
            url: '/default/GetAllActiveToolType',
            cache: false,
            success: function (data) {
                $("#UpdateTypeId").html('').removeClass("selectpicker").append($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                $("#AddTypeId").html('').removeClass("selectpicker").append($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                $(".selectpicker").selectpicker('refresh');
            }
        });

}
$(window).load(function () {
    setTimeout(function () { GetActiveToolType(); }, 500);
});
function AddTool(Permissions, Message) {
    if (Permissions == 'True') {
        $("#frmaddtool").submit();
    } else {
        alertify.log(Message, 'error', 5000);
    }
}