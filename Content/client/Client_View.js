﻿$(document).ready(function () {
    otable = $('.data-table').DataTable({
        "processing": true,
        "serverSide": true,
        "iDisplayLength": 25,
        "order": [],
        "bSort": false,
        "bFilter": true,
        "autoWidth": false,
        'language': {
            'loadingRecords': '&nbsp;',
            'processing': "<img src='/Content/Images/loader.gif'> Loading..."
        },
        "ajax": {
            "url": _ajaxUrl
        }, "oLanguage": { "sSearch": "", 'sLengthMenu': "Show _MENU_" },
        "aoColumns": [
            {
                bSortable: false,
                mRender: function (data, type, full) {
                    if (editpermission == "TRUE") {
                        return '<a href="#" data-phone="' + full.phone + '" data-email="' + full.email + '" data-name="' + full.name + '" class="cedit" data-prjmanagerid="' + full.prjmanagerid + '">' + full.name + '</a>';

                    }
                    else {
                        return '<a href="#" class="disabledbtn" data-msg="You are not authorized to view selected Manager">' + full.name + '</a>';
                    }
                }
            },
            { mData: "email" },
            { mData: "phone" },
            {
                bSortable: false,
                mRender: function (data, type, full) {
                    var tbl = '<div class="btn-group pos-static"><a class="customedropdown" data-toggle="customedropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v fn21"></i> </a><div class="dropdown-menu customdropdown-menu" x-placement="bottom-start" style="top: 0px; transform: translate3d(-38px, 10px, 0px);will-change: transform;position: absolute;left: 0px; display: none;">';
                    if (editpermission == "TRUE") {
                        tbl = tbl + '<a  data-phone="' + full.phone + '" data-email="' + full.email + '" data-name="' + full.name + '" class="btn btn-success btn-xs cedit" data-prjmanagerid="' + full.prjmanagerid + '" href="#"><i class="fa fa-edit"></i> Edit</a>';

                    }
                    else {
                        tbl = tbl + '<a href="#" class="disabledbtn btn btn-success btn-xs" data-msg="You are not authorized to edit selected Manager"><i class="fa fa-edit"></i> Edit</a>';
                    }
                    if (deletepermission == "TRUE") {
                        tbl = tbl + '<a class="btn btn-danger btn-xs cdelete" id="' + full.prj_token + '"  data-clientid="' + full.clt_token + '"><i class="fa fa-trash"></i> Delete</a>';

                    }
                    else {
                        tbl = tbl + '<a href="#" class="disabledbtn btn btn-danger btn-xs" data-msg="you are not authorized to delete selected Manager"><i class="fa fa-trash"></i> Delete</a>';
                    }
                    tbl = tbl + '<a  class="btn btn-info btn-xs"><i class="fa fa-window-close"></i> Close</a>';
                    tbl = tbl + '</div></div>';
                    return tbl;
                }
            }

        ]
    });

    $(".closebox").on("click", function (e) {
        $("#uploadslide").animate({
            width: "hide"
        });
        $("#backdrop").css("display", "none");
    });
});
function Uploadfile() {
    $("#backdrop").css("display", "block");
    $("#uploadslide").animate({
        width: "hide"
    });
    $("#uploadslide").animate({
        width: "show"
    });
}
function add_new(clientid) {
    $('#add-new-manager').modal('show');
    $("#clientid").val(clientid);
}
$("#table").on("click", ".cdelete", function (e) {
    e.preventDefault();
    var id = this.id;
    var client_id = $(this).data('clientid');
    bootbox.confirm("Are you sure?", function (result) {
        if (result) {
            var _url = "/client/delete_manager";
            window.location.href = _url + "?clienttoken=" + client_id + "&token=" + id;
        }
    });
})
$("#table").on("click", ".cedit", function (e) {
    e.preventDefault();
    var prjmanagerid = $(this).data('prjmanagerid');
    var phone = $(this).data('phone');
    var email = $(this).data('email');
    var name = $(this).data('name');
    $("#managerid").val(prjmanagerid);
    $("#phone").val(phone);
    $("#email").val(email);
    $("#name").val(name);
    $("#prj_clientid").val(clientid);
    $('#update-manager').modal('show');
});