﻿$(document).ready(function () {
    otable = $('.data-table').DataTable({
        "processing": true,
        "serverSide": true,
        "autoWidth": false,
        'language': {
            'loadingRecords': '&nbsp;',
            'processing': "<img src='/Content/Images/loader.gif'> Loading...", 
            
             
        },
    "iDisplayLength": 25,
    "order": [],
    "bSort": false,
        "bFilter": true,
        "oLanguage": { "sSearch": "", 'sLengthMenu': "Show _MENU_" },
    "ajax": {
        "url": "/client/Fetchclients"
            },
    "aoColumns": [
    { mData: "index_id" },
    {
        bSortable: false,
        mRender: function (data, type, full) {
            if (editpermission == "TRUE") {
                return '<a href="/client/view?token=' + full.token + '">' + full.name + '</a>';
            }
            else {
                return '<a href="#" class="disabledbtn" data-msg="You are not authorized to view selected client">' + full.name + '</a>';
            }
        }
    },
    { mData: "primarycontactname" },
    { mData: "rate" },
    { mData: "email" },
    { mData: "phone" },
    {
        bSortable: false,
        mRender: function (data, type, full) {
            var tbl = '<div class="btn-group pos-static"><a class="customedropdown" data-toggle="customedropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v fn21"></i> </a><div class="dropdown-menu customdropdown-menu" x-placement="bottom-start" style="top: 0px; transform: translate3d(-38px, 10px, 0px);will-change: transform;position: absolute;left: 0px; display: none;">';
            if (viewpermission == "TRUE") {
                tbl = tbl + '<a class="btn btn-purple btn-xs" href="/client/view?token=' + full.token + '"><i class="fa fa-search"></i> View</a>';

            }
            else {
                tbl = tbl + '<a href="#" class="disabledbtn btn btn-primary btn-xs" data-msg="you are not authorized to view selected client"><i class="fa fa-search"></i> View</a>';
            }
            if (editpermission == "TRUE") {
                tbl = tbl + '<a class="btn btn-success btn-xs" href="/client/edit?token=' + full.token + '"><i class="fa fa-edit"></i> Edit</a>';
            }
            else {
                tbl = tbl + '<a href="#" class="disabledbtn btn btn-success btn-xs" data-msg="You are not authorized to edit selected client"><i class="fa fa-edit"></i> Edit</a>';
            }
            if (deletepermission == "TRUE") {
                tbl = tbl + '<a class="btn btn-danger btn-xs cdelete" id="' + full.token + '"><i class="fa fa-trash"></i> Delete</a>';
            }
            else {
                tbl = tbl + '<a href="#" class="disabledbtn btn btn-danger btn-xs" data-msg="You are not authorized to delete selected client"><i class="fa fa-trash"></i> Delete</a>';
            }
            tbl = tbl + '<a href="#" class="btn btn-info btn-xs" ><i class="fa fa-window-close"></i> Close</a>';
            tbl = tbl + '</div></div>';
            return tbl;
        }
    }

]
        });
    });

$(document).ready(function () {
    $("#table").on("click", ".cdelete", function (e) {
        e.preventDefault();
        var id = this.id;
        bootbox.confirm("Are you sure?", function (result) {
            if (result) {
                var _url = "/client/delete";
                window.location.href = _url + "?token=" + id;
            }
        });
    });

    $(".closebox").on("click", function (e) {
        $("#uploadslide").animate({
            width: "hide"
        });
        $("#backdrop").css("display", "none");
    });
});
function Uploadfile() {
    $("#backdrop").css("display", "block");
    $("#uploadslide").animate({
        width: "hide"
    });
    $("#uploadslide").animate({
        width: "show"
    });
}
