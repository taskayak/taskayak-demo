﻿

function gettechs(stateid) {
    if (stateid != 0) {
        var dataString = 'stateid=' + stateid + "&optvalue=Technician";
        $.ajax
            ({
                type: "POST",
                url: '/default/get_selcted_member_bystate',
                data: dataString,
                cache: false,
                success: function (data) {
                    $("#assignTechnician").removeClass("selectpicker").html($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                    $(".selectpicker").selectpicker('refresh');  
                }
            });
    } else {
        $("#assignTechnician").html('<option value="0">Technician</option>').attr('disabled', true).selectpicker('refresh');
    }
}


function get_city(city_id, state_id) {
    var dataString = 'id=' + state_id + "&ismulti=1&city=" + city_id;
    $.ajax
        ({
            type: "POST",
            url: '/default/get_all_city_by_stateid',
            data: dataString,
            cache: false,
            success: function (data) {
                $('#city_filter_id').multiselect('destroy');
                $("#city_filter_id").html('');
                $("#city_filter_id").html(data).removeAttr('disabled');
                $("#city_filter_id").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
                    enableCaseInsensitiveFiltering: true, enableFiltering: true, buttonText: function (options, select) {
                        return 'City';
                    },
                });
               
            }
        });

}
function get_Manager_selectbyid(_val, cntrlId, memberid, type) {
    var dataString = 'optvalue=' + _val + "&memberid=" + memberid + "&type=" + type;
    $.ajax
        ({
            type: "POST",
            url: '/default/get_all_member',
            data: dataString,
            cache: false,
            success: function (data) {
                $("#" + cntrlId).removeClass("selectpicker").html($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                $(".selectpicker").selectpicker('refresh');  
            }
        });

}
function get_client(client, clientid) {
    var dataString = 'ismulti=0&clientid=' + clientid;
    $.ajax
        ({
            type: "POST",
            url: '/default/get_all_clients',
            data: dataString,
            cache: false,
            success: function (data) {
                $("#" + client).removeClass("selectpicker").html($.parseHTML(data)).addClass("selectpicker");
                $(".selectpicker").selectpicker('refresh');  
            }
        });

}
$("#JobId").on("change", function (e) {
    var _url = "/jobs/validatejobId";
    var jobid = $("#JobId").val();
    $.get(_url, { 'id': jobid }, function (data) {
        if (data == 'True') {
            return true;
        } else {
            alertify.log('JobId ' + jobid + ' already in use', 'error', 5000);
            $("#JobId").val('');
            return false;
        }
    });
});

function sendMessage() {
    var id = $("#mainjid").val();
    var _body = $("#messagebox").val();
    if (_body == '') {
        alertify.log("Please write message", "error", 2000);
        return false;
    }
    var _url = "/jobs/Sendmessage";
    $.post(_url, { 'Comment': _body, 'JobID': id }, function (data) {
        getmessages();

        $(".wysihtml5-sandbox").remove();
        $(".wysihtml5-toolbar").remove();
        $("#messagebox").show();
        $('#messagebox').val('');
        $('#messagebox').wysihtml5({
            toolbar: {
                'fa': true
            }
        });
        alertify.log("Message Sent", "success", 2000);
    });
}

function getmessages() {
    var id = $("#mainjid").val();
    var _url = "/jobs/getnotifications";
    $.get(_url, { 'JobID': id }, function (data) {
        $("#notificationmodal").html(data);
    });
}
function assignjob() {
    var cityid = $("#assigncity_id").val();
    var techid = $("#assignTechnician").val();
    var jobid = $('#assignjobid').val();
    var rate = $("#assignmemberrate").val();
    var stateid = $("#assignstate_id").val();
    if (stateid == 0) {
        alertify.log("Please select state", "error", 2000);
        return false;
    }
    if (techid == 0) {
        alertify.log("Please select technician", "error", 2000);
        return false;
    }
    if ($.trim(rate) == "") {
        alertify.log("Please enter technician rate", "error", 2000);
        return false;
    }
    var _url = "/jobs/updateassign";
    $.post(_url, { 'memberid': techid, 'jobid': jobid, 'rate': rate }, function (data) {
        $("#rate_" + jobid).html(rate);
        $("#member_" + jobid).html(data);
        alertify.log("Job assign to " + data, "success", 2000);
        $('#assigntech').modal('hide');
        otable.page.len(pageSize).draw(false);
    });

}
function getClientrate(cntrl, rateCntrl) {
    var client = $("#" + cntrl).val();
    if (client != 0) {
        var _url = "/client/get_client_rate" + "?id=" + client;
        $.get(_url, function (data) {
            $("#" + rateCntrl).val(data);
        });
    } else {
        $("#" + rateCntrl).val("$1");
    }
}
function getrateTechnician() {
    var memberid = $("#Technician").val();
    if (memberid != 0) {
        var _url = "/member/get_member_rate" + "?id=" + memberid;
        $.get(_url, function (data) {
            $("#techrate").val(data);
        });
    } else {
        $("#techrate").val("$0.00");
    }
}


function updatestatus() {
    var nid = $("#mid").val();
    var nsid = $("#mstatusid").val();
    if (nsid == 0) {
        alertify.log('please select job status', 'error', 5000);
        return false;
    }
    var _url = "/jobs/update_status";
    $.get(_url, { 'id': nid, 'statusid': nsid }, function () {
        var _txt = $("#mstatusid").find("option:selected").text();
        var cls = $("#spn_" + nid).html();
        $("#spn_" + nid).html(_txt);
        $("#spn_" + nid).removeClass(cls.replace(" ", "").toLowerCase()).addClass(_txt.toLowerCase().replace(" ", "").toLowerCase());
        $('#mdlstatus').modal('hide');
        alertify.log('Job status updated succesfully', 'success', 5000);
    });
}

$(document).ready(function () {
    $("#table").on("click", ".notification", function (e) {
        e.preventDefault();
        var id = $(this).data("id");
        var _url = "/jobs/getnotifications";
        $.get(_url, { 'jobid': id }, function (data) {
            $("#mainjid").val(id);
            $("#notificationmodal").html(data);
            $('#messagemodal').modal('show');
        });

    });

    $("#table").on("click", ".cdelete", function (e) {
        e.preventDefault();
        var id = this.id;
        bootbox.confirm("Are you sure?", function (result) {
            if (result) {
                var _url = "/jobs/index";
                window.location.href = _url + "?token=" + id;
            }
        });
    });
    $("#table").on("click", ".stupdater", function (e) {
        e.preventDefault(); var _selectedValue = $(this).html();
        var statusid = $(this).data("statusid");
        var memberId = $(this).data("memberid");
        $("#mid").val(memberId);
        $('#mstatusid option').each(function () {
            var $this = $(this);
            if ($this.text() == _selectedValue) {
                $("#mstatusid").val($this.val());
                return false;
            }
        });
        //$("#mstatusid").val(statusid);
        $('#mstatusid').selectpicker('refresh');
        $('#mdlstatus').modal('show');
    });
    $("#table").on("click", ".assign", function (e) {
        e.preventDefault();
        var jobid = $(this).data("jobid");
        $('#assignjobid').val(jobid);
        $("#assignmemberrate").val('');
        $("#assigncity_id").selectpicker('refresh');
        $("#assignTechnician").selectpicker('refresh');
        $('#assigntech').modal('show');
    });


    $("#client").change(function () {
        var id = $(this).val();
        if (id != 0) {
            var dataString = 'id=' + id;
            $.ajax
                ({
                    type: "POST",
                    url: '/default/get_all_manager_by_clientid',
                    data: dataString,
                    cache: false,
                    success: function (data) {
                        $("#Project_manager").removeClass("selectpicker").html($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                        $(".selectpicker").selectpicker('refresh');  
                    }
                });
        } else {
            $("#Project_manager").html('<option value="0">Manager</option>').attr('disabled', true).selectpicker('refresh');
        }
        getClientrate('client', 'c_techrate');

    });
    $("#cclient").change(function () {
        var id = $(this).val();

        //GetClientManager(0, 'cClientManager', id);
    });


    $("#uplodclient").change(function () {
        var id = $(this).val();
        if (id != 0) {
            var dataString = 'id=' + id;
            $.ajax
                ({
                    type: "POST",
                    url: '/default/get_all_manager_by_clientid',
                    data: dataString,
                    cache: false,
                    success: function (data) {
                        $("#uploadProject_manager").removeClass("selectpicker").html($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                        $(".selectpicker").selectpicker('refresh');  
                    }
                });
        } else {
            $("#uploadProject_manager").html('<option value="0">Manager</option>').attr('disabled', true).selectpicker('refresh');
        }
        getClientrate('uplodclient', 'upclientRate');
         
    });




    $("#state_filter_id").change(function () {
        var id = $(this).val();
        if (id != 0) {
            // $("#city_id").html('');
            var dataString = 'id=' + id + "&ismulti=1";
            $.ajax
                ({
                    type: "POST",
                    url: '/default/get_all_city_by_stateid',
                    data: dataString,
                    cache: false,
                    success: function (data) {
                        $('#city_filter_id').multiselect('destroy');
                        $("#city_filter_id").html('');
                        $("#city_filter_id").html(data).removeAttr('disabled');
                        $("#city_filter_id").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
                            enableCaseInsensitiveFiltering: true, enableFiltering: true, buttonText: function (options, select) {
                                return 'City';
                            },
                        });
                    }
                });
        } else {
            $('#city_filter_id').multiselect('destroy');
            $("#city_filter_id").html('');
            $("#city_filter_id").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
                enableCaseInsensitiveFiltering: true, enableFiltering: true, buttonText: function (options, select) {
                    return 'City';
                },
            });
            //  $("#city_id").html('<option value="0">City</option>').attr('disabled',true);
        }
    });

    $("#assignstate_id").change(function () {
        var id = $(this).val();
        if (id != 0) {
            var dataString = 'id=' + id;
            $.ajax
                ({
                    type: "POST",
                    url: '/default/get_all_city_by_stateid',
                    data: dataString,
                    cache: false,
                    success: function (data) {
                        $("#assigncity_id").html('').removeClass("selectpicker").append($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled').attr('disabled', false);

                        gettechs(id);
                        $(".selectpicker").selectpicker('refresh');  
                    }
                });
        } else {
            $("#assigncity_id").html('<option value="0">City</option>').selectpicker('refresh').attr('disabled', false);
        }
    });



    $("#assigncity_id").on("change", function () {
        var id = $(this).val();
        if (id != 0) {
            var dataString = 'cityid=' + id + "&optvalue=Technician";
            $.ajax
                ({
                    type: "POST",
                    url: '/default/get_selcted_member_bycity',
                    data: dataString,
                    cache: false,
                    success: function (data) {
                        $("#assignTechnician").removeClass("selectpicker").html($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled').attr('disabled', false);
                        $(".selectpicker").selectpicker('refresh');  
                    }
                });
        } else {
            var _id = $("#assignstate_id").val();
            gettechs(_id);
        }
    });
});

function getrate() {
    var memberid = $("#assignTechnician").val();
    if (memberid != 0) {
        var _url = "/users/get_member_rate" + "?id=" + memberid;
        $.get(_url, function (data) {
            $("#assignmemberrate").val(data);
        });
    } else {
        $("#assignmemberrate").val("$0");
    }
}
function Uploadfile() {
    $("#backdrop").css("display", "block");
    $("#uploadslide").animate({
        width: "hide"
    });
    $("#uploadslide").animate({
        width: "show"
    });
}
function getprojectmanagerbymembertype(id) {
    var dataString = 'id=0';
    $.ajax
        ({
            type: "POST",
            url: '/default/get_all_mebertypeprojectmanager',
            data: dataString,
            cache: false,
            success: function (data) {
                $("#" + id).removeClass("selectpicker").html($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                $(".selectpicker").selectpicker('refresh');  
            }
        });

}
$(".closebox").on("click", function (e) {
    $("#backdrop").css("display", "none");
    $("#uploadslide").animate({
        width: "hide"
    });
});
function GetClientManager(id, eleid, clientid) {
    if (clientid == 0) {
        clientid = $("#cclient").val();
    }
    if (clientid != 0) {
        var dataString = 'id=' + id + '&clientid=' + clientid + '&name=Client manager';
        $.ajax
            ({
                type: "POST",
                url: '/default/get_all_manager_by_clientid',
                data: dataString,
                cache: false,
                success: function (e) {
                    $("#" + eleid).removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                    
                    $(".selectpicker").selectpicker('refresh');
                }
            });
    }
}
$(window).load(function () {
    setTimeout(function () { getprojectmanagerbymembertype('Project_manager1'); GetClientManager(0, 'cClientManager', 0); $("#filter_div").css("display", "block");   get_Manager_selectbyid('Technician', 'Technician', 0, 'T'); get_client('client', 0); }, 1500);
    setTimeout(function () { getprojectmanagerbymembertype('uploadProject_manager1'); $("#filter_div").css("display", "block");  get_Manager_selectbyid('Dispatcher', 'Dispatcher', 0, 'D'); get_client('uplodclient', 0); getOpenProjetList('uprojectId', 10, 'Select'); }, 1100);

});
