﻿getjobCalender(_userId, 0);
get_clientmanagermain(cclient, cmanagerId);
$("#Skiil").multiselect({
    buttonContainer: '<div class="btn-group bootstrap-select fit-width" />', enableCaseInsensitiveFiltering: false, enableFiltering: false, buttonText: function (options, select) {
        return 'Skill';
    },
});
$("#cclient").on("change", function () {
    var id = $(this).val();
    get_clientmanagermain(id, 0);
});
$("#JobId").on("change", function (e) {
    var _url = "/jobs/validatejobId";
    var jobid = $("#JobId").val();
    $.get(_url, { 'id': jobid }, function (data) {
        if (data == 'True') {
            return true;
        } else {
            alertify.log('JobId ' + jobid + ' already in use', 'error', 5000);
            $("#JobId").val('');
            return false;
        }
    });
});
function get_clientmanagermain(clientid, managerid) {
    var dataString = 'name=Client Manager&id=' + clientid + '&clientid=' + managerid;
    $.ajax
        ({
            type: "POST",
            url: '/default/get_all_manager_by_clientid',
            data: dataString,
            cache: false,
            success: function (e) {
                $("#cClientManager").removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                $(".selectpicker").selectpicker('refresh');
            }
        });
}
function edit() {
    var id = $("#eventid").val();
    var _url = "/jobs/edit?type=2&token=" + id;
    location.href = _url;
}
function deletejob() {
    var id = $("#eventid").val();
    bootbox.confirm("Are you sure?", function (result) {
        if (result) {
            var _url = "/jobs/index?type=2&token=" + id;
            window.location.href = _url;
        }
    });
}
function view() {
    var id = $("#eventid").val();
    var _url = "/jobs/view?viewtype=2&token=" + id;
    location.href = _url;
}
function getClientrate(cntrl, rateCntrl) {
    var client = $("#" + cntrl).val();
    if (client != 0) {
        var _url = "/client/get_client_rate?id=" + client;
        $.get(_url, function (data) {
            $("#" + rateCntrl).val(data);
        });
    } else {
        $("#" + rateCntrl).val("$1");
    }
}
function getrateTechnician() {
    var memberid = $("#Technician").val();
    if (memberid != 0) {
        var _url = "/member/get_member_rate?id=" + memberid;
        $.get(_url, function (data) {
            $("#techrate").val(data);
        });
    } else {
        $("#techrate").val("$1");
    }
}
function getjobCalender(userid, _searchtype) {
    InitailizeCalender(userid, _searchtype);
}
function getjobCalenderFilter(userId) {
    $("#filterpopup").css("display", "block");
    InitailizeCalender(userId, 1);
}
function TriggerFilter() {
    InitailizeCalender(_userId, 0);
}
function InitailizeCalender(userId, searchType) {
    var skillsSelected = "";
    var _skillsselected = $("#Skiil").find("option:selected");
    _skillsselected.each(function () {
        skillsSelected = skillsSelected + $(this).val() + ",";
    });
    $('#jobcalendar').fullCalendar('destroy');
    var maxDate1 = moment(new Date()).format("YYYY-MM-DD");
    $("#jobcalendar").fullCalendar({
        theme: true,
        header: {
            left: 'prev,next',
            center: 'title',
            right: 'month,agendaDay,agendaWeek'
        },
        contentHeight: 600,
        fixedWeekCount: false,
        firstDay: 1,
        scrollTime: "07:00:00",
        slotEventOverlap: false,
        allDaySlot: false,
        defaultDate: maxDate1,
        navLinks: true, // can click day/week names to navigate views
        eventLimit: true, // allow "more" link when too many events
        //defaultView: 'agendaDay',
        editable: false,
        eventClick: function (event) {
            var id = event.token;
            $("#eventid").val(id);
            $(".box").animate({
                width: "hide"
            });
            $("#viewslide").animate({
                width: "show"
            });
            $("#action").html(event.title);
            $("#_viewID").html(event.title);
            $("#_viewTitle").html(event.title);
            $("#_viewDate").html(event.title);
            $("#_viewStime").html(event.title);
            $("#_viewEtime").html(event.title);
            $("#_viewAddress").html(event.title);
            $("#_viewContact").html(event.title);
            $("#_viewPhone").html(event.title);
            $("#_viewDispatcher").html(event.title);
            $("#_viewTechnician").html(event.title); 
        },
        events: {
            url: '/jobs/GetEvents/',
            cache: true,
            type: 'GET',
            data: {
                id: userId,
                date: $("#date").val(),
                type: 2,
                Location: $("#Filter_Location").val(),
                cclient: $("#cclient").val(),
                cTechnician: $("#cTechnician").val(),
                cDispatcher: $("#cDispatcher").val(),
                Status: $("#Status").val(),
                searchtype: searchType,
                project: $("#fprojectId").val(),
                PmanagerId: $("#cProjectManager").val(),
                cmanagerId: $("#cClientManager").val(),
                screening: $("#Screening").val(),
                Skiil: skillsSelected
            },
            error: function () {
                alert('there was an error while fetching events!');
            },
        }, eventRender: function (event, element) {


        }, navLinkDayClick: function (date, jsEvent) {
            return false;
        },
        dayRender: function (date, cell) {
        },

    });
    $(".fc-next-button").html("Next").addClass("btn").addClass("btn-default").addClass("custom-cal-button");
    $(".fc-prev-button").html("Prev").addClass("btn").addClass("btn-default").addClass("mrl").addClass("custom-cal-button");
    $(".fc-month-button").html("Month").addClass("btn").addClass("btn-default").addClass("mrl").addClass("custom-cal-button");
    $(".fc-agendaDay-button ").html("Day").addClass("btn").addClass("btn-default").addClass("mrl").addClass("custom-cal-button");
    $(".fc-agendaWeek-button").html("Week").addClass("btn").addClass("btn-default").addClass("custom-cal-button");

    $(".fc-month-button").on("click", function () {
        $(".fc-next-button").html("Next").addClass("btn").addClass("btn-default").addClass("custom-cal-button");
        $(".fc-prev-button").html("Prev").addClass("btn").addClass("btn-default").addClass("mrl").addClass("custom-cal-button");

    });
    $(".fc-agendaDay-button").on("click", function () {
        $(".fc-next-button").html("Next").addClass("btn").addClass("btn-default").addClass("custom-cal-button");
        $(".fc-prev-button").html("Prev").addClass("btn").addClass("btn-default").addClass("mrl").addClass("custom-cal-button");

    });
    $(".fc-agendaWeek-button").on("click", function () {
        $(".fc-next-button").html("Next").addClass("btn").addClass("btn-default").addClass("custom-cal-button");
        $(".fc-prev-button").html("Prev").addClass("btn").addClass("btn-default").addClass("mrl").addClass("custom-cal-button");

    });
    var _d = $("#date").val();
    if ($.trim(_d) != "") {
        var d = _d.split("-");
        $("#jobcalendar").fullCalendar('gotoDate', d[0]);
    }
}
function getprojectmanagerbymembertype(id) {
    var dataString = 'id=0';
    $.ajax
        ({
            type: "POST",
            url: '/default/get_all_mebertypeprojectmanager',
            data: dataString,
            cache: false,
            success: function (data) {

                $("#" + id).html('').removeClass("selectpicker").append($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                $(".selectpicker").selectpicker('refresh');
            }
        });
}
$(window).load(function () {
    getprojectmanagerbymembertype('Project_manager1');
    getprojectmanagerbymembertype('uploadProject_manager1');
});
$("#projectId").on("change", function () {
    var prjid = $("#projectId").val();
    if (prjid != 0) {
        var dataString = 'id=' + prjid;
        $.ajax
            ({
                type: "POST",
                url: '/project/GetProjectDetails',
                data: dataString,
                cache: false,
                success: function (data) {
                    $("#client").val(data.clientId).selectpicker('refresh');
                    $("#Project_manager").val(data.ClientManagerId).selectpicker('refresh');
                    $("#Project_manager1").val(data.projectManagerId).selectpicker('refresh');
                }
            });

    }
});
$(document).ready(function () {
    $("#clear").on('click', function () {
        $('#jobcalendar').fullCalendar('destroy');
        $("#Filter_Location").val('');
        $("#date").val('');
        $("#cclient").val('0');
        $("#cTechnician").val('0');
        $("#cDispatcher").val('0');
        $("#Status").val('0');
        $('.selectpicker').selectpicker('refresh');
        getjobCalender(_userId, 2);
    });
    $("#submit").on('click', function () {
        searchtype = 1;
        getjobCalenderFilter(_userId);
    });
    var start = moment();
    var end = moment().add(30, 'days');
    function cb(start, end) {
        $('.dateRangePicker').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
    }
    $('.dateRangePicker').daterangepicker({
        opens: 'left',
        autoUpdateInput: false
    }, cb);
    //cb(start, end);
    $('.dateRangePicker').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
        $("#date").val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY')); TriggerFilter();
    });
    get_client('cclient', cclient);
    get_client('client', 0);
    get_client('uplodclient', 0);
    function get_client(client, val) {
        var dataString = 'ismulti=0&clientid=' + val;
        $.ajax
            ({
                type: "POST",
                url: '/default/get_all_clients',
                data: dataString,
                cache: false,
                success: function (data) {
                    $("#" + client).html('').removeClass("selectpicker").append($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                    $(".selectpicker").selectpicker('refresh');

                }
            });

    }
    $("#client").change(function () {
        var id = $(this).val();
        if (id != 0) {
            var dataString = 'id=' + id;
            $.ajax
                ({
                    type: "POST",
                    url: '/default/get_all_manager_by_clientid',
                    data: dataString,
                    cache: false,
                    success: function (data) {


                        $("#Project_manager").html('').removeClass("selectpicker").append($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                        $(".selectpicker").selectpicker('refresh');
                    }
                });
            getClientrate('client', 'c_techrate');
        } else {
            $("#Project_manager").html('<option value="0">Manager</option>').attr('disabled', true).selectpicker('refresh');
        }
    });
    $("#uplodclient").change(function () {
        var id = $(this).val();
        if (id != 0) {
            var dataString = 'id=' + id;
            $.ajax
                ({
                    type: "POST",
                    url: '/default/get_all_manager_by_clientid',
                    data: dataString,
                    cache: false,
                    success: function (data) {

                        $("#uploadProject_manager").html('').removeClass("selectpicker").append($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                        $(".selectpicker").selectpicker('refresh');
                    }
                });
        } else {
            $("#uploadProject_manager").html('<option value="0">Manager</option>').attr('disabled', true).selectpicker('refresh');
        }
        getClientrate('uplodclient', 'upclientRate');
    });
    get_Manager_selectbyid('Technician', 'Technician', 0, 'T', '@subcntrcter');
    get_Manager_selectbyid('Technician', 'cTechnician', '@cTechnician', 'T', '@subcntrcter');
    get_Manager_selectbyid('Dispatcher', 'Dispatcher', 0, 'D', 'false');
    get_Manager_selectbyid('Dispatcher', 'cDispatcher', '@cDispatcher', 'D', 'false');
    get_Manager_selectbyid('Manager', 'cProjectManager', '@PmanagerId', 'PM', 'false');
    function get_Manager_selectbyid(_val, cntrlId, memberid, type, issubcontractor) {
        var dataString = 'optvalue=' + _val + "&memberid=" + memberid + "&type=" + type + '&issubcontractor=' + issubcontractor;
        $.ajax
            ({
                type: "POST",
                url: '/default/get_all_member',
                data: dataString,
                cache: false,
                success: function (data) { 
                    $("#" + cntrlId).html('').removeClass("selectpicker").append($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                    $(".selectpicker").selectpicker('refresh');
                }
            });

    }
});
function Uploadfile() {
    $(".box").animate({
        width: "hide"
    });
    $("#uploadslide").animate({
        width: "show"
    });
}
$(".closebox").on("click", function (e) {
    $("#uploadslide").animate({
        width: "hide"
    }); $("#viewslide").animate({
        width: "hide"
    });


    $("#backdrop").css("display", "none");
});
$(window).load(function () {
    setTimeout(function () { $("#filter_div").css("display", "block"); }, 2000);
});
getOpenProjetList('projectId', 0);
getOpenProjetList('uprojectId', 0); getOpenProjetList('fprojectId', projectId);
function getOpenProjetList(elementId, id) {
    var dataString = 'id=' + id + '&name=Project';
    $.ajax
        ({
            type: "POST",
            url: '/default/getallopenproject',
            data: dataString,
            cache: false,
            success: function (data) {
                $("#" + elementId).html('').removeClass("selectpicker").append($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                $(".selectpicker").selectpicker('refresh');
            }
        });
}