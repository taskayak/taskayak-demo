﻿

function deleteReminder(projectToken, token) {
    bootbox.confirm("Are you sure?", function (result) {
        if (result) {
            var _url = "/project/deleteReminder";
            window.location.href = _url + "?token=" + token + "&projectToken=" + projectToken;
        }
    });
}
$("#reminderForm").on("submit", function () {
    var repeatId = $("#repeatid").val();
    var Attachment = $("#Attachment").val();
    var type = $("#type").val();
    var info = $("#info").val();
    if (repeatId == 0) {
        alertify.log('Please select calender setting (Hour,Day,Week,Month)', 'error', 5000);
        return false;
    }

    if (info == null) {
        alertify.log('Please select information', 'error', 5000);
        return false;
    }
    if (type == null) {
        alertify.log('Please select type', 'error', 5000);
        return false;
    }
    return true;
});
function addReminder(_value) {
    $("#backdrop").css("display", "block");
    $(".box").animate({
        width: "hide"
    });
    $("#uploadslide").animate({
        width: "show"
    });
    if (_value == "1") {
        $("#spanremindertype").html("Technician");
    } else {
        $("#spanremindertype").html("Dispatcher");
    }


    $("#usertype").val(_value);
}
$(".closebox").on("click", function (e) {
    $("#uploadslide").animate({
        width: "hide"
    });
    $("#backdrop").css("display", "none");
    $("#usertype").val('1');
});


$(document).ready(function () {

    var table = $('.dt2').DataTable({
        "oLanguage": { "sSearch": "", 'sLengthMenu': "Show _MENU_" },
        "bSort": false, "autoWidth": false, "pageLength": 25, "columnDefs": [
            { "width": "7%", "targets": 0 }, { "width": "15%", "targets": 2 }, { "width": "5%", "targets": 3 }, { "width": "12%", "targets": 4 }
        ]
    });
    $('#filtertype').on("change", function () {
        var filter = $('#filtertype').val();
        var tfilter = $('#filtertech').val();
        var dfilter = $('#filterdispatcher').val();
        table.destroy();
        table = $('.dt2').DataTable({
            "bSort": false, "autoWidth": false, "pageLength": 25, "columnDefs": [
                { "width": "7%", "targets": 0 }, { "width": "15%", "targets": 2 }, { "width": "5%", "targets": 3 }, { "width": "12%", "targets": 4 }
            ]
        });
        if (filter == "" && tfilter == "" && dfilter == "") {

            table.draw();
        } else {
            if (filter != "") {

                table.columns(3).search(filter).draw();
            }
            if (tfilter != "") {

                table.columns(5).search(tfilter).draw();
            }
            if (dfilter != "") {
                table.columns(6).search(dfilter).draw();
            }
        }
        //
    });
    $('#filterdispatcher').on("change", function () {
        var filter = $('#filtertype').val();
        var tfilter = $('#filtertech').val();
        var dfilter = $('#filterdispatcher').val();
        table.destroy();
        table = $('.dt2').DataTable({
            "oLanguage": { "sSearch": "", 'sLengthMenu': "Show _MENU_" },
            "bSort": false, "autoWidth": false, "pageLength": 25, "columnDefs": [
                { "width": "7%", "targets": 0 }, { "width": "15%", "targets": 2 }, { "width": "5%", "targets": 3 }, { "width": "12%", "targets": 4 }
            ]
        });
        if (filter == "" && tfilter == "" && dfilter == "") {

            table.draw();
        } else {
            if (filter != "") {

                table.columns(3).search(filter).draw();
            }
            if (tfilter != "") {

                table.columns(5).search(tfilter).draw();
            }
            if (dfilter != "") {
                table.columns(6).search(dfilter).draw();
            }
        }
    });
    $('#filtertech').on("change", function () {
        var filter = $('#filtertype').val();
        var tfilter = $('#filtertech').val();
        var dfilter = $('#filterdispatcher').val();
        table.destroy();
        table = $('.dt2').DataTable({
            "oLanguage": { "sSearch": "", 'sLengthMenu': "Show _MENU_" },
            "bSort": false, "autoWidth": false, "pageLength": 25, "columnDefs": [
                { "width": "7%", "targets": 0 }, { "width": "15%", "targets": 2 }, { "width": "5%", "targets": 3 }, { "width": "12%", "targets": 4 }
            ]
        });
        if (filter == "" && tfilter == "" && dfilter == "") {

            table.draw();
        } else {
            if (filter != "") {

                table.columns(3).search(filter).draw();
            }
            if (tfilter != "") {

                table.columns(5).search(tfilter).draw();
            }
            if (dfilter != "") {
                table.columns(6).search(dfilter).draw();
            }
        }
    });
    $(".tFileDelete").click(function (e) {
        e.preventDefault();
        var id = this.id;
        var project_id = $(this).data('jobid');
        bootbox.confirm("Are you sure?", function (result) {
            if (result) {
                var _url = "/project/delete_file";
                window.location.href = _url + "?jtoken=" + project_id + "&token=" + id;
            }
        });
    });
});
function get_client(client, clientid) {
    var dataString = 'ismulti=0&clientid=' + clientid;
    $.ajax
        ({
            type: "POST",
            url: '/default/get_all_clients',
            data: dataString,
            cache: false,
            success: function (e) {
                $("#" + client).removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
            }
        });
}



$("#clientId").change(function () {
    var id = $(this).val();
    if (id != 0) {
        var dataString = 'id=' + id;
        $.ajax
            ({
                type: "POST",
                url: '/default/get_all_manager_by_clientid',
                data: dataString,
                cache: false,
                success: function (e) {
                    $("#client_manager").removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                }
            });
    } else {
        $("#client_manager").html('<option value="0">Select</option>').attr('disabled', true).selectpicker('refresh');
    }

});

function get_projectmgr_byclient(id, prid) {
    var dataString = 'ismulti=0&id=' + prid;
    $.ajax
        ({
            type: "POST",
            url: '/default/get_all_mebertypeprojectmanager',
            data: dataString,
            cache: false,
            success: function (e) {
                $("#" + id).removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
            }
        });

}

function GetClientManager(clientId, clientManagerId) {
    var dataString = 'id=' + clientId + '&clientid=' + clientManagerId;
    $.ajax
        ({
            type: "POST",
            url: '/default/get_all_manager_by_clientid',
            data: dataString,
            cache: false,
            success: function (e) {
                $("#client_manager").removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
            }
        });
}

function Tooladd() {
    $('#toolAdd').modal('show');
}
$(".cuschk").on("click", function () {
    debugger;
    var id = $(this).data("id");
    if ($(this).hasClass("bck")) {
        $("#" + id).attr("checked", false);
        $(this).removeClass("bck");
    } else {
        $("#" + id).attr("checked", true);
        $(this).addClass("bck");
    }
    if ($("#" + id).hasClass("Confirmation")) {
        if ($(this).hasClass("bck")) {
            $("#" + id).val('1');
        } else {
            $("#" + id).val('0');
        }
    }
});



$(".Attachment").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
    buttonContainer: '<div class="btn-group attchment" />', enableFiltering: false, buttonText: function (options, select) {
        return 'Attachment';
    },
});

$(".info").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
    buttonContainer: '<div class="btn-group information" />', enableFiltering: false, buttonText: function (options, select) {
        return 'Information';
    },
});
$(".type").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
    buttonContainer: '<div class="btn-group type" />', enableFiltering: false, buttonText: function (options, select) {
        return 'Type';
    },
});

$(".dinfo").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
    buttonContainer: '<div class="btn-group dinformation" />', enableFiltering: false, buttonText: function (options, select) {
        return 'Information';
    },
});

$("#Screening").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
    buttonContainer: '<div class="btn-group frmInfo" />', enableFiltering: false, buttonText: function (options, select) {
        return 'Select';
    },
});




$(window).load(function () {
    setTimeout(function () { get_client('clientId', clientId); }, 500);
    setTimeout(function () { get_projectmgr_byclient('Project_managers', projectManagerId); }, 800);
    setTimeout(function () { GetClientManager(clientId, clientManagerId); }, 900);
});
