﻿function updatestatus() {
    var nid = $("#mid").val();
    var nsid = $("#mstatusid").val();
    var offerCount = $("#offercount").val();
    var jobcount = $("#jobcount").val();
    if (nsid == 0) {
        alertify.log('Select project status', 'error', 5000);
        return false;
    }
    if ((offerCount > 0 || jobcount > 0) && nsid == 2) {
        alertify.log('Project with active job/offer can not be closed');
    } else {
        var _url = "/project/update_status";
        $.get(_url, { 'id': nid, 'statusid': nsid }, function () {
            var _txt = $("#mstatusid").find("option:selected").text();
            var cls = "s" + $("#spn_" + nid).html();
            $("#spn_" + nid).html(_txt);
            _txt = "s" + _txt;
            $("#spn_" + nid).removeClass(cls.replace(" ", "").toLowerCase()).addClass(_txt.toLowerCase().replace(" ", "").toLowerCase());
            $('#mdlstatus').modal('hide');
            alertify.log('Project status updated succesfully', 'success', 5000);  
        });
    }
}

function ClearFilter() {
    ClearText('date', 'date', false);
    ClearSingleDropDown('pmanager', 'Project_managers', false);
    ClearSingleDropDown('screening', 'screening', false);
    searchtype = 2;
    $("#date").val('');
    pageSize = 25;
    $("#cclient").val('0'); 
    client = 0;
    f_pmanager = 0; 
    cmanager=0; 
    $("#client_manager").val('0');
    $("#Project_managers").val('0');
    $("#Status").val('0');
    $("#screening").val('0')
    $('.selectpicker').selectpicker('refresh');
    $("#filterpopup").css("display", "none");     
    otable.page.len(pageSize).draw(true);
}

function TriggerFilter() { 
    var IsFilterActive = true;
    searchtype = 0;  
    client = $("#cclient").val(); 
    f_pmanager = $("#Project_managers").val(); 
    cmanager=$("#client_manager").val();
    var Status = $("#Status").val();//0 
    var screening = $("#screening").val();//0 
    var date = $("#date").val();//''  
    if (date == "" && screening == "0" && Status == "0" && f_pmanager == "0" && cmanager == "0" && client == "0") {
        IsFilterActive = false;
    }
    if (IsFilterActive) {
        $("#filterpopup").css("display", "block");
    } else {
        $("#filterpopup").css("display", "none");
    }
    otable.page.len(pageSize).draw(true);
}

function FilterTable() {
    $("#filterpopup").css("display", "block");
    searchtype = 1; client = $("#cclient").val();
    f_pmanager = $("#Project_managers").val();
    cmanager = $("#client_manager").val();
    otable.page.len(pageSize).draw(true);
}  
$(document).ready(function () {
    $("#table").on("click", ".cdelete", function (e) {
        e.preventDefault();
        var id = this.id;
        var offerCount = $(this).data('offercount');
        var jobCount = $(this).data('jobcount');
        if (jobCount > 0 || offerCount > 0) {
            alertify.log('Project with active job/offer can not be deleted');
            return false;
        } else {
            bootbox.confirm("Are you sure?", function (result) {
                if (result) {
                    var _url = "/project/index";
                    window.location.href = _url + "?token=" + id;
                }
            });
        }
    });
    $("#table").on("click", ".stupdater", function (e) {
        e.preventDefault();
        var offerCount = $(this).data('offercount');
        var jobCount = $(this).data('jobcount');
        var statusid = $(this).data("statusid");
        var memberId = $(this).data("memberid");
        $("#offercount").val(offerCount);
        $("#jobcount").val(jobCount);
        $("#mid").val(memberId);
        $("#mstatusid").val(statusid);
        $('#mstatusid').selectpicker('refresh');
        $('#mdlstatus').modal('show');
    });
    var start = moment();
    var end = moment().add(30, 'days');
    function cb(start, end) {
        $('.dateRangePicker').val(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
        TriggerFilter();
    }
    $('.dateRangePicker').daterangepicker({
        opens: 'left',
        autoUpdateInput: false
    }, cb);
    //cb(start, end);
    $('.dateRangePicker').on('apply.daterangepicker', function (picker) {
        $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
        $("#date").val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
        TriggerFilter();
    });
    function get_client(client, clientid) {
        var dataString = 'ismulti=0&clientid=' + clientid;
        $.ajax
            ({
                type: "POST",
                url: '/default/get_all_clients',
                data: dataString,
                cache: false,
                success: function (e) {
                    $("#" + client).removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                    $(".selectpicker").selectpicker('refresh');
                }
            });

    }
    $("#cclient").change(function () {
        var id = $(this).val();
        client = id;
        if (id != 0) {
            var dataString = 'name=Client Manager&id=' + id;
            $.ajax
                ({
                    type: "POST",
                    url: '/default/get_all_manager_by_clientid',
                    data: dataString,
                    cache: false,
                    success: function (e) {
                        $("#client_manager").removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                        $(".selectpicker").selectpicker('refresh');
                    }
                });
        } else {
            $("#client_manager").html('<option value="0">Client Manager</option>').attr('disabled', true).selectpicker('refresh');
        }

    });

    function get_projectmgr_byclient(id, managerid) {
        var dataString = 'ismulti=0&id=' + managerid + '&name=Project Manager';
        $.ajax
            ({
                type: "POST",
                url: '/default/get_all_mebertypeprojectmanager',
                data: dataString,
                cache: false,
                success: function (e) {
                    $("#" + id).removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                    $(".selectpicker").selectpicker('refresh');
                }
            });

    }
    function get_clientmanager(clientid, managerid) { 
        var dataString = 'name=Client Manager&id=' + clientid + '&clientid=' + managerid;
        $.ajax
            ({
                type: "POST",
                url: '/default/get_all_manager_by_clientid',
                data: dataString,
                cache: false,
                success: function (e) {
                    $("#client_manager").removeClass("selectpicker").html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                    $(".selectpicker").selectpicker('refresh');
                }
            });
    }
    get_client('cclient', client);
    get_clientmanager( client,cmanager);
    get_projectmgr_byclient('Project_managers', f_pmanager); 
});

$("#Project_managers").on("change", function () {
    var id = $(this).val();
    f_pmanager = id;
});
$("#client_manager").on("change", function () {
    var id = $(this).val();
    cmanager = id;
});
$(document).ready(function () {
    otable = $('.data-table').DataTable({
        "processing": true,
        "serverSide": true,
        'language': {
            'loadingRecords': '&nbsp;',
            'processing': "<img src='/Content/Images/loader.gif'> Loading..."
        }, "oLanguage": { "sSearch": "", 'sLengthMenu': "Show _MENU_" },
        "drawCallback": function (settings) {
            settings._iDisplayLength = pageSize;
        },
        "iDisplayLength": pageSize,
        "autoWidth": false,
        "order": [],
        "bSort": false,
        "bFilter": true,
        "ajax": {
            "url": "/project/Fetchprojects",
            "data": function (d) {
                d.date = $("#date").val();
                d.cclient = client;/*$("#cclient").val();*/
                d.Status = $("#Status").val();
                d.cmanager = cmanager; /*$("#client_manager").val();*/
                d.pmanager = f_pmanager; /*$("#Project_managers").val();*/
                d.searchtype = searchtype;
                d.Screening = $("#screening").val();
            }
        },
        "aoColumns": [
            {
                bSortable: false,
                mRender: function (data, type, full) {
                    return '<span>' + full.project_ID + '</span>';
                }
            },
            {
                bSortable: false,
                mRender: function (data, type, full) {
                    return '<a  href="/project/view?token=' + full.token + '">' + full.Title + '</a>';
                }
            },
            {
                bSortable: false,
                mRender: function (data, type, full) {
                    return '<input type="button" class=" offerbutton" value="' + full.offerCount + '">|<input type = "button" class=" jobbutton" value = "' + full.jobCount + '" >';
                }
                , className: "centertext"
            },
            { mData: "Client" },
            { mData: "clientmanager" },
            { mData: "Project_manager" },

            { mData: "reminderCount", className: "centertext" },

            {
                bSortable: false,
                mRender: function (data, type, full) {
                    return "<span data-offerCount='" + full.offerCount + "' data-jobCount='" + full.jobCount + "' id='spn_" + full.ID + "' data-statusid='" + full.status_id + "' data-memberid='" + full.ID + "' class='stupdater s" + full.Status.replace(" ", "").toLowerCase() + "'>" + full.Status + "</span>";
                }
            },
            {
                bSortable: false,
                mRender: function (data, type, full) {
                    var tbl = '<div class="btn-group pos-static"><a class="customedropdown" data-toggle="customedropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v fn21"></i> </a><div class="dropdown-menu customdropdown-menu" x-placement="bottom-start" style="top: 0px; transform: translate3d(-94px, 10px, 0px);will-change: transform;position: absolute;left: 0px; display: none;">';
                    if (viewpermission == "TRUE") {
                        tbl = tbl + '<a class="btn btn-complete btn-xs" href="/project/view?token=' + full.token + '"><i class="fa fa-eye"></i> View</a>';
                    }
                    else {
                        tbl = tbl + '<a href="#" class="disabledbtn btn btn-complete btn-xs" data-msg="You are not authorized to view selected Project"><i class="fa fa-eye"></i> View</a>';
                    }
                    if (editpermission == "TRUE") {
                        tbl = tbl + '<a class="btn btn-success btn-xs" href="/project/edit?token=' + full.token + '"><i class="fa fa-edit"></i> Edit</a>';

                    }
                    else {
                        tbl = tbl + '<a href="#" class="disabledbtn btn btn-success btn-xs" data-msg="You are not authorized to edit selected Project"><i class="fa fa-edit"></i> Edit</a>';
                    }
                    if (deletepermission == "TRUE") {
                        if (full.offerCount == "0") {
                            tbl = tbl + '<a  class="btn btn-danger btn-xs cdelete" id="' + full.token + '" data-offercount="' + full.offerCount + '" data-jobcount="' + full.jobCount + '"><i class="fa fa-trash"></i> Delete</a>';
                        } else {
                            tbl = tbl + '<a   data-msg="Project can not deleted with active job/offer"  class="btn btn-danger btn-xs disabledbtn" id="' + full.token + '" data-offercount="' + full.offerCount + '" data-jobcount="' + full.jobCount + '"><i class="fa fa-trash"></i> Delete</a>';
                        }
                    }
                    else {
                        tbl = tbl + '<a href="#" class="disabledbtn btn btn-danger btn-xs" data-msg="You are not authorized to delete selected Project"><i class="fa fa-trash"></i> Delete</a>';
                    }
                    tbl = tbl + '<a  class="btn btn-info btn-xs"><i class="fa fa-window-close"></i> Close</a>';
                    tbl = tbl + '</div></div>';
                    return tbl;
                }
            }

        ]
    });
    otable.on('length.dt', function (e, settings, len) {
        pageSize = len;
        
    });
});

$(window).load(function () { 
    setTimeout(function () { $("#filter_div").css("display", "block"); }, 2500);
});
