﻿$("#Screening").removeClass('selectpicker').attr("multiple", "multiple").multiselect({
    buttonContainer: '<div class="btn-group customwidth" />'
    , buttonText: function (options, select) {
        if (options.length === 0) {
            return 'Screening';
        }
        else {
            var labels = [];
            options.each(function () {
                if ($(this).attr('label') !== undefined) {
                    labels.push($(this).attr('label'));
                }
                else {
                    labels.push($(this).html());
                }
            });
            return 'Screening (' + labels.join(', ') + ')';
        }
    }
});


function GetClientManager(clientId, clientManagerId) {
    var dataString = 'id=' + clientId + '&clientid=' + clientManagerId;
    $.ajax
        ({
            type: "POST",
            url: '/default/get_all_manager_by_clientid',
            data: dataString,
            cache: false,
            success: function (e) {
                $("#client_manager").addClass("selectpicker").html($.parseHTML(e)).selectpicker('refresh').removeAttr('disabled');
                $(".selectpicker").selectpicker('refresh');
            }
        });
}



$(".cuschk").on("click", function () {
    var id = $(this).data("id");
    if ($(this).hasClass("bck")) {
        $("#" + id).attr("checked", false);
        $(this).removeClass("bck");
    } else {
        $("#" + id).attr("checked", true);
        $(this).addClass("bck");
    }

});
function get_client(client, clientid) {
    var dataString = 'ismulti=0&clientid=' + clientid;
    $.ajax
        ({
            type: "POST",
            url: '/default/get_all_clients',
            data: dataString,
            cache: false,
            success: function (e) {
                $("#" + client).html($.parseHTML(e)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
                $(".selectpicker").selectpicker('refresh');
            }
        });

}
$("#clientId").change(function () {
    var id = $(this).val();
    if (id != 0) {
        var dataString = 'id=' + id;
        $.ajax
            ({
                type: "POST",
                url: '/default/get_all_manager_by_clientid',
                data: dataString,
                cache: false,
                success: function (e) {
                    $("#client_manager").html("selectpicker").append($.parseHTML(e)).selectpicker('refresh').removeAttr('disabled');
                    $(".selectpicker").selectpicker('refresh');
                }
            });
    } else {
        $("#client_manager").html('<option value="0">Select</option>').attr('disabled', true).selectpicker('refresh');
        $(".selectpicker").selectpicker('refresh');
    }

});

function get_projectmgr_byclient(id, prjmanager) {
    var dataString = 'ismulti=0&id=' + prjmanager;
    $.ajax
        ({
            type: "POST",
            url: '/default/get_all_mebertypeprojectmanager',
            data: dataString,
            cache: false,
            success: function (e) {
                $("#" + id).addClass("selectpicker").html($.parseHTML(e)).selectpicker('refresh').removeAttr('disabled');
                $(".selectpicker").selectpicker('refresh');
            }
        });

}







function Tooladd() {
    $('#toolAdd').modal('show');
}

function calculateselectedtools() {
    var count = 0;
    $("input[name='tools']").each(function () {
        if ($(this).next().next().hasClass("bck")) {
            count = count + 1;
        }
    });
    $("#toolspan").html(count);
}

function AddToolscloseModal() {
    calculateselectedtools();
    $('#toolAdd').modal('hide');
}



function Skilladd() {
    $('#skillAdd').modal('show');
}

function calculateselectedskills() {
    var count = 0;
    $("input[name='skills']").each(function () {
        if ($(this).next().next().hasClass("bck")) {
            count = count + 1;
        }
    });
    $("#skillspan").html(count);
}

function AddskillscloseModal() {
    calculateselectedskills();
    $('#skillAdd').modal('hide');
}

function screeningAdd() {
    $('#screeningAdd').modal('show');
}

function calculateselectedscreening() {
    var count = "";
    $("input[name='screening']").each(function () {
        if ($(this).next().hasClass("bck")) {
            if (count != "") {
                count = count + ",";
            }
            count += $(this).val();
        }
    });
    $("#screeningspan").html(count);
}


function AddscreeningcloseModal() {
    calculateselectedscreening();
    $('#screeningAdd').modal('hide');
}
$(window).load(function () {
    setTimeout(function () { get_client('clientId', clientId); }, 500);
    setTimeout(function () { get_projectmgr_byclient('Project_managers', projectManagerId); }, 800);
    setTimeout(function () { GetClientManager(clientId, clientManagerId); }, 900);
});
