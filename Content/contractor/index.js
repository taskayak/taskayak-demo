﻿$(document).ready(function () {

    otable = $('.data-table').DataTable({
        "processing": true,
        "serverSide": true,
        'language': {
            'loadingRecords': '&nbsp;',
            'processing': "<img src='/Content/Images/loader.gif'> Loading..."
        },
        "oLanguage": { "sSearch": "", 'sLengthMenu': "Show _MENU_" },
        "iDisplayLength": 25,
        "order": [],
        "bSort": false,
        "bFilter": true,
        "autoWidth": false,
        "ajax": {
            "url": "/Contractor/FetchContractor"
        },
        "aoColumns": [
            {
                bSortable: false,
                mRender: function (data, type, full) {
                    if (viewpermission == "TRUE") {
                        return '<a href="/Contractor/view?token=' + full.token + '">' + full.name + '</a>';

                    }
                    else {
                        return '<a href="#" class="disabledbtn" data-msg="You are not authorized to view selected company">' + full.name + '</a>';
                    }
                }
            },
            { mData: "primarycontactname" },
            { mData: "rate" },
            { mData: "email" },
            { mData: "phone" },
            {
                bSortable: false,
                mRender: function (data, type, full) {
                    var tbl = '<div class="btn-group pos-static"><a class="customedropdown" data-toggle="customedropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v fn21"></i> </a><div class="dropdown-menu customdropdown-menu" x-placement="bottom-start" style="top: 0px; transform: translate3d(-72px, 10px, 0px);will-change: transform;position: absolute;left: 0px; display: none;">';
                    if (viewpermission == "TRUE") {
                        tbl = tbl + '<a class="btn btn-purple btn-xs" href="/Contractor/view?token=' + full.token + '"><i class="fa fa-search"></i> View</a>';

                    }
                    else {
                        tbl = tbl + '<a href="#" class="disabledbtn btn btn-purple btn-xs" data-msg="You are not authorized to view selected company"><i class="fa fa-search"></i> View</a>';
                    }
                    if (editpermission == "TRUE") {
                        tbl = tbl + '<a class="btn btn-success btn-xs" href="/Contractor/edit?token=' + full.token + '"><i class="fa fa-edit"></i> Edit</a>';

                    }
                    else {
                        tbl = tbl + '<a href="#" class="disabledbtn btn btn-success btn-xs" data-msg="You are not authorized to edit selected company"><i class="fa fa-edit"></i> Edit</a>';
                    }
                    if (deletepermission == "TRUE") {
                        tbl = tbl + '<a class="btn btn-danger btn-xs cdelete" id="' + full.token + '"><i class="fa fa-trash"></i> Delete</a>';

                    }
                    else {
                        tbl = tbl + '<a href="#" class="disabledbtn btn btn-danger btn-xs" data-msg="You are not authorized to delete selected company"><i class="fa fa-trash"></i> Delete</a>';
                    }
                    tbl = tbl + '<a  class="btn btn-info btn-xs"><i class="fa fa-window-close"></i> Close</a>';
                    tbl = tbl + '</div></div>';
                    return tbl;
                }
            }

        ]
    });
});

$(document).ready(function () {
    $("#table").on("click", ".cdelete", function (e) {
        e.preventDefault();
        var id = this.id;
        bootbox.confirm("Are you sure?", function (result) {
            if (result) {
                var _url = "/Contractor/delete";
                window.location.href = _url + "?token=" + id;
            }
        });
    });
});
function Uploadfile() {
    $('#upload').modal('show');
}