﻿
getallmember(_contractorId);
getManager('manager_id', _contractorId);
get_state('0', 'companystate_id');

$(document).ready(function () {
    otable = $('.data-table').DataTable({
        "processing": true,
        "serverSide": true,
        "iDisplayLength": 25,
        "order": [],
        "bSort": false,
        "bFilter": true,
        "autoWidth": false,
        'language': {
            'loadingRecords': '&nbsp;',
            'processing': "<img src='/Content/Images/loader.gif'> Loading..."
        }, "oLanguage": { "sSearch": "", 'sLengthMenu': "Show _MENU_" },
        "ajax": {
            "url": _ajaxUrl
        },
        "aoColumns": [
            {
                bSortable: false,
                mRender: function (data, type, full) {
                    if (editpermission == "TRUE") {
                        return '<a href="#" data-phone="' + full.phone + '" data-email="' + full.email + '" data-name="' + full.name + '" class="cedit" data-prjmanagerid="' + full.prjmanagerid + '">' + full.name + '</a>';

                    }
                    else {
                        return '<a href="#" class="disabledbtn" data-msg="You are not authorized to view selected User">' + full.name + '</a>';
                    }
                }
            },
            { mData: "position" },
            { mData: "email" },
            { mData: "phone" },
            { mData: "manager" },
            {
                bSortable: false,
                mRender: function (data, type, full) {
                    var tbl = '<div class="btn-group pos-static"><a class="customedropdown" data-toggle="customedropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v fn21"></i> </a><div class="dropdown-menu customdropdown-menu" x-placement="bottom-start" style="top: 0px; transform: translate3d(-72px, 10px, 0px);will-change: transform;position: absolute;left: 0px; display: none;">';
                    if (editpermission == "TRUE") {
                        tbl = tbl + '<a  data-phone="' + full.phone + '" data-email="' + full.email + '" data-name="' + full.name + '" class="btn btn-success btn-xs cedit" data-prjmanagerid="' + full.prjmanagerid + '" href="#"><i class="fa fa-edit"></i> Edit</a>';

                    }
                    else {
                        tbl = tbl + '<a href="#" class="disabledbtn btn btn-success btn-xs" data-msg="You are not authorized to edit selected user"><i class="fa fa-edit"></i> Edit</a>';
                    }
                    if (deletepermission == "TRUE") {
                        tbl = tbl + '<a class="btn btn-danger btn-xs cdelete" id="' + full.prj_token + '"  data-clientid="' + full.clt_token + '"><i class="fa fa-trash"></i> Delete</a>';

                    }
                    else {
                        tbl = tbl + '<a href="#" class="disabledbtn btn btn-danger btn-xs" data-msg="You are not authorized to deactivate selected user"><i class="fa fa-trash"></i> Deactivate</a>';
                    } tbl = tbl + '<a  class="btn btn-info btn-xs"><i class="fa fa-window-close"></i> Close</a>';
                    tbl = tbl + '</div></div>';
                    return tbl;
                }
            }

        ]
    });
});

function getallmember(clientid) { 
    var dataString = 'cid=' + clientid;
    $.ajax
        ({
            type: "POST",
            url: '/default/get_all_memberid/")',
            data: dataString,
            cache: false,
            success: function (data) {
                $("#member_id").prop('disabled', false).removeClass("selectpicker").html($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
            }
        });

}
function OpenCompany(clientid, cmpnyname, cprimarycontactname, website, rate, email, phone, caddress, stateid, cvalue, zip) {
    $("#cuserid").val(clientid);
    $("#cname").val(cmpnyname);
    $("#caddress").val(caddress);
    $("#cprimarycontactname").val(cprimarycontactname);
    $("#cwebsite").val(website);
    $("#cemail").val(email);
    $("#crate").val(rate);
    $("#cphone").val(phone);
    $("#czipcode").val(zip);
    $("#companycity_id").val(cvalue);
    $("#companystate_id").val(stateid).selectpicker('refresh');
    $("#compnaymodel").modal("show");
}

$("#member_id").on("change", function () {
    var _id = $("#member_id").val();
    var url = '/Contractor/Getmeberbyid';
    $.get(url, { 'id': _id }, function (data) {
        $("#membertype").val(data.UserType).selectpicker('refresh');
        $("#Position").val(data.Position); 
         $("#suite").val(data.Zipcode); 
        $("#MapValidateAddress").val(data.Address);
        $("#phone").val(data.PhoneNumber);
        $("#email").val(data.EmailAddress);
        $("#fname").val(data.FirstName);
        $("#lname").val(data.LastName);
        $("#manager_id").val(data.ManagerId).selectpicker('refresh');
    });
});

function getManager(elementid, CompanyId) {
    var dataString = 'id=' + CompanyId;
    $.ajax
        ({
            type: "POST",
            url: '/default/get_all_active_memberbycompanyid',
            data: dataString,
            cache: false,
            success: function (data) {
                $("#" + elementid).prop('disabled', false).removeClass("selectpicker").html($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
            }
        });
}

function getManager(elementid, CompanyId,UserId) {
    var dataString = 'id=' + CompanyId + '&userId=' + UserId;
    $.ajax
        ({
            type: "POST",
            url: '/default/get_all_active_memberbycompanyid',
            data: dataString,
            cache: false,
            success: function (data) {
                $("#" + elementid).prop('disabled', false).removeClass("selectpicker").html($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
            }
        });
}
$(".closebox").on("click", function (e) {
    $("#uploadslide").animate({
        width: "hide"
    });
    $("#backdrop").css("display", "none");
});
function Uploadfile() {
    $("#backdrop").css("display", "block");
    $("#uploadslide").animate({
        width: "hide"
    });
    $("#uploadslide").animate({
        width: "show"
    });
}
function add_new(clientid) {
    $('#add-new-manager').modal('show');
    $("#clientid").val(clientid);
}
$("#table").on("click", ".cdelete", function (e) {
    e.preventDefault();
    var id = this.id;
    var client_id = $(this).data('clientid');
    bootbox.confirm("Are you sure?", function (result) {
        if (result) {
            var _url = "/Contractor/delete_member";
            window.location.href = _url + "?clienttoken=" + client_id + "&token=" + id;
        }
    });
})
function get_state(state_id, id) {
    var dataString = 'ismulti=0&stateid=' + state_id;
    $.ajax
        ({
            type: "POST",
            url: '/default/get_all_state',
            data: dataString,
            cache: false,
            success: function (data) {
                $("#" + id).prop('disabled', false).removeClass("selectpicker").html($.parseHTML(data)).addClass("selectpicker").selectpicker('refresh').removeAttr('disabled');
            }
        });

}

$("#table").on("click", ".cedit", function (e) {
    e.preventDefault();
    var _id = $(this).data('prjmanagerid');
    $("#euserid").val(_id);
    var _clientid = $("#clientid").val();
    getManager('emanager_id', _clientid, _id);
    var url = '/Contractor/Getmeberbyid';
    $.get(url, { 'id': _id }, function (data) { 
        $("#emembertype").val(data.UserType).selectpicker('refresh');
        $("#ePosition").val(data.Position);
        $("#ecity").val(data.CityName);
        $("#estate").val(data.StateId).selectpicker('refresh');
        $("#esuite").val(data.Zipcode);
        $("#eMapValidateAddress").val(data.Address);  
        $("#ephone").val(data.PhoneNumber);
        $("#eemail").val(data.EmailAddress);
        $("#efname").val(data.FirstName);
        $("#elname").val(data.LastName);
        $("#emanager_id").val(data.ManagerId).selectpicker('refresh');
        $("#emember_id").val(data.UserId);
       
    }); 
    $('#update-manager').modal('show'); 
});