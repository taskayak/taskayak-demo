/*
--------------------------------------
---------- Input Group File ----------
--------------------------------------
*/

$.fn.inputFile = function () {
    var $this = $(this);
    $this.find('input[type="file"]').on('change', function () {
        $this.find('input[type="text"]').val($(this).val());
    });
};
$('input, :input, select').attr('autocomplete', 'random');
$('form').attr('autocomplete', 'random');
$('.input-group-file').inputFile();

$(document).on('click', '[data-toggle="customedropdown"]', function () {
    if (!$(this).parent().hasClass("open")) {
        $(".customdropdown-menu").hide();
        $(this).next("div").show();
        $(this).parent().addClass("open");
    } else {
        $(this).parent().removeClass("open");
        $(".customdropdown-menu").hide();
    }
});

$(document).on('click', function (e) {
    if (e.target.nodeName != "I") { $(".customdropdown-menu").hide(); }
});
function get_format($format) {

    switch ($format) {
        case 'd/m/Y':
            return 'DD/MM/YYYY';
            break;

        case 'd.m.Y':
            return 'DD.MM.YYYY';
            break;

        case 'd-m-Y':
            return 'DD-MM-YYYY';
            break;

        case 'm/d/Y':
            return 'MM/DD/YYYY';
            break;

        case 'Y/m/d':
            return 'YYYY/MM/DD';
            break;

        case 'Y-m-d':
            return 'YYYY-MM-DD';
            break;

        case 'M d Y':
            return 'MMMM DD YYYY';
            break;

        case 'd M Y':
            return 'DD MMMM YYYY';
            break;

        case 'jS M y':
            return 'Do MMMM YY';
            break;

        default:
            return 'YYYY-MM-DD';
            break;

    }

}

var dateFormat = $('#_DatePicker').val();


/*
--------------------------------------
---------- Date Time Picker ----------
--------------------------------------
*/
$('.datePicker').datetimepicker({
    keepOpen: true,
    format: get_format(dateFormat)
});

$('.monthPicker').datetimepicker({
    keepOpen: true,
    format: 'YYYY-MM'
});

$('.timePicker').datetimepicker({
    keepOpen: true,
    format: 'LT'
});

$('.dateTimePicker').datetimepicker({
    keepOpen: true
});

/*
--------------------------------------
---------- Input type remove autocomplete ----------
--------------------------------------
*/
function makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}
$(window).load(function () {
    $('input, :input, select, .pac-target-input').attr('autocomplete', 'random');
    setTimeout(function () {
        if ($("#alert").length > 0) {
            var _class = $("#alert").data('class');
            var _text = $("#alert").data('text');
            alertify.log(_text, _class, 5000);
        }
    }, 500);
    $("input").focusin(
        function () {
            if (this.hasAttribute('name')) {
                var name = $(this).attr("name");
                var x = Math.floor((Math.random() * 10) + 1);
                var x1 = makeid(x);
                if (!this.hasAttribute('data-namev1')) {
                    $(this).attr('data-namev1', name);
                }
                $(this).attr('name', name + x1);
            }
        }).focusout(
            function () {
                if (this.hasAttribute('name')) {
                    var name = $(this).data("namev1");
                    $(this).attr('name', name);
                }
            });
    $("form").removeAttr("novalidate");
    $("html, body").animate({ scrollTop: 0 }, "fast");
    $("input:hidden").each(function () {
        if (this.hasAttribute('data-namev1')) {
            var name = $(this).data("namev1");
            $(this).attr('name', name);
        }
    });
    $('input[type=text][readonly]').each(function () {
        if (this.hasAttribute('data-namev1')) {
            var name = $(this).data("namev1");
            $(this).attr('name', name);
        }
    });
    $("input:text:visible:first").focus();
    $("input:text:visible:first").click();
});
/*
--------------------------------------
---------- Slide filter ----------
--------------------------------------
*/

(function ($, window) {
    $.fn.resizedropdown = function (settings) {
        return this.each(function () {
            $(this).change(function () {
                var $this = $(this);
                var text = $this.find("option:selected").text();
                var $test = $("<span>").html(text).css({
                    "font-size": $this.css("font-size"), // ensures same size text
                    "visibility": "hidden" 							 // prevents FOUC
                });
                $test.appendTo($this.parent());
                var width = $test.width();
                $test.remove();
                var arrowWidth = $(this).data("width");
                var _width = width + arrowWidth;
                var isMulti = $this.prop('multiple'); // true/false
                if (isMulti == true) {
                    $this.next(".btn-group").find(".dropdown-toggle").attr('style', 'width:' + arrowWidth + 'px !important');
                } else {
                    $this.next().attr('style', 'width:' + _width + 'px !important');
                }
                //$this.next().attr('style', 'width:' + _width + 'px !important');
            }).change();
        });
    };
})(jQuery, window);


function FilterOpen() {
    $("#backdrop").css("display", "none");
    $(".box").animate({
        width: "hide"
    });
    $("#FilterSlide").animate({
        width: "show"
    });
}
$(".closebox").on("click", function (e) {
    $(".box").animate({
        width: "hide"
    });
    $("#backdrop").css("display", "none");
});

function ClearText(Name, ElementId, TriggerFunction) {
    $("#Filter_" + Name + "_div").hide("slow");
    $("#" + ElementId).val('');
    $("#bck_chk_" + Name).removeClass("bck");
    $("#chk_" + Name).attr("checked", false);
    if (TriggerFunction) {
        TriggerFilter();
    }
}
function ClearSingleDropDown(Name, ElementId, TriggerFunction) {
    $("#Filter_" + Name + "_div").hide("slow");
    $("#" + ElementId).val('0');
    $(".selectpicker").selectpicker('refresh');
    $("#bck_chk_" + Name).removeClass("bck");
    $("#chk_" + Name).attr("checked", false);
    if (TriggerFunction) {
        TriggerFilter();
    }
}
function ClearMultiDropDown(Name, ElementId, TriggerFunction) {
    $("#Filter_" + Name + "_div").hide("slow");
    $('#' + ElementId).multiselect('deselectAll', false);
    $(".selectpicker").selectpicker('refresh');
    $("#bck_chk_" + Name).removeClass("bck");
    $("#chk_" + Name).attr("checked", false);
    if (TriggerFunction) {
        TriggerFilter();
    }
}

/*  Type 1=Textbox
 *  Type 2=SingledropDown
 *   Type 3=MultidropDown
*/
$(".FilterCheck").on("click", function () {
    var id = $(this).data("id");
    var divid = $(this).data("div");
    var elementid = $(this).data("element");
    var type = $(this).data("type");
    if ($(this).hasClass("bck")) {
        $("#" + id).attr("checked", false);
        $(this).removeClass("bck");
        $("#" + divid).hide("slow");
        if (type == 1) {
            $("#" + elementid).val('');
        } else if (type == 2) {
            $("#" + elementid).val('0');
            $("#" + elementid).selectpicker('refresh');
        }
        else {
            $('#' + elementid).multiselect('deselectAll', false);
        }
        TriggerFilter();
    } else {
        $("#" + id).attr("checked", true);
        $(this).addClass("bck");
        $("#" + divid).show("slow");
    }

});
