var $ = jQuery;

$(document).ready(function(){
	if ( $('.textarea-wysihtml5').length > 0 ) {
		$('.textarea-wysihtml5').wysihtml5({
			toolbar: {
				'fa': true
			}
		});
	}
 
});


$('#left-nav .nav-bottom-sec').slimScroll({
	height: '100%',
	size: '4px',
	color: '#999'
});

$(window).load(function () {
    $("#table_wrapper>.row>.col-sm-12").addClass("newscroll");
    $(".dataTables_length").parent().addClass("pull-right");
    $(".dataTables_filter").parent().addClass("pull-left");
    $(".dataTables_length>label").addClass("pull-right");
    $(".dataTables_filter>label").addClass("pull-left"); 
    $(".dataTables_filter input").attr("placeholder", "Search");
    $(".dataTables_length select[name='table_length']").removeClass("form-control").addClass("Selectpicker"); 
});
 

$('#bar-setting').click(function (e) { 
	e.preventDefault();
	if ( $(window).width() > 767 ) {
        if ($('body.has-left-bar').hasClass('left-bar-open')) {
            $('body.has-left-bar').removeClass('left-bar-open').addClass('new-bar-open');
            //if ($(window).width() > 1840) {
            //    $(".table-ultra-responsive").removeClass("overflwx");
            //}
           
        } else {
            $('body.has-left-bar').addClass('left-bar-open').removeClass('new-bar-open');
            //$(".table-ultra-responsive").addClass("overflwx");
         
        }
       
	} else {
		$('.left-nav-bar .nav-bottom-sec').slideToggle(slow, function(){
            $('body.has-left-bar').toggleClass('left-bar-open');
            $('.nav-bottom-sec').css("display", "block");
		});
	}

});



$('#left-navigation').find('li.has-sub>a').on('click', function(e){
	e.preventDefault();
	var $thisParent = $(this).parent();

	if ( $thisParent.hasClass('sub-open') ) {

		// Hide the Submenu
		$thisParent.removeClass('sub-open').children('ul.sub').slideUp(500);

	} else {

		// Show the Submenu
		$thisParent.addClass('sub-open').children('ul.sub').slideDown(500);

		// Hide Others Submenu
		$thisParent.siblings('.sub-open').removeClass('sub-open').children('ul.sub').slideUp(150);

	}
});


// alertify customize

alertify.warning = alertify.extend("warning");
alertify.info = alertify.extend("info");

// Tooltip init

$(function () {
    $('[data-toggle="tooltip"]').tooltip({
        trigger: 'hover'
    }) 

    $('[data-toggle="tooltip"]').on('click', function () {
    $(this).tooltip('hide')
})
});
// Form
(function(){

	$('.form-group.form-group-default .form-control').on('focus', function(e){
		$(this).closest('.form-group').addClass('focused');
	}).on('blur', function(e){
		var $closest = $(this).closest('.form-group');
		if ($(this).val().length > 0) {
			$closest.addClass('filled');
		} else {
			$closest.removeClass('filled');
		}
		$closest.removeClass('focused');
	});

	$('.form-group.form-group-default select.form-control').on('change', function(){
		$(this).closest('.form-group').addClass('filled');
    });
    $("#popuptable").on("click", ".parentcomment", function () {
        var msgid = $(this).data("id");
        var jobid = $(this).data("jobid");
        var isoffer = $(this).hasClass("offer");
        if (isoffer) {
            updateofferread(msgid);
        } else {
            updateread(msgid);
        }
        $(".childcommentcomment").css("display", "none");
        var _count = parseInt($("#commentbadge_" + jobid).html());
        if ($(this).hasClass("unread")) {
            _count = _count - 1;
        }
        $(this).removeClass("unread");
        if ($(this).next('.childcommentcomment').hasClass("hide")) {

            $(this).next('.childcommentcomment').removeClass("hide").addClass("show").removeClass("unread");
        } else {
            $(this).next('.childcommentcomment').removeClass("show").addClass("hide").removeClass("unread");
        }

        if (_count > 0) {
            $("#commentbadge_" + jobid).html(_count);
        } else {
            $("#outside_" + jobid).html('--').removeClass("glyphicon").removeClass("glyphicon-comment").removeClass("outside").removeClass("notification");
            $("#commentbadge_" + jobid).remove();
        }

    });

})();
 


$(".parentcomment").on("click", function () {
    var msgid = $(this).data("id");
    var jobid = $(this).data("jobid");
    var isoffer = $(this).hasClass("offer");
    if (isoffer) {
        updateofferread(msgid);
    } else {
        updateread(msgid);
    }
    $(".childcommentcomment").css("display", "none");
    var _count = parseInt($("#commentbadge_" + jobid).html());
    if ($(this).hasClass("unread")) {
        _count = _count - 1;
    }
    $(this).removeClass("unread");
    if ($(this).next('.childcommentcomment').hasClass("hide")) {

        $(this).next('.childcommentcomment').removeClass("hide").addClass("show").removeClass("unread");
    } else {
        $(this).next('.childcommentcomment').removeClass("show").addClass("hide").removeClass("unread");
    }
   
    if (_count > 0) {
        $("#commentbadge_" + jobid).html(_count);
    } else {
        $("#outside_" + jobid).html('--').removeClass("glyphicon").removeClass("glyphicon-comment").removeClass("outside").removeClass("notification");
        $("#commentbadge_" + jobid).remove();
    }

});

function updateread(id) {
    var _url = "https://app.taskayak.com/jobs/update_readstatus2";
    $.get(_url, { 'jobid': id }, function (

    ) { });
}

function updateofferread(id) {
    var _url = "https://app.taskayak.com/offer/update_readstatus";
    $.get(_url, { 'jobid': id }, function (

    ) { });
}

$(document).ready(function () {
    $("body").on("click", ".disabledbtn", function () {
        var messgae = $(this).data('msg');
        alertify.log(messgae, 'error', 5000);
        //var _url = "http://localhost:50912/account/isfreeaccount";
        //var data = "Taskayak free"
        //if (data == "Taskayak free") {
        //    var msg = "Get more of the wonderful features of Taskayak by subscribing to a payment plan. Saving money by purchase SMS/Email packages or a plan that include SMS/Email as part of the plan and save.";
        //    alertify.set('closable', true);
        //    alertify.confirm(msg, function (e) { 
        //        if (e) {
        //            location.href = "http://localhost:50912/account/pricing";
        //        }  

        //    }, 'error', "Subscriptions Required");
        //} else {
        //    alertify.alert(messgae, function () {
        //    }, 'error', "Permission Alert");
        //}
        //$.post(_url, {  }, function (data) {
        //    if (data == "Taskayak free") {
        //        var msg = "Get more of the wonderful features of Taskayak by subscribing to a payment plan. Saving money by purchase SMS/Email packages or a plan that include SMS/Email as part of the plan and save.";
        //        alertify.confirm(msg, function (e) {
        //            if (e) {
        //                location.href = "http://localhost:50912/account/pricing";
        //            }  
                    
        //        }, 'error', "Subscriptions Required");
        //    } else {
        //        alertify.alert(messgae, function () {
        //        }, 'error', "Permission Alert");
        //    }
            
        //});
       

    }); 
    $(".ratetext").on("change", function () {
        var _val = $(this).val();
        var res = $.trim(_val).replace(/\$/g, "");
        if (res != '') {
            var isnumeric = $.isNumeric(res);
            if (isnumeric == false) {
                alertify.log('Rate must be numeric', 'error', 5000);
                $(this).val("");
                $(this).focus();
            } else {
                var value = parseFloat(res).toFixed(2);
                $(this).val("$" + value);
            }
        }
    });
    $(".phonenumber").on("change", function () {
        var _val = $(this).val();
        var res = $.trim(_val).replace(/\-/g, "");
        if (res != '') {
            res = res.replace(".", "");
            res = res.replace("(", "");
            res = res.replace(")", "");
            res = res.replace(" ", "");
            var isnumeric = $.isNumeric(res);
            if (isnumeric == false) {
                alertify.log('Phone number must be numeric', 'error', 5000);
                $(this).val("");
                $(this).focus();
            } else {
                if ($.trim(res).length < 10 || $.trim(res).length > 10) {

                    alertify.log('Phone number must be 10 digits', 'error', 5000);
                    $(this).focus();
                } else {
                    res = res.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, "$1-$2-$3");
                    $(this).val(res);
                }
            }
        }
    });
});

 

