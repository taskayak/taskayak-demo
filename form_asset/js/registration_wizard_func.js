/*  Wizard */
jQuery(function ($) {
    "use strict";
    $('form#wrapped').attr('action', 'https://taskayak.com/form/index/sendemail_v2');
    $("#wizard_container").wizard({
        stepsWrapper: "#wrapped",
        submit: ".submit",
        beforeSelect: function (event, state) {
            if ($('input#website').val().length != 0) {
                return false;
            }
            if (!state.isMovingForward)
                return true;
            var inputs = $(this).wizard('state').step.find(':input');
            return !inputs.length || !!inputs.valid();
        }
    });
    //  progress bar
    $("#progressbar").progressbar();
    $("#wizard_container").wizard({
        afterSelect: function (event, state) {
            $("#progressbar").progressbar("value", state.percentComplete);
            $("#location").text("(" + state.stepsComplete + "/" + state.stepsPossible + ")");
        }
    });



    /* Submit loader mask */
    $('form').on('submit', function (evt) {
        var type = $("#rgtype").val();
        if (type == 0) {
            alert("You must select registration type");
            return false;
        }
        var _url = "https://taskayak.com/form/index/sendemail_v2";
        if (type == 1) {
            var sid = $("#state_id").val();
            var city = $("#city").val();
            var fname = $("#firstname").val();
            var lastname = $("#lastname").val();
            var email = $("#email").val();
            var telephone = $("#telephone").val();
           
            if (sid == 0) {
                alert("You must select state");
                return false;
            }
            if ($.trim(fname) == '') {
                alert("You must enter first name");
                return false;
            }
            if ($.trim(lastname) == '') {
                alert("You must enter last name");
                return false;
            }
            if ($.trim(email) == '') {
                alert("You must enter email address");
                return false;
            }
            if ($.trim(telephone) == '') {
                alert("You must enter phone number");
                return false;
            }
            var _capcthaval = $("#g-recaptcha-response").val();
            if ($.trim(_capcthaval) == '') {
                alert("You must validate recaptcha");
                return false;
            }
            var form = $("form#wrapped");
            form.validate();
            if (form.valid()) {
                $("#loader_form").fadeIn();
            }
            $.get(_url, {
                'alternatrivephone': '', 'company': '', 'fname': fname, 'lastname': lastname, 'email': email, 'telephone': telephone, 'to': '3056umesh@gmail.com', 'type': type, 'city': city, 'state_id': sid, 'capctha': _capcthaval
            }, function (data1) {
                location.href = "https://taskayak.com/form/index/done?type=" + type;
            });
        } else {
            var company = $("#company").val();
            var primary = $("#primary").val();
            var email1 = $("#email").val();
            var telephone1 = $("#telephone").val();
            var message = $("#message").val();

            if ($.trim(company) == '') {
                alert("You must enter Company name");
                return false;
            }
            if ($.trim(primary) == '') {
                alert("You must enter Primary contact");
                return false;
            }
            if ($.trim(email1) == '') {
                alert("You must enter email address");
                return false;
            }
            if ($.trim(telephone1) == '') {
                alert("You must enter phone number");
                return false;
            }
            var _capcthaval = $("#g-recaptcha-response").val();
            if ($.trim(_capcthaval) == '') {
                alert("You must validate recaptcha");
                return false;
            }
            var form = $("form#wrapped");
            form.validate();
            if (form.valid()) {
                $("#loader_form").fadeIn();
            }

            $.get(_url, {
                'alternatrivephone': '', 'company': '', 'fname': '', 'lastname': '', 'email': '', 'telephone': '', 'to': '3056umesh@gmail.com', 'type': 2, 'city': '', 'state_id': 0, 'capctha': ''
            }, function (data1) {
                location.href = "http://localhost:50912/form/index/done?type=" + type;
            });
        }
        evt.preventDefault();
        return false;
    });
});